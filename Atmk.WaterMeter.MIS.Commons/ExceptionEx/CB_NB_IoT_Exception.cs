﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ExceptionEx
{
    /// <summary>
    /// 电信NB平台逻辑错误信息
    /// </summary>
    public class CB_NB_IoT_Exception:Exception
    {
        public CB_NB_IoT_Exception(string msg) : base(msg)
        {

        }
    }
}
