using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Commons
{
    /// <summary>
    /// 访问权限令牌
    /// </summary>
    public class Token
    {
        public Header header { get; } = new Header();
        public Payload payload { get; } = new Payload();

        public class Header
        {
            public string alg { get; } = "HS256";
            public string typ { get; } = "JWT";
        }

        public class Payload
        {
            /// <summary>
            /// 用户ID
            /// </summary>
            public string id { get; set; }
            /// <summary>
            /// 用户名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 角色
            /// </summary>
            public string role { get; set; }

            /// <summary>
            /// 项目Id-登陆用户所在的项目
            /// </summary>
            public string areaid { get; set; }

            /// <summary>
            /// Issuer，该JWT的签发者，是否使用是可选的；
            /// </summary>
            public string iss { get; set; }
            /// <summary>
            /// Subject，主题
            /// </summary>
            public string sub { get; set; }
            /// <summary>
            /// Audience，观众,接收该JWT的一方，是否使用是可选的；
            /// </summary>
            public string aud { get; set; }
            /// <summary>
            /// Expiration time，过期时间, 什么时候过期，这里是一个Unix时间戳，是否使用是可选的；
            /// </summary>
            public string exp { get; set; }
            /// <summary>
            /// Not before 如果当前时间在nbf里的时间之前，则Token不被接受；一般都会留一些余地，比如几分钟；是否使用是可选的；
            /// </summary>
            public string nbf { get; set; }
            /// <summary>
            /// Issued at，发行时间
            /// </summary>
            public string iat { get; set; }
            /// <summary>
            /// JWT ID
            /// </summary>
            public string jti { get; set; }
        }
    }
}