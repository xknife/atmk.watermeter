﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.GateWay;

namespace Atmk.WaterMeter.MIS.Commons.Expands
{
    public static class ModelsConvertEx
    {
        public static MeterFreezData ToMeterFreezData(this MeterFreezDataEntity mf)
        {
            var m = new MeterFreezData
            {
                deviceId = mf.deviceId,
                timerecord = Simple.CtStringConvertDateTime(mf.timestamp),
                time = String64ToDateTime(mf.time),
                freezetype = GetFreez(mf.freezetype)
            };
            m.freezedata = Base64Convert.FrozensDatasBase64ConvertDouble(mf.freezedata,m.freezetype);
            return m;
        }

        private static DateTime String64ToDateTime(string s64)
        {
            var s = Base64Convert.ConvertIntString(s64);
            return DateTime.ParseExact(s, "yyMMddHHmmss", CultureInfo.CurrentCulture);
        }

        private static FrozenType GetFreez(int f)
        {
            Enum.TryParse<FrozenType>(f.ToString(), out var result);
            return result;
        }
    }
}
