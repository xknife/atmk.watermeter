﻿using System;

namespace Atmk.WaterMeter.MIS.Commons
{
    ///***********************************************
    /// <summary>
    /// 依赖注入的供给器。静态工具类。
    /// 关于依赖注入可参考：http://www.cnblogs.com/dotNETCoreSG/p/aspnetcore-3_10-dependency-injection.html
    /// kan,2018/8/2
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class DI
    {
        /// <summary>
        /// ASP.NET Core 包含的默认支持构造函数注入的简单内置容器
        /// </summary>
        private static IServiceProvider _serviceProvider;

        /// <summary>
        /// 配置本工具类，在Startup类的Configure中被调用与赋值。
        /// </summary>
        /// <param name="provider">容器管理器</param>
        public static void Configure(IServiceProvider provider)
        {
            _serviceProvider = provider;
        }

        /// <summary>
        /// 根据指定类型解析得到实例
        /// </summary>
        /// <param name="type">希望得到的实例的类型</param>
        /// <returns>实例</returns>
        public static object Resolve(Type type)
        {
            return _serviceProvider?.GetService(type);
        }

        /// <summary>
        /// 根据指定泛型类型解析得到实例
        /// </summary>
        public static T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }
    }
    //*****************************************/
}
