﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public class PaymentList:BaseResult
    {
        /// <summary>
        /// 
        /// </summary>
        public PaymentListData data { get; set; }
    }
    
    public class PaymentListData
    {
        /// <summary>
        /// 
        /// </summary>
        public string total { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int pageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PaymentList> list { get; set; }
    }
}
