﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public class EnumListResult:BaseResult
    {
        /// <summary>
        /// 枚举转换的集合
        /// </summary>
        public List<string> list { get; set; }
    }
}
