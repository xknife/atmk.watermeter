﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public class PaymentListItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ownerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string houseNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double balance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string paymentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string paymentTypeText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double cost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string message { get; set; }
    }

}
