﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public class PassWordResult:BaseResult
    {
        /// <summary>
        /// 操作员Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 操作员密码
        /// </summary>
        public string password { get; set; }
    }
}
