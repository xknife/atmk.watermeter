﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter
{
    public class BaseMeterCommandPost
    {
        /// <summary>
        /// 片区Id
        /// </summary>
        public string aid { get; set; }
        /// <summary>
        /// 水表编号
        /// </summary>
        public string meterNumber { get; set; }
    }
}
