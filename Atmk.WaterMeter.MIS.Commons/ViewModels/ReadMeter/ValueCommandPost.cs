﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter
{
    public class ValueCommandPost
    {
        /// <summary>
        /// 片区Id
        /// </summary>
        public string aid { get; set; }
        /// <summary>
        /// 电信唯一标识
        /// </summary>
        public string CTdeviceId { get; set; }

        public string meterNumber { get; set; }
        /// <summary>
        /// 阀门操作类型，3 关阀 2 开阀  int类型
        /// </summary>
        public int tapType { get; set; }
    }
}
