﻿using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter
{
    /// <summary>
    /// 冻结数据-中转对象
    /// </summary>
    public class MeterFreezData
    {
        public string deviceId { get; set; }
        public DateTime time { get; set; }
        /// <summary>
        /// 冻结数据的记录本身接收时间 对应timestamp
        /// </summary>
        public DateTime timerecord { get; set; }
        public string endpointid { get; set; }
        /// <summary>
        /// 冻结类型
        /// </summary>
        public FrozenType freezetype { get; set; }
        /// <summary>
        /// 冻结数据
        /// </summary>
        public List<double> freezedata { get; set; }
    }
}
