﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.PriceStep
{
    /// <summary>
    /// 阶梯价格里的费用信息
    /// </summary>
    public class PriceStepFeeItem
    {
        public string id { get; set; } //guid字符串
        public string priceId { get; set; } //阶梯价格的id
        public string index { get; set; }           //显示序号
        public string title { get; set; }    //费用名称
        public double price1 { get; set; }     //单价1
        public double price2 { get; set; }        //单价2
        public double price3 { get; set; }      //单价3
        public string remark { get; set; }       //备注
    }
}
