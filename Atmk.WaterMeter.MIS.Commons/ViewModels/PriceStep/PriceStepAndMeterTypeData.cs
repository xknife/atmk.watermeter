﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.PriceStep
{
    public class PriceStepAndMeterTypeData:PriceStepData
    {
        /// <summary>
        /// 表类型列表
        /// </summary>
        public List<string> meterList { get; set; }

    }
}
