﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.PriceStep
{
    /// <summary>
    /// 阶梯价格信息
    /// </summary>
    public class PriceStepItem
    {
        public string id { get; set; } //guid字符串
        public string index { get; set; }           //显示序号
        public string title { get; set; }    //价格名称
        public string meterType { get; set; }       //显示水表类型
        public double price1 { get; set; }     //单价1
        public double price2 { get; set; }        //单价2
        public double price3 { get; set; }      //单价3
        public double separation1 { get; set; }    //分界点1
        public double separation2 { get; set; }    //分界点2
        public string unit { get; set; }      //单位
        public string remark { get; set; }       //备注

    }
}
