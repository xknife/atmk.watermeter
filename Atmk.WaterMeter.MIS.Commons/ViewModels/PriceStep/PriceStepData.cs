﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.PriceStep
{
    public class PriceStepData:IData
    {
        /// <summary>
        /// 价格列表
        /// </summary>
        public List<PriceStepItem> priceList { get; set; }
        /// <summary>
        /// 费用列表
        /// </summary>
        public List<PriceStepFeeItem> feeList { get; set; }
    }
}
