﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Area
{
    /// <summary>
    /// 
    /// </summary>
    public class Area
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string siteName { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// 高程
        /// </summary>
        public double elevation { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string siteAddress { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }
}
