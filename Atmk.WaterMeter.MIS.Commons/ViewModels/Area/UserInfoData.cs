﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Area
{
    public class UserInfoData : IData
    {
        public string[] role { get; set; }
        public object[] authority { get; set; }
        public string name { get; set; }
        /// <summary>
        /// 项目id
        /// </summary>
        public string projectId { get; set; }
    }
}
