﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Area
{
    public class AreaOperatorData:IData
    {
        public List<Area> siteList { get; set; }
        public List<Operator> operatorList { get; set; }

    }
}
