﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Area
{
    /// <summary>
    /// 操作员
    /// </summary>
    public class Operator
    {
        /// <summary>
        /// 操作员Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 登陆名
        /// </summary>
        public string operatorName { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string surname { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 角色id
        /// </summary>
        public string roleId { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string role { get; set; }
        /// <summary>
        /// 站点id
        /// </summary>
        public string siteId { get; set; }
        /// <summary>
        /// 站点名称
        /// </summary>
        public string siteName { get; set; }

        /// <summary>
        /// 每页显示行数
        /// </summary>
        public int pageLines { get; set; }
        /// <summary>
        /// 片区id集合-旧版用，重新涉及项目片区和操作员关系后，该字段不会再启用
        /// </summary>
        public string[] district { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string createTime { get; set; }
    }
}
