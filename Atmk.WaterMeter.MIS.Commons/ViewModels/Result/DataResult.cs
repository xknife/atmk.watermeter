﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Result
{
    public class DataResult:BaseResult
    {
        public IData data { get; set; }
    }
}
