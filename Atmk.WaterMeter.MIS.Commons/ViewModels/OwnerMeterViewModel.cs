﻿namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    /// <summary>
    ///     业主与附属信息及水表信息的集合
    /// </summary>
    public class OwnerMeterViewModel
    {
        /// <summary>
        ///     业主编码
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        ///     业主姓名
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        ///     业主信息
        /// </summary>
        public string OwnerMessage { get; set; }

        /// <summary>
        ///     区域编码
        /// </summary>
        public string DistrictId { get; set; }

        /// <summary>
        ///     区域信息
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        ///     水表编码
        /// </summary>
        public string MeterNumber { get; set; }

        /// <summary>
        ///     水表名称
        /// </summary>
        public string MeterName { get; set; }

        /// <summary>
        ///     水表类型
        /// </summary>
        public string MeterType { get; set; }

        /// <summary>
        ///     水表数据信息
        /// </summary>
        public string MeterDatas { get; set; }
    }
}