﻿namespace Atmk.WaterMeter.MIS.Commons.ViewModels.OwnerMeter
{
    /// <summary>
    /// 销表接收参数
    /// </summary>
    public class CancelMeter
    {
        /// <summary>
        /// 业主Id
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 水表号
        /// </summary>
        public string meterNumber { get; set; }
        /// <summary>
        /// 销表原因
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 最后读数
        /// </summary>
        public double lastRead { get; set; }
    }
}
