﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin
{
    public class WxPayDataVm
    {
        /// <summary>
        /// 小程序ID
        /// </summary>
        public string appId { get; set; }
        /// <summary>
        /// 时间戳 从1970年1月1日00:00:00至今的秒数,即当前的时间
        /// </summary>
        public string timeStamp { get; set; }
        /// <summary>
        /// 随机串  一次下单中使用的
        /// </summary>
        public string nonceStr { get; set; }
        /// <summary>
        /// 数据包  格式"prepay_id={prepay_id=}"
        /// </summary>
        public string package { get; set; }

        /// <summary>
        /// 签名类型 默认MD5
        /// </summary>
        public string signType { get; set; } = "MD5";
        /// <summary>
        /// 签名
        /// </summary>
        public string paySign { get; set; }

    }
}
