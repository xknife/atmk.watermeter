﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin
{
    /// <summary>
    /// 微信统一下单返回结果对象
    /// </summary>
    public class ResponUnifiedorder
    {
        public string appid { get; set; }
        public string mch_id { get; set; }
        public string device_info { get; set; }
        public string nonce_str { get; set; }
        public string sign { get; set; }
        public string result_code { get; set; }
        public string err_code { get; set; }
        public string err_code_des { get; set; }
        public string trade_type { get; set; }
        public string prepay_id { get; set; }
        public string code_url { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string out_trade_no { get; set; }
    }
}
