﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin
{
    public class WxAccountVm
    {
        public string openid { get; set; }
        public int Isbind { get; set; }
        public string ownerName { get; set; }
        public int balance { get; set; }
        public string meterNumber { get; set; }
        public string houseNumber { get; set; }
    }
}
