﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin
{
    public class WxRecordsVm
    {
        public string openid { get; set; }
        public string meterNumber { get; set; }
        public int amount { get; set; }
        public string houseNumber { get; set; }
        public string creatTime { get; set; }
    }
}
