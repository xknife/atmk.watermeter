﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区节点集合返回对象
    /// </summary>
    public class DistrictNodesResult:BaseResult
    {
        public DistrictNodesData data { get; set; }
    }
}
