﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区详细信息结果
    /// </summary>
    public class DistrictInformationResult:BaseResult
    {
        
        public DistrictInformationData data { get; set; }
    }
}
