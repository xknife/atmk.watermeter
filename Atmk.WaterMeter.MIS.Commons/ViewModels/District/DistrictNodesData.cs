﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区节点集合
    /// </summary>
    public class DistrictNodesData:IData
    {
        public List<DistrictNode> areaList { get; set; }
    }
}
