﻿namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区节点
    /// </summary>
    public class DistrictNode
    {
        /// <summary>
        /// 片区id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 片区父亲id
        /// </summary>
        public string parentId { get; set; }
        /// <summary>
        /// 片区名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 节点类型：‘root:根节点’, 'branch：中间节点', 'leaf: 叶子节点'
        /// </summary>
        public string nodeType { get; set; }
        /// <summary>
        ///  排序，数字越大排序越靠前
        /// </summary>
        public int displayOrder { get; set; }

        //public int IsOwner { get; set; } = 0;
    }
}
