﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区节点序号信息
    /// </summary>
    public class DistrictNodeSequence
    {
        public string districtId { get; set; }
        public int displayOrder { get; set; }
    }
}
