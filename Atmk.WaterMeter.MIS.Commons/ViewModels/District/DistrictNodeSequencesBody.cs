﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区节点需要修改的排序信息
    /// </summary>
    public class DistrictNodeSequencesBody
    {
        /// <summary>
        /// 修改排序信息的片区节点集合
        /// </summary>
        public List<DistrictNodeSequence> list { get; set; }
    }
}
