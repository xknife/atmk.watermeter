﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    public class DistrictInfomationBody
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public string projectId { get; set; }
        /// <summary>
        /// 片区名称
        /// </summary>
        public string districtName { get; set; }
        /// <summary>
        /// 片区节点类型：'branch：中间节点(片区)','last：片区最后的节点(后续可以连接业主但不可添加片区了)'
        /// </summary>
        public string nodeType { get; set; }
        /// <summary>
        /// 片区父id
        /// </summary>
        public string parentDistrictId { get; set; }

        /// <summary>
        /// 片区所在高程
        /// </summary>
        public string elevation { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }
}
