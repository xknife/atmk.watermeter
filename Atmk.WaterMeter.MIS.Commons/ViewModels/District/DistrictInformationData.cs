﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.District
{
    /// <summary>
    /// 片区详细数据
    /// </summary>
    public class DistrictInformationData
    {
        /// <summary>
        /// 片区id
        /// </summary>
        public string districtId { get; set; }

        /// <summary>
        /// 片区名称
        /// </summary>
        public string districtName { get; set; }

        /// <summary>
        /// 片区父id
        /// </summary>
        public string parentDistrictId { get; set; }

        /// <summary>
        /// 片区所在高程
        /// </summary>
        public string elevation { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 坐标点集合
        /// </summary>
        public List<MapCoordinat> coordinates { get; set; }
    }
}