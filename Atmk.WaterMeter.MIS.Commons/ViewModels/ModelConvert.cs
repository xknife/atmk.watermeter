﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public static class ModelConvert
    {
        public static WxRecordsVm ToWxRecordsVm(this WxRechargeRecords wxRechargeRecord)
        {
            return new WxRecordsVm
            {
                openid = wxRechargeRecord.OpenId,
                meterNumber = wxRechargeRecord.Number,
                houseNumber = wxRechargeRecord.HouseNumber,
                amount = decimal.ToInt32(wxRechargeRecord.RefillSum * 100),
                creatTime = wxRechargeRecord.CreateTime.ToString("yyyyMMddHHmmss")
            };
        }
        public static WxRecordsVm ToWxSrOrderVm(this WxSrOrderRecords wxSrOrder)
        {
            return new WxRecordsVm
            {
                openid = wxSrOrder.OpenId,
                meterNumber = wxSrOrder.Number,
                houseNumber = wxSrOrder.HouseNumber,
                amount = decimal.ToInt32(wxSrOrder.RefillSum * 100),
                creatTime = wxSrOrder.CreateTime.ToString("yyyyMMddHHmmss")
            };
        }

    }
}
