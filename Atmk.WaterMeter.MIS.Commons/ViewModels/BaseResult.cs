﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels
{
    public class BaseResult
    {
        /// <summary>
        /// 反馈错误编码
        /// </summary>
        public int errcode { get; set; }
        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }
    }
}
