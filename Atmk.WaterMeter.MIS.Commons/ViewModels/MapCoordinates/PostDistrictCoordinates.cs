﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{
    /// <summary>
    /// 片区post参数
    /// </summary>
    public  class PostDistrictCoordinates
    {
        /// <summary>
        /// 片区id
        /// </summary>
        public string districtId { get; set; }


        /// <summary>
        /// 坐标点集合
        /// </summary>
        public List<MapCoordinat> coordinates { get; set; }
    }
}
