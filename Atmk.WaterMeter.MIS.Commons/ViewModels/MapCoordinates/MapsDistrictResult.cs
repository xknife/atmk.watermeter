﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{
    public class MapsDistrictResult : BaseResult
    {
        public MapsDistrictDate datas { get; set; }
    }

    public class MapsDistrictDate
    {
        public List<GetDistrictCoordinates> list { get; set; }
    }
}
