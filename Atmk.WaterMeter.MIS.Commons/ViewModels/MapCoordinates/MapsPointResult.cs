﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{
    public class MapsPointResult:BaseResult
    {
        public MapsPointData datas { get; set; }
    }

    public class MapsPointData
    {
        public List<PostPointMapCoordinat> list { get; set; }
    }
}
