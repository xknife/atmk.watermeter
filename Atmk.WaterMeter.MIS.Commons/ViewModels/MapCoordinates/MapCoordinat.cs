﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{
    /// <summary>
    /// 地图坐标点:经纬度
    /// </summary>
    public class MapCoordinat
    {
        /// <summary>
        /// 经度
        /// </summary>
        public double lng { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public double lat { get; set; }
    }
}
