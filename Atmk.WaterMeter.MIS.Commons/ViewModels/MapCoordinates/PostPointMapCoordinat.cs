﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{
    public class PostPointMapCoordinat
    {
        /// <summary>
        /// 节点Id(对应项目、片区或业主的Id）
        /// </summary>
        public string nodeId { get; set; }
        /// <summary>
        /// 坐标点类型(Project、片区或业主)
        /// </summary>
        public CoordinateType nodeType { get; set; }
        /// <summary>
        /// 坐标点
        /// </summary>
        public MapCoordinat coordinates { get; set; }
    }
}
