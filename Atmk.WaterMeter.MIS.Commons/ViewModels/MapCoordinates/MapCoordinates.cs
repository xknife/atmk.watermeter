﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates
{

    public class MapCoordinates
    {
        /// <summary>
        /// 坐标点集合
        /// </summary>
        public List<MapCoordinat> coordinates { get; set; }
    }
}
