﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 一个片区下的按指定条件统计的用水总量
    /// </summary>
    public class WaterConsumptionsCountListItem
    {
        /// <summary>
        /// 片区Id
        /// </summary>
        public string districtId { get; set; }

        /// <summary>
        /// 片区名称
        /// </summary>
        public string districtName { get; set; }

        /// <summary>
        /// 按阶梯用水量查询时：集合按顺序显示阶梯用水量：第一阶梯0-180 ，第二阶梯181-260，第三阶梯260以上
        /// 按一个季度用水量查询时：集合按顺序显示该季度三个月用水量
        /// 按年用水量查询时：集合按顺序显示1到12月的用水量
        /// </summary>
        public List<double> waterConsumptions { get; set; }
    }

    /// <summary>
    /// 按片区用水量多级统计数据-组合使用
    /// </summary>
    public class WaterConsumptionsCountData
    {
        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }

        /// <summary>
        /// 数据集合
        /// </summary>
        public List<WaterConsumptionsCountListItem> list { get; set; }
    }

    /// <summary>
    /// 按片区用水量多级统计-独立使用
    /// </summary>
    public class WaterConsumptionsCount : BaseResult
    {
        /// <summary>
        /// 按片区用水量多级统计数据
        /// </summary>
        public WaterConsumptionsCountData data { get; set; }
    }
}