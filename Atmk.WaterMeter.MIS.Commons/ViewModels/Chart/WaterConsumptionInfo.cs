﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 总用水量统计数据-组合使用
    /// </summary>
    public class WaterConsumptionInfoData
    {
        /// <summary>
        /// 昨日用水总量
        /// </summary>
        public int yesterday { get; set; }
        /// <summary>
        /// 本月用水量总量
        /// </summary>
        public int nowMonth { get; set; }
        /// <summary>
        /// 上月用水总量
        /// </summary>
        public int lastMonth { get; set; }
        /// <summary>
        /// 同期平均用水量
        /// </summary>
        public double sameMonth { get; set; }
        public double sameYear { get; set; }
        public double yearDosage { get; set; }
        public double lastYearDosage { get; set; }
    }
    /// <summary>
    /// 总用水量统计-独立使用
    /// </summary>
    public class WaterConsumptionInfo:BaseResult
    {

        /// <summary>
        /// 总用水量统计数据
        /// </summary>
        public WaterConsumptionInfoData data { get; set; }
    }
}
