﻿using System;
using System.Collections.Generic;
using System.Text;
// ReSharper disable InconsistentNaming

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 图表返回结果
    /// </summary>
    public class ChartResult:BaseResult
    {
        public ChartDatas datas { get; set; }
    }
    /// <summary>
    /// 图表视图
    /// </summary>
    public class ChartDatas
    {
        /// <summary>
        /// 费用报警
        /// </summary>
        public CostReminderData costReminderData { get; set; }
        /// <summary>
        /// 关阀列表
        /// </summary>
        public ClosingvalveData closingvalveData { get; set; }
        /// <summary>
        /// 昨日用水量
        /// </summary>
        public YesterdayConsumptionData yesterdayConsumptionData { get; set; }
        /// <summary>
        /// 上季度片区用水量
        /// </summary>
        public WaterConsumptionsCountData lastSeasonWaterConsumptionsCountData { get; set; }
        /// <summary>
        /// 上月片区阶梯用水量统计
        /// </summary>
        public WaterConsumptionsCountData lastMonthDistrictConsumptionsCountData { get; set; }
        /// <summary>
        /// 去年片区按月用水总量统计
        /// </summary>
        public YearWaterConsumptionsCountData lastYearWaterConsumptionsCountData { get; set; }
        /// <summary>
        /// 昨日各片区抄表数据统计
        /// </summary>
        public DistrictReadRatioData districtReadRatioData { get; set; }
        /// <summary>
        /// 统计信息
        /// </summary>
        public StatisticsInfoData statisticsInfoData { get; set; }
        /// <summary>
        /// 用水情况
        /// </summary>
        public WaterConsumptionInfoData waterConsumptionInfoData { get; set; }

        //前3天用量统计
        public WaterConsumptionsCountData firstThreeDays { get; set; }

        //财务信息
    }
}
