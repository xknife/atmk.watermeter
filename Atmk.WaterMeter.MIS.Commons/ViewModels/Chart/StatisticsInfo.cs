﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 统计信息数据-组合使用
    /// </summary>
    public class StatisticsInfoData
    {
        /// <summary>
        /// 片区数量
        /// </summary>
        public int districtCount { get; set; }
        /// <summary>
        /// 业主数量
        /// </summary>
        public int ownerCount { get; set; }
        /// <summary>
        /// 水表统计
        /// </summary>
        public int meterCount { get; set; }
        /// <summary>
        /// 水表在线率
        /// </summary>
        public double onlineRatio { get; set; }
    }
    /// <summary>
    /// 统计信息-独立使用
    /// </summary>
    public class StatisticsInfo:BaseResult
    {
        /// <summary>
        /// 统计信息数据
        /// </summary>
        public StatisticsInfoData data { get; set; }
    }
}
