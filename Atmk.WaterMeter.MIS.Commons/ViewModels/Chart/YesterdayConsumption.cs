﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    public class YesterdayConsumptionListItem
    {
        /// <summary>
        /// 业主Id
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 门牌号
        /// </summary>
        public string houseNumber { get; set; }
        /// <summary>
        /// 用水量
        /// </summary>
        public double waterConsumption { get; set; }
    }
    /// <summary>
    /// 昨日用水量统计--组合使用
    /// </summary>
    public class YesterdayConsumptionData
    {
        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }
        /// <summary>
        /// 数据集合
        /// </summary>
        public List<YesterdayConsumptionListItem> list { get; set; }
    }
    /// <summary>
    /// 昨日用水量统计--独立使用
    /// </summary>
    public class YesterdayConsumption : BaseResult
    {
       
        /// <summary>
        /// 
        /// </summary>
        public YesterdayConsumptionData data { get; set; }
    }
}
