﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 包含业主姓名及业主账户余额的费用提醒信息
    /// </summary>
    public class CostReminderItem
    {
        /// <summary>
        /// 业主Id
        /// </summary>
        public string ownerId { get; set; }

        /// <summary>
        /// 业主名称
        /// </summary>
        public string ownerName { get; set; }

        /// <summary>
        /// 账户id
        /// </summary>
        public string accountId { get; set; }

        /// <summary>
        /// 账户余额
        /// </summary>
        public decimal balance { get; set; }
    }
    /// <summary>
    /// 费用提醒数据-组合使用
    /// </summary>
    public class CostReminderData
    {
        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }

        /// <summary>
        /// 记录集合
        /// </summary>
        public List<CostReminderItem> list { get; set; }
    }
    /// <summary>
    /// 费用提醒-独立使用
    /// </summary>
    public class CostReminder:BaseResult
    {
        /// <summary>
        /// 费用提醒数据
        /// </summary>
        public CostReminderData data { get; set; }
    }
}