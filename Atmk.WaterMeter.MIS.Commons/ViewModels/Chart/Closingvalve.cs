﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 一条记录信息
    /// </summary>
    public class ClosingValveOwnerItem
    {
        /// <summary>
        /// 业主Id
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 门牌号
        /// </summary>
        public string houseNumber { get; set; }
        /// <summary>
        /// 业主名称
        /// </summary>
        public string ownerName { get; set; }
    }
    /// <summary>
    /// 关阀记录数据-组合使用时
    /// </summary>
    public class ClosingvalveData
    {
        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }
        /// <summary>
        /// 记录集合
        /// </summary>
        public List<ClosingValveOwnerItem> list { get; set; }
    }

    /// <summary>
    /// 关阀记录-独立用时
    /// </summary>
    public class Closingvalve:BaseResult
    {

        /// <summary>
        /// 关阀记录数据
        /// </summary>
        public ClosingvalveData data { get; set; }
    }
}
