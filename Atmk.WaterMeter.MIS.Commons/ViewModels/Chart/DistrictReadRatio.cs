﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    /// 片区的抄表成功率
    /// </summary>
    public class DistrictReadRatioItem
    {
        /// <summary>
        /// 片区Id
        /// </summary>
        public string districtId { get; set; }

        /// <summary>
        /// 片区名称
        /// </summary>
        public string districtName { get; set; }

        /// <summary>
        /// 读取成功率
        /// </summary>
        public double readRatio { get; set; }
    }
    /// <summary>
    /// 按片区抄回率数据-组合使用
    /// </summary>
    public class DistrictReadRatioData
    {
        public DistrictReadRatioData()
        {
            list = new List<DistrictReadRatioItem>();
        }

        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }

        /// <summary>
        /// 数据集合
        /// </summary>
        public List<DistrictReadRatioItem> list { get; set; }
    }
    /// <summary>
    /// 按片区抄回率-独立使用
    /// </summary>
    public class DistrictReadRatio : BaseResult
    {
        /// <summary>
        /// 按片区抄回率数据
        /// </summary>
        public DistrictReadRatioData data { get; set; }
    }
}