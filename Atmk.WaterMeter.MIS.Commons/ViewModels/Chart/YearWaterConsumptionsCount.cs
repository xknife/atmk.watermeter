﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Chart
{
    /// <summary>
    ///  按片区年用水量多级统计-组合使用
    /// </summary>
    public class YearWaterConsumptionsCountData
    {
        /// <summary>
        /// list数据数量
        /// </summary>
        public string total { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public int year { get; set; }

        /// <summary>
        /// 数据集合
        /// </summary>
        public List<WaterConsumptionsCountListItem> list { get; set; }
    }
    /// <summary>
    /// 按片区年用水量多级统计-独立使用
    /// </summary>
    public class YearWaterConsumptionsCount : BaseResult
    {/// <summary>
        /// 
        /// </summary>
        public YearWaterConsumptionsCountData data { get; set; }
    }
}
