﻿

// ReSharper disable once CheckNamespace
namespace System
{
    public static class GuidEx
    {
        public static string ToId(this Guid guid)
        {
            return guid.ToString();
        }
    }
}
