﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;


namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IDistrictRepository : IRepository
    {
        /// <summary>
        /// 获取所有叶子节点
        /// </summary>
        /// <param name="staff">操作员</param>
        /// <returns></returns>
        List<District> GetLeafNodeDistricts(User user);

        /// <summary>
        /// 获取所有父节点片区
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        List<District> GetFatherNodeDistricts(User user);

        /// <summary>
        /// 根据父节点获取叶子节点
        /// </summary>
        /// <param name="fatherNode"></param>
        /// <returns></returns>
        List<District> GetLefNodeDistricts(List<District> fatherNode);

        /// <summary>
        /// 根据id获取片区
        /// </summary>
        /// <param name="districtsId">操作员</param>
        /// <returns></returns>
        List<District> GetDistrictsById(params string[] districtsId);


        /// <summary>
        /// 根据项目ID获取片区集合
        /// 这些片区代表树状图中的各个节点
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        List<District> GetDistrictEntities(string areaId);

        /// <summary>
        /// 根据项目ID获取叶子片区集合
        /// 这些片区直连了业主信息，没有被片区节点作为父片区，属于片区节点的叶子端
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<District> GetLeafDistrictEntities(string areaId,string districtId);

        /// <summary>
        ///  根据项目ID获取叶子片区集合
        /// 这些片区直连了业主信息，没有被片区节点作为父片区，属于片区节点的叶子端
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        List<District> GetLeafDistrictEntities(string areaId);
        /// <summary>
        ///  获取叶子片区集合
        /// 这些片区直连了业主信息，没有被片区节点作为父片区，属于片区节点的叶子端
        /// </summary>
        /// <returns></returns>
        List<District> GetLeafDistrictEntities();

        /// <summary>
        /// 根据项目和片区，获取叶子片区集合
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<District> GetBaseDistrictEntities(string areaId, string districtId);
        /// <summary>
        /// 根据操作员id，获取该操作员的自定义片区排列顺序
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        List<DistrictSequences> GetDistrictSequencebyStaff(string staffId);

        ///<summary>
        /// 添加操作员下的片区排序
        /// </summary>
        /// <param name="DistrictSequences"></param>
        /// <returns></returns>
        int AddDistrictSequence(DistrictSequences DistrictSequences);

        /// <summary>
        /// 修改操作员下的片区排序
        /// </summary>
        /// <param name="DistrictSequences"></param>
        /// <returns></returns>
        int UpdateDistrictSequence(DistrictSequences DistrictSequences);
        /// <summary>
        /// 根据操作员id和父级片区id,获取该操作员的同级片区排序信息
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        List<DistrictSequences> GetSameParentDistrictSequence(string staffId, Guid parentId);
        /// <summary>
        /// 清除表内所有数据
        /// </summary>
        /// <returns></returns>
        int Clear();
    }
}
