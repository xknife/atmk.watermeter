﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IRolesRepository : IRepository
    {
        /// <summary>
        /// 获取用户的角色
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        List<Roles> GetRoleByUser(string uid);

        /// <summary>
        /// 获取角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Roles GetRoleById(string id);

        /// <summary>
        /// 获取除超级管理员以外的角色
        /// </summary>
        /// <returns></returns>
        List<Roles> GetRoleExceptAdmin();
    }
}
