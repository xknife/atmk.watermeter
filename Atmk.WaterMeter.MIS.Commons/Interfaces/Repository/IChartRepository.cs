﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    /// <inheritdoc />
    /// <summary>
    /// 图表数据库查询
    /// </summary>
    public interface IChartRepository : IRepository
    {
        /// <summary>
        /// 获取片区信息
        /// </summary>
        /// <param name="districtIds">片区Id集合</param>
        /// <returns></returns>
        List<District> GetDistricts(params string[] districtIds);

        /// <summary>
        /// 获取业主信息
        /// </summary>
        /// <param name="districtIds">片区Id集合</param>
        /// <returns></returns>
        List<Owner> GetOwners(params string[] districtIds);

        /// <summary>
        /// 获取账户数据
        /// </summary>
        /// <param name="ownerIds">业主Id集合</param>
        /// <returns></returns>
        //List<Account> GetAccounts(params string[] ownerIds);

        /// <summary>
        /// 获取水表信息
        /// </summary>
        /// <param name="ownerIds">业主数据集合</param>
        /// <returns></returns>
        List<Meter> GetMeters(params string[] ownerIds);

        /// <summary>
        /// 获取水表记录
        /// </summary>
        /// <param name="meterNumbers">水表编号集合</param>
        /// <returns></returns>
        List<MeterReadingRecord> GetMeterReadings(params string[] meterNumbers);
    }
}