﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMeterCodeIdMapRepository : IRepository
    {
        /// <summary>
        /// 根据设备id获取水表编号
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        string GetMeterNumberByNBId(string deviceId);

        /// <summary>
        /// 保存水表对应列表
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        bool Save(string meterNumber, string deviceId,string imei);

        /// <summary>
        /// 获取所有对应列表
        /// </summary>
        /// <returns></returns>
        List<MeterCodeIdMap> Seletc();
        /// <summary>
        /// 移除列表数据
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        int Remove(params string[] meterNumber);
        /// <summary>
        /// 移除列表数据
        /// </summary>
        /// <param name="idAndNbIdMap"></param>
        /// <returns></returns>
        int Remove(params MeterCodeIdMap[] idAndNbIdMap);

    }
}
