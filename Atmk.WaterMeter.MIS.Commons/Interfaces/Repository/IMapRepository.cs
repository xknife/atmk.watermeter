﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMapRepository : IRepository
    {
        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        MapParameter FirstCoordinatePoint(string nodeid, CoordinateType coordinateType);

        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        MapParameter FirstCoordinatePoint(string nodeid);
        /// <summary>
        /// 获取坐标点集合
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        List<MapParameter> CoordinatesPoints(string nodeid, CoordinateType coordinateType);
        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="elevation"></param>
        void CoordinatePoint(string nodeid, CoordinateType coordinateType, out double longitude,
            out double latitude, out double elevation);
        //马贤辉 增加高效防死锁 查询
        List<MapParameter> CoordinatePoint_mxh(CoordinateType coordinateType);
        /// <summary>
        /// 编辑节点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="elevation"></param>
        /// <returns></returns>
        int Edit(string nodeid, CoordinateType coordinateType, double longitude,
            double latitude, double elevation);
        /// <summary>
        /// 彻底删除坐标
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        int DeleteTrue(string nodeid);
        /// <summary>
        /// 删除坐标
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        int Delete(string nodeid);

    }
}
