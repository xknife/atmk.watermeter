﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IStepTariffRepository : IRepository
    {
        /// <summary>
        /// 查询价格名称
        /// </summary>
        /// <param name="priceId"></param>
        /// <returns></returns>
        string GetPriceName(string priceId);



        /// <summary>
        /// 获取水表编号下的阶梯水价
        /// </summary>
        /// <param name="meters"></param>
        /// <returns>返回水表编号和对应编号的阶梯水价</returns>
        Dictionary<string, PrcieStep> GetMeterNumberPriceStep(params Meter[] meters);
        /// <summary>
        /// 获取该项目id下的所有价格信息
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        List<PrcieStep> GetPrcieStepList(string projectId);
        /// <summary>
        /// 获取指定阶梯价格下的费用信息
        /// </summary>
        /// <param name="PrcieStepId"></param>
        /// <returns></returns>
        List<FeePrice> GetPrcieStepFeeList(params PrcieStep[] PrcieStepId);
    }
}
