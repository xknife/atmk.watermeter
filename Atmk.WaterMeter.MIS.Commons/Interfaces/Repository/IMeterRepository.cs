﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMeterRepository : IRepository
    {
        ///// <summary>
        ///// 修改水表状态
        ///// </summary>
        ///// <param name="meterNumber">水表编号</param>
        ///// <param name="meterDocumentState"></param>
        ///// <returns></returns>
        //int ChangMeterState(string meterNumber, MeterDocumentState meterDocumentState);

        /// <summary>
        /// 获取指定业主Id下包含的水表信息集合
        /// </summary>
        /// <param name="ownerIds"></param>
        /// <returns></returns>
        List<Meter> GetMetersByOwnerIds(params string[] ownerIds);
        /// <summary>
        /// 修改水表状态
        /// </summary>
        /// <param name="meterNumber">水表编号</param>
        /// <param name="meterDocumentState"></param>
        /// <param name="staffId">操作员Id</param>
        /// <param name="modifiedLog">执行记录信息</param>
        /// <returns></returns>
        int ChangMeterState(string meterNumber, MeterDocumentState meterDocumentState,string staffId,string modifiedLog);

        /// <summary>
        /// 获取水表状态的枚举
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        MeterDocumentState MeterState(string meterNumber);
    }
}