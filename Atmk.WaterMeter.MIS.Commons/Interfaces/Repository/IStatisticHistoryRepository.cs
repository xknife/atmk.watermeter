﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IStatisticHistoryRepository
    {
        /// <summary>
        /// 添加指定统计类型的统计数据
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="statisticType"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        int Add(string projectId, StatisticType statisticType, string data);

        /// <summary>
        /// 根据参数，按时间获取最新的统计记录的数据
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="statisticType"></param>
        /// <returns></returns>
        string GetNewStatisticData(string projectId, StatisticType statisticType);
    }
}
