﻿using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Entities.CTCloud;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMeterReadRecordRepository
    {
        /// <summary>
        ///     添加电信云Post进本平台的数据包
        /// </summary>
        int Insert(params MeterReadingRecord[] entity);

#if DEBUG
        /// <summary>
        ///     清除所有电信云Post进本平台的数据包
        /// </summary>
        void Clear();
#endif

        /// <summary>
        ///     获取分页记录
        /// </summary>
        IEnumerable<MeterReadingRecord> Page(SortMode sortMode,int pageCurrent, int pageCount);


        /// <summary>
        ///     获取指定水表记录
        /// </summary>
        /// <returns></returns>
        IEnumerable<MeterReadingRecord> PageById(string id, SortMode sortMode);


        /// <summary>
        ///     获取指定时间记录
        /// </summary>
        IEnumerable<MeterReadingRecord> PageByDate(DateTime beginDate, DateTime endDate, SortMode sortMode, string meterId="");

        /// <summary>
        ///     获取低电压记录
        /// </summary>
        IEnumerable<MeterReadingRecord> PageByLowVoltage(SortMode sortMode, int minVoltage, int maxVoltage);

    }
}