﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMapsCoordinatesRepository : IRepository
    {
        /// <summary>
        /// 添加坐标点
        /// </summary>
        /// <param name="mapParameter"></param>
        /// <returns></returns>
        bool Insert(MapParameter mapParameter);
        /// <summary>
        /// 修改坐标点
        /// </summary>
        /// <param name="mapParameter"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool Update(MapParameter mapParameter,string nodeId);
        /// <summary>
        /// 删除坐标点
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool Delete(string nodeId);
    }
}
