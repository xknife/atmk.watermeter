﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IStaffRepository : IRepository
    {
        /// <summary>
        /// 根据登陆名查找操作员集合
        /// </summary>
        User FindByName(string name, string password);
        /// <summary>
        /// 根据登陆名查找操作员集合
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<User> FindByName(string name);

        /// <summary>
        /// 修改用户片区编码
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="districtId"></param>
        /// <param name="insert"></param>
        /// <returns></returns>
        int UpdateUserDistrict(string uid, string districtId, bool insert);

        /// <summary>
        /// 获取可以控制该片区的用户
        /// </summary>
        /// <param name="dstrictId"></param>
        /// <returns></returns>
        List<User> SelectUserByDistrict(string dstrictId);
    }
}
