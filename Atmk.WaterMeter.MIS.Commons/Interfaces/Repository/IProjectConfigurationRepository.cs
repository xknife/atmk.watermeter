﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IProjectConfigurationRepository : IRepository
    {
        /// <summary>
        /// 获取项目配置信息
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        ParameterList GetConfiguration(string projectId);
        /// <summary>
        /// 修改项目配置信息
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="jsonConfig"></param>
        /// <returns></returns>
        int UpdateConfig(string projectId, string jsonConfig);
    }
}