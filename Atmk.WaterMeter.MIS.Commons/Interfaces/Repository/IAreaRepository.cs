﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    /// <summary>
    /// 项目管理
    /// </summary>
    public interface IAreaRepository:IRepository
    {
        /// <summary>
        /// 获取项目对象
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        Project GetAreaEntities(string areaId);
    }
}
