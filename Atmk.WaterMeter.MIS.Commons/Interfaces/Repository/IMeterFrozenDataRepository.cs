﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMeterFrozenDataRepository:IRepository
    {
        /// <summary>
        /// 获取冻结数据
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="checkDate">判断日期</param>
        /// <param name="frozenType"></param>
        /// <returns></returns>
        //List<FrozenDataRecord> FrozenDatas(string meterNumber, DateTime checkDate, FrozenType frozenType);
    }
}
