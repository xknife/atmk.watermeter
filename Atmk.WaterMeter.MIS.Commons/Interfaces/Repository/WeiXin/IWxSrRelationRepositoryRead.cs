﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin
{
    public interface IWxSrRelationRepositoryRead
    {
        /// <summary>
        /// 获取该用户openId下的所有关系对象
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        List<WxSrRelations> SelectArrayRelations(string openId);
    }

}
