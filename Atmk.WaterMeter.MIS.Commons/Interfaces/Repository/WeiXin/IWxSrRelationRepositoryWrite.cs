﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin
{
    public interface IWxSrRelationRepositoryWrite
    {
        /// <summary>
        /// 添加小程序与水表关系
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="Number"></param>
        /// <param name="OwnerId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        bool AddWxSrRelation(string openId, string Number,string OwnerId,string AccountId);

        /// <summary>
        /// 添加小程序与水表关系
        /// </summary>
        /// <param name="relation"></param>
        /// <returns></returns>
        bool AddWxSrRelation(WxSrRelations relation);
        /// <summary>
        /// 注销小程序与水表关系的记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        bool DeleteWxSrRelation(string openId, string meterNumber);
    }
}
