﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin
{
    public interface IWxRechargeRepositoryWrite
    {
        /// <summary>
        /// 添加表单
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        bool AddOrder(WxSrOrderRecords orderRecord);

        /// <summary>
        /// 修改表单
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        bool UpdataOrder(WxSrOrderRecords orderRecord);
    }
}
