﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin
{
    public interface IWxRechargeRepositoryRead
    {
        /// <summary>
        /// 获取指定表单
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="outTradeNo"></param>
        /// <returns></returns>
        /// <exception cref="SqlNullValueException">查询空数据错误</exception>
        WxSrOrderRecords SelectOrder(string openId, string outTradeNo);

        /// <summary>
        /// 获取用户充值表单记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        List<WxSrOrderRecords> SelectOrders(string openId, string meterNumber);
    }

}
