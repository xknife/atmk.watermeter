﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IAccountRepository : IRepository
    {
        /// <summary>
        /// 根据业主信息获取账户信息集合
        /// </summary>
        /// <param name="ownerIds"></param>
        /// <returns></returns>
        List<OrderDetail> GetAccounts(params string[] ownerIds);
    }
}
