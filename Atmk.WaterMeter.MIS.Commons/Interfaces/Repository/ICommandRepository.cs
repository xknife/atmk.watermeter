﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface ICommandRepository : IRepository
    {
        List<MeterCommand> GetCommandsByCommandId(string commandId);
    }
}
