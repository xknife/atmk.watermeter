﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMergeQueryRepository
    {
        /// <summary>
        /// Lambda表达式读取的用户所有信息
        /// </summary>
        /// <returns></returns>
        IQueryable<object> SelectLambdaUserInfo();

        /// <summary>
        /// 获取指定片区下的业主水表信息
        /// </summary>
        /// <returns></returns>
        List<OwnerMeterViewModel> SelectOwmerMeter(string[] districtcode);
    }
}
