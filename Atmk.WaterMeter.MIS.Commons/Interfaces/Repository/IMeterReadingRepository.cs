﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IMeterReadingRepository : IRepository
    {
        /// <summary>
        /// 获取水表当前最后的有效读数记录
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        //MeterReadingRecord LastReadingRecord(string meterNumber);

        /// <summary>
        /// 添加水表读数记录
        /// </summary>
        /// <param name="meterNumber">水表编号</param>
        /// <param name="cTime">读取时间</param>
        /// <param name="value">增量</param>
        /// <param name="valveState"></param>
        /// <param name="voltageState">欠压状态</param>
        bool AddRecorcd(string meterNumber, DateTime cTime, double value, ValveState valveState = ValveState.Opening,
            int voltageState=0);

        /// <summary>
        ///  获取指定水表列表中，指定接收日期里最新的各水表的抄表数据
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        List<MeterReadingRecord> NowDataMeterReadingRecords(List<string> meterNumbers, DateTime date);

        /// <summary>
        /// 获取指定水表列表中，离指定接收日期里最近的各水表的抄表数据
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        List<MeterReadingRecord> LastDateMeterReadingRecords(List<string> meterNumbers, DateTime date);

        /// <summary>
        /// 获取指定范围日期内，最新的各水表的抄表数据
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        List<MeterReadingRecord> RangeNewDateMeterReadingRecords(List<string> meterNumbers, DateTime sDate,
            DateTime eDate);

        /// <summary>
        /// 获取指定水表编号范围内的水表读数记录
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <returns></returns>
        List<MeterReadingRecord> GetDateMeterReadingRecords(params string[] meterNumbers);

        /// <summary>
        /// 获取MeterReadingRecord 数据表所有记录
        /// 2019-8-24 马贤辉
        /// </summary>
        List<MeterReadingRecord> Get_MeterReadingRecord_List();
    }
}