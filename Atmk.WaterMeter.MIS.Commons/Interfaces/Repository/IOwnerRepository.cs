﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Repository
{
    public interface IOwnerRepository : IRepository
    {
        /// <summary>
        /// 根据叶子节点的片区集合获取该片区下的业主信息集合
        /// </summary>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        List<Owner> GetOwners(params string[] leafDistrictIds);
        /// <summary>
        /// 根据水表号获取业主信息集合
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <returns></returns>
        List<Owner> GetOwnersByMeterNo(params string[] meterNumbers);
    }
}