﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces
{
    public interface IDemo
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [DataMember, Key]
        Guid Id { get; set; }

        /// <summary>
        /// 采用Json格式存储其他未定义的属性
        /// </summary>
        [MaxLength]
        string Memo { get; set; }

        /// <summary>
        /// 采用Json格式存储记录的操作信息
        /// </summary>
        [MaxLength]
        string Log { get; }

        /// <summary>
        /// 记录状态
        /// </summary>
        [Required]
        RecordStateEnum RecordState { get; set; }

        /// <summary>
        ///  添加记录操作信息
        /// </summary>
        /// <param name="user">记录者</param>
        /// <param name="annotation">注释,注解</param>
        void ModifiedLog(string user, string annotation = "");

    }
}
