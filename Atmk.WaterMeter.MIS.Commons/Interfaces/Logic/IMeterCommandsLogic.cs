﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 水表系统指令逻辑
    /// </summary>
    public interface IMeterCommandsLogic
    {
        /// <summary>
        /// 判断是否与未发送指令重复
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        bool SameCommand(string meterNumber, int TapValue);

        /// <summary>
        /// 设置开关阀指令
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="replace"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        bool SetValveCommand(string CTdeviceId, int CommandTapValue);

        /// <summary>
        /// 设置抄表指令
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        bool SetReadCommand(string meterNumber, CommandType commandType);

        /// <summary>
        /// 撤销水表指令
        /// </summary>
        /// <param name="commandId"></param>
        /// <returns></returns>
        bool DeleteCommand(string commandId);

        /// <summary>
        /// 查询水表信息
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="staffId"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        object GetMeters(string aid, int page, string staffId, string ProjectsId,string searchType = "", string searchValue = "");

        /// <summary>
        /// 查询所有水表命令
        /// </summary>
        /// <param name="districtId">片区Id</param>
        /// <param name="meterNumber"></param>
        /// <param name="page"></param>
        /// <param name="staffId"></param>
        /// <param name="statusType"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        object GetCommand(string meterNumber, int page,string userid);
    }
}
