﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface ISystemParamentsLogic
    {
        /// <summary>
        /// 获取水表类型集合
        /// </summary>
        /// <returns></returns>
        List<string> MeterTypeGet(string projectId);

        /// <summary>
        /// 获取初始参数
        /// </summary>
        /// <returns></returns>
        object InitialParamentSelect(string projectId);

        /// <summary>
        /// 修改初始参数
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="obj"></param>
        /// <param name="warn"></param>
        /// <returns></returns>
        bool InitialParamentUpdate(string projectId, dynamic obj, out string warn);

        /// <summary>
        /// 获取配置参数
        /// </summary>
        /// <returns></returns>
        object SystemParamentSelect(string projectId);

        /// <summary>
        /// 修改配置参数
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="obj"></param>
        /// <param name="warn"></param>
        /// <returns></returns>
        bool SystemParamentUpdate(string projectId, dynamic obj, out string warn);

        /// <summary>
        /// 获取基础项目信息
        /// </summary>
        /// <returns></returns>
        object SysBaseDataSelect();

        /// <summary>
        /// 获取基础项目信息
        /// </summary>
        /// <returns></returns>
        object SysBaseDataSelect(string name);

        /// <summary>
        /// 添加基础项目信息
        /// </summary>
        /// <param name="role"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool AddSysBaseData(dynamic obj, out Exception exception);

        /// <summary>
        /// 修改基础项目信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool UpdateSysBaseData(dynamic obj, out Exception exception);

        /// <summary>
        /// 删除基础项目信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool DeleteBaseData(string id, out Exception exception);
    }
}
