﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.OwnerMeter;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IOwnerMeterLogic
    {
        /// <summary>
        /// 查询指定区域列表数据
        /// </summary>
        /// <param name="type">0 普通 1 换表</param>
        /// <param name="areaId">项目Id</param>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="meterState">表状态</param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        object Select(int type, string areaId, string id, string nodeType,int page, int pageSize, out int count,
           int meterState=0, string searchType = "", string searchValue = "");

        object SelectOwner(string ownerid, int page, int pageSize, out int count);
        /// <summary>
        /// 换表对外方法
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Exchange(string uid, dynamic obj);
        /// <summary>
        /// 销表方法
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="cancelMeter"></param>
        /// <returns></returns>
        bool Cancel(string uid, CancelMeter cancelMeter);

        /// <summary>
        /// 添加用户和水表信息
        /// </summary>
        /// <param name="districtId"></param>
        /// <param name="owner"></param>
        /// <param name="meter"></param>
        /// <param name="respOwnerId"></param>
        /// <param name="respMeterId"></param>
        /// <returns></returns>
        bool InsertOwner(string districtId, dynamic owner, Token token,out string ErrorMessage);
        /// <summary>
        /// 修改用户和水表信息
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="meter"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool Update(string districtId, dynamic owner,  out Exception exception);

        /// <summary>
        /// 删除水表及用户信息
        /// </summary>
        /// <param name="ownerId"></param>
        /// <param name="meterId"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        object Delete(string ownerId, string meterId, out Exception exception);
        /// <summary>
        /// 通过片区Id,获取业主信息
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<Owner> GetOwner(params string[] districtId);
    }

}
