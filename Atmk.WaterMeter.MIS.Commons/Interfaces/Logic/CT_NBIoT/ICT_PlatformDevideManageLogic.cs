﻿using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.CT_NBIoT
{
    public interface ICT_PlatformDevideManageLogic
    {
        /// <summary>
        /// 向平台添加设备
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="model"></param>
        /// <param name="name"></param>
        /// <returns>返回值里 Message为创建成功后的设备Id</returns>
        Task<BooleanResult> AddDevice(string imei, string model, string name);

        /// <summary>
        /// 删除平台上指定设备
        /// </summary>
        /// <param name="deviceid"></param>
        /// <returns>返回值 bool 和 ErrorMessage</returns>
        Task<BooleanResult> DeleteDevice(string deviceid);
        /// <summary>
        /// 获取平台所有设备信息
        /// </summary>
        /// <returns></returns>
        Task<ResDevices> GetDevices();
    }

}
