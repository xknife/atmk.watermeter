﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.ViewModels.ReadMeter;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.CT_NBIoT
{
    public interface ICT_PlatformDeviceInfoLogic
    {
        /// <summary>
        /// 获取指定设备的读数信息
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task<MeterReadingRecord> ReadFirstMeterRecord(string deviceId);

        /// <summary>
        /// 发送开关阀指令，并返回指令Id
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="valveState"></param>
        /// <returns></returns>
        Task<BooleanResult> SetValveState(string deviceId, ValveState valveState);
        /// <summary>
        /// 批量 发送开关阀指令，并返回指令Id
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="valveState"></param>
        /// <returns></returns>
        Task<BooleanResult> BatchOpenValve(string[] deviceIds);
        /// <summary>
        /// 获取冻结数据集合
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="freezingType"></param>
        /// <returns></returns>
        Task<List<MeterFreezData>> ReadFreezData(string deviceId, int freezingType);

        /// <summary>
        /// 获取最新冻结数据
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="freezingType"></param>
        /// <returns></returns>
        Task<MeterFreezData> ReadNewFreezData(string deviceId, int freezingType);
        /// <summary>
        /// 发送查询冻结数据指令
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="freezingType"></param>
        /// <returns></returns>
        Task<BooleanResult> SetFreezingDataCommand(string deviceId, int freezingType);
    }
}
