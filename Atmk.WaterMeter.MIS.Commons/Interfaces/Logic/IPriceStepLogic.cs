﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 阶梯价格费用
    /// </summary>
    public interface IPriceStepLogic
    {
        /// <summary>
        /// 获取指定水表类型和价格名称下的价格
        /// </summary>
        /// <param name="meterType"></param>
        /// <param name="priceType"></param>
        /// <returns></returns>
        List<double> GetPriceList(string meterType, string priceType);

        /// <summary>
        /// 查询所有价格及费用情况
        /// </summary>
        /// <returns></returns>
        object SelectPriceFee(string projectId,string meterType = "");

        /// <summary>
        /// 添加价格信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        bool InsertPrice(dynamic obj,string projectId);
        /// <summary>
        /// 修改价格信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdatePrice(dynamic obj);
        /// <summary>
        /// 删除价格信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeletePrice(string id);

        /// <summary>
        /// 添加费用信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool Insertfee(dynamic obj, out Exception exception);

        /// <summary>
        /// 修改费用信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool Updatefee(dynamic obj, out Exception exception);

        /// <summary>
        /// 删除费用信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool Deletefee(string id, out Exception exception);

        /// <summary>
        /// 是否存在相同的价格名称
        /// </summary>
        /// <param name="priceName"></param>
        /// <param name="priceId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        bool SameName(string priceName, string priceId,string projectId);
    }
}
