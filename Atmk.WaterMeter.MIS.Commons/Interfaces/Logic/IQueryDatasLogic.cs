﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IQueryDatasLogic
    {
        Token Token { get; set; }
        string DistrictId { get; set; }
        string SearchType { get; set; }
        string SearchValue { get; set; }

        /// <summary>
        /// 获取抄表数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="dateType"></param>
        /// <param name="sTime"></param>
        /// <param name="eTime"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        object SelectMeterDates(Token token, string aid ,string searchType, string searchValue, int page, int pageSize, string dateType, DateTime sTime, DateTime eTime,out int count);

        /// <summary>
        /// 查询指定日期未抄回数据表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="dateType"></param>
        /// <param name="meterType"></param>
        /// <param name="date"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        object SelectNoDatesMeter(int page, int pageSize, string dateType, string meterType, DateTime date,
            out int count);

        /// <summary>
        /// 获取平局用水量
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="statisticsType"></param>
        /// <param name="useageGreat"></param>
        /// <param name="useageLess"></param>
        /// <param name="sTime"></param>
        /// <param name="eTime"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        object SelectWaterDosage(int page, int pageSize, int statisticsType, int useageGreat, int useageLess,
            DateTime sTime, DateTime eTime, out int count);
        /// <summary>
        /// 获取最新的水表上报记录
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        MeterReadingRecord SelectNearRecord(string meterNumber);
    }
}
