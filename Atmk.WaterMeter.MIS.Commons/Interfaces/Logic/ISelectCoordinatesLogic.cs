﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface ISelectCoordinatesLogic:ILogic
    {
        /// <summary>
        /// 根据坐标点关联的节点id(Project、District、业主的Id)获取坐标点
        /// </summary>
        /// <returns></returns>
        PostPointMapCoordinat SelectPointMapCoordinat(string nodeid);
        /// <summary>
        /// 获取片区或项目id下的业主坐标点集合
        /// </summary>
        /// <param name="id"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        List<PostPointMapCoordinat> SelectPointMapCoordinat(string id, CoordinateType coordinateType);

        /// <summary>
        /// 获取项目Id下的片区坐标集合
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<GetDistrictCoordinates> SelectDistrictMapCoordinat(string areaId,string districtId="");
        /// <summary>
        /// 获取片区坐标集合
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<MapCoordinat> GetCoordinate(string districtId);
    }
}
