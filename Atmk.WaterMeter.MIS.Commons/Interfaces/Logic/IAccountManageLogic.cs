﻿using System;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 账户管理逻辑
    /// </summary>
    public interface IAccountManageLogic : ILogic
    {
        /// <summary>
        ///     查询账户信息
        /// </summary>
        /// <param name="token"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        object SelectAccount(string areaId, string aid, int page, int pageSize, out int count,
            string searchType = "", string searchValue = "");

        /// <summary>
        ///     充值
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="payment"></param>
        /// <param name="lastBalance"></param>
        /// <param name="refill"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool RechargeAccount(string staffId, string staffName, string ownerId, string accountId, decimal payment, decimal lastBalance,
            decimal refill, out Exception exception);

        /// <summary>
        ///     退费
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="returns"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool ReturnsAccount(string staffId, string ownerId, string accountId, dynamic returns,
            out Exception exception);

        /// <summary>
        ///     查询扣费信息
        /// </summary>
        /// <param name="payloadId"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="ownerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        object SelectPayment(string payloadId, string aid, int page, int pageSize, out int count,
            string ownerId, string accountId);

        /// <summary>
        ///     查询扣费信息
        /// </summary>
        /// <param name="token"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="endTime"></param>
        /// <param name="count"></param>
        /// <param name="paymentType"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <param name="ownerId"></param>
        /// <param name="accountId"></param>
        /// <param name="starTime"></param>
        /// <returns></returns>
        object SelectPayment(string areaId, string aid, int page, int pageSize, DateTime starTime,
            DateTime endTime, int paymentType, out int count,
            string searchType = "", string searchValue = "", string ownerId = "", string accountId = "");

        /// <summary>
        ///     费用减免--暂停
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="money"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool FeeDerate(Token token, string userId, string accountId, decimal money, out Exception exception);

        /// <summary>
        ///     查询充值记录
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="count"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        object SelectRefills(string areaId, string aid, int page, int pageSize, DateTime startTime, DateTime endTime,
            out int count, string searchType, string searchValue);

        /// <summary>
        ///     撤销充值缴费记录
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="refillId"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool RevertRefill(string staffId, string ownerId, string accountId, string refillId, out Exception exception);

        /// <summary>
        ///     销户操作
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool ClosingAccount(string staffId, string staffName,string ownerId, string accountId, out string message);
    }
}