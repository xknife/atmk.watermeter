﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public  interface IReponstoryManageLogic
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool StaffClear();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool AllClear();
        /// <summary>
        /// 清空所有记录
        /// </summary>
        bool ClearRecords();

        /// <summary>
        /// 初始化抄表记录
        /// </summary>
        /// <returns></returns>
        bool MeterRecordClear(params string[] meterNumbers);
    }
}
