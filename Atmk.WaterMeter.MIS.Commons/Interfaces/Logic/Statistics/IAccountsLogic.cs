﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using Atmk.WaterMeter.MIS.Entities;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 账户信息统计接口
    /// </summary>
    public interface IAccountsLogic : ILogic
    {
        /// <summary>
        /// 获取指定片区或片区集合下的包含业主姓名和账户余额的费用提醒信息列表
        /// </summary>
        /// <param name="leafDistrictIds">叶子片区Id</param>
        /// <returns></returns>
        CostReminderData GetCostReminderItemList(params string[] leafDistrictIds);
    }
}
