﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    public interface IChartLogic
    {
        //费用提醒 已关阀
        Task<ChartDatas> FYTX_YGF(string projectId);
        StatisticsInfoData Top1(string projectId);
        WaterConsumptionInfoData Top3_4(string projectId);

        DistrictReadRatioData GetDistrictReadRatioData();
        WaterConsumptionsCountData GetLastSeasonDosage(string projectId);
        WaterConsumptionsCountData GetLastMonthDosage(string projectId);
        YearWaterConsumptionsCountData GetLastYearWaterDosage(string projectId);
        //前三天用量
        WaterConsumptionsCountData GetFirstThreeDays(string projectId);
    }
}
