﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 用水量统计逻辑
    /// </summary>
    public interface IWaterConsumptionLogic:ILogic
    {
        /// <summary>
        /// 获取个各业主的用水总量
        /// </summary>
        YesterdayConsumptionData GetWaterConsumption(Period period, DateTime dateTime, params string[] leafDistrictIds);

        /// <summary>
        /// 根据业主获取当前日期所在的一段时间(月、季度、年)内的阶梯用水量
        /// </summary>
        /// <param name="period"></param>
        /// <param name="dateTime"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        List<double> GetPriceStepWaterConsumption(Period period, DateTime dateTime,string ownerId);



        /// <summary>
        /// 获取指定片区集合下的，当前日期所在的一段时间(按月、季度)内的每月用水总量
        /// </summary>
        /// <param name="period"></param>
        /// <param name="dateTime"></param>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        WaterConsumptionsCountData GetDistrictMonthWatherConsumption(Period period, DateTime dateTime, params string[] leafDistrictIds);

        /// <summary>
        /// 获取指定片区集合下的，当前日期所在的一段时间(年)内的每月用水总量
        /// </summary>
        /// <param name="period"></param>
        /// <param name="dateTime"></param>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        YearWaterConsumptionsCountData GetYearWaterConsumptions(Period period, DateTime dateTime, params string[] leafDistrictIds);

        /// <summary>
        /// 获取指定片区集合下的，当前日期所在的一段时间(按月、季度、年)内的阶梯用水总量统计
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="leafDistrictIds"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        WaterConsumptionsCountData GetDistrictPriceStepWaterConsumption(Period period, DateTime dateTime, params string[] leafDistrictIds);

        /// <summary>
        /// 获取指定片区集合下的总用水量统计
        /// </summary>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        WaterConsumptionInfoData GetWaterConsumptionInfoData(params string[] leafDistrictIds);
        /// <summary>
        /// 获取项目里的用水情况
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        WaterConsumptionInfoData GetWaterConsumptionInfoData(string areaId);
    }
}
