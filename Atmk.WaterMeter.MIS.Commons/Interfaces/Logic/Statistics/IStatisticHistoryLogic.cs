﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    public interface IStatisticHistoryLogic
    {
        /// <summary>
        /// 保存统计数据
        /// </summary>
        /// <typeparam name="T">统计数据的类型，是哪个viewmodel</typeparam>
        /// <param name="projectId">项目id</param>
        /// <param name="statisticType">对象的类型</param>
        /// <param name="data">对象数据</param>
        /// <returns>成功，失败</returns>
        bool Save<T>(string projectId, StatisticType statisticType, T data);

        /// <summary>
        /// 获取最新的一条统计数据
        /// </summary>
        /// <typeparam name="T">统计数据的类型，是哪个viewmodel</typeparam>
        /// <param name="projectId">项目id</param>
        /// <param name="statisticType">对象的类型</param>
        /// <returns>返回的对象</returns>
        T GetNewData<T>(string projectId, StatisticType statisticType);
    }
}
