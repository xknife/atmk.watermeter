﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 业主信息统计接口
    /// </summary>
    public interface IOwnersLogic:ILogic
    {
        
        /// <summary>
        /// 获取叶子节点的片区集合里，所有关阀的业主信息
        /// </summary>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        ClosingvalveData GetCloseClosingValveOwnerItems(params string[] leafDistrictIds);
    }
}
