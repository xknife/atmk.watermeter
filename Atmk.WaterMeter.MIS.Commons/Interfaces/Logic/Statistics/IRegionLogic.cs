﻿using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 地区/片区统计逻辑接口
    /// </summary>
    public interface IRegionLogic : ILogic
    {
        /// <summary>
        /// 根据项目获取片区统计信息
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        StatisticsInfoData GetStatisticsInfoData(string areaId);

        /// <summary>
        /// 根据项目获取叶子节点集合
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        List<District> GetLeafDistrictEntities(string areaId);
    }
}
