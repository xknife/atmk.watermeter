﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 水表信息统计接口
    /// </summary>
    public interface IWaterMetersLogic:ILogic
    {
        /// <summary>
        /// 获取传入片区下，所有水表的数量
        /// </summary>
        /// <param name="districtIds"></param>
        /// <returns></returns>
        int GetMeterCount(params string[] districtIds);

        /// <summary>
        /// 在指定片区或片区集合下，根据指定日期，获取该日期的抄表成功率
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        DistrictReadRatioData GetDistrictReadRatioItems(DateTime dateTime,params string[] leafDistrictIds);

     
    }
}
