﻿using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Commons.ViewModels;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics
{
    /// <summary>
    /// 基本统计信息逻辑
    /// </summary>
    public interface IBaseInfoLogic
    {
        #region 水表
    

        /// <summary>
        ///     获取业主与水表的数据集合
        /// </summary>
        /// <param name="sortOrder"></param>
        /// <param name="districtIds">片区编码集合</param>
        /// <returns></returns>
        List<OwnerMeterViewModel> OwnerMeterData(string sortOrder, string[] districtIds);

        /// <summary>
        ///     查询指定区域业主及水表数据
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="me"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <param name="ow"></param>
        /// <returns></returns>
        void SelectOwnerMeter(string uid, string id, int page, int pageSize, 
            out List<Owner> ow,
            out List<Meter> me, 
            string searchType = "", 
            string searchValue = "");
#endregion

        /// <summary>
        ///     根据条件获取业主查询结果
        /// </summary>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <param name="owners"></param>
        /// <returns></returns>
        List<Owner> QuerySearchOwner(string searchType, string searchValue,
            List<Owner> owners);

        /// <summary>
        ///     根据条件获取业主查询结果
        /// </summary>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <param name="meters"></param>
        /// <param name="owners"></param>
        /// <returns></returns>
        List<Meter> QuerySearchMeter(string searchType, string searchValue,
            List<Meter> meters, List<Owner> owners = null);

        /// <summary>
        ///     获取每页显示行数
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int PageSize(string userId);

        
    }
}