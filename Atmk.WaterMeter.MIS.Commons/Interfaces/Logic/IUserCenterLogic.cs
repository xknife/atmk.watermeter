﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Area;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;
using Atmk.WaterMeter.MIS.Entities;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IUserCenterLogic
    {
        /// <summary>
        /// 获取操作员信息
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        IData UserInfo(string staffId);


        /// <summary>
        /// 查询所有站点信息及用户信息
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="siteid"></param>
        /// <returns></returns>
        AreaOperatorData SelectUserSite(string staffId, string siteid = "");

        /// <summary>
        /// 判断是否超级管理员
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        bool SuperAdmin(string uid);

        /// <summary>
        /// 获取所有项目
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="siteid"></param>
        /// <returns></returns>
        List<Area> SelectAreas(string staffId, string siteid);

        /// <summary>
        /// 添加站点信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        bool AddSite(dynamic obj,out string message,out string siteId);

        /// <summary>
        /// 修改站点信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool UpdateSite(dynamic obj, out string message);

        /// <summary>
        /// 删除站点
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool DeleteSite(string id, out string message);

        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool AddUser(dynamic obj, out string message);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool EditPassWord(string id, dynamic obj, out string message);

        /// <summary>
        /// 验证密码是否正确
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="provingPass"></param>
        /// <returns></returns>
        bool VerifyPassWord(string uid, string provingPass);

        /// <summary>
        /// 初始化密码
        /// </summary>
        /// <param name="staffid">要被初始化的操作员</param>
        /// <param name="password"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool InitialisePassWord(string staffid,out string password, out string message);

        /// <summary>
        /// 获取操作员密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        string GetPassWord(string id, out string message);

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool EditUser(dynamic obj, out string message);

        /// <summary>
        /// 获取用户显示行数
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        object SelectLines(string staffId);

        /// <summary>
        /// 修改用户显示行数
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="staffId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool UpdateLines(dynamic obj, string staffId, out string message);

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool DeleteUser(string id, out string message);
    }
}
