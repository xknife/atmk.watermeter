﻿using Atmk.WaterMeter.MIS.Entities.CTCloud;
using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IMeterReadRecordLogic
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Save(params MeterReadingRecord[] entity);


    }
}
