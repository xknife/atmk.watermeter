﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IProjectsLogic
    {
        /// <summary>
        /// 根据id,获取项目对象
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        Project GetProject(Guid areaId); 
    }
}
