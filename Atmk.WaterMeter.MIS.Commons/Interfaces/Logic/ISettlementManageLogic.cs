﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface ISettlementManageLogic
    {
        /// <summary>
        /// 查询结算信息
        /// </summary>
        /// <param name="payloadId"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="meterType"></param>
        /// <param name="endTime"></param>
        /// <param name="count"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <param name="startTime"></param>
        /// <returns></returns>
        //object SelectSettlement(string payloadId, string aid, int page, int pageSize, string meterType,
        //    DateTime startTime, DateTime endTime, out int count, string searchType = "", string searchValue = "");

        ///// <summary>
        ///// 撤销结算
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="settlementId"></param>
        ///// <param name="exception"></param>
        ///// <returns></returns>
        //bool RevertSettlement(string userId, string settlementId, out Exception exception);

        ///// <summary>
        ///// 结算方法
        ///// </summary>
        ///// <param name="staffId"></param>
        ///// <param name="aid"></param>
        ///// <param name="month"></param>
        ///// <param name="message"></param>
        ///// <param name="exception"></param>
        ///// <returns></returns>
        //bool Settlement(string staffId,string aid, DateTime month, out int message, out Exception exception);
    }

}
