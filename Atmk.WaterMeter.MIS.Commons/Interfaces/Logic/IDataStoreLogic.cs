﻿using System;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Entities;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 数据存储逻辑
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IDataStoreLogic
    {
        /// <summary>
        /// 保存水表读数记录数据
        /// </summary>
        /// <param name="meterNumber">水表编码</param>
        /// <param name="serviceData"></param>
        /// <param name="eventTime"></param>
        //int MeterRecordSave(string meterNumber, DeviceData serviceData, DateTime eventTime);

        /// <summary>
        /// 保存小时冻结数据
        /// </summary>
        /// <param name="meterCode"></param>
        /// <param name="history"></param>
        /// <param name="eventTime"></param>
        //void HourFrozenSave(string meterNumber, byte[] history, DateTime eventTime);

        ///// <summary>
        ///// 保存日冻结数据
        ///// </summary>
        ///// <param name="meterNumber"></param>
        ///// <param name="history"></param>
        ///// <param name="eventTime"></param>
        //void DayFrozenSave(string meterNumber, byte[] history, DateTime eventTime);

        ///// <summary>
        ///// 保存月冻结数据
        ///// </summary>
        ///// <param name="meterNumber"></param>
        ///// <param name="history"></param>
        ///// <param name="eventTime"></param>
        //void MonthFrozenSave(string meterNumber, byte[] history, DateTime eventTime);
    }

}
