﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.District;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 片区逻辑层
    /// </summary>
    public interface IDistrictLogic
    {
        /// <summary>
        /// 获取Id对应片区下的所有片区信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="arrayDistrictEntities"></param>
        /// <returns></returns>
        List<District> DistrictEntitiesById(string id, List<District> arrayDistrictEntities);

        /// <summary>
        /// 获取指定用户ID和区域集合里的主区域信息
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="districts"></param>
        /// <returns></returns>
        List<District> GetDistrictsMainByUid(string uid, List<District> districts);

        /// <summary>
        /// 获得用户ID下所有片区信息
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="districts"></param>
        /// <returns></returns>
        List<District> GetDistrictsByUid(string uid, List<District> districts);

        /// <summary>
        /// 获取所有区域信息
        /// </summary>
        /// <returns></returns>
        object SelectDistrict(string uid = "");

        /// <summary>
        /// 查询指定区域列表数据
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="id">片区id</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        object SelectDistrict(string uid, string id, int page, int pageSize, out int count);

        /// <summary>
        /// 添加片区信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="staffId"></param>
        /// <param name="districtId"></param>
        /// <returns></returns>
        bool InsertDistrict(DistrictInfomationBody obj,string staffId, out string districtId);

        /// <summary>
        /// 修改片区信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool UpadateDistrict(dynamic obj, out Exception exception);

        /// <summary>
        /// 删除片区信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        bool DeleteDistrict(string id, string uid, out Exception exception);

        /// <summary>
        /// 根据项目获取所有片区信息
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        List<District> GetDistricts(Guid projectId);
        List<District> GetDistrictsLv2(string projectId);
        /// <summary>
        /// 根据操作员id，获取其所有片区的排序信息
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        List<DistrictSequences> GetDistrictOrder(string staffId);
        /// <summary>
        /// 根据id获取片区
        /// </summary>
        /// <param name="districtGuid"></param>
        /// <returns></returns>
        District GetDistrict(Guid districtGuid);
        /// <summary>
        /// 修改片区排序序号
        /// </summary>
        /// <param name="sequences"></param>
        /// <returns></returns>
        bool UpdateSequences(DistrictNodeSequence[] sequences);
    }
}
