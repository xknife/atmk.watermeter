﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 数据联合查询逻辑
    /// </summary>
    public interface IMergeStatisticsLogic
    {
        /// <summary>
        /// 获取抄回/未抄回数据
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        object MeterCopyCount(Token userPayload, DateTime date);

        /// <summary>
        /// 根据token获取用户负责的所有水表
        /// </summary>
        /// <param name="userPayload"></param>
        /// <returns></returns>
        List<OwnerMeterViewModel> MeterForDistrictList(Token userPayload);

        /// <summary>
        /// 获取水表状态统计
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        object MeterStateCount(Token userPayload, DateTime date);

        /// <summary>
        /// 获取一年的月用水量
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        object WaterConsumptionMonth(Token userPayload, DateTime date);
    }
}
