﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.OwnerMeter;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IMeterLogic
    {
        bool InsertMeter(string districtId, dynamic owner, dynamic meter, Token token, out string ErrorMessage);
    }
}
