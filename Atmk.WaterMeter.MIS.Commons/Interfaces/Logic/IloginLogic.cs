﻿
using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 登陆逻辑
    /// </summary>
    public interface ILoginLogic
    {
        /// <summary>
        /// 登陆验证登陆名，密码是否正确
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <param name="staffId">操作员id</param>
        /// <param name="roleId">角色id</param>
        /// <param name="areaId"></param>
        /// <returns></returns>
        bool LoginMatching(string name, string password, out string staffId, out string roleId, out string areaId);

        //更正新版登录
        //马贤辉2019-08-29
        User LoginMatching(string name, string password);
        int Add(int i, int j);
    }
}
