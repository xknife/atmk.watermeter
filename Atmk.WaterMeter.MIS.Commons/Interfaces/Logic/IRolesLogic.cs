﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IRolesLogic
    {
        /// <summary>
        /// 查询所有角色信息
        /// </summary>
        /// <returns></returns>
        object SelectRoles();

        /// <summary>
        /// 添加角色信息
        /// </summary>
        /// <param name="role"></param>
        /// <param name="exception"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool AddRoles(dynamic role, out string exception,out string id);

        /// <summary>
        /// 修改角色信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool UpdateRoles(dynamic obj, out string exception);

        /// <summary>
        /// 删除角色信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool DeleteRole(string id, out string exception);

        /// <summary>
        /// 获取权限信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        object SelectParmissions(string id);

        /// <summary>
        /// 修改权限信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="authList"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        object UpdateParmissions(string id, string authList, out string exception);
    }
}
