﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin
{
    /// <summary>
    /// 充值记录逻辑  用于用户订单，水表对应账户的充值记录的写入和读取
    /// </summary>
    public interface IWxRechargeRecordLogic
    {
        /// <summary>
        /// 创建订单记录
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="meterNumber"></param>
        /// <param name="out_trade_no"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        BooleanResult CreateOrderRecored(string openid, string meterNumber, string out_trade_no, int amount);

        /// <summary>
        /// 修改订单状态
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="meterNumber"></param>
        /// <param name="out_trade_no"></param>
        /// <param name="orderstatus"></param>
        /// <returns></returns>
        BooleanResult UpdataOrderState(string openid, string out_trade_no,
            OrderStatus orderstatus);

        /// <summary>
        /// 通过订单信息充值
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="out_trade_no"></param>
        /// <returns></returns>
        BooleanResult RechargeAccount(string openid, string out_trade_no);

        /// <summary>
        /// 获取该名用户的缴费充值记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        Task<List<WxSrOrderRecords>> ChargingRecords(string openId, string meterNumber);
        /*
         * 充值记录逻辑方法
         * 1 修改金额
         * 2 写入充值记录 
        */

        /* 订单保存方法/订单查询方法
         * 
         */
    }
}
