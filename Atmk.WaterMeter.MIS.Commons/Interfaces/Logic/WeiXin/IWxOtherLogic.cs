﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin
{
     public interface IWxOtherLogic
     {
        /// <summary>
        /// 获取微信问答答案
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
         Task<Dictionary<string, string>> SelectWxQuestions(string openId);
     }
}
