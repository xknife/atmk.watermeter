﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin
{
    public interface IWxAccountLogic
    {
        /// <summary>
        /// 通过用户id获取账户信息
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <param name="houseNumber"></param>
        /// <param name="balance"></param>
        /// <param name="meterNo">水表编号，如果有就填上</param>
        /// <returns></returns>
        Task<bool> SelectAccount(string openId, out string meterNumber, out string houseNumber, out decimal balance, string meterNo = "");


        /// <summary>
        /// 通过用户id获取水表编号集合--复数时用
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        Task<List<string>> SelectAllMeterNumber(string openId);

        /// <summary>
        /// 添加用户Id和水表的关系记录，并返回该水表所在位置的门牌号和
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <param name="houseNumber"></param>
        /// <param name="balance"></param>
        /// <returns></returns>
        Task<bool> AddAccount(string openId, string meterNumber, out string name, out string houseNumber, out decimal balance);

        bool AddAccount2(string openId, Meter meter, Owner owner);
        Owner GetOwner(string openId, string meterNumber, out decimal balance);
        List<Meter> GetMeters(string ownerid);
        /// <summary>
        /// 注销用户Id和水表关系记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        Task<bool> LogoutAccount(string openId, string meterNumber);
        
    }
}
