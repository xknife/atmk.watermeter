﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    /// <summary>
    /// 坐标点逻辑接口
    /// </summary>
    public interface IEditCoordinatesLogic:ILogic
    {
        /// <summary>
        /// 添加片区坐标集合
        /// </summary>
        /// <param name="postPoint"></param>
        /// <returns></returns>
        bool InsertDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId);
        /// <summary>
        /// 修改片区坐标集合
        /// </summary>
        /// <param name="mapCoordinat"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool UpdateDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId);
        /// <summary>
        /// 删除坐标点
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool DeleteDistrictCoordinates(string nodeId);

        /// <summary>
        /// 添加坐标点
        /// </summary>
        /// <param name="postPoint"></param>
        /// <returns></returns>
        bool InsertPoint(MapCoordinat mapCoordinat, string nodeId,CoordinateType coordinateType);
        /// <summary>
        /// 修改坐标点
        /// </summary>
        /// <param name="mapCoordinat"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool UpdatePoint(MapCoordinat mapCoordinat, string nodeId);
        /// <summary>
        /// 删除坐标点
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        bool DeletePoint(string nodeId);
    }
}
