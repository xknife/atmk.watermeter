﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.Logic
{
    public interface IMeterModelLogic
    {
        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <returns></returns>
        object SelectModNum(string id = "");

        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool AddModNum(dynamic obj, out Exception exception);

        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool UpdateModNum(dynamic obj, out Exception exception);

        /// <summary>
        /// 删除水表型号信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        bool DeleteModNum(string id, out Exception exception);
    }

}
