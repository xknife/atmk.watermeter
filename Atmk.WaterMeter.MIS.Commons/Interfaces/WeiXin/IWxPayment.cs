﻿using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin
{
    public interface IWxPayment
    {
        /// <summary>
        /// 小程序的订单生成方法
        /// </summary>
        /// <param name="appid">小程序id</param>
        /// <param name="openId">用户id</param>
        /// <param name="mch_id">商户id</param>
        /// <param name="callBackUrl">回调地址</param>
        /// <param name="mis_key">商户服务商key(自定义)</param>
        /// <param name="total_fee">充值金额(分)</param>
        /// <returns></returns>
        Task<ResponUnifiedorder> SmallRoutineUnifiedorder(string appid, string openId,
            string mch_id, string callBackUrl, string mis_key, int total_fee);



    }
}