﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin
{
    public interface IWxSignService
    {
        /// <summary>
        /// 获取微信小程序登陆后服务器返回的token
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="session_key"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        bool TryGetToken(string openid, string session_key, out string token);
    }
}
