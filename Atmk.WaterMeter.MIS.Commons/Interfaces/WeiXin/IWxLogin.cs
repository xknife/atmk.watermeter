﻿using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin
{
    public interface IWxLogin
    {
        /// <summary>
        /// 登录凭证校验：
        /// 通过微信小程序获得临时登录凭证，调用code2Session完成登录流程。
        /// </summary>
        /// <param name="appid">小程序id</param>
        /// <param name="secret">小程序appSecret</param>
        /// <param name="code">临时登录凭证</param>
        /// <returns></returns>
        Task<ResponCode2Session> LoginCode2Session(string appid, string secret, string code);
    }
}
