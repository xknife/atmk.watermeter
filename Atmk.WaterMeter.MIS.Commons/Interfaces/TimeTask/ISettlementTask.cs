﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask
{
    public interface ISettlementTask
    {
        void run();

        void Settlement(object state);
    }
}
