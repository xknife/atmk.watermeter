﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask
{
    public interface IWaterConsumptionTask
    {
        /// <summary>
        /// 执行水量统计任务
        /// </summary>
        void Start(string projcectId);
    }
}
