﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask
{
    public interface IOwnerTask
    {
        /// <summary>
        /// 执行与业主(户)有关的统计
        /// </summary>
        void Start(string projcectId);
    }
}
