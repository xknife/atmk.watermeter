﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask
{
    public interface IRegionTask
    {
        /// <summary>
        /// 执行一便片区统计任务
        /// </summary>
        void Start(string projcectId);
    }
}
