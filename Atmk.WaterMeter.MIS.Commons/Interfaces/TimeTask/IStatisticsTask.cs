﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask
{
    public interface IStatisticsTask
    {
        /// <summary>
        /// 运行统计任务 
        /// </summary>
        void Run(string projectId="");

        /// <summary>
        /// 停止任务运行
        /// </summary>
        void Stop();
    }
}
