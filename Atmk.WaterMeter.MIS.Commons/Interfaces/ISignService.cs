using System;
using System.Data;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces
{
    /// <summary>
    ///     用户签入签出服务
    /// </summary>
    public interface ISignService
    {
        /// <summary>
        ///     尝试签入。1.比对用户名与密码是否匹配；2.返回该用户的权限列表；3.返回该用户的Token
        /// </summary>
        /// <returns>用户名与密码是否匹配</returns>
        bool TrySign(string name, string staffId,string roleId,string areaId, out string token);

        string GetVerifyCodeToken(string code);
        bool CheckVerifyCodeToken(string code, string token);

        /// <summary>
        ///     退出登陆时清楚
        /// </summary>
        void ClearToken();

        /// <summary>
        /// 根据Token解密信息
        /// </summary>
        /// <returns></returns>
        //Token GetTokenByHttpContext();
    }

    /// <summary>
    ///     与权限判断等相关的接口定义
    /// </summary>
    public interface IPermissionService
    {
        #region 用户权限判断相关(需要实现对外调用)

        /// <summary>
        ///     01.用户是否在指定的角色里
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleName">角色名称</param>
        /// <returns>在角色里</returns>
        bool IsInRole(Guid staff, string roleName);

        /// <summary>
        ///     02.当前用户是否有相应的操作权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="permissionItemCode">操作权限编号</param>
        /// <returns>是否有权限</returns>
        bool IsAuthorized(Guid staff, string permissionItemCode);

        /// <summary>
        ///     03.某个用户是否有相应的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="permissionItemCode">操作权限编号</param>
        /// <returns>是否有权限</returns>
        bool IsAuthorizedByUser(Guid staff, string permissionItemCode);

        /// <summary>
        ///     04.某个角色是否有相应的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>是否有权限</returns>
        bool IsAuthorizedByRole(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     05.用户是否超级管理员
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        bool IsAdministrator(Guid staff);

        /// <summary>
        ///     07.获得当前用户的所有权限列表
        /// </summary>
        /// <param name="staff">用户</param>
        DataTable GetPermissionGroup(Guid staff);

        /// <summary>
        ///     09.当前用户是否对某个模块有相应的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="moduleCode">模块编号</param>
        /// <returns>是否有权限</returns>
        bool ModuleIsAuthorized(Guid staff, string moduleCode);

        /// <summary>
        ///     10.某个用户是否对某个模块有相应的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="moduleCode">模块编号</param>
        /// <returns>是否有权限</returns>
        bool ModuleIsAuthorizedByUser(Guid staff, string userId, string moduleCode);

        #endregion

        #region 用户权限范围判断相关(需要实现对外调用)

        /// <summary>
        ///     12.按某个数据集权限获取组织列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>数据表</returns>
        DataTable GetOrganizeDTByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     13.按某个数据集权限获取组织主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>主键数组</returns>
        string[] GetOrganizeIdsByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     14.按某个数据集权限获取角色列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>数据表</returns>
        DataTable GetRoleDTByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     15.按某个数据集权限获取角色主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>主键数组</returns>
        string[] GetRoleIdsByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     16.按某个权限域获取用户列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>数据表</returns>
        DataTable GetUserDTByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     17.按某个数据集权限获取用户主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemScopeCode">权限域编号</param>
        /// <returns>主键数组</returns>
        string[] GetUserIdsByPermission(Guid staff, string userId, string permissionItemScopeCode);

        /// <summary>
        ///     18.按某个权限域获取模块列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限域编号</param>
        /// <returns>数据表</returns>
        DataTable GetModuleDTByPermission(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     19.有授权权限的权限列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限域编号</param>
        /// <returns>数据表</returns>
        DataTable GetPermissionItemDTByPermission(Guid staff, string userId, string permissionItemCode);

        #endregion

        #region 角色权限关联关系相关

        /// <summary>
        ///     20.获取角色权限主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <returns>主键数组</returns>
        string[] GetRolePermissionIds(Guid staff, string roleId);

        /// <summary>
        ///     21.授予角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantPermissionIds">授予权限数组</param>
        /// <returns>影响的行数</returns>
        int GrantRolePermissions(Guid staff, string roleId, string[] grantPermissionIds);

        /// <summary>
        ///     22.授予角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantPermissionId">授予权限</param>
        /// <returns>影响的行数</returns>
        string GrantRolePermission(Guid staff, string roleId, string grantPermissionId);

        /// <summary>
        ///     23.授予角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantPermissionId">授予权限</param>
        /// <returns>影响的行数</returns>
        string GrantRolePermissionById(Guid staff, string roleId, string grantPermissionId);

        /// <summary>
        ///     24.撤消角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantPermissionIds">授予权限数组</param>
        /// <param name="revokePermissionIds">撤消权限数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRolePermissions(Guid staff, string roleId, string[] revokePermissionIds);

        /// <summary>
        ///     25.撤消角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="revokePermissionId">撤消权限数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRolePermission(Guid staff, string roleId, string revokePermissionId);

        /// <summary>
        ///     26.撤消角色的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="revokePermissionId">撤消权限数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRolePermissionById(Guid staff, string roleId, string revokePermissionId);

        /// <summary>
        ///     27.获取角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetRoleScopeUserIds(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     28.获取角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetRoleScopeRoleIds(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     29.获取角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetRoleScopeOrganizeIds(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     30.授予角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantUserIds">授予用户主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantRoleUserScopes(Guid staff, string roleId, string permissionItemCode, string[] grantUserIds);

        /// <summary>
        ///     31.撤消角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeUserIds">撤消的用户主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRoleUserScopes(Guid staff, string roleId, string permissionItemCode, string[] revokeUserIds);

        /// <summary>
        ///     32.授予角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantRoleIds">授予角色主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantRoleRoleScopes(Guid staff, string roleId, string permissionItemCode, string[] grantRoleIds);

        /// <summary>
        ///     33.撤消角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeRoleIds">撤消的角色主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRoleRoleScopes(Guid staff, string roleId, string permissionItemCode, string[] revokeRoleIds);

        /// <summary>
        ///     34.授予角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantOrganizeIds">授予组织主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantRoleOrganizeScopes(Guid staff, string roleId, string permissionItemCode, string[] grantOrganizeIds);

        /// <summary>
        ///     35.撤消角色的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeOrganizeIds">撤消的组织主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRoleOrganizeScopes(Guid staff, string roleId, string permissionItemCode, string[] revokeOrganizeIds);

        /// <summary>
        ///     36.获取角色授权权限列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">操作权限项编号</param>
        /// <returns>主键数组</returns>
        string[] GetRoleScopePermissionIds(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     37.授予角色的授权权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">操作权限编号</param>
        /// <param name="grantPermissionIds">授予的权限主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantRolePermissionItemScopes(Guid staff, string roleId, string permissionItemCode, string[] grantPermissionIds);

        /// <summary>
        ///     38.授予角色的授权权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">操作权限编号</param>
        /// <param name="revokePermissionIds">撤消的权限主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRolePermissionItemScopes(Guid staff, string roleId, string permissionItemCode, string[] revokePermissionIds);

        /// <summary>
        ///     39.清除权限
        /// </summary>
        /// <param name="staff">操作员</param>
        /// <param name="id">主键</param>
        /// <returns>数据表</returns>
        int ClearRolePermission(Guid staff, string id);

        int ClearRolePermissionScope(Guid staff, string id, string permissionItemCode);

        #endregion

        #region 用户权限关联关系相关

        /// <summary>
        ///     40.获取用户权限主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <returns>主键数组</returns>
        string[] GetUserPermissionIds(Guid staff, string userId);

        /// <summary>
        ///     41.授予用户的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="grantPermissionIds">授予权限数组</param>
        /// <returns>影响的行数</returns>
        int GrantUserPermissions(Guid staff, string userId, string[] grantPermissionIds);

        /// <summary>
        ///     42.授予用户的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="grantPermissionId">授予权限数组</param>
        /// <returns>影响的行数</returns>
        string GrantUserPermissionById(Guid staff, string userId, string grantPermissionId);

        /// <summary>
        ///     43.撤消用户的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="revokePermissionIds">撤消权限数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserPermission(Guid staff, string userId, string[] revokePermissionIds);

        /// <summary>
        ///     44.撤消用户的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="revokePermissionId">撤消权限</param>
        /// <returns>影响的行数</returns>
        int RevokeUserPermissionById(Guid staff, string userId, string revokePermissionId);

        /// <summary>
        ///     45.获取用户的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetUserScopeOrganizeIds(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     46.设置用户的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantOrganizeIds">授予的组织主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantUserOrganizeScopes(Guid staff, string userId, string permissionItemCode, string[] grantOrganizeIds);

        /// <summary>
        ///     47.设置用户的某个权限域的组织范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeOrganizeIds">撤消的组织主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserOrganizeScopes(Guid staff, string userId, string permissionItemCode, string[] revokeOrganizeIds);

        /// <summary>
        ///     48.获取用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetUserScopeUserIds(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     49.设置用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantUserIds">授予的用户主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantUserUserScopes(Guid staff, string userId, string permissionItemCode, string[] grantUserIds);

        /// <summary>
        ///     50.设置用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeUserIds">撤消的用户主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserUserScopes(Guid staff, string userId, string permissionItemCode, string[] revokeUserIds);

        /// <summary>
        ///     51.获取用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetUserScopeRoleIds(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     52.设置用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="grantRoleIds">授予的用户主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantUserRoleScopes(Guid staff, string userId, string permissionItemCode, string[] grantRoleIds);

        /// <summary>
        ///     53.设置用户的某个权限域的用户范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <param name="revokeRoleds">撤消的用户主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserRoleScopes(Guid staff, string userId, string permissionItemCode, string[] revokeRoleds);

        /// <summary>
        ///     54.获取用户授权权限列表
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>主键数组</returns>
        string[] GetUserScopePermissionIds(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     55.授予用户的授权权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="grantPermissionIds">授予的权限主键数组</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响的行数</returns>
        int GrantUserPermissionItemScopes(Guid staff, string userId, string permissionItemCode, string[] grantPermissionIds);

        /// <summary>
        ///     56.撤消用户的授权权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="revokePermissionIds">撤消的权限主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserPermissionItemScopes(Guid staff, string userId, string permissionItemCode, string[] revokePermissionIds);

        /// <summary>
        ///     57.清除权限
        /// </summary>
        /// <param name="staff">操作员</param>
        /// <param name="id">主键</param>
        /// <returns>数据表</returns>
        int ClearUserPermission(Guid staff, string id);

        /// <summary>
        ///     58.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="id"></param>
        /// <param name="permissionItemCode"></param>
        /// <returns></returns>
        int ClearUserPermissionScope(Guid staff, string id, string permissionItemCode);

        #endregion

        #region 用户关联模块关系相关

        /// <summary>
        ///     59.获得用户有权限的模块
        /// </summary>
        /// <param name="staff">用户</param>
        /// <returns>数据表</returns>
        DataTable GetModule(Guid staff);

        /// <summary>
        ///     60.获得用户有权限的模块
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <returns>数据表</returns>
        DataTable GetModuleByUser(Guid staff, string userId);

        /// <summary>
        ///     61.获取用户模块权限范围主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetUserScopeModuleIds(Guid staff, string userId, string permissionItemCode);

        /// <summary>
        ///     62.授予用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="grantModuleId">授予模块主键</param>
        /// <returns>影响的行数</returns>
        string GrantUserModuleScope(Guid staff, string userId, string permissionScopeItemCode, string grantModuleId);

        /// <summary>
        ///     63.授予用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="grantModuleIds">授予模块主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantUserModuleScopes(Guid staff, string userId, string permissionScopeItemCode, string[] grantModuleIds);

        /// <summary>
        ///     64.撤消用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="revokeModuleId">撤消模块主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserModuleScope(Guid staff, string userId, string permissionScopeItemCode, string revokeModuleId);

        /// <summary>
        ///     65.撤消用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="revokeModuleIds">撤消模块主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeUserModuleScopes(Guid staff, string userId, string permissionScopeItemCode, string[] revokeModuleIds);

        #endregion

        #region 角色模块关联关系相关

        /// <summary>
        ///     66.获取用户模块权限范围主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetRoleScopeModuleIds(Guid staff, string roleId, string permissionItemCode);

        /// <summary>
        ///     67.授予用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantModuleIds">授予模块主键数组</param>
        /// <returns>影响的行数</returns>
        int GrantRoleModuleScopes(Guid staff, string roleId, string permissionItemScopeCode, string[] grantModuleIds);

        /// <summary>
        ///     68.授予用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="grantModuleId">授予模块主键</param>
        /// <returns>影响的行数</returns>
        string GrantRoleModuleScope(Guid staff, string roleId, string permissionItemScopeCode, string grantModuleId);

        /// <summary>
        ///     69.撤消用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="revokeModuleIds">撤消模块主键数组</param>
        /// <returns>影响的行数</returns>
        int RevokeRoleModuleScopes(Guid staff, string roleId, string permissionItemScopeCode, string[] revokeModuleIds);

        /// <summary>
        ///     70.撤消用户模块的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="roleId">角色主键</param>
        /// <param name="revokeModuleId">撤消模块主键</param>
        /// <returns>影响的行数</returns>
        int RevokeRoleModuleScope(Guid staff, string roleId, string permissionItemScopeCode, string revokeModuleId);

        #endregion

        #region 资源权限设定关系相关

        /// <summary>
        ///     71.获取资源权限主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源</param>
        /// <returns>主键数组</returns>
        string[] GetResourcePermissionIds(Guid staff, string resourceCategory, string resourceId);

        /// <summary>
        ///     72.授予资源的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="grantPermissionIds">权限主键</param>
        /// <returns>影响的行数</returns>
        int GrantResourcePermission(Guid staff, string resourceCategory, string resourceId, string[] grantPermissionIds);

        /// <summary>
        ///     73.撤消资源的权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="revokePermissionIds">权限主键</param>
        /// <returns>影响的行数</returns>
        int RevokeResourcePermission(Guid staff, string resourceCategory, string resourceId, string[] revokePermissionIds);

        #endregion

        #region 资源权限范围设定关系相关

        /// <summary>
        ///     74.获取资源权限范围主键数组
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetPermissionScopeTargetIds(Guid staff, string resourceCategory, string resourceId, string targetCategory, string permissionItemCode);

        /// <summary>
        ///     75.获取数据集权限目标主键
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源类别</param>
        /// <param name="targetResourceId">资源主键</param>
        /// <param name="targetResourceCategory">目标资源</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetPermissionScopeResourceIds(Guid staff, string resourceCategory, string targetResourceId, string targetResourceCategory, string permissionItemCode);

        /// <summary>
        ///     76.授予资源的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="grantTargetIds">目标主键数组</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响的行数</returns>
        int GrantPermissionScopeTargets(Guid staff, string resourceCategory, string resourceId, string targetCategory, string[] grantTargetIds, string permissionItemCode);

        /// <summary>
        ///     77.授予数据集权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源类别</param>
        /// <param name="resourceIds">资源主键数组</param>
        /// <param name="targetCategory">目标资源类别</param>
        /// <param name="grantTargetId">目标资源主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响行数</returns>
        int GrantPermissionScopeTarget(Guid staff, string resourceCategory, string[] resourceIds, string targetCategory, string grantTargetId, string permissionItemCode);

        /// <summary>
        ///     78.撤消资源的权限范围
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源分类</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="revokeTargetIds">目标主键数组</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响的行数</returns>
        int RevokePermissionScopeTargets(Guid staff, string resourceCategory, string resourceId, string targetCategory, string[] revokeTargetIds, string permissionItemCode);

        /// <summary>
        ///     79.撤销数据集权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源类别</param>
        /// <param name="resourceIds">资源主键数组</param>
        /// <param name="targetCategory">目标分类</param>
        /// <param name="revokeTargetId">目标主键</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响行数</returns>
        int RevokePermissionScopeTarget(Guid staff, string resourceCategory, string[] resourceIds, string targetCategory, string revokeTargetId, string permissionItemCode);

        /// <summary>
        ///     80.清除数据集权限
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="resourceCategory">资源类别</param>
        /// <param name="resourceId">资源主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="permissionItemCode">权限主键</param>
        /// <returns>影响行数</returns>
        int ClearPermissionScopeTarget(Guid staff, string resourceCategory, string resourceId, string targetCategory, string permissionItemCode);

        /// <summary>
        ///     81.获取用户的某个资源的权限范围(列表资源)
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <returns>主键数组</returns>
        string[] GetResourceScopeIds(Guid staff, string userId, string targetCategory, string permissionItemCode);

        /// <summary>
        ///     82.获取用户的某个资源的权限范围(树型资源)
        /// </summary>
        /// <param name="staff">用户</param>
        /// <param name="userId">用户主键</param>
        /// <param name="targetCategory">目标类别</param>
        /// <param name="permissionItemCode">权限编号</param>
        /// <param name="childrens">是否含子节点</param>
        /// <returns>主键数组</returns>
        string[] GetTreeResourceScopeIds(Guid staff, string userId, string targetCategory, string permissionItemCode, bool childrens);

        #endregion
    }
}