﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces
{
    /// <summary>
    /// 数据库操作窗口
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// 添加一条对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Add(IRecord entity);
        int Add<T>(T entities);
        /// <summary>
        /// 添加一组对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int AddRang(params IRecord[] entities);
        int AddRang<T>(T entities);
        /// <summary>
        /// 添加一组对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int AddRang2(IEnumerable<IRecord> entities);
        /// <summary>
        /// 删除(设置已删除标记)
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Delete(params IRecord[] entities);

        /// <summary>
        /// 彻底删除
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Remove(params IRecord[] entities);
        int Remove<T>(T entities);
        /// <summary>
        /// 修改对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Update(params IRecord[] entities);
        int Update<T>(T entities);
        /// <summary>
        /// 查询所有
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> FindAll<T>() where T : class;

        /// <summary>
        /// 仅查询，不进行跟踪
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> FindAllAsNoTracking<T>() where T : class;

        /// <summary>
        /// 根据编码和名称查询
        /// </summary>
        IRecord FindById(string id);

        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        T FindById<T>(string id) where T : BaseRecord;


        /// <summary>
        /// 参数查询
        /// </summary>
        /// <param name="sortOrder">排序字段</param>
        /// <param name="currentFilter"></param>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        Task<PaginatedList<IRecord>> FindIndex<T>(string sortOrder,
            string currentFilter,
            string searchString,
            int? page) where T : class;

    }
}