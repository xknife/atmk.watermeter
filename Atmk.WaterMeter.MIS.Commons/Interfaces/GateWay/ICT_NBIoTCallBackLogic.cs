﻿using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack;
using System;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay
{
    /// <summary>
    /// 处理回调参数的逻辑问题
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ICT_NBIoTCallBackLogic
    {
        /// <summary>
        /// 平台数据上传回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        //void MeterDatasLogic(DeviceDataPost value, string meterCode, System.DateTime eventTime);

        /// <summary>
        /// 指令回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        void CommandStatusLogic(CommandRespond value);
    }
}
