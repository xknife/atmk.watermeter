﻿using System;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay
{
    /// <summary>
    /// 对电信NB平台的设备进行管理，包括注册设备、修改设备、删除设备
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ICT_NBIoT_DevicesManage
    {
        /// <summary>
        /// 注册设备
        /// </summary>
        /// <param name="nodeId">IMEI</param>
        /// <returns></returns>
        Guid RegisterDevice(string nodeId);

        /// <summary>
        /// 修改设备信息
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <param name="name">设备名称，默认填入水表号</param>
        /// <param name="model">设备型号，填入在电信平台设置好profile的设备型号</param>
        /// <returns></returns>
        bool UpdateDeviceInfo(string deviceId, string name,string model);

        /// <summary>
        /// 删除设备
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns></returns>
        bool DeleteDevice(string deviceId);

        /// <summary>
        /// 判断设备状态
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns>
        /// 200 设备处于激活状态
        /// 201 设备处于未激活状态
        /// 404 设备不存在
        /// </returns>
        int DeviceStatus(string deviceId);

        /// <summary>
        /// 获取所有设备信息
        /// </summary>
        /// <returns></returns>
        ResDevices GetDevices();
    }
}
