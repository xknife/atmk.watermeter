﻿using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.GateWay;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay
{
    // ReSharper disable once InconsistentNaming
    public interface ICT_NBIoT_CommandSenderHelper
    {
        /// <summary>
        ///     获取抄表数据--数据采集--全部历史数据
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        List<MeterReadEntity> ReadDataHistory(string deviceId);

        /// <summary>
        ///     发送开关阀命令
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="tap"></param>
        /// <returns>返回结果类</returns>
        DeviceCommandResp SendTapCommand(string deviceId, int tap);
        /// <summary>
        ///    批量 发送开关阀命令
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="tap"></param>
        /// <returns>返回结果类</returns>
        ApiResult batchTapCommand(string[] deviceIds);
        /// <summary>
        ///     读取指令发送状态
        /// </summary>
        /// <param name="deviceid"></param>
        /// <returns></returns>
        List<CommandResultEntity> ReadCommandStatue(string deviceid = "");

        /// <summary>
        ///  获取冻结数据--数据采集--电信平台全部历史数据
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        List<MeterFreezDataEntity> ReadFreezDatasHistory(string deviceId);
        /// <summary>
        /// 发送获取冻结数据指令
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="frozenType"></param>
        /// <returns></returns>
        DeviceCommandResp SendFreezingCommand(string deviceId, FrozenType frozenType);
    }
}
