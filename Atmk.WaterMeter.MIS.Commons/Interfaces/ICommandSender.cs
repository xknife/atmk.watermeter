﻿using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces
{
    /// <summary>
    /// 水表指令发送
    /// </summary>
    public interface ICommandSender
    {
        /// <summary>
        /// 下发水表指令
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandType"></param>
        CommandStatus Send(ref MeterCommand command, CommandType commandType);
    }
}
