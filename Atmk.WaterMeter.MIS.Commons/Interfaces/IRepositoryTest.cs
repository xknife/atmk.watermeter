﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Commons.Interfaces
{
    /// <summary>
    ///     数据库操作窗口
    /// </summary>
    public interface IRepositoryTest
    {
        /// <summary>
        ///     添加一条对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Add<T>(T entity) where T : IRecord;

        /// <summary>
        ///     添加一组对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int AddRang<T>(params T[] entities) where T : IRecord;

        /// <summary>
        ///     删除(设置已删除标记)
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Delete<T>(params T[] entities) where T : IRecord;

        /// <summary>
        ///     彻底删除
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Remove<T>(params T[] entities) where T : IRecord;

        /// <summary>
        ///     修改对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int Update<T>(params T[] entities) where T : IRecord;

        /// <summary>
        ///     查询所有
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> FindAll<T>() where T : class;


        /// <summary>
        ///     参数查询
        /// </summary>
        /// <param name="sortOrder">排序字段</param>
        /// <param name="currentFilter"></param>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        Task<PaginatedList<IRecord>> FindIndex<T>(string sortOrder,
            string currentFilter,
            string searchString,
            int? page) where T : IRecord;
    }
}