﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons
{
    /***
    // ReSharper disable once InconsistentNaming
    public static class DI
    {
        private static readonly IReadOnlyKernel _ReadOnlyKernel;

        static DI()
        {
            KernelConfiguration configuration = new KernelConfiguration(GetModules());
            configuration.Settings.Set(nameof(InjectAttribute), typeof(DependencyAttribute));

            _ReadOnlyKernel = configuration.BuildReadonlyKernel();
        }

        private static INinjectModule[] GetModules()
        {
            Assembly assembly = Assembly.Load(new AssemblyName(typeof(DI).Namespace));
            INinjectModule[] modules = assembly.DefinedTypes
                .Where(a => a.GetInterface(nameof(INinjectModule)) != null)
                .Select(a => (INinjectModule)assembly.CreateInstance(a.FullName)).ToArray();

            return modules;
        }

        public static T Resolve<T>()
        {
            return _ReadOnlyKernel.Get<T>();
        }
    }
    **/
}
