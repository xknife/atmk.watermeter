﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Atmk.WaterMeter.MIS.Commons
{
    public class BooleanResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        /// <summary>
        /// 设备响应的JObject对象
        /// </summary>
        public JObject ResJObject { get; set; }
    }
}
