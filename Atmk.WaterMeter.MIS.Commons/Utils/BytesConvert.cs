﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore.Internal;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class BytesConvert
    {
        /// <summary>
        /// 将字节数组按指定长度转换为数字
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="len">长度不超过8</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">参数异常</exception>
        public static List<ulong> BitConvertInts(byte[] buffer, int len)
        {
            if (buffer.Length % len > 0)
                throw new ArgumentException($"数组长度错误，数组长度{buffer.Length},指定长度{len},无法整除");
            if (len > 8)
                throw new ArgumentException($"指定长度{len},超过最大转换长度8");
            var zeroBytes = new byte[8 - len];
            return Simple.Chunk(buffer, len).ToList()
                .Select(a => BitConverter.ToUInt64(Merged(a.ToArray(), zeroBytes).ToArray(), 0))
                .ToList();
        }

        public static IEnumerable<TValue> Merged<TValue>(IEnumerable<TValue> value1, IEnumerable<TValue> value2)
        {
            var result = new List<TValue>();
            result.AddRange(value1);
            result.AddRange(value2);
            return result;
        }

        /// <summary>
        /// 根据字节面上数字转为int 例如0x0310  转为数字310
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static double BitConvertByteForStringInt(byte[] buffer)
        {
            var arrayInts = BitConverter.ToString(buffer).Replace("-", "").Reverse()
                .Select(s => Convert.ToInt32(s.ToString())).ToArray();
            return arrayInts.Sum(s => Math.Pow(10, arrayInts.IndexOf(s)) * s);
        }
        /// <summary>
        /// 将HEX数字字符串转为HEX的字节数组
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] BitConvertHexStringForByte(string s)
        {
            if(s.Any( c=> (c < '0' || c > '9')&&(c<'a'||c>'z')))
                throw new ArgumentException($"字符串包含非HEX的字符");
            if (s.Length % 2 != 0)
                s = $"0{s}";
            var hexString = new List<string>();
            for (var i = 0; i < s.Length/2; i++)
            {
                hexString.Add(s.Substring(i*2,2));
            }
            var result = new List<byte>();
            hexString.ForEach(
                h=>
                result.Add(byte.Parse(h, System.Globalization.NumberStyles.HexNumber))
                );
            return result.ToArray();
        }
        /// <summary>
        /// 将数字字符串转为字节数组
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] BitConvertIntStringForByte(string s)
        {
            if (s.Any(c => c < '0' || c > '9'))
                throw new ArgumentException($"字符串包含非整数的字符");
            if (s.Length % 2 != 0)
                s = $"0{s}";
            var intString = new List<string>();
            for (var i = 0; i < s.Length / 2; i++)
            {
                intString.Add(s.Substring(i * 2, 2));
            }
           return intString.Select(i => byte.Parse(i, NumberStyles.Number)).ToArray();
        }
    }
}