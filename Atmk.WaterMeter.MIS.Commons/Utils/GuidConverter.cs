﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class GuidConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsAssignableFrom(typeof(Guid));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {
                return serializer.Deserialize<Guid>(reader);
            }
            catch
            {
                //如果传进来的值造成异常，则赋值一个初值  
                return Guid.Empty;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
