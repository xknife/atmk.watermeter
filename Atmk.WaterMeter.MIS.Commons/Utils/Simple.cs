﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class Simple
    {
        public static int IntConvertString(string value, int def = 0)
        {
            return int.TryParse(value, out var result) ? result : def;
        }

        public static double DoubleConvertString(string value)
        {
            double.TryParse(value, out var result);
            return result;
        }

        public static decimal DecimalConvertString(string value)
        {
            decimal.TryParse(value, out var result);
            return result;
        }

        /// <summary>
        /// 电信平台时间字符串转换
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime CtStringConvertDateTime(string value)
        {
            DateTime dt;
            var replace = value.Replace("Z", ""); //如果添加字母Z，时间转换就会得到错误结果
            dt = DateTime.ParseExact(replace, "yyyyMMddTHHmmss", CultureInfo.CurrentCulture);
            return dt;
        }
        /// <summary>
        /// 电信平台时间字符串转换
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime CtStringConvertDateTime2(string value)
        {
            DateTime dt;
            dt = DateTime.ParseExact(value, "yyyyMMddTHHmmssZ", CultureInfo.CurrentCulture);
            return dt;
        }
        /// <summary>
        /// 将数组按指定长度分组
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="values"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<TValue>> Chunk<TValue>(
            IEnumerable<TValue> values, int chunkSize)
        {
            return values
                .Select((v, i) => new {v, groupIndex = i / chunkSize})
                .GroupBy(x => x.groupIndex)
                .Select(g => g.Select(x => x.v));
        }

        /// <summary>
        /// 使用RNGCryptoServiceProvider生成种子
        /// </summary>
        /// <returns></returns>
        public static int GetRandomSeed()
        {
            var bytes = new byte[4];
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            rng.GetBytes(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// 生成随机密码
        /// </summary>
        /// <param name="pwdLength">密码长度</param>
        /// <returns></returns>
        public static string MakePassword(int pwdLength)
        {
            //声明要返回的字符串    
            var tmpstr = "";
            //密码中包含的字符数组    
            const string pwdchars = "abcdefghjkmnprstwxy123456789@#$%&*";
            //随机数生成器    
            for (var i = 0; i < pwdLength; i++)
            {
                //Random类的Next方法生成一个指定范围的随机数     
                var iRandNum = new Random(GetRandomSeed()).Next(pwdchars.Length - 1);
                //tmpstr随机添加一个字符     
                tmpstr += pwdchars[iRandNum];
            }
            return tmpstr;
        }

        /// <summary>
        /// 获取本地ip,获取失败则为空字符串
        /// </summary>
        /// <returns></returns>
        public static string GetLocalIP()
        {
            try
            {
                var HostName = Dns.GetHostName(); //得到主机名
                var IpEntry = Dns.GetHostEntry(HostName);
                foreach (var t in IpEntry.AddressList)
                {
                    //从IP地址列表中筛选出IPv4类型的IP地址
                    //AddressFamily.InterNetwork表示此IP为IPv4,
                    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                    if (t.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return t.ToString();
                    }
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 获取随机正常电压值
        /// </summary>
        /// <returns></returns>
        public static string RundVoltage()
        {
            try
            {
                var value = new Random(GetRandomSeed()).Next(360, 365);
                return (value * 0.01).ToString(CultureInfo.CurrentCulture);
            }
            catch
            {
                return "3.62";
            }
        }
    }
}