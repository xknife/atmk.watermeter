﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class GenericsJsonConvert
    {
        public static List<T> JsonConvertList<T>(string json)
        {
            return JsonConvert.DeserializeObject<List<T>>(json);
        }
        public static T JsonConvertSingle<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        public static T[] JsonConvertArray<T>(string json)
        {
            return JsonConvert.DeserializeObject<T[]>(json);
        }
        /// <summary>
        /// 将json反序列化后获取参数值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="objectJson"></param>
        /// <returns></returns>
        public static string ObjectJsonParament(string key, string objectJson)
        {
            if (objectJson.Trim().Length == 0)
                return "";
            var obj = (JObject)JsonConvert.DeserializeObject(objectJson);
            if (obj == null)
                throw new NullReferenceException("反序列化失败,获取Jobject为空");
            return obj[key].ToString();
        }
        /// <summary>
        /// json反序列化后添加或修改对应参数
        /// </summary>
        /// <param name="kvDictionary"></param>
        /// <param name="objectJson"></param>
        /// <returns></returns>
        public static JObject ObjectJsonUpdateParam(Dictionary<string,string> kvDictionary, string objectJson)
        {
            if (objectJson.Trim().Length == 0)
                return CreatJObjectForParam(kvDictionary);
            var obj = (JObject)JsonConvert.DeserializeObject(objectJson);
            if (obj == null)
                throw new NullReferenceException("反序列化失败,获取Jobject为空");
            foreach (var item in kvDictionary)
            {
                if (obj.ContainsKey(item.Key))
                    obj[item.Key] = item.Value;
                else
                {
                    obj.Add(item.Key, item.Value);
                }
            }
            return obj;
        }

        public static JObject CreatJObjectForParam(Dictionary<string, string> kvDictionary)
        {
            var jAdd = new JObject();
            foreach (var item in kvDictionary)
            {
                jAdd.Add(item.Key, item.Value);
            }
            return jAdd;
        }

        /// <summary>
        /// json反序列化后添加或修改对应参数
        /// </summary>
        /// <param name="kvDictionary"></param>
        /// <param name="objectJson"></param>
        /// <returns></returns>
        public static string StringJsonUpdateParam(Dictionary<string, string> kvDictionary, string objectJson)
        {
            var jobj = ObjectJsonUpdateParam(kvDictionary, objectJson);
            return jobj == null ? "" : JsonConvert.SerializeObject(jobj);
        }

        /// <summary>
        /// 将C#数据实体转化为JSON数据
        /// </summary>
        /// <param name="obj">要转化的数据实体</param>
        /// <returns>JSON格式字符串</returns>
        public static string JsonSerialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream stream = new MemoryStream();
            serializer.WriteObject(stream, obj);
            stream.Position = 0;

            StreamReader sr = new StreamReader(stream);
            string resultStr = sr.ReadToEnd();
            sr.Close();
            stream.Close();

            return resultStr;
        }

    }
}
