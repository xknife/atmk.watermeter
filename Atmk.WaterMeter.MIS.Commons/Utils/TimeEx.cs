﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public static class TimeEx
    {
        /// <summary>
        /// 1970年1月1日刻度
        /// </summary>
        public static long Ticks1970 { get; }
        public static DateTime DateTime1970 { get; }

        static TimeEx()
        {
            DateTime1970 = new DateTime(1970, 1, 1, 0, 0, 0);
            Ticks1970 = DateTime1970.Ticks; //1970年1月1日刻度
        }
        /// <summary>
        /// 获取标准时间戳：从1970年1月1日00:00:00至今的秒数,即当前的时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int TimeStamp(this DateTime dateTime)
        {
            var span = dateTime - DateTime1970;
            return (int)span.TotalSeconds;
        }
        /// <summary>
        /// 获取base格式的时间字节字符串，原时间字节结构  byte: 0xyy,0xMM,0xdd,0xHH,0xmm,0xss
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string TimeBase64ToString(this DateTime dateTime)
        {
            var dtString = dateTime.ToString("yyMMddHHmmss");
            var dtBytes = BytesConvert.BitConvertIntStringForByte(dtString);
            return Convert.ToBase64String(dtBytes);
        }
    }
}
