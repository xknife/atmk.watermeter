﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class MD5Encrypt
    {
        public static string Encrypt(string text)
        {
                var result = Encoding.UTF8.GetBytes(text.Trim());    //tbPass为输入密码的文本框
                MD5 md5 = new MD5CryptoServiceProvider();
                var output = md5.ComputeHash(result);
                return BitConverter.ToString(output).Replace("-", "");  //tbMd5pass为输出加密文本的文本框          
        }
    }
}
