﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Commons.Utils
{
    public class Base64Convert
    {
        /// <summary>
        /// Base64字符串转换字节
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] ConvertBytes(string value)
        {
            return Convert.FromBase64String(value);
        }
        /// <summary>
        /// Base64字符串转换字节字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertHexString(string value)
        {
            var bytes = ConvertBytes(value);
            return BitConverter.ToString(bytes).Replace("-", "");
        }
        /// <summary>
        /// Base64字符串转换Int字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertIntString(string value)
        {
            var array = ConvertIntArrayStrings(value);
            return string.Join("", array);
        }
        /// <summary>
        /// Base64字符串转换Int字符串数组
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string[] ConvertIntArrayStrings(string value)
        {
            var bytes = ConvertBytes(value).Select(b=>b.ToString().PadLeft(2,'0')).ToArray();
            return bytes;
        }

        /// <summary>
        ///  Base64字符串转换字节字符串数组
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string[] ConvertHexArrayStrings(string value)
        {
            var bytes = ConvertBytes(value);
            return BitConverter.ToString(bytes).Split('-');
        }
        /// <summary>
        /// 电信平台 base64冻结数据转换
        /// </summary>
        /// <param name="base64String"></param>
        /// <param name="ftype"></param>
        /// <returns></returns>
        public static List<double> FrozensDatasBase64ConvertDouble(string base64String, FrozenType ftype)
        {
            var sourceBytes = ConvertBytes(base64String);
            var len = 24*6;
            if (ftype == FrozenType.Day)
                len = 30*6;
            var hourFrozensBytes = new byte[len];
            Array.Copy(sourceBytes, 0, hourFrozensBytes, 0, len);
            var hourFrozens = new List<byte[]>();
            for (var i = 0; i < hourFrozensBytes.Length / 6; i++)
            {
                var temp = new byte[6];
                Array.Copy(hourFrozensBytes, i * 6, temp, 0, 6);
                hourFrozens.Add(temp);
            }
            var historys = hourFrozens.Select(h =>
                    (BytesConvert.BitConvertInts(h, 6).First() / 100.00))
                .ToList();
            return historys;
        }
    }
}
