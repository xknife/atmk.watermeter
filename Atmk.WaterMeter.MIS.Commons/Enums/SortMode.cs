﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Enums
{
    public enum SortMode
    {
        None, ASC, DESC
    }
}
