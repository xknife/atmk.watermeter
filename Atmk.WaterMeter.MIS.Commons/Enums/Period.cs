﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.Enums
{
    /// <summary>
    /// 声明日期期间类型枚举
    /// </summary>
    public enum Period { Day, Week, Month, Quarter, Year };
}
