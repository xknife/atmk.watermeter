﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons
{
    /// <summary>
    /// 微信小服务的token
    /// </summary>
    public class WxSrToken
    {
        /// <summary>
        /// 微信api返回的小程序用户id
        /// </summary>
        public string OpenId { get; set; }
        /// <summary>
        /// 会话密钥 用于验证小程序用户的，不可以直接发给小程序
        /// </summary>
        public string SessionKey { get; set; }
    }
}
