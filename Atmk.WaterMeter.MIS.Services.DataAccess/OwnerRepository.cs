﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Microsoft.EntityFrameworkCore;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class OwnerRepository : BaseRepository, IOwnerRepository
    {
        /// <summary>
        /// 获取业主信息集合
        /// </summary>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        public List<Owner> GetOwners(params string[] leafDistrictIds)
        {
            return Context.Owner.Where(o => leafDistrictIds.Contains(o.DistrictId)).ToList();
        }

        /// <summary>
        /// 根据水表号获取业主信息集合
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <returns></returns>
        public List<Owner> GetOwnersByMeterNo(params string[] meterNumbers)
        {
            var ownerids = Context.Meter
                .Where(m => m.RecordState == (int)RecordStateEnum.Normal && meterNumbers.Contains(m.MeterNumber))
                .Select(m => m.OwnerId)
                .ToList();
            return Context.Owner.Where(o => ownerids.Contains(o.Id)).ToList();
        }


    }
}