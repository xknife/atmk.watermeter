﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Models;
using NLog;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.WeiXin
{
   public class WxRechargeWrite : IWxRechargeRepositoryWrite
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 添加表单
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        public bool AddOrder(WxSrOrderRecords orderRecord)
        {
            using (var c = ContextBuilder.Build())
            {
                if (c.WxSrOrderRecords.FirstOrDefault(
                    w => w.OpenId == orderRecord.OpenId 
                    && w.OutTradeNo == orderRecord.OutTradeNo
                    &&w.Number == orderRecord.Number)!=null)
                {
                    _logger.Warn("已有该表单，添加失败");
                    return false;
                }
                c.WxSrOrderRecords.Add(orderRecord);
                return c.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 修改表单
        /// </summary>
        /// <param name="orderRecord"></param>
        /// <returns></returns>
        public bool UpdataOrder(WxSrOrderRecords orderRecord)
        {
            using (var c = ContextBuilder.Build())
            {
                if (c.WxSrOrderRecords.Any(
                    w => w.Id == orderRecord.Id))
                {
                    c.WxSrOrderRecords.Update(orderRecord);
                    return c.SaveChanges() > 0;
                }
                _logger.Warn("没有找到该表单，修改失败");
                return false;
            
            }
        }
    }
}
