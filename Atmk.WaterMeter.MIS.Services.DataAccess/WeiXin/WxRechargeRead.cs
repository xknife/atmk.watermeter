﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using NLog;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.WeiXin
{
   
    public class WxRechargeRead : IWxRechargeRepositoryRead
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 获取指定表单
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="outTradeNo"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        /// <exception cref="SqlNullValueException">查询空数据错误</exception>
        public WxSrOrderRecords SelectOrder(string openId,string outTradeNo)
        {
            using (var c = ContextBuilder.Build())
            {
                var array = c.WxSrOrderRecords.Where(
                    w => w.OpenId == openId
                         && w.OutTradeNo == outTradeNo
                         ).ToList();
                if (array.Count>0)
                {
                   return array.First();
                }
                throw new SqlNullValueException("找不到指定表单信息");
            }
        }
        /// <summary>
        /// 获取用户充值表单记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        public List<WxSrOrderRecords> SelectOrders(string openId, string meterNumber)
        {
            using (var c = ContextBuilder.Build())
            {
                var array = c.WxSrOrderRecords.Where(
                    w => w.OpenId == openId
                         && w.Number == meterNumber
                         &&w.RecordState ==(int)RecordStateEnum.Normal
                ).ToList();
                return array;
            }
        }
    }
}
