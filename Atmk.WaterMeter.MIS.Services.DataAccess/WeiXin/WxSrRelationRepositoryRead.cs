﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using NLog;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.WeiXin
{
    public class WxSrRelationRepositoryRead : IWxSrRelationRepositoryRead
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 获取该用户openId下的所有关系对象
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public List<WxSrRelations> SelectArrayRelations(string openId)
        {
            using (var c = ContextBuilder.Build())
            {
                return c.WxSrRelations.Where(w => w.OpenId == openId && w.RecordState == (int)RecordStateEnum.Normal)
                    .ToList();
            }
        }
    }
}