﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using NLog;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.WeiXin
{
    /// <summary>
    /// 添加和注销绑定信息
    /// </summary>
    public class WxSrRelationRepositoryWrite : IWxSrRelationRepositoryWrite
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 添加小程序与水表关系 若记录有，则修改记录状态
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="number"></param>
        /// <param name="ownerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public bool AddWxSrRelation(string openId, string number, string ownerId, string accountId)
        {
            using (var c = ContextBuilder.Build())
            {
                var wxList = c.WxSrRelations.Where(w => w.OpenId == openId && w.Number == number);
                if (wxList.Any())
                {
                    if (wxList.Any(w => w.RecordState == (int)RecordStateEnum.Normal))
                    {
                        _logger.Warn("已有小程序与水表关系");
                        return false;
                    }
                    var wx = wxList.First();
                    wx.RecordState = (int)RecordStateEnum.Normal;
                    c.WxSrRelations.Update(wx);
                    return c.SaveChanges() > 0;
                }
                var newWx = new WxSrRelations
                {
                    OpenId = openId,
                    AccountId = accountId,
                    OwnerId = ownerId,
                    Number = number
                };
                c.WxSrRelations.Add(newWx);
                return c.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 添加小程序与水表关系 若记录有，则修改记录状态
        /// </summary>
        /// <param name="relation"></param>
        /// <returns></returns>
        public bool AddWxSrRelation(WxSrRelations relation)
        {
            using (var c = ContextBuilder.Build())
            {
                var wxList = c.WxSrRelations.Where(w => w.OpenId == relation.OpenId && w.Number == relation.Number);
                if (wxList.FirstOrDefault() != null)
                {
                    if (wxList.FirstOrDefault(w => w.RecordState == (int)RecordStateEnum.Normal) != null)
                    {
                        _logger.Warn("已有小程序与水表关系");
                        return false;
                    }
                    var wx = wxList.First();
                    wx.RecordState = (int)RecordStateEnum.Normal;
                    c.WxSrRelations.Update(wx);
                    return c.SaveChanges() > 0;
                }
                c.WxSrRelations.Add(relation);
                return c.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 注销小程序与水表关系的记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        public bool DeleteWxSrRelation(string openId, string meterNumber)
        {
            using (var _context = ContextBuilder.Build())
            {
                var wxList = _context.WxSrRelations.Where(m => m.OpenId == openId && m.Number == meterNumber&& m.RecordState == (int)RecordStateEnum.Normal).FirstOrDefault();
                if (wxList == null)
                    return false;
                wxList.RecordState = (int)RecordStateEnum.Deleted;
                _context.WxSrRelations.Update(wxList);
                return _context.SaveChanges() > 0;
            }
        }
    }
}
