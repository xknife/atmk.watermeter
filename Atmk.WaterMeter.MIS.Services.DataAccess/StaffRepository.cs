﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class StaffRepository : BaseRepository, IStaffRepository
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly Context _Context;
       
        public StaffRepository()
        {
            _Context = ContextBuilder.Build();
        }
        /// <summary>
        /// 查询操作员信息
        /// </summary>
        /// <param name="sortOrder"></param>
        /// <param name="currentFilter"></param>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        //public override async Task<PaginatedList<User>> FindIndex<User>(string sortOrder,
        //    string currentFilter,
        //    string searchString,
        //    int? page)
        //{
        //    var staff = _Context.User.Where(s => s.RecordState == (int)RecordStateEnum.Normal);

        //        switch (currentFilter)
        //        {
        //            case "Name":
        //                staff = staff.Where(s => s.Name.Contains(searchString));
        //                break;
        //            case "Surname":
        //                staff = staff.Where(s => s.Surname.Contains(searchString));
        //                break;
        //            case "RoleId":
        //                staff = staff.Where(s => s.RoleId.Contains(searchString));
        //                break;
        //            case "DistrictId":
        //                staff = staff.Where(s => s.DistrictId.Contains(searchString));
        //                break;
        //            default:
        //                break;
        //        }
        //    if (!string.IsNullOrEmpty(currentFilter))
        //    {
        //        switch (sortOrder)
        //        {
        //            case "name_desc":
        //                staff = staff.OrderByDescending(s => s.Name);
        //                break;

        //            case "Name":
        //                staff = staff.OrderBy(s => s.Name);
        //                break;
        //            case "Surname_desc":
        //                staff = staff.OrderByDescending(s => s.Surname.Length).ThenBy(s => s.Surname);
        //                break;
        //            default:
        //                staff = staff.OrderBy(s => s.Surname.Length).ThenBy(s => s.Surname);
        //                break;
        //        }
        //    }
        //    //默认值，后续需要添加配置表
        //    var pageSize = page ?? staff.Count();
        //    return await PaginatedList<User>.CreateAsync(staff, page ?? 1, pageSize);
        //}
        /// <summary>
        /// 根据登陆名查找操作员集合
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<User> FindByName(string name)
        {
            var results = Context.User.Where(x => x.Name == name).ToList();
            return results;
        }
        /// <summary>
        /// 新版登录
        /// 马贤辉 2019-08-29
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public User FindByName(string name, string password)
        {
            var results = Context.User.FirstOrDefault(x => x.Name == name && x.Password == password);
            return results;
        }
        /// <summary>
        /// 修改用户片区编码
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="districtId"></param>
        /// <param name="insert"></param>
        /// <returns></returns>
        public int UpdateUserDistrict(string uid, string districtId,bool insert)
        {
            try
            {
                var staffs = _Context.User.Where(u => u.Id.ToString() == uid).ToList();
                if (staffs.Count == 0)
                {
                    _logger.Warn("没有用户信息");
                    return 0;
                }
                var staff = staffs.First();
                var arrayDis = staff.DistrictId?.Split(',').ToList() ?? new List<string>();
                arrayDis.Remove("");
                if (!insert)
                    arrayDis.Remove(districtId);
                else
                    arrayDis.Add(districtId);
                staff.DistrictId = string.Join(",", arrayDis);
                using (var _context = ContextBuilder.Build())
                {
                    _context.Update(staff);
                    return _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
               _logger.Error(e);
                throw;
            }
        }
        /// <summary>
        /// 获取可以控制该片区的用户
        /// </summary>
        /// <param name="dstrictId"></param>
        /// <returns></returns>
        public List<User> SelectUserByDistrict(string dstrictId)
        {
            var staffs = _Context.User.Where(u => u.DistrictId.Contains(dstrictId)).ToList();
            var result = new List<User>();
            staffs.ForEach(s =>
            {
                if(s.DistrictId.Split(',').Contains(dstrictId))
                    result.Add(s);
            });
            return result;
        }

    }

}
