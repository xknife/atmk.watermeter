﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class MapRepository : BaseRepository, IMapRepository
    {
        private readonly Context _Context;

        public MapRepository()
        {
            _Context = ContextBuilder.Build();
        }

        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        public MapParameter FirstCoordinatePoint(string nodeid, CoordinateType coordinateType)
        {
            var mapParameterEntity =
                _Context.MapParameter.First(m =>
                    m.NodeId == nodeid && m.NodeType == (int)coordinateType && m.RecordState == (int)RecordStateEnum.Normal);
            return mapParameterEntity;
        }

        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        public MapParameter FirstCoordinatePoint(string nodeid)
        {
            var mapParameterEntity =
                _Context.MapParameter.First(m => m.NodeId == nodeid && m.RecordState == (int)RecordStateEnum.Normal);
            return mapParameterEntity;
        }

        /// <summary>
        /// 获取坐标点集合
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        public List<MapParameter> CoordinatesPoints(string nodeid, CoordinateType coordinateType)
        {
            var mapParameterEntity =
                _Context.MapParameter.Where(m =>
                        m.NodeId == nodeid && m.NodeType == (int)coordinateType && m.RecordState == (int)RecordStateEnum.Normal)
                    .ToList();
            return mapParameterEntity;
        }

        /// <summary>
        /// 获取坐标点
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="elevation"></param>
        public void CoordinatePoint(string nodeid, CoordinateType coordinateType, out double longitude,
            out double latitude, out double elevation)
        {
            try
            {
                var mapParameterEntity =
                    _Context.MapParameter.First(m =>
                        m.NodeId == nodeid && m.NodeType == (int)coordinateType && m.RecordState == (int)RecordStateEnum.Normal);
                longitude = mapParameterEntity.Longitude;
                latitude = mapParameterEntity.Latitude;
                elevation = mapParameterEntity.Elevation;
            }
            catch (Exception e)
            {
                longitude = 0;
                latitude = 0;
                elevation = 0;
            }
        }

        /// <summary>
        /// 获取坐标点
        /// </summary>马贤辉 增加高效防死锁 查询
        /// <param name="nodeid"></param>
        /// <param name="coordinateType"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="elevation"></param>
        public List<MapParameter> CoordinatePoint_mxh(CoordinateType coordinateType)
        {
            var mapParameterEntity =
                _Context.MapParameter.Where(m => m.NodeType == (int)coordinateType && m.RecordState == (int)RecordStateEnum.Normal).ToList();

            return mapParameterEntity;
        }

        /// <summary>
        /// 编辑节点
        /// </summary>
        /// <param name="nodeid">业主ID</param>
        /// <param name="coordinateType"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="elevation"></param>
        /// <returns></returns>
        public int Edit(string nodeid, CoordinateType coordinateType, double longitude,
            double latitude, double elevation)
        {
            var coordinate = _Context.MapParameter.FirstOrDefault(m => m.NodeId == nodeid && m.RecordState == (int)RecordStateEnum.Normal);
            if (coordinate != null)
            {
                coordinate.NodeId = nodeid;
                coordinate.NodeType = (int)coordinateType;
                coordinate.Longitude = longitude;
                coordinate.Latitude = latitude;
                coordinate.Elevation = elevation;
                coordinate.Radius = 1; //临时项
                _Context.MapParameter.Update(coordinate);
                //return Update(coordinate);
            }
            else
            {
                coordinate = new MapParameter()
                {
                    NodeId = nodeid,
                    NodeType = (int)coordinateType,
                    Longitude = longitude,
                    Latitude = latitude,
                    Elevation = elevation,
                    Radius = 1 //临时项
                };
                //return Add(coordinate);
                _Context.MapParameter.Add(coordinate);
            }
            return _Context.SaveChanges();
        }

        /// <summary>
        /// 彻底删除坐标
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        public int DeleteTrue(string nodeid)
        {
            var coordinates = FindAll<MapParameter>()
                .Where(m => m.NodeId == nodeid && m.RecordState == (int)RecordStateEnum.Normal).ToArray();
            using (var _context = ContextBuilder.Build())
            {
                if (coordinates.Length > 0)
                {
                    _context.RemoveRange(coordinates);
                    return _context.SaveChanges();
                }
                else
                {
                    return 1;
                }
            }
        }

        /// <summary>
        /// 删除坐标
        /// </summary>
        /// <param name="nodeid"></param>
        /// <returns></returns>
        public int Delete(string nodeid)
        {
            var coordinates = FindAll<MapParameter>()
                .Where(m => m.NodeId == nodeid && m.RecordState == (int)RecordStateEnum.Normal).ToArray();
            foreach (var item in coordinates)
            {
                item.RecordState = (int)RecordStateEnum.Deleted;
            }
            return Update(coordinates);
        }
    }
}