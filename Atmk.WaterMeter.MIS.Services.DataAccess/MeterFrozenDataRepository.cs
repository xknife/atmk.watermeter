﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Records;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class MeterFrozenDataRepository:BaseRepository,IMeterFrozenDataRepository
    {
        /// <summary>
        /// 获取冻结数据
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="checkDate">判断日期</param>
        /// <param name="frozenType"></param>
        /// <returns></returns>
        public List<FrozenDataRecord> FrozenDatas(string meterNumber, DateTime checkDate, FrozenType frozenType)
        {
            return Context.MeterFrozenData.Where(m =>
                m.Number == meterNumber
                && m.FrozenDate.Date >= checkDate.Date 
                && m.FrozenType == frozenType).ToList();
        }
    }

   
}
