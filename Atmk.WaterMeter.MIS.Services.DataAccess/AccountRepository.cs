﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Microsoft.EntityFrameworkCore;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        /// <summary>
        /// 根据业主信息获取账户信息集合
        /// </summary>
        /// <param name="ownerIds"></param>
        /// <returns></returns>
        public List<OrderDetail> GetAccounts(params string[] ownerIds)
        {
            return Context.OrderDetail.Where(a => ownerIds.Contains(a.OwnerId)).ToList();
        }
    }
}