﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Atmk.WaterMeter.MIS.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class RolesRepository:BaseRepository, IRolesRepository
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 获取用户的角色
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public List<Roles> GetRoleByUser(string uid)
        {
            var staffs = Context.User.Where(u => u.Id.ToString() == uid);
            if (!staffs.Any()) throw new Exception("没有该操作员信息");
            var result = Context.Roles.Where(r => r.Id.ToString() == staffs.First().RoleId).ToList();
            if(!result.Any())throw new Exception("该用户没有查到任何角色,该用户角色是否不存在");
            return result;
        }

        /// <summary>
        /// 获取除超级管理员以外的角色
        /// </summary>
        /// <returns></returns>
        public List<Roles> GetRoleExceptAdmin()
        {
            var result = Context.Roles.Where(r => r.Name != "超级管理员").ToList();
            return result;
        }

        /// <summary>
        /// 异步，获取角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Roles GetRoleById(string id)
        {
            return  Context.Roles.First(r => r.Id.ToString() == id);
        }
    }

}
