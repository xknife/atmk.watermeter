﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class MeterCodeIdMapRepository:BaseRepository,IMeterCodeIdMapRepository
    {
        /// <summary>
        /// 根据设备id获取水表编号
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public string GetMeterNumberByNBId(string deviceId)
        {
            var meterMap = Context.MeterCodeIdMap.Where(m => m.CtNbId == deviceId).ToList();
            if(meterMap.Count==0)
                throw new Exception("没有找到对应水表编号");
            return meterMap.First().Number;
        }
        /// <summary>
        /// 保存水表对应列表
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public bool Save(string meterNumber, string deviceId,string imei)
        {
            var meterMap = new MeterCodeIdMap
            {
                Number = meterNumber,
                CtNbId = deviceId,
                Imei = imei

            };
            Context.MeterCodeIdMap.Add(meterMap);
            Context.SaveChanges();
            return true;
        }
        /// <summary>
        /// 获取所有对应列表
        /// </summary>
        /// <returns></returns>
        public List<MeterCodeIdMap> Seletc()
        {
            return Context.MeterCodeIdMap.ToList();
        }
        /// <summary>
        /// 通过水表编号移除
        /// </summary>
        /// <param name="meterNumbers"></param>
        /// <returns></returns>
        public int Remove(params string[] meterNumbers)
        {
            var ents = Context.MeterCodeIdMap.Where(m =>meterNumbers.Contains(m.Number)).ToList();
            return Remove(ents.ToArray());
        }

        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="idAndNbIdMap"></param>
        /// <returns></returns>
        public int Remove(params MeterCodeIdMap[] idAndNbIdMap)
        {
            Context.MeterCodeIdMap.RemoveRange(idAndNbIdMap);
            return Context.SaveChanges();
        }
    }

   
}
