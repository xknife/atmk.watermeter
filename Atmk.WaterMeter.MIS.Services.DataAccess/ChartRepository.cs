﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Microsoft.EntityFrameworkCore;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class ChartRepository:BaseRepository,IChartRepository
    {
        /// <inheritdoc/>
        public List<District> GetDistricts(params string[] districtIds)
        {
           //获取
           return Context.District.AsNoTracking().Where(d => districtIds.Contains(d.Id.ToString())).ToList();
        }
        /// <inheritdoc />
        public List<Owner> GetOwners(params string[] districtIds)
        {
            //获取
            return Context.Owner.AsNoTracking().Where(o => districtIds.Contains(o.DistrictId)).ToList();
        }

        ///// <inheritdoc />
        //public List<Account> GetAccounts(params string[] ownerIds)
        //{
        //    return Context.Accounts.AsNoTracking().Where(a => ownerIds.Contains(a.OwnerId)).ToList();
        //}

        /// <inheritdoc />
        public List<Meter> GetMeters(params string[] ownerIds)
        {
            return Context.Meter.AsNoTracking().Where(a => ownerIds.Contains(a.OwnerId)).ToList();
        }

        /// <inheritdoc />
        public List<MeterReadingRecord> GetMeterReadings(params string[] meterNumbers)
        {
            return Context.MeterReadingRecord.AsNoTracking().Where(a => meterNumbers.Contains(a.CtdeviceId)).ToList();
        }
    }
}
