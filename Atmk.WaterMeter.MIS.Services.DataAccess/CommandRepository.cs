﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class CommandRepository : BaseRepository,ICommandRepository
    {
        /// <summary>
        /// 根据commandId获取水表指令集合
        /// </summary>
        /// <param name="commandId"></param>
        /// <returns></returns>
        public List<MeterCommand> GetCommandsByCommandId(string commandId)
        {
            return Context.MeterCommand.Where(m => m.Memo.Contains(commandId)).ToList();
        }
    }
}