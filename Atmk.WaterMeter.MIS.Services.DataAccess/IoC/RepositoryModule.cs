﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository.WeiXin;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Atmk.WaterMeter.MIS.Services.DataAccess.MergeQuery;
using Autofac;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.IoC
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<BaseRepository>().As<IRepository>();
            builder.RegisterType<MergeQueryRepository>().As<IMergeQueryRepository>();
            builder.RegisterType<AccountRepository>().As<IAccountRepository>();
            builder.RegisterType<CommandRepository>().As<ICommandRepository>();
            builder.RegisterType<DistrictRepository>().As<IDistrictRepository>();
            builder.RegisterType<MapRepository>().As<IMapRepository>();
            //builder.RegisterType<MeterCodeIdMapRepository>().As<IMeterCodeIdMapRepository>();
            //builder.RegisterType<MeterFrozenDataRepository>().As<IMeterFrozenDataRepository>();
            builder.RegisterType<MeterOperationRepository>().As<IMeterOperationRepository>();
            builder.RegisterType<MeterReadingRepository>().As<IMeterReadingRepository>();
            builder.RegisterType<MeterRepository>().As<IMeterRepository>();
            builder.RegisterType<OwnerRepository>().As<IOwnerRepository>();
            builder.RegisterType<ProjectConfigurationRepository>().As<IProjectConfigurationRepository>();
            builder.RegisterType<PaymentRepository>().As<IPaymentRepository>();
            builder.RegisterType<RefillRepository>().As<IRefillRepository>();
            builder.RegisterType<RolesRepository>().As<IRolesRepository>();
            builder.RegisterType<SettlementRepository>().As<ISettlementRepository>();
            builder.RegisterType<StaffRepository>().As<IStaffRepository>();
            builder.RegisterType<StepTariffRepository>().As<IStepTariffRepository>();
            builder.RegisterType<AreaRepository>().As<IAreaRepository>();
            builder.RegisterType<StatisticHistoryRepository>().As<IStatisticHistoryRepository>();

            builder.RegisterType<WeiXin.WxSrRelationRepositoryRead>().As<IWxSrRelationRepositoryRead>();
            builder.RegisterType<WeiXin.WxSrRelationRepositoryWrite>().As<IWxSrRelationRepositoryWrite>();
            builder.RegisterType<WeiXin.WxRechargeRead>().As<IWxRechargeRepositoryRead>();
            builder.RegisterType<WeiXin.WxRechargeWrite>().As<IWxRechargeRepositoryWrite>();

        }
    }
}
