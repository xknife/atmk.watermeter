﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Microsoft.EntityFrameworkCore;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class AreaRepository : BaseRepository, IAreaRepository
    {
        /// <inheritdoc />
        public Project GetAreaEntities(string areaId)
        {
           return Context.Project.AsNoTracking().First(a => a.Id.ToString() == areaId);
        }
    }
}
