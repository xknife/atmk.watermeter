﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Services.DataAccess.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Services.DataAccess
{
    public class MeterRepository : BaseRepository, IMeterRepository
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        ///// <summary>
        ///// 修改水表状态
        ///// </summary>
        ///// <param name="meterNumber">水表编号</param>
        ///// <param name="meterDocumentState"></param>
        ///// <returns></returns>
        //public int ChangMeterState(string meterNumber, MeterDocumentState meterDocumentState)
        //{
        //    try
        //    {
        //        var meter = FindAll<Meter>().First(m => m.Number == meterNumber);
        //        meter.MeterState = Enum.GetName(typeof(MeterDocumentState), meterDocumentState);
        //        return Update(meter);
        //    }
        //    catch (Exception e)
        //    {
        //       _logger.Error(e);
        //       throw new Exception(e.Message,e);
        //    }
        //}

        /// <summary>
        /// 获取指定业主Id下包含的水表信息集合
        /// </summary>
        /// <param name="ownerIds"></param>
        /// <returns></returns>
        public List<Meter> GetMetersByOwnerIds(params string[] ownerIds)
        {
            return Context.Meter.AsNoTracking().Where(m =>
                ownerIds.Contains(m.OwnerId) &&
                m.MeterState != Enum.GetName(typeof(MeterDocumentState), MeterDocumentState.销户) &&
                m.RecordState == (int)RecordStateEnum.Normal).ToList();
        }

        /// <inheritdoc />
        /// <summary>
        /// 修改水表信息
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <param name="meterDocumentState"></param>
        /// <param name="staffId"></param>
        /// <param name="modifiedLog"></param>
        /// <returns></returns>
        public int ChangMeterState(string meterNumber, MeterDocumentState meterDocumentState, string staffId,
            string modifiedLog)
        {
            try
            {
                var meter = FindAll<Meter>().First(m => m.MeterNumber == meterNumber);
                meter.MeterState = Enum.GetName(typeof(MeterDocumentState), meterDocumentState);
                //meter.ModifiedLog(staffId, modifiedLog);
                using (var _context = ContextBuilder.Build())
                {
                    _context.Update(meter);
                    return _context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw new Exception(e.Message, e);
            }
        }

        /// <summary>
        /// 获取水表状态的枚举
        /// </summary>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        public MeterDocumentState MeterState(string meterNumber)
        {
            var meter = FindAll<Meter>().First(m => m.MeterNumber == meterNumber);
            Enum.TryParse<MeterDocumentState>(meter.MeterState, out var result);
            return result;
        }
    }
}