﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.ViewModels;
using Atmk.WaterMeter.MIS.Datas;

namespace Atmk.WaterMeter.MIS.Services.DataAccess.MergeQuery
{
    public class MergeQueryRepository:IMergeQueryRepository
    {
        private readonly Context _Context;

        public MergeQueryRepository()
        {
            _Context = ContextBuilder.Build();
        }
        /// <summary>
        /// Lambda表达式读取的用户所有信息
        /// </summary>
        /// <returns></returns>
        public IQueryable<object> SelectLambdaUserInfo()
        {
            throw new NullReferenceException();
            //var query = _Context.Users.Join(_Context.Roles, u => u.RoleId, r => r.Id, (u, r)
            //        => new
            //        {
            //            u.Id,
            //            OwnerName = u.Name,
            //            RoleName = r.Name,
            //            r.Parmissions,
            //            u.DistrictId
            //        })
            //    .GroupJoin(_Context.Districts, u => u.DistrictId, d => d.Id, (u, d) => new {u, d})
            //    .SelectMany(ud => ud.d.DefaultIfEmpty(), (ud, d) =>
            //        new
            //        {
            //            ud.u.Id,
            //            ud.u.OwnerName,
            //            ud.u.RoleName,
            //            ud.u.Parmissions,
            //            DistrictName = d.Name,
            //            d.DistrictMessage
            //        });
            //return query;
        }

        /// <summary>
        /// 获取指定片区下的业主水表信息
        /// </summary>
        /// <returns></returns>
        public List<OwnerMeterViewModel> SelectOwmerMeter(string[] districtcode)
        {
            var ownerMeterEntitises = 
                _Context.Owner.Join(_Context.District,o=>o.DistrictId,d=>d.Id.ToString(),(o,d)
                    => new OwnerMeterViewModel
                    {
                        OwnerId = o.Id.ToString(),
                        OwnerName=o.Name,
                        OwnerMessage =o.Memo,
                        DistrictId = d.Id.ToString(),
                        DistrictName=d.Name
                    })
                .Join(_Context.Meter,ow=>ow.MeterNumber,m=>m.MeterNumber, (ow,m)
                    => new OwnerMeterViewModel
                    {
                        OwnerId = ow.OwnerId,
                        OwnerName = ow.OwnerName,
                        OwnerMessage = ow.OwnerMessage,
                        DistrictId = ow.DistrictId,
                        DistrictName = ow.DistrictName,
                        MeterNumber = m.MeterNumber,
                        //MeterName = m.Name,
                        MeterDatas=m.Memo,
                        MeterType=m.MeterType
                    }
                ).Where(w=>districtcode.Contains(w.DistrictId)).ToList();
            return ownerMeterEntitises;
        }
    }
}
