﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class NBRelationIMEI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IMEI",
                table: "MeterCodeIdMap",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IMEI",
                table: "MeterCodeIdMap");
        }
    }
}
