﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class WxSrOrderRecord2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountId",
                table: "WxSrOrderRecords",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "HouseNumber",
                table: "WxSrOrderRecords",
                maxLength: 40,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LastBalance",
                table: "WxSrOrderRecords",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Number",
                table: "WxSrOrderRecords",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "OwnerId",
                table: "WxSrOrderRecords",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "RefillSum",
                table: "WxSrOrderRecords",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ShouldPayment",
                table: "WxSrOrderRecords",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "HouseNumber",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "LastBalance",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "RefillSum",
                table: "WxSrOrderRecords");

            migrationBuilder.DropColumn(
                name: "ShouldPayment",
                table: "WxSrOrderRecords");
        }
    }
}
