﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class StatisticHistories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StatisticHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    ProjectId = table.Column<string>(nullable: true),
                    StatisticType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatisticHistories", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StatisticHistories");
        }
    }
}
