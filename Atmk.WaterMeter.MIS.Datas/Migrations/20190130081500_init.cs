﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OwnerId = table.Column<string>(maxLength: 50, nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    Arrears = table.Column<decimal>(nullable: false),
                    AccountStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataBase",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Config = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataBase", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Parent = table.Column<string>(maxLength: 50, nullable: true),
                    NodeType = table.Column<int>(nullable: false),
                    ProjectId = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FeePrice",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price1 = table.Column<double>(nullable: false),
                    Price2 = table.Column<double>(nullable: false),
                    Price3 = table.Column<double>(nullable: false),
                    PiceStepId = table.Column<string>(maxLength: 60, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeePrice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MapParameter",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    NodeId = table.Column<string>(maxLength: 50, nullable: true),
                    NodeType = table.Column<int>(nullable: false),
                    Radius = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Elevation = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapParameter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterCodeIdMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Number = table.Column<string>(maxLength: 50, nullable: true),
                    CT_NbId = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterCodeIdMap", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterCommand",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    MeterNumber = table.Column<string>(maxLength: 20, nullable: false),
                    Command = table.Column<string>(maxLength: 200, nullable: false),
                    CommandStatus = table.Column<int>(maxLength: 20, nullable: false),
                    CommandCount = table.Column<int>(nullable: false),
                    ExecuteTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterCommand", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterFrozenData",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Number = table.Column<string>(maxLength: 20, nullable: false),
                    FrozenDate = table.Column<DateTime>(nullable: false),
                    FrozenType = table.Column<int>(nullable: false),
                    Unit = table.Column<int>(nullable: false),
                    FrozenRead = table.Column<int>(nullable: false),
                    FrozenRecord = table.Column<string>(maxLength: 20, nullable: true),
                    CloseState = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterFrozenData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterOperationRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Number = table.Column<string>(maxLength: 20, nullable: false),
                    OperationType = table.Column<int>(maxLength: 20, nullable: false),
                    OperationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterOperationRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterReadingRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Number = table.Column<string>(maxLength: 20, nullable: false),
                    ReadTime = table.Column<DateTime>(nullable: false),
                    Value = table.Column<double>(nullable: false),
                    LastValue = table.Column<double>(nullable: false),
                    ValveState = table.Column<int>(maxLength: 20, nullable: false),
                    Voltage = table.Column<string>(maxLength: 20, nullable: false),
                    CloseState = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterReadingRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Meters",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MeterNumber = table.Column<string>(maxLength: 20, nullable: true),
                    MeterType = table.Column<string>(maxLength: 30, nullable: true),
                    CommType = table.Column<string>(maxLength: 30, nullable: true),
                    PrcieStepId = table.Column<string>(maxLength: 50, nullable: true),
                    OwnerId = table.Column<string>(maxLength: 50, nullable: true),
                    RefillType = table.Column<string>(nullable: true),
                    MeterState = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ModelNum",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MaxRange = table.Column<string>(maxLength: 20, nullable: true),
                    MaxFlow = table.Column<string>(maxLength: 20, nullable: true),
                    LifeTime = table.Column<string>(maxLength: 20, nullable: true),
                    Caliber = table.Column<string>(maxLength: 20, nullable: true),
                    Protocol = table.Column<string>(maxLength: 20, nullable: true),
                    FactoryCode = table.Column<string>(maxLength: 20, nullable: true),
                    FactoryName = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelNum", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Owners",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DistrictId = table.Column<string>(maxLength: 50, nullable: true),
                    HouseNumber = table.Column<string>(maxLength: 40, nullable: true),
                    Mobile = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Owners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParameterList",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Config = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParameterList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    OwnerId = table.Column<string>(maxLength: 50, nullable: false),
                    MeterNumber = table.Column<string>(maxLength: 50, nullable: true),
                    AccountId = table.Column<string>(maxLength: 50, nullable: false),
                    StaffId = table.Column<string>(maxLength: 50, nullable: false),
                    SettlementId = table.Column<string>(nullable: true),
                    AmountChangedDownMode = table.Column<int>(nullable: false),
                    PaymentSum = table.Column<decimal>(nullable: false),
                    PaymentMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrcieStep",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false),
                    MeterType = table.Column<int>(nullable: false),
                    PriceUnit = table.Column<int>(nullable: false),
                    CutPoint1 = table.Column<int>(nullable: false),
                    CutPoint2 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrcieStep", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefillRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    OwnerId = table.Column<string>(maxLength: 50, nullable: false),
                    AccountId = table.Column<string>(maxLength: 50, nullable: false),
                    StaffId = table.Column<string>(maxLength: 50, nullable: false),
                    RefillType = table.Column<int>(nullable: false),
                    RefillSum = table.Column<decimal>(nullable: false),
                    ShouldPayment = table.Column<decimal>(nullable: false),
                    LastBalance = table.Column<decimal>(nullable: false),
                    Rescind = table.Column<int>(nullable: false),
                    RefillMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefillRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: false),
                    Parmissions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SettlementRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    SettlementTime = table.Column<DateTime>(nullable: false),
                    MeterNumber = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    StartRead = table.Column<int>(nullable: false),
                    EndRead = table.Column<int>(nullable: false),
                    MeterValue = table.Column<int>(nullable: false),
                    Price1 = table.Column<decimal>(nullable: false),
                    SettlementMoney = table.Column<decimal>(nullable: false),
                    MeterReadingIds = table.Column<string>(nullable: true),
                    SettlementMessage = table.Column<string>(nullable: true),
                    PaymentStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SettlementRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Password = table.Column<string>(maxLength: 20, nullable: false),
                    RoleId = table.Column<string>(maxLength: 50, nullable: false),
                    PageLines = table.Column<int>(nullable: false),
                    DistrictId = table.Column<string>(nullable: true),
                    SiteId = table.Column<string>(maxLength: 200, nullable: true),
                    Surname = table.Column<string>(maxLength: 50, nullable: false),
                    Mobile = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.DropTable(
                name: "DataBase");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "FeePrice");

            migrationBuilder.DropTable(
                name: "MapParameter");

            migrationBuilder.DropTable(
                name: "MeterCodeIdMap");

            migrationBuilder.DropTable(
                name: "MeterCommand");

            migrationBuilder.DropTable(
                name: "MeterFrozenData");

            migrationBuilder.DropTable(
                name: "MeterOperationRecords");

            migrationBuilder.DropTable(
                name: "MeterReadingRecords");

            migrationBuilder.DropTable(
                name: "Meters");

            migrationBuilder.DropTable(
                name: "ModelNum");

            migrationBuilder.DropTable(
                name: "Owners");

            migrationBuilder.DropTable(
                name: "ParameterList");

            migrationBuilder.DropTable(
                name: "PaymentRecords");

            migrationBuilder.DropTable(
                name: "PrcieStep");

            migrationBuilder.DropTable(
                name: "RefillRecords");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "SettlementRecords");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
