﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class mapSequence : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Sequence",
                table: "MapParameter",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sequence",
                table: "MapParameter");
        }
    }
}
