﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Atmk.WaterMeter.MIS.Datas.Migrations
{
    public partial class WeiXinTable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WxRechargeRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    OpenId = table.Column<string>(maxLength: 50, nullable: false),
                    AccountId = table.Column<string>(maxLength: 50, nullable: false),
                    HouseNumber = table.Column<string>(maxLength: 40, nullable: true),
                    Number = table.Column<string>(maxLength: 20, nullable: false),
                    RefillSum = table.Column<decimal>(nullable: false),
                    ShouldPayment = table.Column<decimal>(nullable: false),
                    LastBalance = table.Column<decimal>(nullable: false),
                    Rescind = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WxRechargeRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WxSrOrderRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Memo = table.Column<string>(nullable: true),
                    Log = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    RecordState = table.Column<int>(nullable: false),
                    OpenId = table.Column<string>(maxLength: 50, nullable: false),
                    OutTradeNo = table.Column<string>(maxLength: 32, nullable: false),
                    OrderStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WxSrOrderRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WxSrRelations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    OpenId = table.Column<string>(maxLength: 50, nullable: false),
                    Number = table.Column<string>(maxLength: 20, nullable: false),
                    OwnerId = table.Column<string>(maxLength: 50, nullable: true),
                    AccountId = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WxSrRelations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WxRechargeRecords");

            migrationBuilder.DropTable(
                name: "WxSrOrderRecords");

            migrationBuilder.DropTable(
                name: "WxSrRelations");
        }
    }
}
