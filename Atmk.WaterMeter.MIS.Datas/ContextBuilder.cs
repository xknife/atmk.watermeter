﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace Atmk.WaterMeter.MIS.Datas
{
    /// <summary>
    /// 数据上下文的创建工具
    /// </summary>
    public class ContextBuilder
    {
        private static DbContextOptions<Context> _Option;

        private static DbContextOptions<Context> GetOption()
        {
            if (_Option == null)
            {
                var dbContextOptions = new DbContextOptions<Context>();
                var dbContextOptionsBuilder = new DbContextOptionsBuilder<Context>(dbContextOptions);
                _Option = dbContextOptionsBuilder.UseMySQL(Connection()).Options;
            }
            return _Option;
        }

        private static IConfigurationSource GetConfig()
        {
            return new JsonConfigurationSource
            {
                Path = "MySqlsettings.json",
                /*
                 * 如果为True,会在一定读写后报错，详细见连接 https://blog.csdn.net/hiliqi/article/details/80953502
                 * 描述:部署一个asp.net core程序，用着用着占用线程数越来越多，看报错日志发现这么一行：
                 * the configured user limit (128) on the number of inotify instances has been reached。
                 * 谷歌查到Stackoverflow上也有人遇到这个问题，原来是读取json文件造成的。
                 * 原因是：You are creating file watchers, every time you access an setting. The 3rd parameter is reloadOnChange.
                 * 大意是创建了一个文件监视器，由于第三个参数设为true，导致每次请求都要新开一个线程来读取文件。
                 * 解决办法是将第三个参数设置为false，不过貌似不是最理想的解决办法，最好的办法是采用依赖注入。
                */
                ReloadOnChange = false 
            };
        }

        public static string Connection()
        {
            var config = new ConfigurationBuilder().Add(GetConfig()).Build();
            return config.GetConnectionString("MySqlConnection");
        }

        public static Context Build()
        {
            return new Context(GetOption());
        }
    }
}