﻿using Atmk.WaterMeter.EF.ModelPuls;
using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Atmk.WaterMeter.MIS.TimerTask
{
    public class SettlementTask
    {
        private static Timer timer;
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        public void run()
        {
            timer = new Timer(Settlement, "", TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(29));
            //timer = new Timer(Settlement, "", TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(3));
        }
        public void Settlement(object state)
        {
            //检查
            _logger.Debug("自动结算检查时间："+DateTime.Now.ToString("HH"));
            if (DateTime.Now.ToString("HH")=="00")
            {
                //_logger.Debug("时间：" + DateTime.Now.ToString());
                using (var context = new atmk_watermeter_db_custum_luxiContext())
                {
                    using (var tran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string ProjectId = "434d6136-99f4-414b-bd08-02a27971bc73";
                            //所有数据未结算数据
                            List<SettlementDay> settleDays_all = context.SettlementDay.Where(m=>m.PorjectId== ProjectId&&m.SettlementState==0).ToList();

                            //查询“今年的”、“已结算”的用量
                            List<SettlementDay> settleDays_Current_Year = context.SettlementDay.Where(m => m.PorjectId == ProjectId && m.SettlementState == 1 && m.ReadTime.ToString("yyyy")==DateTime.Now.Year.ToString()).ToList();

                            //按表号分组
                            var meterNumbers = settleDays_all.GroupBy(m => m.MeterNumber).Select(m => m.Key).ToArray();

                            var prcieStep = context.PrcieStep.Where(m => m.ProjectId == ProjectId).ToList();
                            var prcieStepIds = prcieStep.Select(m => m.Id.ToString());
                            var feePrices = context.FeePrice.Where(m => prcieStepIds.Contains(m.PiceStepId)).ToList();

                            var total_Meter = context.Meters.Where(m => meterNumbers.Contains(m.MeterNumber)).ToList();

                            //表用量
                            var settleDaysGroup = settleDays_all.GroupBy(m => m.MeterNumber).Select(k => 
                            new Out_SettleDayGroup 
                            { 
                                MeterNumber = k.Key, 
                                WaterVolume = k.Sum(l => l.Dosage),
                                Dosage_Year = settleDays_Current_Year.Where(m => m.MeterNumber== k.Key).Sum(m=>m.Dosage)
                            });

                            var result_SettleDayGroup = new List<Out_SettleDayGroup>();
                            foreach (var item in settleDaysGroup)
                            {
                                var nr = new Out_SettleDayGroup();
                                Meters meter = total_Meter.First(m => m.MeterNumber == item.MeterNumber);

                                FeePrice feePrice = feePrices.First(m => m.PiceStepId == meter.StepPriceId);

                                nr.MeterNumber = item.MeterNumber;
                                nr.WaterVolume = item.WaterVolume;
                                nr.Dosage_Year = item.Dosage_Year;
                                nr.PiceStepId = meter.StepPriceId;
                                nr.OwnerId = meter.OwnerId;
                                nr.MeterId = meter.Id;
                                nr.CutPoint1 = prcieStep.First(m => m.Id.ToString() == meter.StepPriceId).CutPoint1;
                                nr.CutPoint2 = prcieStep.First(m => m.Id.ToString() == meter.StepPriceId).CutPoint2;
                                nr.Price1 = feePrice.Price1;
                                nr.Price2 = feePrice.Price2;
                                nr.Price3 = feePrice.Price3;
                                result_SettleDayGroup.Add(nr);
                            }

                            foreach (var item in result_SettleDayGroup)
                            {
                                var orderDetail = new OrderDetail();
                                orderDetail.CostType = 1;//水表扣费
                                orderDetail.RechargeChannels = "水表扣费";
                                orderDetail.WaterVolume = item.WaterVolume;
                                orderDetail.OwnerId = item.OwnerId;
                                orderDetail.MeterNumber = item.MeterNumber;
                                orderDetail.MeterId = item.MeterId;
                                orderDetail.CreateTime = DateTime.Now;
                                //阶梯280以上
                                if (item.Dosage_Year >= item.CutPoint2)
                                {
                                    orderDetail.WaterPrice = item.Price3;
                                    orderDetail.Money = -item.WaterVolume * item.Price3;
                                }
                                //阶梯160-280
                                if (item.Dosage_Year > item.CutPoint1 && item.Dosage_Year <= item.CutPoint2)
                                {
                                    //判断年用水量 + 本次用水量 是否超过第二阶梯
                                    if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint2)
                                    {
                                        //阶梯2用水量
                                        double JT2 = (item.CutPoint2 - item.Dosage_Year);
                                        //阶梯3用水量
                                        double JT3 = item.WaterVolume - JT2;
                                        double part2 = JT2 * item.Price2;
                                        double part3 = JT3 * item.Price3;
                                        orderDetail.WaterPrice = 777;//3个7 的价格  看备注
                                        orderDetail.Money = -(part2 + part3);
                                        orderDetail.Remarks = "部分1用量：" + JT2 + "，部分1价格：" + item.Price2 + "。部分2用量：" + JT3 + "，部分2价格：" + item.Price3;
                                    }
                                    else
                                    {
                                        //年用水量 + 本次用水量 未超过第二阶梯
                                        orderDetail.WaterPrice = item.Price2;
                                        orderDetail.Money = -item.WaterVolume * item.Price2;
                                    }
                                }
                                //阶梯0-160
                                if (item.Dosage_Year < item.CutPoint1)
                                {
                                    double JT1 = item.CutPoint1 - item.Dosage_Year;
                                    double JT2 = 0;
                                    double JT3 = 0;
                                    if ((item.Dosage_Year + item.WaterVolume) <= item.CutPoint1)
                                    {
                                        JT1 = item.WaterVolume - item.Dosage_Year;
                                    }
                                    //判断年用水量 + 本次用水量 是否超过第一阶梯
                                    if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint1 && (item.Dosage_Year + item.WaterVolume) <= item.CutPoint2)
                                    {
                                        JT2 = item.WaterVolume - JT1;
                                        orderDetail.WaterPrice = 777;
                                        orderDetail.Remarks = "部分1用量：" + JT1 + "方 价格：" + item.Price1 + "；部分2用量：" + JT2 + "方 价格：" + item.Price2;
                                    }
                                    //判断年用水量 + 本次用水量 是否超过第二阶梯
                                    if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint2)
                                    {
                                        JT2 = item.CutPoint2 - item.CutPoint1;
                                        JT3 = item.WaterVolume - (JT1 + JT2);
                                        orderDetail.WaterPrice = 777;
                                        orderDetail.Remarks = "部分1用量：" + JT1 + "方 价格：" + item.Price1 + "；部分2用量：" + JT2 + "方 价格：" + item.Price2 + "；部分3用量：" + JT3 + "，价格：" + item.Price3;
                                    }
                                    double part1 = JT1 * item.Price1;
                                    double part2 = JT2 * item.Price2;
                                    double part3 = JT3 * item.Price3;
                                    orderDetail.WaterPrice = orderDetail.WaterPrice == 777 ? 777 : item.Price1;//3个7 的价格  看备注
                                    orderDetail.Money = -(part1 + part2 + part3);
                                }
                                if (orderDetail.Money != 0)
                                {
                                    context.Add(orderDetail);
                                }
                            }
                            //int Count = context.SaveChanges();
                            //改变该月水表的 结算状态
                            foreach (var item in settleDays_all)
                            {
                                item.SettlementState = 1;
                                context.Update(item);
                            }
                            int Count = context.SaveChanges();
                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            _logger.Fatal($"结算失败:{ex.Message}");
                            tran.Rollback();
                        }
                    }
                }
            }
        }
    }
}
