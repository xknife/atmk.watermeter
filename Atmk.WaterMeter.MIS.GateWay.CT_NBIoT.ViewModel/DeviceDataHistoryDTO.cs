﻿using Atmk.WaterMeter.MIS.Entities;
using Newtonsoft.Json.Linq;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class DeviceDataHistoryDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string gatewayId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string appId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceId { get; set; }
        /// <summary>
        /// 自定义的上传数据
        /// </summary>
        public JObject data { get; set; }
        //public DeviceData data { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public Dictionary<string, string> data { get; set; }
        //public string DataString
        //{
        //    get
        //    {
        //        string result = "";
        //        foreach (KeyValuePair<string, string> item in data)
        //        {
        //            result += item.Key + ":" + item.Value + "\r\n";
        //        }
        //        return result;
        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        public string timestamp { get; set; }
    }
}
