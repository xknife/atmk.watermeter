﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class ApiResult
    {
        public int StatusCode { get; set; }

        public string Result { get; set; }

        public string Errcode { get; set; }

        public string Memo { get; set; }
    }
}
