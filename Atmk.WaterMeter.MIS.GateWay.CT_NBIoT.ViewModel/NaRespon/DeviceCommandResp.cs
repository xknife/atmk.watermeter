﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon
{
    public class DeviceCommandQueryResp
    {
        public Pagination pagination { get; set; }
        public List<DeviceCommandResp> data { get; set; }
    }

    /// <summary>
    /// 命令响应类
    /// </summary>
    public class DeviceCommandResp
    {
       public string commandId { get; set; }
        public string appId { get; set; }
        public string deviceId { get; set; }
        public CommandDTOV4 command { get; set; }
        public string callbackUrl { get; set; }
        public int expireTime { get; set; }
        public string status { get; set; }
        public object result { get; set; }
        public string creationTime { get; set; }
        public string executeTime { get; set; }
        public string platformIssuedTime { get; set; }
        public string deliveredTime { get; set; }
        public int issuedTimes { get; set; }
    }

    public class CommandDTOV4
    {
        public string serviceId { get; set; }
        public string method { get; set; }
        public Dictionary<string,object> paras { get; set; }
    }
}
