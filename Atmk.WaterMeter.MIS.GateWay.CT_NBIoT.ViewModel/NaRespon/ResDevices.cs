﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon
{
    /// <summary>
    /// 设备信息
    /// </summary>
    public class ResDevices
    {
        public long totalCount { get; set; }
        public long pageNo { get; set; }
        public long pageSize { get; set; }
        public List<GetDeviceRspDto> devices { get; set; }
    }
    /// <summary>
    /// 设备信息
    /// </summary>
    public class ResDeviceslist
    {
        public long totalCount { get; set; }
        public long pageNo { get; set; }
        public long pageSize { get; set; }
        public List<DeviceDataHistoryDTO> deviceDataHistoryDTOs { get; set; }
    }
}
