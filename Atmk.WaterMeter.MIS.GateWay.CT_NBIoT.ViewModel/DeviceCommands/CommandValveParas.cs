﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    /// <summary>
    /// NB协议发送指令数据参数-阀门控制
    /// </summary>
    public class CommandValveParas:CommandParas
    {
        /// <summary>
        /// 控制指令 二进制最低位是阀门，其余待议
        /// </summary>
        public int operation { get; set; }
    }
}
