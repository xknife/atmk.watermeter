﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    public class TaskDeviceCommand
    {
        public string appId { get; set; }
        public int timeout { get; set; }
        public string taskName { get; set; }
        public string taskType { get; set; }
        public TaskParam param { get; set; }
    }
}
