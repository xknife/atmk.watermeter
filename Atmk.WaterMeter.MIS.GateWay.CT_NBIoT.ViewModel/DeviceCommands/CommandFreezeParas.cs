﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
   public class CommandFreezeParas:CommandParas
    {
        /// <summary>
        /// 冻结数据类型
        /// </summary>
        public int freezetype { get; set; }
        
    }
}
