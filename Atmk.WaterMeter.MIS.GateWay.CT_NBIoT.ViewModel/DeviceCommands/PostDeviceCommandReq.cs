﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    public class PostDeviceCommandReq
    {

        /// <summary>
        /// 
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Command command { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string callbackUrl { get; set; }

        /// <summary>
        /// 指令重传次数
        /// </summary>
        public int maxRetransmit { get; set; }
    }
}
