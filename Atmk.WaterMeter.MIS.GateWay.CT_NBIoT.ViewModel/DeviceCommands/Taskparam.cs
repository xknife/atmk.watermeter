﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    public class TaskParam
    {
        public string type { get; set; }
        public string[] deviceList { get; set; }
        public Command command { get; set; }
    }
}
