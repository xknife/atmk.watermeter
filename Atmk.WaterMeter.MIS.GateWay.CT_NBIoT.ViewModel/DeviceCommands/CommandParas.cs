﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    /// <summary>
    /// NB协议发送指令数据参数
    /// </summary>
    public class CommandParas
    {
        public CommandParas()
        {
            msglen = 1;
            start = 2;
            stop = 1;
            crc = 1;
            msgtype = 192;
        }

        public int msglen { get;}
        public int start { get; }
        public int stop { get; }
        public int crc { get; }

        public string endpointid { get; set; }
       
   
        /// <summary>
        /// 时间字符串:格式转为base64
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// 功能类型 C0 192
        /// </summary>
        public int msgtype { get; set; }
    }
}
