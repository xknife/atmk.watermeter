﻿using Newtonsoft.Json.Linq;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceCommands
{
    public class Command
    {
        /// <summary>
        /// 
        /// </summary>
        public string serviceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string method { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public JObject paras { get; set; }
    }
}
