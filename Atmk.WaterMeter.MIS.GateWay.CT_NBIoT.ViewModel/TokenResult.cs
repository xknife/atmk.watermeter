﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    /// <summary>
    /// 设备profile：data1 int   data2 string(8,32)  ret1 int ret2 string(8,32) para1 para2 para12 para22  
    /// 0xf0上报0xff命令相应0x91 cmd1 0x92 cmd2
    /// </summary>
    public class TokenResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TokenType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ExpiresIn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Scope { get; set; }
    }

}
