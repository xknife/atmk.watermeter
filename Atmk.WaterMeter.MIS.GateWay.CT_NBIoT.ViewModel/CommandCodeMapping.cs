﻿using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT
{
    /// <summary>
    /// 命令映射
    /// </summary>
    public class CommandCodeMapping
    {
        private readonly Dictionary<string, CommandStatus> _CodeStatusDictionary;

        private CommandCodeMapping()
        {
            _CodeStatusDictionary = new Dictionary<string, CommandStatus>
            {
                ["SENT"] = CommandStatus.已发送,
                ["DELIVERED"] = CommandStatus.已送达,
                ["EXECUTED"] = CommandStatus.已执行,
                ["PENDING"] = CommandStatus.等待,
                ["DEFAULT"] = CommandStatus.未发送,
                ["EXPIRED"] = CommandStatus.命令过期,
                ["SUCCESSFUL"] = CommandStatus.执行成功,
                ["FAILED"] = CommandStatus.执行失败,
                ["TIMEOUT"] = CommandStatus.执行超时,
                ["CANCELED"] = CommandStatus.撤销,
            };
        }

        public static CommandCodeMapping Builder()
        {
            return new CommandCodeMapping();
        }
        /// <summary>
        /// 获取指令映射
        /// </summary>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public CommandStatus CodeMapping(string statusCode)
        {
            return !_CodeStatusDictionary.ContainsKey(statusCode)
                ? CommandStatus.其他
                : _CodeStatusDictionary[statusCode];
        }
    }
}
