﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage
{
    /// <summary>
    /// 注册设备参数
    /// </summary>
    public class DeviceManagePostParameter
    {
        public string appId { get; set; }
        public string verifyCode { get; set; }
        public string nodeId { get; set; }
        public string endUserId { get; set; }
        public string psk { get; set; }
        public int timeout { get; set; }
    }
}
