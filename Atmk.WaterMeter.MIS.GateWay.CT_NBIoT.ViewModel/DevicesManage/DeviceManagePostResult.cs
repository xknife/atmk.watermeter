﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage
{
    /// <summary>
    /// 注册设备后收到的反馈
    /// </summary>
    public class DeviceManagePostResult
    {
        public string deviceId { get; set; }
        public string verifyCode { get; set; }
        public int timeout { get; set; }
        public string psk { get; set; }
    }
}
