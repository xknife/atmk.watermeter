﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage
{
    /// <summary>
    /// 修改设备信息的请求参数
    /// 详细说明见中国电信平台-文档中心：接口列表-设备管理-修改设备信息
    /// </summary>
    public class DeviceManagePutParameter
    {
        public string deviceId { get; set; }
        public string appId { get; set; }
        public string name { get; set; }
        public string endUser { get; set; }
        public Mute mute { get; set; }
        public string manufacturerId { get; set; }
        public string manufacturerName { get; set; }
        public string deviceType { get; set; }
        public string model { get; set; }
        public string location { get; set; }
        public string protocolType { get; set; }
        public DeviceConfigDTO deviceConfig { get; set; }
        public string region { get; set; }
        public string organization { get; set; }
        public string timezone { get; set; }
        public bool isSecure { get; set; }
        public string psk { get; set; }
    }

    public enum Mute
    {
        TRUE,
        FALSE
    }
}
