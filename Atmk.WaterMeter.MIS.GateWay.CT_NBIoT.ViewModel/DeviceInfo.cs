﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class DeviceInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string nodeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string manufactureId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string manufacturerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mac { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string location { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string deviceType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string swVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fwVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string hwVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string protocolType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string signalStrength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string bridgeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string supportedSecurity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isSecurity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sigVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string runningStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string statusDetail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mute { get; set; }
    }
}
