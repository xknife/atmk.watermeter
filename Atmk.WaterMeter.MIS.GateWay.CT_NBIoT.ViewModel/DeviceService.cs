﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class DeviceService
    {
        /// <summary>
        /// 
        /// </summary>
        public string serviceType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DataString
        {
            get
            {
                string result = "";
                foreach (KeyValuePair<string, string> item in data)
                {
                    result += item.Key + ":" + item.Value + "\r\n";
                }
                return result;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("time")]
        [JsonConverter(typeof(CustomizedDateTimeConverer))]
        public DateTime eventTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceInfo { get; set; }
    }
}
