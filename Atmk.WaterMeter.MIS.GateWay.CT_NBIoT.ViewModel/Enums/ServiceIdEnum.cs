﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.Enums
{
    /// <summary>
    /// 电信平台设备profile的服务名称枚举
    /// </summary>
    public enum ServiceIdEnum
    {
        /// <summary>
        /// 水表上传普通数据
        /// </summary>
        uploadwatermetermsg,
        /// <summary>
        /// 水表上传冻结数据
        /// </summary>
        wmfreezedatamsg,
        /// <summary>
        /// 水表上传开关阀指令返回数据
        /// </summary>
        controlvalve
    }
}
