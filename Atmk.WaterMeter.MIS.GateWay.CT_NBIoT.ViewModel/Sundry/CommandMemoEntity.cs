﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.Sundry
{
    public class CommandMemoEntity
    {
        public CommandMemoEntity()
        {
            CommandType = "NB";
        }

        /// <summary>
        /// 平台类型
        /// </summary>
        public string CommandType { get; }
        /// <summary>
        /// 平台运行ID
        /// </summary>
        public string CommandRefId { get; set; }
    }
}
