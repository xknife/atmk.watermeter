﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class DeviceCapabilityDTO
    {
      public string  deviceId { get; set; }
      public List<ServiceCapabilityDTO> serviceCapabilities { get; set; }
    }
}
