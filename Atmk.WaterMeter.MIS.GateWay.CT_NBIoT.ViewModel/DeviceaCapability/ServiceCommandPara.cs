﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class ServiceCommandPara
    {
        public string paraName { get; set; }
        public string dataType { get; set; }
        public bool required { get; set; }
        public string min { get; set; }
        public string max { get; set; }
        public double step { get; set; }
        public int maxLength { get; set; }
        public string unit { get; set; }
        public List<string> enumList { get; set; }
    }
}
