﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class ServiceCommandResponse
    {
        public string responseName { get; set; }
        public List<ServiceCommandPara> paras { get; set; }
    }
}
