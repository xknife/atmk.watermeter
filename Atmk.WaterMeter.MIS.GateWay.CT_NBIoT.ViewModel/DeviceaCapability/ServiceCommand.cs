﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class ServiceCommand
    {
        public string commandName { get; set; }
        public List<ServiceCommandPara> paras { get; set; }
        public List<ServiceCommandResponse> responses { get; set; }
    }
}
