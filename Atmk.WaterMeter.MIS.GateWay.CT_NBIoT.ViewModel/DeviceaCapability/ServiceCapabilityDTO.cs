﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class ServiceCapabilityDTO
    {
        public string serviceId { get; set; }
        public string serviceType { get; set; }
        public string description { get; set; }
        public List<ServiceCommand> commands { get; set; }
        public List<ServiceProperty> properties { get; set; }
    }
}
