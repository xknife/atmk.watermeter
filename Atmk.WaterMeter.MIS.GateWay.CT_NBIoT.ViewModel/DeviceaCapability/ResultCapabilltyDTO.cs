﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DeviceaCapability
{
    public class ResultCapabilltyDTO
    {
        public List<DeviceCapabilityDTO> deviceCapabilities { get; set; }
    }
}
