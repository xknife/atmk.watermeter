﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.GateWay
{
    public class CommandResultEntity
    {
        /// <summary>
        /// 命令id
        /// </summary>
        public string CommandId { get; set; }
        /// <summary>
        /// 命令状态
        /// </summary>
        public string CommandStatue { get; set; }

    }
}
