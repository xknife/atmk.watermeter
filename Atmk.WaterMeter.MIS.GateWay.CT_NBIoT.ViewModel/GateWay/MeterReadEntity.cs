﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.GateWay
{
    /// <summary>
    /// 水表读取数据类
    /// </summary>
    public class MeterReadEntity
    {
        /// <summary>
        /// 设备Id
        /// </summary>
        public string DeviceId { get; set; }
        /// <summary>
        /// 水表编号
        /// </summary>
        public string MeterCode { get; set; }
        /// <summary>
        /// 水表上传数据
        /// </summary>
        public DeviceData Data { get; set; }
        //public Dictionary<string, string> Data { get; set; }
        ///// <summary>
        ///// 数据字符串
        ///// </summary>
        //public string DataString
        //{
        //    get
        //    {
        //        string result = "";
        //        foreach (KeyValuePair<string, string> item in Data)
        //        {
        //            result += item.Key + ":" + item.Value + "\r\n";
        //        }
        //        return result;
        //    }
        //}
        /// <summary>
        /// 读取时间字符串
        /// </summary>
        public string TimeStamp { get; set; }
        /// <summary>
        /// 读取时间
        /// </summary>
        public DateTime GetTime
        {
            get
            {

                var dt = DateTime.ParseExact(TimeStamp, "yyyyMMddTHHmmssZ", CultureInfo.CurrentCulture);
                return dt;
            }
        }
    }
}
