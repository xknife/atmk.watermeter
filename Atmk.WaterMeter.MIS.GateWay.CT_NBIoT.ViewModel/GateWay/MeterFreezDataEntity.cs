﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.GateWay
{
    /// <summary>
    /// 冻结数据-中转对象
    /// </summary>
    public class MeterFreezDataEntity
    {
        public string deviceId { get; set; }
        /// <summary>
        /// 数据上报时间
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// 数据时间戳
        /// </summary>
        public string timestamp { get; set; }
        public string endpointid { get; set; }
        /// <summary>
        /// 冻结类型
        /// </summary>
        public int freezetype { get; set; }
        /// <summary>
        /// 冻结数据
        /// </summary>
        public string freezedata { get; set; }
    }
}
