﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.RequestParam
{
    /// <summary>
    /// 设备信息请求参数类型
    /// </summary>
    public class DeviceGetParam
    {
        public DeviceGetParam()
        {
            pageSize = 20;
        }

        public string appId { get; set; }
        public string gatewayId { get; set; }
        public string nodeType { get; set; }
        public string deviceType { get; set; }
        public string protocolType { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public string status { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string sort { get; set; }
    }
}
