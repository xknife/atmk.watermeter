﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.RequestParam
{
    /// <summary>
    /// 能力查询参数
    /// </summary>
    public class DeviceaCapabilityParam
    {
       public string  appId { get; set; }
        public string deviceId { get; set; }
        public string gatewayId { get; set; }
    }
}
