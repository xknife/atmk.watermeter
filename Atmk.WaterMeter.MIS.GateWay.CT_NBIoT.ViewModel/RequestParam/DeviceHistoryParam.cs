﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.RequestParam
{
    public class DeviceHistoryParam
    {
        public string  deviceId { get; set; }
        public string gatewayId { get; set; }
        public string serviceId { get; set; }
        public string property { get; set; }
        public string appId { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
    }
}
