﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.RequestParam
{
    /// <summary>
    /// 设备请求命令
    /// </summary>
    public class DeviceCommandsParam
    {
        public string appId { get; set; }
    }
    /// <summary>
    /// 设备命令查询
    /// </summary>
    public class GetCommandsParam
    {
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public string deviceId { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string appId { get; set; }
    }
}
