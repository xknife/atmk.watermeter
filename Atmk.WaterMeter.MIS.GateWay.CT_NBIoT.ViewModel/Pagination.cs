﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class Pagination
    {
       public long pageNo { get; set; }
        public long pageSize { get; set; }
        public long totalSize { get; set; }
    }
}
