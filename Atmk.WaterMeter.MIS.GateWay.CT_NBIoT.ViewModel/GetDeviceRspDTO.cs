﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel
{
    public class GetDeviceRspDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string gatewayId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string nodeType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lastModifiedTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DeviceInfo deviceInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DeviceService> services { get; set; }
    }
}
