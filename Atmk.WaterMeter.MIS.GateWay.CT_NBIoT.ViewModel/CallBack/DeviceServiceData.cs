﻿using Atmk.WaterMeter.MIS.Entities;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack
{
    /// <summary>
    /// 数据服务返回类
    /// </summary>
    public class DeviceServiceData
    {
        /// <summary>
        /// 服务id，由平台给出，本系统中仅保存，暂无实际意义
        /// </summary>
        public string serviceId { get; set; }
        /// <summary>
        /// 服务类型，由平台给出，本系统中仅保存，暂无实际意义
        /// </summary>
        public string serviceType { get; set; }
        /// <summary>
        /// 上传数据
        /// </summary>
        public DeviceData data { get; set; }
        /// <summary>
        /// 事件生时间：时间格式：yyyymmddThhmissZ，例如20151213T082233Z
        /// </summary>
        public string eventTime { get; set; }
    }
}
