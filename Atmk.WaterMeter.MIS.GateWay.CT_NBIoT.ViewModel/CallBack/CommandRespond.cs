﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack
{
    /// <summary>
    /// 指令结果响应类
    /// </summary>
    public class CommandRespond
    {
        public string deviceId { get; set; }
        public string commandId { get; set; }
        public CommandResult result { get; set; }
    }

    public class CommandResult
    {
        public string resultCode { get; set; }
        public string resultDetail { get; set; }
    }
}
