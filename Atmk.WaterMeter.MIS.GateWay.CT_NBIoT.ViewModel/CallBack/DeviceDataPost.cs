﻿namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack
{
    /// <summary>
    /// 设备请求类
    /// </summary>
    public class DeviceDataPost
    {
        /// <summary>
        /// 通知类型，由平台给出，本系统中仅保存，暂无实际意义
        /// </summary>
        public string notifyType { get; set; }
        /// <summary>
        /// 消息的序列号，由平台给出，本系统中仅保存，暂无实际意义
        /// </summary>
        public string requestId { get; set; }
        /// <summary>
        /// 设备id，由平台生成的设备唯一ID，在本系统中已创建该ID与表编号的映射
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// 网关ID，由平台给出，本系统中仅保存，暂无实际意义
        /// </summary>
        public string gatewayId { get; set; }
        /// <summary>
        /// 上传数据
        /// </summary>
        public DeviceServiceData service { get; set; }

        /// <summary>返回表示当前对象的字符串。</summary>
        /// <returns>表示当前对象的字符串。</returns>
        public override string ToString()
        {
            return $">>> {service.eventTime} --+-- {service.data.current}";
        }
    }
}
