﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities.CTCloud;
using Atmk.WaterMeter.MIS.Services.DataAccess;
using Xunit;

namespace Atmk.WaterMeter.MIS.Tests.XunitTest.RepositoryTest
{
    public class MeterReadRecordRepositoryTest
    {
        private static IMeterReadRecordRepository GetRepository()
        {
            return null;
        }

        /// <summary>
        ///     测试根据ID查询 返回记录状态
        /// </summary>
        [Fact]
        public void PageByIdTest01()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, false));
            var result = r.PageById("88888-88888-88888-88888", SortMode.None).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///  测试根据ID查询-空记录时 返回状态
        /// </summary>
        [Fact]
        public void PageByIdTest02()
        {
            var r = GetRepository();
            r.Clear();
            var result = r.PageById("88888-88888-88888-88888", SortMode.None).Count();
            Assert.Equal(0, result);
        }
        /// <summary>
        ///  测试根据ID查询 - 1条记录时 返回状态
        /// </summary>
        [Fact]
        public void PageByIdTest03()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, false));
            var result = r.PageById("88888-88888-88888-88888", SortMode.None).Count();
            Assert.Equal(1, result);
        }
        /// <summary>
        ///     测试传入参数ID类型错误
        /// </summary>
        [Fact]
        public void PageByIdTest04()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true));
            var result = r.PageById("0", SortMode.None).Count();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试ID为空时
        /// </summary>
        [Fact]
        public void PageByIdTest05()
        {
            var r = GetRepository();
            Assert.Throws<NullReferenceException>(() => r.PageById(string.Empty, SortMode.None));
        }

        /// <summary>
        ///     测试 记录排序（正序）
        /// </summary>
        [Fact]
        public void PageByIdTest06()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, false));
            var result = r.PageById("88888-88888-88888-88888", SortMode.ASC).ToArray();
            Assert.Equal(20, result.Length);
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime > result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///     测试 记录排序（倒序）
        /// </summary>
        [Fact]
        public void PageByIdTest07()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, false));
            var result = r.PageById("88888-88888-88888-88888", SortMode.DESC).ToArray();
            Assert.Equal(20, result.Length);
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime < result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///  测试根据日期查询 - 正常返回记录状态
        /// </summary>
        [Fact]
        public void PageByDateTest01()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, false));
            var result = r.PageByDate(DateTime.Now.AddHours(-1), DateTime.Now.AddHours(1), SortMode.None).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///  测试根据日期查询 - 空数据是返回结果
        /// </summary>
        [Fact]
        public void PageByDateTest02()
        {
            var r = GetRepository();
            r.Clear();
            var result = r.PageByDate(DateTime.Now.AddHours(-1), DateTime.Now.AddHours(1), SortMode.None).Count();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试日期记录排序（正序）
        /// </summary>
        [Fact]
        public void PageByDateTest06()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true));
            var result = r.PageByDate(DateTime.Now.AddHours(-1), DateTime.Now.AddHours(1), SortMode.ASC).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime > result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///     测试日期记录排序(倒序)
        /// </summary>
        [Fact]
        public void PageByDateTest07()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true));
            var result = r.PageByDate(DateTime.Now.AddHours(-1), DateTime.Now.AddHours(1), SortMode.DESC).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime < result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }

        /// <summary>
        ///  测试低电压-正常<3伏的记录状态
        /// </summary>
        [Fact]
        public void PageByVoltageTest01()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true));
            var result = r.PageByLowVoltage(SortMode.None,0,3).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///  测试低电压-正常>3伏的记录状态
        /// </summary>
        [Fact]
        public void PageByVoltageTest02()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, false));
            var result = r.PageByLowVoltage(SortMode.None, 3, 4).Count();
            //因为数据中无超过3V的记录  所以和0比较
            Assert.Equal(0, result);
        }

        /// <summary>
        ///  测试低电压-正常>3伏的记录状态
        /// </summary>
        [Fact]
        public void PageByVoltageTest03()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, true));
            var result = r.PageByLowVoltage(SortMode.None, 3, 4).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///  测试低电压-正常>3伏的记录状态(正序)
        /// </summary>
        [Fact]
        public void PageByVoltageTest04()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, true));
            var result = r.PageByLowVoltage(SortMode.ASC,3,4).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime > result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///  测试低电压-正常>3伏的记录状态(倒序)
        /// </summary>
        [Fact]
        public void PageByVoltageTest05()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, true));
            var result = r.PageByLowVoltage(SortMode.DESC, 3, 4).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime < result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }

        /// <summary>
        ///     测试低电压-返回数据0条
        /// </summary>
        [Fact]
        public void PageByVoltageTest06()
        {
            var r = GetRepository();
            r.Clear();
            var result = r.PageByLowVoltage(SortMode.None,0,4).Count();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///  测试低电压-返回数据1条
        /// </summary>
        [Fact]
        public void PageByVoltageTest07()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(1, true, true, false));
            var result = r.PageByLowVoltage(SortMode.None,0,3).Count();
            Assert.Equal(1, result);
        }
        /// <summary>
        ///  测试低电压-返回数据(正序)
        /// </summary>
        [Fact]
        public void PageByVoltageTest08()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, false));
            var result = r.PageByLowVoltage(SortMode.ASC,0,3).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime > result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///  测试低电压-返回数据(倒序)
        /// </summary>
        [Fact]
        public void PageByVoltageTest09()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true, false));
            var result = r.PageByLowVoltage(SortMode.DESC,0,3).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime < result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///     测试insert函数 - 正常插入记录时函数返回状态
        /// </summary>
        [Fact]
        public void InsertTest01()
        {
            var r = GetRepository();
            var result = r.Insert(new MeterReadingRecord()
            {
                Id = Guid.NewGuid().ToString(),
                CTDeviceId = Guid.NewGuid().ToString(),
                Value = 12,
                Warning = 0,
                ReadTime = DateTime.Now,
                MsgType = 1,
                ValveState = 0
            });
            Assert.Equal(1, result);
        }

        /// <summary>
        ///     测试insert函数 - 传入空实体时，函数返回状态
        /// </summary>
        [Fact]
        public void InsertTest02()
        {
            var r = GetRepository();
            var result = r.Insert(new MeterReadingRecord());
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试insert函数 - 传入错误实体，函数报错状态
        /// </summary>
        [Fact]
        public void InsertTest03()
        {
            var r = GetRepository();
            var id = Guid.NewGuid().ToString();
            var result = r.Insert(new MeterReadingRecord
            {
                Id = id,
                CTDeviceId = Guid.NewGuid().ToString(),
                Value = 12,
                Warning = 0,
                ReadTime = DateTime.Now
            });
            result = r.Insert(new MeterReadingRecord
            {
                Id = id,
                CTDeviceId = Guid.NewGuid().ToString(),
                Value = 12,
                Warning = 0,
                ReadTime = DateTime.Now
            });
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试insert函数 - 无实体时，函数状态
        /// </summary>
        [Fact]
        public void InsertTest04()
        {
            var r = GetRepository();
            var result = r.Insert();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试insert函数 - 实体缺少属性，必填字段为空时，函数状态
        /// </summary>
        [Fact]
        public void InsertTest05()
        {
            var r = GetRepository();
            var result = r.Insert(new MeterReadingRecord
            {
                Id = Guid.NewGuid().ToString(),
                CTDeviceId = null
            });
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试insert函数 - 传入多个实体时 未赋值，函数状态
        /// </summary>
        [Fact]
        public void InsertTest06()
        {
            var r = GetRepository();
            var result = r.Insert(new List<MeterReadingRecord>().ToArray());
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试insert函数 - 传入多个实体时，函数状态
        /// </summary>
        [Fact]
        public void InsertTest07()
        {
            var r = GetRepository();
            var m = new List<MeterReadingRecord>();
            for (var i = 0; i < 100; i++)
            {
                var rr = new MeterReadingRecord();
                rr.Id = Guid.NewGuid().ToString();
                rr.CTDeviceId = Guid.NewGuid().ToString();
                m.Add(rr);
            }
            var result = r.Insert(m.ToArray());
            Assert.Equal(100, result);
        }

        /// <summary>
        ///     测试分页 - 正常返回记录状态
        /// </summary>
        [Fact]
        public void PageTest01()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(50, true, true));
            var result = r.Page(SortMode.None, 1, 20).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///     测试分页 - 空记录 返回状态
        /// </summary>
        [Fact]
        public void PageTest02()
        {
            var r = GetRepository();
            r.Clear();
            var result = r.Page(SortMode.None, 1, 20).Count();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试分页 - 1条记录 返回状态
        /// </summary>
        [Fact]
        public void PageTest03()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(1, true, true));
            var result = r.Page(SortMode.None, 1, 20).Count();
            Assert.Equal(1, result);
        }

        /// <summary>
        ///     测试分页 - 大于20条记录 返回状态
        /// </summary>
        [Fact]
        public void PageTest04()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(21, true, true));
            var result = r.Page(SortMode.None,1, 20).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///     测试分页 - 有参数，但参数错误
        /// </summary>
        [Fact]
        public void PageTest06()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(20, true, true));
            var result = r.Page(SortMode.None, 1, 20).Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///     测试分页 - 返回空时
        /// </summary>
        [Fact]
        public void PageTest07()
        {
            var r = GetRepository();
            r.Clear();
            var result = r.Page(SortMode.None, 1, 20).Count();
            Assert.Equal(0, result);
        }

        /// <summary>
        ///     测试分页 - 数据转换是否异常（ToArray()）
        /// </summary>
        [Fact]
        public void PageTest08()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(20, true, true));
            var result = r.Page(SortMode.None, 1, 20).ToArray().Count();
            Assert.Equal(20, result);
        }

        /// <summary>
        ///     测试分页 - 排序（正序）
        /// </summary>
        [Fact]
        public void PageTest09()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(20, true, true));
            var result = r.Page(SortMode.ASC,1, 20).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime > result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }
        /// <summary>
        ///     测试分页 - 排序（倒序）
        /// </summary>
        [Fact]
        public void PageTest10()
        {
            var r = GetRepository();
            r.Clear();
            r.Insert(GetTestData(20, true, true));
            var result = r.Page(SortMode.DESC, 1, 20).ToList();
            Assert.Equal(20, result.Count());
            for (int i = 0; i < result.Count(); i++)
            {
                if (i != 19)
                {
                    var b = result[i + 1].ReadTime < result[i].ReadTime;
                    Assert.True(b);
                }
            }
        }

        private MeterReadingRecord[] GetTestData(int count, bool hasId, bool hasDeviceNumber, bool lowVoltage = false)
        {
            List<MeterReadingRecord> list = new List<MeterReadingRecord>(count);
            for (int i = 0; i < count; i++)
            {
                list.Add(GetSingleTestData(hasId, hasDeviceNumber, lowVoltage));
            }
            return list.ToArray();
        }

        private MeterReadingRecord GetSingleTestData(bool hasId, bool hasDeviceNumber, bool lowVoltage = false)
        {
            var data = new MeterReadingRecord();
            data.Id = hasId ? Guid.NewGuid().ToString() : "0";
            data.CTDeviceId = hasDeviceNumber ? Guid.NewGuid().ToString() : "88888-88888-88888-88888";
            data.Value = 12;
            data.Warning = 0;
            data.ReadTime = DateTime.Now;
            data.Voltage = lowVoltage ? 3.3 : 2.6;
            return data;
        }

    }
}