﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;
using FluentAssertions;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class ConvertHelperTest
    {
        [Fact]
        public void ConvertIntStringTest()
        {
            var name = Base64Convert.ConvertIntArrayStrings("EwMdChsi");
            var same = name.Length == 6;
            same.Should().BeTrue();
        }


        [Fact]
        public void BitConvertIntStringForByteTest()
        {
            var name = BytesConvert.BitConvertIntStringForByte("1221");
            var same = name.Length == 2;
            same.Should().BeTrue();
        }
    }
}
