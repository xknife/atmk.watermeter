﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Utils;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class ReqUrlParamUtilTest
    {
        public class TestClass1
        {
            public string appId { get; set; }
            public string verifyCode { get; set; }
            public string nodeId { get; set; }
            public string endUserId { get; set; }
            public string psk { get; set; }
            public int timeout { get; set; }
        }
        public class TestClass2
        {
            public string appId { get; set; }
            public string verifyCode { get; set; }
            public string nodeId { get; set; }
        }
        [Fact]
        public void GetJsonParamBaseTest()
        {
            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
            var c1 = new TestClass1
            {
                appId="123",
                verifyCode = "1",
                nodeId = "1"
            };
            var c2 = new TestClass2
            {
                appId = "123",
                verifyCode = "1",
                nodeId = "1"
            };
            var json1 = ReqParameterUtil.GetJsonParam(c1, "endUserId", "psk", "timeout");
           Debug.WriteLine(json1);
        }
    }
    
}
