using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Logic;
using Autofac.Extras.Moq;
using FluentAssertions;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class BaseInfoLogicTest
    {
        [Fact]
        public void QuerySearchOwnerTest()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // The AutoMock class will inject a mock IDependency
                // into the SystemUnderTest constructor
                var logic = mock.Create<BaseInfoLogic>();
                var list = logic.QuerySearchOwner("", "", new List<Owner>());
                list.Should().BeEmpty();
            }
        }
    }
}
