﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Logic;
using Autofac.Extras.Moq;
using FluentAssertions;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class SimpleTest
    {
        [Fact]
        public void CtStringConvertDateTimeTest()
        {
            
                // The AutoMock class will inject a mock IDependency
                // into the SystemUnderTest constructor
                var list = Simple.CtStringConvertDateTime("20190123T173716Z");
                var same = new DateTime(2019, 1, 23, 17, 37, 16);
                list.Should().BeSameDateAs(same.Date);
            

        }
        [Fact]
        public void DecimalTest()
        {

            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
            const decimal d = 50.000000000000000000000000000000m;
            var d2 = decimal.ToDouble(d);
            var d3 = Convert.ToDouble(d);
            Console.WriteLine(d2);
            Console.WriteLine(d3);
            var same = Math.Abs(d2 - d3) <= 0;
            same.Should().BeTrue();
            var same2 = d2.ToString(CultureInfo.InvariantCulture) == "50";
            same2.Should().BeTrue();
        }

        [Fact]
        public void Md5Test()
        {
            var text = "appId=wxd678efh567hg6787&nonceStr=5K8264ILTKCH16CQ2502SI8ZNMTM67VS&package=prepay_id=wx2017033010242291fcfe0db70013231072&signType=MD5&timeStamp=1490840662&key=qazwsxedcrfvtgbyhnujmikolp111111";
            var value = MD5Encrypt.Encrypt(text);
            Console.WriteLine(value);
            var same = value == "22D9B4E54AB1950F51E0649E8810ACD6";
            same.Should().BeTrue();
        }
        [Fact]
        public void RundVoltage()
        {
            var value = Simple.RundVoltage();
            Console.WriteLine(value);
            var same = value.Contains("3.6");
            same.Should().BeTrue();
        }
    }
}
