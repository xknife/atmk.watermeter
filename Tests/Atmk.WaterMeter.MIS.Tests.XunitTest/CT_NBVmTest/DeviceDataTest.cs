﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Entities;
using FluentAssertions;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest.CT_NBVmTest
{
    public class DeviceDataTest
    {
        [Fact]
        public void Testvoltage()
        {
            var dd = new DeviceData();
            var v1 = dd.voltageTrue.ToString("F");
            dd.voltage = 211;
            var v2 = dd.voltageTrue.ToString("F");
            var same1 = v1 == "1.50" || v1 == "0";
            var same2 = v2 == "3.61";
            same1.Should().BeTrue();
            same2.Should().BeTrue();
        }
    }
}
