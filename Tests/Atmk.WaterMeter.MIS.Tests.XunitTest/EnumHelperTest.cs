﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities.Enums;
using FluentAssertions;
using FluentAssertions.Common;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class EnumHelperTest
    {
        [Fact]
        public void TryEnumByDescriptionTest()
        {
            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
            var bv = EnumHelper.TryEnumByDescription("生活热水水表", out MeterType result);
            bv.Should().BeTrue();
            var same = result == MeterType.HotWaterMeter;
            same.Should().BeTrue();
        }
        [Fact]
        public void TryEnumByDescriptionEmptyTest()
        {
            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
            var bv = EnumHelper.TryEnumByDescription("", out MeterType result);
            bv.Should().BeFalse();
            var same = result == MeterType.ColdWaterMeter;
            same.Should().BeTrue();
        }

        [Fact]
        public void TryEnumByDescriptionNoDescriptionTest()
        {
            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
            try
            {
                var bv = EnumHelper.TryEnumByDescription("已结算", out int result);
                bv.Should().BeFalse();
                var same = result == 0;
                same.Should().BeFalse();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
            }
        }
        [Fact]
        public void GetDescriptionByEnumTrueValueTest()
        {
            // The AutoMock class will inject a mock IDependency
            // into the SystemUnderTest constructor
           var result = EnumHelper.GetDescriptionByEnum(MeterType.DrinkingWaterMeter);
                var same = result == "直饮水水表";
                same.Should().BeTrue();

            var result2 = EnumHelper.GetDescriptionByEnum(AccountStatus.开户);
            var same2 = result2 == "";
            same2.Should().BeTrue();


        }
    }
}
