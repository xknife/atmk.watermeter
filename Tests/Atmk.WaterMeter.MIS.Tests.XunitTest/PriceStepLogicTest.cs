﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Logic;
using Autofac.Extras.Moq;
using FluentAssertions;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class PriceStepLogicTest
    {
        [Fact]
        public void QuerySearchOwnerTest()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // The AutoMock class will inject a mock IDependency
                // into the SystemUnderTest constructor
                var logic = mock.Create<PriceStepLogic>();
                var list = logic.SameName("饮水水价2", "0d506935-aba6-4196-9898-4be6811dade6", "5e890445-3a5c-41f6-8b64-304cd34c1565");
                list.Should().BeTrue();
            }
        }
    }
}
