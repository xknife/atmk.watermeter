﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Atmk.WaterMeter.MIS.XUnitTest
{
    public class PropertyHelperTest
    {
        [Fact]
        public void GetPropertyNameBaseTest()
        {
            var name = PropertyHelper.GetPropertyName<ReqUrlParamUtilTest.TestClass1>(t => t.endUserId);
            var same = name == "endUserId";
            same.Should().BeTrue();
        }
        [Fact]
        public void GetPropertyNameNoValueTest()
        {
            var temp = new DeviceManagePutParameter
            {
                appId = "123123213",
                deviceId = "fsdfsdfsdf",
                name = "110011001"
            };
            var result = PropertyHelper.GetNoValuePropertyName(temp);
            var same = result.Count > 0;
            same.Should().BeTrue();

        }
        [Fact]
        public void SetJobjectePropertyNameTest()
        {
            var temp = new _Temp();
            var j = new JObject
            {
                ["appId"] = "123123213",
                ["deviceId"] = "fsdfsdfsdf",
                ["intd"] =123
            };
            var result = PropertyHelper.SetJobjectePropertyName(temp,j);
            var same = result.appId == "123123213" && result.intd == 123;

            same.Should().BeTrue();

        }

        public class _Temp
        {
            public string appId { get; set; }
            public string deviceId { get; set; }
            public string name { get; set; }
            public int intd { get; set; }
        }
    }
}
