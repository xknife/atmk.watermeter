﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Dialogs;
using Atmk.WaterMeter.MIS.TestForm.Views;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock;
using Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm
{
    public partial class Workbench : Form
    {
        private static readonly ILog _Logger = LogManager.GetLogger<Workbench>();

        private bool _isLogin = false;

        public Workbench()
        {
            _Logger.Info("Workbench init...");
            InitializeComponent();
            InitializeDockPanel();
            Activated += (s, e) =>
            {
                new Thread(() =>
                {
                    if (!_isLogin)
                    {
                        _isLogin = true;
                        var dialog = new LoginDialog();
                        this.ThreadSafeInvoke(() =>
                        {
                            dialog.ShowDialog(this);
                            Activate();
                            Focus();
                        });
                    }
                }).Start();
            };
            _AddressTree_ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new AddressTreeTestView();
                view.Show(_dockPanel, DockState.Document);
            };
            _GetChartDataToolStripMenuItem.Click += (s, e) =>
            {
                var view = new ChartDataView();
                view.Show(_dockPanel, DockState.Document);
            };
            _hlgToolStripMenuItem.Click += (s, e) =>
            {
                var view = new MockDistrictDataView();
                view.Show(_dockPanel, DockState.Document);
            };
            _OwnerMeterToolStripMenuItem.Click += (s, e) =>
            {
                var view = new OwnerMeterView();
                view.Show(_dockPanel,DockState.Document);
            };
            _项目和操作员信息ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new AreaAndStaffView();
                view.Show(_dockPanel, DockState.Document);
            };
            _树状图查询ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new TestTreeView();
                view.Show(_dockPanel, DockState.Document);
            };
            _阶梯价格设置ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new PriceStepView();
                view.Show(_dockPanel, DockState.Document);
            };
            _设备管理ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new CT_NB_IoTPlatformView();
                view.Show(_dockPanel, DockState.Document);
            };
            _设备信息ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new CT_NB_IoTPlatformView();
                view.Show(_dockPanel, DockState.Document);
            };
        }
     
        #region DockPanel

        private const string DOCK_PANEL_CONFIG = "dockpanel3.config";
        private DockPanel _dockPanel;
        private LoggerView _loggerView;

        private static string GetLayoutConfigFile()
        {
            var dir = Path.GetDirectoryName(Application.ExecutablePath);
            return dir != null ? Path.Combine(dir, DOCK_PANEL_CONFIG) : DOCK_PANEL_CONFIG;
        }

        private void InitializeDockPanel()
        {
            SuspendLayout();
            
            _dockPanel = new DockPanel();
            _StripContainer.ContentPanel.Controls.Add(_dockPanel);

            _dockPanel.DocumentStyle = DocumentStyle.DockingWindow;
            _dockPanel.Theme = new VS2015BlueTheme();
            _dockPanel.Dock = DockStyle.Fill;
            _dockPanel.BringToFront();

            DockPanelLoadFromXml();

            if (_loggerView == null)
            {
                _loggerView = new LoggerView(); //DI.Get<LoggerView>();
                _loggerView.Show(_dockPanel, DockState.DockBottom);
            }

            PerformLayout();
            ResumeLayout(false);
        }

        /// <summary>
        ///   引发 <see cref="E:System.Windows.Forms.Form.Closing" /> 事件。
        /// </summary>
        /// <param name="e">
        ///   包含事件数据的 <see cref="T:System.ComponentModel.CancelEventArgs" />。
        /// </param>
        protected override void OnClosing(CancelEventArgs e)
        {
            DockPanelSaveAsXml();
            base.OnClosing(e);
        }

        /// <summary>
        ///     控件提供了一个保存布局状态的方法，它默认是没有保存的。
        /// </summary>
        private void DockPanelSaveAsXml()
        {
            _dockPanel.SaveAsXml(GetLayoutConfigFile());
        }

        private void DockPanelLoadFromXml()
        {
            //加载布局
            var deserializeDockContent = new DeserializeDockContent(GetViewFromPersistString);
            var configFile = GetLayoutConfigFile();
            if (File.Exists(configFile))
                _dockPanel.LoadFromXml(configFile, deserializeDockContent);
        }

        private IDockContent GetViewFromPersistString(string persistString)
        {
            if (persistString == typeof(LoggerView).ToString())
            {
                return _loggerView;
            }

            //            if (persistString == typeof(InterfaceTreeView).ToString())
            //            {
            //                if (_InterfaceTreeViewMenuItem.Checked)
            //                    return _InterfaceTreeView;
            //            }
            //            if (persistString == typeof(DataMangerView).ToString())
            //            {
            //                if (_DataManagerViewMenuItem.Checked)
            //                    return _DataManagerView;
            //            }
            //            if (persistString == typeof(CommandConsoleView).ToString())
            //            {
            //                if (_CommandConsoleViewMenuItem.Checked)
            //                    return _CommandConsoleView;
            //            }
            return null;
        }

        #endregion

     
    }
}
