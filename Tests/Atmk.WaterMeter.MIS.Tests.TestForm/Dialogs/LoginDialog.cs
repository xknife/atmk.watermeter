﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Area;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Common.Logging;
using Newtonsoft.Json;
using NKnife;
using NKnife.ControlKnife;
using NKnife.IoC;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Dialogs
{
    public partial class LoginDialog : SimpleForm
    {
        private static readonly ILog _Logger = LogManager.GetLogger<LoginDialog>();

        public LoginDialog()
        {
            InitializeComponent();
        }

        private void _acceptButton_Click(object sender, EventArgs e)
        {
            var kernel = DI.Get<Kernel>();
            var request = new RestRequest(UrlManager.SignIn, Method.POST);
            request.AddJsonBody(new sign_vo(_userNameTextBox.Text, _passwordTextBox.Text));

            kernel.Client.ExecuteAsync(request, response =>
            {
                _Logger.Trace(response.ResponseUri.AbsoluteUri);
                _Logger.Debug(response.Content);
                try
                {
                    var signReturn = JsonConvert.DeserializeObject<SignResult>(response.Content);
                    kernel.Token = signReturn.data.token;
                    _Logger.Trace(kernel.Token);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }

            });
            Close();
        }
       
        internal class sign_vo
        {
            public sign_vo(string username, string password)
            {
                this.username = username;
                this.password = password;
            }
            public string username { get; set; }
            public string password { get; set; }
        }

        public class TokenData
        {
            public string token { get; set; }
        }

        public class SignResult : Result
        {
            public TokenData data { get; set; }
        }
       

        public class UserInfoResult:Result
        {
            /// <summary>
            /// 
            /// </summary>
            public UserInfoData data { get; set; }
        }


    }
}
