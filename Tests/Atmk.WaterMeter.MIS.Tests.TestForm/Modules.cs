﻿using System.Windows.Forms;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper;
using Ninject.Modules;

namespace Atmk.WaterMeter.MIS.TestForm
{
    public class Modules : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
            Bind<Kernel>().ToSelf().InSingletonScope();

            #region Mock35个片区的一些功能用到的
            Bind<CurrentMock>().ToSelf().InSingletonScope();
            #endregion
        }
    }
}