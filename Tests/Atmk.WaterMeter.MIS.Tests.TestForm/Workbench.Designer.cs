﻿namespace Atmk.WaterMeter.MIS.TestForm
{
    partial class Workbench
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this._StripContainer = new System.Windows.Forms.ToolStripContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.逻辑LToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._GetChartDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._OwnerMeterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._项目和操作员信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._树状图查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._阶梯价格设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.其他FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._AddressTree_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.模拟数据MToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._hlgToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.电信平台ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._设备管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._设备信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._StripContainer.TopToolStripPanel.SuspendLayout();
            this._StripContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _StripContainer
            // 
            // 
            // _StripContainer.ContentPanel
            // 
            this._StripContainer.ContentPanel.Size = new System.Drawing.Size(1008, 704);
            this._StripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._StripContainer.Location = new System.Drawing.Point(0, 0);
            this._StripContainer.Name = "_StripContainer";
            this._StripContainer.Size = new System.Drawing.Size(1008, 729);
            this._StripContainer.TabIndex = 0;
            this._StripContainer.Text = "toolStripContainer1";
            // 
            // _StripContainer.TopToolStripPanel
            // 
            this._StripContainer.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件FToolStripMenuItem,
            this.逻辑LToolStripMenuItem,
            this.其他FToolStripMenuItem,
            this.模拟数据MToolStripMenuItem,
            this.电信平台ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件FToolStripMenuItem
            // 
            this.文件FToolStripMenuItem.Name = "文件FToolStripMenuItem";
            this.文件FToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.文件FToolStripMenuItem.Text = "文件(&F)";
            // 
            // 逻辑LToolStripMenuItem
            // 
            this.逻辑LToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._GetChartDataToolStripMenuItem,
            this._OwnerMeterToolStripMenuItem,
            this._项目和操作员信息ToolStripMenuItem,
            this._树状图查询ToolStripMenuItem,
            this._阶梯价格设置ToolStripMenuItem});
            this.逻辑LToolStripMenuItem.Name = "逻辑LToolStripMenuItem";
            this.逻辑LToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.逻辑LToolStripMenuItem.Text = "逻辑(&L)";
            // 
            // _GetChartDataToolStripMenuItem
            // 
            this._GetChartDataToolStripMenuItem.Name = "_GetChartDataToolStripMenuItem";
            this._GetChartDataToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this._GetChartDataToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._GetChartDataToolStripMenuItem.Text = "获取Chart数据";
            // 
            // _OwnerMeterToolStripMenuItem
            // 
            this._OwnerMeterToolStripMenuItem.Name = "_OwnerMeterToolStripMenuItem";
            this._OwnerMeterToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._OwnerMeterToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._OwnerMeterToolStripMenuItem.Text = "业主和水表信息";
            // 
            // _项目和操作员信息ToolStripMenuItem
            // 
            this._项目和操作员信息ToolStripMenuItem.Name = "_项目和操作员信息ToolStripMenuItem";
            this._项目和操作员信息ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._项目和操作员信息ToolStripMenuItem.Text = "项目和操作员信息";
            // 
            // _树状图查询ToolStripMenuItem
            // 
            this._树状图查询ToolStripMenuItem.Name = "_树状图查询ToolStripMenuItem";
            this._树状图查询ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._树状图查询ToolStripMenuItem.Text = "树状图查询";
            // 
            // _阶梯价格设置ToolStripMenuItem
            // 
            this._阶梯价格设置ToolStripMenuItem.Name = "_阶梯价格设置ToolStripMenuItem";
            this._阶梯价格设置ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._阶梯价格设置ToolStripMenuItem.Text = "阶梯价格设置";
            // 
            // 其他FToolStripMenuItem
            // 
            this.其他FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._AddressTree_ToolStripMenuItem});
            this.其他FToolStripMenuItem.Name = "其他FToolStripMenuItem";
            this.其他FToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.其他FToolStripMenuItem.Text = "实体(&F)";
            // 
            // _AddressTree_ToolStripMenuItem
            // 
            this._AddressTree_ToolStripMenuItem.Name = "_AddressTree_ToolStripMenuItem";
            this._AddressTree_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F12)));
            this._AddressTree_ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this._AddressTree_ToolStripMenuItem.Text = "地址树";
            // 
            // 模拟数据MToolStripMenuItem
            // 
            this.模拟数据MToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._hlgToolStripMenuItem});
            this.模拟数据MToolStripMenuItem.Name = "模拟数据MToolStripMenuItem";
            this.模拟数据MToolStripMenuItem.Size = new System.Drawing.Size(88, 21);
            this.模拟数据MToolStripMenuItem.Text = "模拟数据(&M)";
            // 
            // _hlgToolStripMenuItem
            // 
            this._hlgToolStripMenuItem.Name = "_hlgToolStripMenuItem";
            this._hlgToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F9)));
            this._hlgToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this._hlgToolStripMenuItem.Text = "回龙观自来水公司";
            // 
            // 电信平台ToolStripMenuItem
            // 
            this.电信平台ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._设备管理ToolStripMenuItem,
            this._设备信息ToolStripMenuItem});
            this.电信平台ToolStripMenuItem.Name = "电信平台ToolStripMenuItem";
            this.电信平台ToolStripMenuItem.Size = new System.Drawing.Size(84, 21);
            this.电信平台ToolStripMenuItem.Text = "电信平台(&C)";
            // 
            // _设备管理ToolStripMenuItem
            // 
            this._设备管理ToolStripMenuItem.Name = "_设备管理ToolStripMenuItem";
            this._设备管理ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this._设备管理ToolStripMenuItem.Text = "设备管理";
            // 
            // _设备信息ToolStripMenuItem
            // 
            this._设备信息ToolStripMenuItem.Name = "_设备信息ToolStripMenuItem";
            this._设备信息ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this._设备信息ToolStripMenuItem.Text = "设备信息";
            // 
            // Workbench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this._StripContainer);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Workbench";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Atmk.WaterMenter.MIS.TestForm";
            this._StripContainer.TopToolStripPanel.ResumeLayout(false);
            this._StripContainer.TopToolStripPanel.PerformLayout();
            this._StripContainer.ResumeLayout(false);
            this._StripContainer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer _StripContainer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 逻辑LToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 其他FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _AddressTree_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _GetChartDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 模拟数据MToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _hlgToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _OwnerMeterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _项目和操作员信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _树状图查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _阶梯价格设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 电信平台ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _设备管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _设备信息ToolStripMenuItem;
    }
}

