﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Commons.ViewModels.Area
{
    public class AreaOperatorResult:BaseResult
    {
        public AreaOperatorData data { get; set; }
    }
}
