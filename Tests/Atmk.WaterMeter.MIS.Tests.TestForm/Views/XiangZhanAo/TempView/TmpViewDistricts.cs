﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo.TempView
{
    public class ParentDistrictItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string parentDistrictId { get; set; }
        /// <summary>
        /// 秋爽小区
        /// </summary>
        public string parentDistrictName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> district { get; set; }
    }

    public class Data
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ParentDistrictItem> parentDistrict { get; set; }
    }

    public class TmpViewDistricts
    {
        /// <summary>
        /// 
        /// </summary>
        public int errcode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Data data { get; set; }
    }
}
