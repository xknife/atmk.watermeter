﻿using System;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Common.Logging;
using NKnife.IoC;
using RestSharp;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    public partial class ChartDataView : DockContent
    {
        private static readonly ILog _Logger = LogManager.GetLogger<ChartDataView>();

        public ChartDataView()
        {
            InitializeComponent();
            Load += (s, e) =>
            {
                var kernel = DI.Get<Kernel>();
                var request = new RestRequest(UrlManager.GetCharDatas, Method.GET);
                request.AddHeader("X-Token", kernel.Token);
                kernel.Client.ExecuteAsync(request, response =>
                {
                    var data = response.Content;
                    _dataTextBox.ThreadSafeInvoke(() => _dataTextBox.Text = data);

                   
                   // ChartResult chartResult = null;
                    try
                    {
                        _Logger.Debug(data);
                       // chartResult = JsonConvert.DeserializeObject<ChartResult>(AndMeterTypeData);
                    }
                    catch (Exception exception)
                    {
                        _Logger.Error(exception);
                    }
                    /*
                   this.ThreadSafeInvoke(() =>
                   {
                       if (chartResult == null)
                           return;
                       _propertyGrid1.SelectedObject = chartResult.datas.closingvalveData;
                       _propertyGrid2.SelectedObject = chartResult.datas.costReminderData;
                       _propertyGrid3.SelectedObject = chartResult.datas.districtReadRatioData;
                       _propertyGrid4.SelectedObject = chartResult.datas.lastMonthDistrictConsumptionsCountData;
                       _propertyGrid5.SelectedObject = chartResult.datas.lastSeasonWaterConsumptionsCountData;
                       _propertyGrid6.SelectedObject = chartResult.datas.lastYearWaterConsumptionsCountData;
                       _propertyGrid9.SelectedObject = chartResult.datas.yesterdayConsumptionData;
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.closingvalveData.list));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.costReminderData.list));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.districtReadRatioData));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.lastMonthDistrictConsumptionsCountData));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.lastSeasonWaterConsumptionsCountData));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.lastYearWaterConsumptionsCountData));
                       _listBox1.Items.Add(JsonConvert.SerializeObject(chartResult.datas.yesterdayConsumptionData));

                       _propertyGrid7.SelectedObject = chartResult.datas.statisticsInfoData;
                       _propertyGrid8.SelectedObject = chartResult.datas.waterConsumptionInfoData;
                   });
                   */
                });
            };
        }
    }
}
