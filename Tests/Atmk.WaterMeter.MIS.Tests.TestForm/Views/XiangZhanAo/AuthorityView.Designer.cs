﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class AuthorityView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CanControlButton = new System.Windows.Forms.Button();
            this._BuildRoleButton = new System.Windows.Forms.Button();
            this._BuildStaffButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _CanControlButton
            // 
            this._CanControlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CanControlButton.Location = new System.Drawing.Point(12, 12);
            this._CanControlButton.Name = "_CanControlButton";
            this._CanControlButton.Size = new System.Drawing.Size(165, 56);
            this._CanControlButton.TabIndex = 11;
            this._CanControlButton.Text = "获取可用功能";
            this._CanControlButton.UseVisualStyleBackColor = true;
            // 
            // _BuildRoleButton
            // 
            this._BuildRoleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildRoleButton.Location = new System.Drawing.Point(12, 74);
            this._BuildRoleButton.Name = "_BuildRoleButton";
            this._BuildRoleButton.Size = new System.Drawing.Size(165, 56);
            this._BuildRoleButton.TabIndex = 13;
            this._BuildRoleButton.Text = "创建角色";
            this._BuildRoleButton.UseVisualStyleBackColor = true;
            // 
            // _BuildStaffButton
            // 
            this._BuildStaffButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildStaffButton.Location = new System.Drawing.Point(12, 136);
            this._BuildStaffButton.Name = "_BuildStaffButton";
            this._BuildStaffButton.Size = new System.Drawing.Size(165, 56);
            this._BuildStaffButton.TabIndex = 12;
            this._BuildStaffButton.Text = "创建操作员";
            this._BuildStaffButton.UseVisualStyleBackColor = true;
            // 
            // AuthorityView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._BuildRoleButton);
            this.Controls.Add(this._BuildStaffButton);
            this.Controls.Add(this._CanControlButton);
            this.Name = "AuthorityView";
            this.Text = "AuthorityView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _CanControlButton;
        private System.Windows.Forms.Button _BuildRoleButton;
        private System.Windows.Forms.Button _BuildStaffButton;
    }
}