﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class OwnerMeterView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._GetChangeMeters = new System.Windows.Forms.Button();
            this._SelOwnerMeterButton = new System.Windows.Forms.Button();
            this._ResultTreeView = new System.Windows.Forms.TreeView();
            this._AreaListBox = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._DistrictListBox = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _GetChangeMeters
            // 
            this._GetChangeMeters.Location = new System.Drawing.Point(160, 68);
            this._GetChangeMeters.Name = "_GetChangeMeters";
            this._GetChangeMeters.Size = new System.Drawing.Size(157, 40);
            this._GetChangeMeters.TabIndex = 0;
            this._GetChangeMeters.Text = "获取换表数据";
            this._GetChangeMeters.UseVisualStyleBackColor = true;
            // 
            // _SelOwnerMeterButton
            // 
            this._SelOwnerMeterButton.Location = new System.Drawing.Point(160, 12);
            this._SelOwnerMeterButton.Name = "_SelOwnerMeterButton";
            this._SelOwnerMeterButton.Size = new System.Drawing.Size(157, 40);
            this._SelOwnerMeterButton.TabIndex = 1;
            this._SelOwnerMeterButton.Text = "获取业主和水表数据";
            this._SelOwnerMeterButton.UseVisualStyleBackColor = true;
            // 
            // _ResultTreeView
            // 
            this._ResultTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ResultTreeView.Location = new System.Drawing.Point(334, 12);
            this._ResultTreeView.Name = "_ResultTreeView";
            this._ResultTreeView.Size = new System.Drawing.Size(454, 426);
            this._ResultTreeView.TabIndex = 2;
            // 
            // _AreaListBox
            // 
            this._AreaListBox.FormattingEnabled = true;
            this._AreaListBox.ItemHeight = 12;
            this._AreaListBox.Location = new System.Drawing.Point(6, 20);
            this._AreaListBox.Name = "_AreaListBox";
            this._AreaListBox.Size = new System.Drawing.Size(130, 88);
            this._AreaListBox.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._AreaListBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 121);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "项目列表";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._DistrictListBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 121);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "片区列表";
            // 
            // _DistrictListBox
            // 
            this._DistrictListBox.FormattingEnabled = true;
            this._DistrictListBox.ItemHeight = 12;
            this._DistrictListBox.Location = new System.Drawing.Point(6, 20);
            this._DistrictListBox.Name = "_DistrictListBox";
            this._DistrictListBox.Size = new System.Drawing.Size(130, 88);
            this._DistrictListBox.TabIndex = 3;
            // 
            // OwnerMeterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._ResultTreeView);
            this.Controls.Add(this._SelOwnerMeterButton);
            this.Controls.Add(this._GetChangeMeters);
            this.Name = "OwnerMeterView";
            this.Text = "OwnerMeterView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _GetChangeMeters;
        private System.Windows.Forms.Button _SelOwnerMeterButton;
        private System.Windows.Forms.TreeView _ResultTreeView;
        private System.Windows.Forms.ListBox _AreaListBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox _DistrictListBox;
    }
}