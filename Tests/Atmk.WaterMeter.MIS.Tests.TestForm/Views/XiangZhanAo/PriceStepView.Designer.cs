﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class PriceStepView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ProjectIdBtn = new System.Windows.Forms.Button();
            this._PriceStepListBtn = new System.Windows.Forms.Button();
            this._AddPriceBtn = new System.Windows.Forms.Button();
            this._AddFeeBtn = new System.Windows.Forms.Button();
            this._MeterTypelistBox = new System.Windows.Forms.ListBox();
            this._DgvPrice = new System.Windows.Forms.DataGridView();
            this._DgvFee = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this._DgvPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._DgvFee)).BeginInit();
            this.SuspendLayout();
            // 
            // _ProjectIdBtn
            // 
            this._ProjectIdBtn.Location = new System.Drawing.Point(51, 49);
            this._ProjectIdBtn.Name = "_ProjectIdBtn";
            this._ProjectIdBtn.Size = new System.Drawing.Size(109, 49);
            this._ProjectIdBtn.TabIndex = 0;
            this._ProjectIdBtn.Text = "获取项目Id";
            this._ProjectIdBtn.UseVisualStyleBackColor = true;
            // 
            // _PriceStepListBtn
            // 
            this._PriceStepListBtn.Location = new System.Drawing.Point(51, 104);
            this._PriceStepListBtn.Name = "_PriceStepListBtn";
            this._PriceStepListBtn.Size = new System.Drawing.Size(109, 49);
            this._PriceStepListBtn.TabIndex = 2;
            this._PriceStepListBtn.Text = "查询水价及费用信息";
            this._PriceStepListBtn.UseVisualStyleBackColor = true;
            // 
            // _AddPriceBtn
            // 
            this._AddPriceBtn.Location = new System.Drawing.Point(51, 159);
            this._AddPriceBtn.Name = "_AddPriceBtn";
            this._AddPriceBtn.Size = new System.Drawing.Size(109, 49);
            this._AddPriceBtn.TabIndex = 3;
            this._AddPriceBtn.Text = "添加水价";
            this._AddPriceBtn.UseVisualStyleBackColor = true;
            // 
            // _AddFeeBtn
            // 
            this._AddFeeBtn.Location = new System.Drawing.Point(51, 214);
            this._AddFeeBtn.Name = "_AddFeeBtn";
            this._AddFeeBtn.Size = new System.Drawing.Size(109, 49);
            this._AddFeeBtn.TabIndex = 4;
            this._AddFeeBtn.Text = "添加费用";
            this._AddFeeBtn.UseVisualStyleBackColor = true;
            // 
            // _MeterTypelistBox
            // 
            this._MeterTypelistBox.FormattingEnabled = true;
            this._MeterTypelistBox.ItemHeight = 12;
            this._MeterTypelistBox.Location = new System.Drawing.Point(183, 49);
            this._MeterTypelistBox.Name = "_MeterTypelistBox";
            this._MeterTypelistBox.Size = new System.Drawing.Size(119, 184);
            this._MeterTypelistBox.TabIndex = 5;
            // 
            // _DgvPrice
            // 
            this._DgvPrice.AllowUserToAddRows = false;
            this._DgvPrice.AllowUserToDeleteRows = false;
            this._DgvPrice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._DgvPrice.BackgroundColor = System.Drawing.Color.White;
            this._DgvPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._DgvPrice.Location = new System.Drawing.Point(317, 49);
            this._DgvPrice.Name = "_DgvPrice";
            this._DgvPrice.ReadOnly = true;
            this._DgvPrice.RowHeadersVisible = false;
            this._DgvPrice.RowTemplate.Height = 23;
            this._DgvPrice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._DgvPrice.Size = new System.Drawing.Size(681, 184);
            this._DgvPrice.TabIndex = 6;
            // 
            // _DgvFee
            // 
            this._DgvFee.AllowUserToAddRows = false;
            this._DgvFee.AllowUserToDeleteRows = false;
            this._DgvFee.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._DgvFee.BackgroundColor = System.Drawing.Color.White;
            this._DgvFee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._DgvFee.Location = new System.Drawing.Point(317, 252);
            this._DgvFee.Name = "_DgvFee";
            this._DgvFee.ReadOnly = true;
            this._DgvFee.RowHeadersVisible = false;
            this._DgvFee.RowTemplate.Height = 23;
            this._DgvFee.Size = new System.Drawing.Size(681, 184);
            this._DgvFee.TabIndex = 7;
            // 
            // PriceStepView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 499);
            this.Controls.Add(this._DgvFee);
            this.Controls.Add(this._DgvPrice);
            this.Controls.Add(this._MeterTypelistBox);
            this.Controls.Add(this._AddFeeBtn);
            this.Controls.Add(this._AddPriceBtn);
            this.Controls.Add(this._PriceStepListBtn);
            this.Controls.Add(this._ProjectIdBtn);
            this.Name = "PriceStepView";
            this.Text = "PriceStepView";
            ((System.ComponentModel.ISupportInitialize)(this._DgvPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._DgvFee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _ProjectIdBtn;
        private System.Windows.Forms.Button _PriceStepListBtn;
        private System.Windows.Forms.Button _AddPriceBtn;
        private System.Windows.Forms.Button _AddFeeBtn;
        private System.Windows.Forms.ListBox _MeterTypelistBox;
        private System.Windows.Forms.DataGridView _DgvPrice;
        private System.Windows.Forms.DataGridView _DgvFee;
    }
}