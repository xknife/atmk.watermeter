﻿using System;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Common.Logging;
using NKnife.IoC;
using RestSharp;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    public partial class AreaAndStaffView : DockContent
    {
        private static readonly ILog _Logger = LogManager.GetLogger<AreaAndStaffView>();
        public AreaAndStaffView()
        {
            InitializeComponent();
            _GetAreaUseInfoButton.Click += (s, e) =>
            {
                GetAreaUse();
            };
        }

        private void GetAreaUse(string id="")
        {
            var kernel = DI.Get<Kernel>();
            var resoure = UrlManager.GetAreaUser + (id.Length>0?$"/{id}":"");
            var request = new RestRequest(resoure, Method.GET);
            request.AddHeader("X-Token", kernel.Token);
            kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                try
                {
                    _Logger.Debug(data);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }
    }
}
