﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Area;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Common.Utils;
using Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo.TempView;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    public partial class OwnerMeterView : DockContent
    {
        private static readonly ILog _Logger = LogManager.GetLogger<ChartDataView>();
        private readonly Kernel _kernel;
        public OwnerMeterView()
        {
            InitializeComponent();
            _kernel = DI.Get<Kernel>();
            BindDatas();
            BindClick();
        }
        /// <summary>
        /// 
        /// </summary>
        private void BindDatas()
        {
            GetAreaUse();
            GetDistricts();
        }

        private void GetDistricts(string id = "")
        {
            var kernel = DI.Get<Kernel>();
            var resoure = UrlManager.GetDistricts + (id.Length > 0 ? $"/{id}" : ""); ;
            var request = new RestRequest(resoure, Method.GET);
            request.AddHeader("X-Token", kernel.Token);
            kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                try
                {
                    _Logger.Debug(data);
                    var result = JsonConvert.DeserializeObject<TmpViewDistricts>(data);
                    var list = new Dictionary<string, string>();
                    result?.data.parentDistrict.ForEach(s => list[s.parentDistrictId] = s.parentDistrictName);
                    BindList(_DistrictListBox,list);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }

        private void GetAreaUse(string id = "")
        {
            var kernel = DI.Get<Kernel>();
            var resoure = UrlManager.GetAreaUser + (id.Length > 0 ? $"/{id}" : "");
            var request = new RestRequest(resoure, Method.GET);
            request.AddHeader("X-Token", kernel.Token);
            kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                try
                {
                    _Logger.Debug(data);
                    var result = JsonConvert.DeserializeObject<AreaOperatorResult>(data);
                    var list = new Dictionary<string, string>();
                    result?.data.siteList.ForEach(s=> list[s.id]=s.siteName);
                    BindList(_AreaListBox, list);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }
        private void BindClick()
        {
            _SelOwnerMeterButton.Click += (s, e) =>
            {
                SelectOwnerMeter();
            };
            _GetChangeMeters.Click += (s, e) => { ChangeMeters(); };
        }

        private void SelectOwnerMeter(string id = "")
        {
            var resoure = UrlManager.SelectSearch + (id.Length > 0 ? $"/{id}" : "");
            var request = new RestRequest(resoure, Method.POST);
            request.AddHeader("X-Token", _kernel.Token);
            var aid = _DistrictListBox.SelectedValue.ToString();
            var pm = new PostOwnerMeter { aid = aid, page = 0,meterState=0, searchType = "", searchValue = "" };
            request.AddJsonBody(pm);
            _kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                try
                {
                    _Logger.Debug(data);

                    BindTree(data);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }

        private void ChangeMeters()
        {
            var request = new RestRequest(UrlManager.GetQueryOwMe, Method.POST);
            request.AddHeader("X-Token", _kernel.Token);
            var pm = new PostMeter { aid = "", page = 0, searchType = "", searchValue = "" };
            request.AddJsonBody(pm);
            _kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                try
                {
                    _Logger.Debug(data);

                    BindTree(data);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }

        private void BindTree(string data)
        {
            Invoke((EventHandler)delegate{
                JsonTreeViewHelper.BindTreeView(_ResultTreeView, data);
            });
        }

        private void BindList(System.Windows.Forms.ListBox listBox  ,Dictionary<string,string> value)
        {
            Invoke((EventHandler) delegate
            {
                var item = value.Select(a => new
                {
                    key = a.Key,
                    value = a.Value
                });
                listBox.DataSource = item.ToList();
                listBox.DisplayMember = "value";
                listBox.ValueMember = "key";
            });
        }
    }
    public class PostMeter
    {
        /// <summary>
        /// 
        /// </summary>
        public string aid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string searchType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string searchValue { get; set; }
    }

    public class PostOwnerMeter : PostMeter
    {
        /// <summary>
        /// 表状态
        /// </summary>
        public int meterState { get; set; }
    }

}
