﻿using System;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper;
using Common.Logging;
using NKnife.IoC;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    public partial class AuthorityView : DockContent
    {
        private readonly MockParam _mockParam = new MockParam();
        private static readonly ILog _Logger = LogManager.GetLogger<AuthorityView>();
        public AuthorityView()
        {
            InitializeComponent();
        }
        private void _CanControlButton_Click(object sender, EventArgs e)
        {
            var kernel = DI.Get<Kernel>();
            if (!kernel.RoleName.Contains("超级管理员"))
            {
                //_BuildAreaButton.Enabled = false;
                //_BuildStaffButton.Enabled = false;
                _Logger.Info($"{kernel.RoleName[0]},可使用部分操作。");
            }
            else
            {
                _Logger.Info($"超级管理员,可使用所有操作。");
            }
        }
        private void _BuildStaffButton_Click(object sender, EventArgs e)
        {
            new UtilBuildStaff().Run(_mockParam.staffCount);
            _Logger.Info($"回龙观地区{_mockParam.staffCount}个操作员创建完毕。");
        }



        private void _BuildRoleButton_Click(object sender, EventArgs e)
        {
            new UtilBuildRole().Run();
            _Logger.Info($"回龙观地区1个角色创建完毕。");
        }
    }
}
