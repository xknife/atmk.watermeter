﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class ChartDataView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._dataTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._listBox1 = new System.Windows.Forms.ListBox();
            this._propertyGrid9 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid8 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid7 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid6 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid5 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid4 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid3 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this._propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(48, 25);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(881, 531);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._dataTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(873, 498);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "数据";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _dataTextBox
            // 
            this._dataTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataTextBox.Location = new System.Drawing.Point(3, 4);
            this._dataTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._dataTextBox.MaxLength = 3276700;
            this._dataTextBox.Multiline = true;
            this._dataTextBox.Name = "_dataTextBox";
            this._dataTextBox.ReadOnly = true;
            this._dataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataTextBox.Size = new System.Drawing.Size(867, 490);
            this._dataTextBox.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._listBox1);
            this.tabPage2.Controls.Add(this._propertyGrid9);
            this.tabPage2.Controls.Add(this._propertyGrid8);
            this.tabPage2.Controls.Add(this._propertyGrid7);
            this.tabPage2.Controls.Add(this._propertyGrid6);
            this.tabPage2.Controls.Add(this._propertyGrid5);
            this.tabPage2.Controls.Add(this._propertyGrid4);
            this.tabPage2.Controls.Add(this._propertyGrid3);
            this.tabPage2.Controls.Add(this._propertyGrid2);
            this.tabPage2.Controls.Add(this._propertyGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(935, 718);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "数据分项";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _listBox1
            // 
            this._listBox1.FormattingEnabled = true;
            this._listBox1.ItemHeight = 17;
            this._listBox1.Location = new System.Drawing.Point(552, 8);
            this._listBox1.Name = "_listBox1";
            this._listBox1.Size = new System.Drawing.Size(345, 650);
            this._listBox1.TabIndex = 9;
            // 
            // _propertyGrid9
            // 
            this._propertyGrid9.Location = new System.Drawing.Point(371, 454);
            this._propertyGrid9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid9.Name = "_propertyGrid9";
            this._propertyGrid9.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid9.TabIndex = 8;
            // 
            // _propertyGrid8
            // 
            this._propertyGrid8.Location = new System.Drawing.Point(190, 454);
            this._propertyGrid8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid8.Name = "_propertyGrid8";
            this._propertyGrid8.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid8.TabIndex = 7;
            // 
            // _propertyGrid7
            // 
            this._propertyGrid7.Location = new System.Drawing.Point(9, 454);
            this._propertyGrid7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid7.Name = "_propertyGrid7";
            this._propertyGrid7.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid7.TabIndex = 6;
            // 
            // _propertyGrid6
            // 
            this._propertyGrid6.Location = new System.Drawing.Point(371, 231);
            this._propertyGrid6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid6.Name = "_propertyGrid6";
            this._propertyGrid6.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid6.TabIndex = 5;
            // 
            // _propertyGrid5
            // 
            this._propertyGrid5.Location = new System.Drawing.Point(190, 231);
            this._propertyGrid5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid5.Name = "_propertyGrid5";
            this._propertyGrid5.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid5.TabIndex = 4;
            // 
            // _propertyGrid4
            // 
            this._propertyGrid4.Location = new System.Drawing.Point(9, 231);
            this._propertyGrid4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid4.Name = "_propertyGrid4";
            this._propertyGrid4.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid4.TabIndex = 3;
            // 
            // _propertyGrid3
            // 
            this._propertyGrid3.Location = new System.Drawing.Point(371, 8);
            this._propertyGrid3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid3.Name = "_propertyGrid3";
            this._propertyGrid3.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid3.TabIndex = 2;
            // 
            // _propertyGrid2
            // 
            this._propertyGrid2.Location = new System.Drawing.Point(190, 8);
            this._propertyGrid2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid2.Name = "_propertyGrid2";
            this._propertyGrid2.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid2.TabIndex = 1;
            // 
            // _propertyGrid1
            // 
            this._propertyGrid1.Location = new System.Drawing.Point(9, 8);
            this._propertyGrid1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid1.Name = "_propertyGrid1";
            this._propertyGrid1.Size = new System.Drawing.Size(175, 215);
            this._propertyGrid1.TabIndex = 0;
            // 
            // ChartDataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 531);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ChartDataView";
            this.Text = "ChartDataView";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox _dataTextBox;
        private System.Windows.Forms.PropertyGrid _propertyGrid1;
        private System.Windows.Forms.PropertyGrid _propertyGrid8;
        private System.Windows.Forms.PropertyGrid _propertyGrid7;
        private System.Windows.Forms.PropertyGrid _propertyGrid6;
        private System.Windows.Forms.PropertyGrid _propertyGrid5;
        private System.Windows.Forms.PropertyGrid _propertyGrid4;
        private System.Windows.Forms.PropertyGrid _propertyGrid3;
        private System.Windows.Forms.PropertyGrid _propertyGrid2;
        private System.Windows.Forms.PropertyGrid _propertyGrid9;
        private System.Windows.Forms.ListBox _listBox1;
    }
}