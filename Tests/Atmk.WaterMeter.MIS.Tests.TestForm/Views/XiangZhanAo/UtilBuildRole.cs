﻿using System;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{

    public class UtilBuildRole
    {
        private static readonly ILog _Logger = LogManager.GetLogger<UtilBuildDistrict>();
        private readonly CurrentMock _current;
        private readonly Kernel _kernel;

        public UtilBuildRole()
        {
            _kernel = DI.Get<Kernel>();
            _current = DI.Get<CurrentMock>();
        }
        public void Run()
        {
            var request = new RestRequest(UrlManager.InsertRoles, Method.POST);
            request.AddHeader("X-Token", _kernel.Token);
            var d = new Role
            {
                roleName = $"普通操作员",
                remark = ""
            };
            request.AddJsonBody(d);
            _kernel.Client.ExecuteAsync(request, response =>
            {
                var data = response.Content;
                _Logger.Trace(data);
                try
                {
                    var staffResult = JsonConvert.DeserializeObject<BaseAddResult>(data);
                    var staffid = staffResult.id;
                    _current.RoleId = staffid;
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }

                _Logger.Trace($"{d.ToString()}创建成功");
            });
        }
    }
}

