﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class CT_NB_IoTPlatformView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._listBox = new System.Windows.Forms.ListBox();
            this._DeviceId = new System.Windows.Forms.TextBox();
            this._MeterNoText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._DelDevice = new System.Windows.Forms.Button();
            this._AddDevice = new System.Windows.Forms.Button();
            this._IMEI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._DevicesBtn = new System.Windows.Forms.Button();
            this._ReadMetersBtn = new System.Windows.Forms.Button();
            this._ValveOpenBtn = new System.Windows.Forms.Button();
            this._ValveCloseBtn = new System.Windows.Forms.Button();
            this._PropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // _listBox
            // 
            this._listBox.FormattingEnabled = true;
            this._listBox.ItemHeight = 12;
            this._listBox.Location = new System.Drawing.Point(12, 12);
            this._listBox.Name = "_listBox";
            this._listBox.Size = new System.Drawing.Size(187, 472);
            this._listBox.TabIndex = 13;
            // 
            // _DeviceId
            // 
            this._DeviceId.Enabled = false;
            this._DeviceId.Location = new System.Drawing.Point(303, 100);
            this._DeviceId.Name = "_DeviceId";
            this._DeviceId.Size = new System.Drawing.Size(332, 21);
            this._DeviceId.TabIndex = 12;
            // 
            // _MeterNoText
            // 
            this._MeterNoText.Location = new System.Drawing.Point(303, 16);
            this._MeterNoText.Name = "_MeterNoText";
            this._MeterNoText.Size = new System.Drawing.Size(332, 21);
            this._MeterNoText.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "平台设备Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "水表编号";
            // 
            // _DelDevice
            // 
            this._DelDevice.Location = new System.Drawing.Point(364, 150);
            this._DelDevice.Name = "_DelDevice";
            this._DelDevice.Size = new System.Drawing.Size(119, 52);
            this._DelDevice.TabIndex = 8;
            this._DelDevice.Text = "删除设备";
            this._DelDevice.UseVisualStyleBackColor = true;
            // 
            // _AddDevice
            // 
            this._AddDevice.Location = new System.Drawing.Point(218, 150);
            this._AddDevice.Name = "_AddDevice";
            this._AddDevice.Size = new System.Drawing.Size(119, 52);
            this._AddDevice.TabIndex = 7;
            this._AddDevice.Text = "注册设备";
            this._AddDevice.UseVisualStyleBackColor = true;
            // 
            // _IMEI
            // 
            this._IMEI.Location = new System.Drawing.Point(303, 59);
            this._IMEI.Name = "_IMEI";
            this._IMEI.Size = new System.Drawing.Size(332, 21);
            this._IMEI.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "IMEI";
            // 
            // _DevicesBtn
            // 
            this._DevicesBtn.Location = new System.Drawing.Point(516, 150);
            this._DevicesBtn.Name = "_DevicesBtn";
            this._DevicesBtn.Size = new System.Drawing.Size(119, 52);
            this._DevicesBtn.TabIndex = 16;
            this._DevicesBtn.Text = "获取所有设备信息";
            this._DevicesBtn.UseVisualStyleBackColor = true;
            // 
            // _ReadMetersBtn
            // 
            this._ReadMetersBtn.Location = new System.Drawing.Point(218, 222);
            this._ReadMetersBtn.Name = "_ReadMetersBtn";
            this._ReadMetersBtn.Size = new System.Drawing.Size(119, 52);
            this._ReadMetersBtn.TabIndex = 17;
            this._ReadMetersBtn.Text = "读取设备数据";
            this._ReadMetersBtn.UseVisualStyleBackColor = true;
            // 
            // _ValveOpenBtn
            // 
            this._ValveOpenBtn.Location = new System.Drawing.Point(364, 222);
            this._ValveOpenBtn.Name = "_ValveOpenBtn";
            this._ValveOpenBtn.Size = new System.Drawing.Size(119, 52);
            this._ValveOpenBtn.TabIndex = 18;
            this._ValveOpenBtn.Text = "开阀";
            this._ValveOpenBtn.UseVisualStyleBackColor = true;
            // 
            // _ValveCloseBtn
            // 
            this._ValveCloseBtn.Location = new System.Drawing.Point(516, 222);
            this._ValveCloseBtn.Name = "_ValveCloseBtn";
            this._ValveCloseBtn.Size = new System.Drawing.Size(119, 52);
            this._ValveCloseBtn.TabIndex = 19;
            this._ValveCloseBtn.Text = "关阀";
            this._ValveCloseBtn.UseVisualStyleBackColor = true;
            // 
            // _PropertyGrid
            // 
            this._PropertyGrid.Location = new System.Drawing.Point(218, 280);
            this._PropertyGrid.Name = "_PropertyGrid";
            this._PropertyGrid.Size = new System.Drawing.Size(417, 204);
            this._PropertyGrid.TabIndex = 20;
            // 
            // CT_NB_IoTPlatformView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 517);
            this.Controls.Add(this._PropertyGrid);
            this.Controls.Add(this._ValveCloseBtn);
            this.Controls.Add(this._ValveOpenBtn);
            this.Controls.Add(this._ReadMetersBtn);
            this.Controls.Add(this._DevicesBtn);
            this.Controls.Add(this._IMEI);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._listBox);
            this.Controls.Add(this._DeviceId);
            this.Controls.Add(this._MeterNoText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._DelDevice);
            this.Controls.Add(this._AddDevice);
            this.Name = "CT_NB_IoTPlatformView";
            this.Text = "CT_NB_IoTPlatformView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _listBox;
        private System.Windows.Forms.TextBox _DeviceId;
        private System.Windows.Forms.TextBox _MeterNoText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _DelDevice;
        private System.Windows.Forms.Button _AddDevice;
        private System.Windows.Forms.TextBox _IMEI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _DevicesBtn;
        private System.Windows.Forms.Button _ReadMetersBtn;
        private System.Windows.Forms.Button _ValveOpenBtn;
        private System.Windows.Forms.Button _ValveCloseBtn;
        private System.Windows.Forms.PropertyGrid _PropertyGrid;
    }
}