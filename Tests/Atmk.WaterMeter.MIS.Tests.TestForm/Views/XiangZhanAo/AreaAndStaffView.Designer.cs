﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class AreaAndStaffView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._GetAreaUseInfoButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _GetAreaUseInfoButton
            // 
            this._GetAreaUseInfoButton.Location = new System.Drawing.Point(12, 12);
            this._GetAreaUseInfoButton.Name = "_GetAreaUseInfoButton";
            this._GetAreaUseInfoButton.Size = new System.Drawing.Size(188, 39);
            this._GetAreaUseInfoButton.TabIndex = 0;
            this._GetAreaUseInfoButton.Text = "获取项目和操作员信息";
            this._GetAreaUseInfoButton.UseVisualStyleBackColor = true;
            // 
            // AreaAndStaffView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 483);
            this.Controls.Add(this._GetAreaUseInfoButton);
            this.Name = "AreaAndStaffView";
            this.Text = "AreaAndStaffView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _GetAreaUseInfoButton;
    }
}