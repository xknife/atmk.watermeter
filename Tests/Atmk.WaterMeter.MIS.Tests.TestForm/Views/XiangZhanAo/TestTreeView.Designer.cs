﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    partial class TestTreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._TreeView = new System.Windows.Forms.TreeView();
            this._GetTreeButton = new System.Windows.Forms.Button();
            this._AddDistrictButton = new System.Windows.Forms.Button();
            this._ProjecCombox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._DeleteButton = new System.Windows.Forms.Button();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this._SelectNodeInfo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _TreeView
            // 
            this._TreeView.Location = new System.Drawing.Point(12, 12);
            this._TreeView.Name = "_TreeView";
            this._TreeView.Size = new System.Drawing.Size(350, 407);
            this._TreeView.TabIndex = 0;
            // 
            // _GetTreeButton
            // 
            this._GetTreeButton.Location = new System.Drawing.Point(420, 67);
            this._GetTreeButton.Name = "_GetTreeButton";
            this._GetTreeButton.Size = new System.Drawing.Size(95, 50);
            this._GetTreeButton.TabIndex = 1;
            this._GetTreeButton.Text = "获取树";
            this._GetTreeButton.UseVisualStyleBackColor = true;
            // 
            // _AddDistrictButton
            // 
            this._AddDistrictButton.Location = new System.Drawing.Point(420, 123);
            this._AddDistrictButton.Name = "_AddDistrictButton";
            this._AddDistrictButton.Size = new System.Drawing.Size(95, 50);
            this._AddDistrictButton.TabIndex = 2;
            this._AddDistrictButton.Text = "添加片区";
            this._AddDistrictButton.UseVisualStyleBackColor = true;
            // 
            // _ProjecCombox
            // 
            this._ProjecCombox.FormattingEnabled = true;
            this._ProjecCombox.Location = new System.Drawing.Point(434, 17);
            this._ProjecCombox.Name = "_ProjecCombox";
            this._ProjecCombox.Size = new System.Drawing.Size(121, 20);
            this._ProjecCombox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(374, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "选中项目";
            // 
            // _DeleteButton
            // 
            this._DeleteButton.Location = new System.Drawing.Point(420, 236);
            this._DeleteButton.Name = "_DeleteButton";
            this._DeleteButton.Size = new System.Drawing.Size(95, 50);
            this._DeleteButton.TabIndex = 5;
            this._DeleteButton.Text = "删除片区";
            this._DeleteButton.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid1.Location = new System.Drawing.Point(575, 12);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(213, 426);
            this.propertyGrid1.TabIndex = 6;
            // 
            // _SelectNodeInfo
            // 
            this._SelectNodeInfo.Location = new System.Drawing.Point(420, 179);
            this._SelectNodeInfo.Name = "_SelectNodeInfo";
            this._SelectNodeInfo.Size = new System.Drawing.Size(95, 50);
            this._SelectNodeInfo.TabIndex = 7;
            this._SelectNodeInfo.Text = "读取片区详细信息";
            this._SelectNodeInfo.UseVisualStyleBackColor = true;
            // 
            // TestTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._SelectNodeInfo);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this._DeleteButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._ProjecCombox);
            this.Controls.Add(this._AddDistrictButton);
            this.Controls.Add(this._GetTreeButton);
            this.Controls.Add(this._TreeView);
            this.Name = "TestTreeView";
            this.Text = "树状图测试";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView _TreeView;
        private System.Windows.Forms.Button _GetTreeButton;
        private System.Windows.Forms.Button _AddDistrictButton;
        private System.Windows.Forms.ComboBox _ProjecCombox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _DeleteButton;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Button _SelectNodeInfo;
    }
}