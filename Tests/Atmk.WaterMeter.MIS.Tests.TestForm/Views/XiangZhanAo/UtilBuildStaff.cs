﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Dialogs;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Views.XiangZhanAo
{
    public class UtilBuildStaff
    {
        private static readonly ILog _Logger = LogManager.GetLogger<UtilBuildDistrict>();
        private readonly CurrentMock _current;
        private readonly Kernel _kernel;

        public UtilBuildStaff()
        {
            _kernel = DI.Get<Kernel>();
            _current = DI.Get<CurrentMock>();
        }

        public void Run(int count)
        {
          for (var i = 0; i < count; i++)
            {
                var request = new RestRequest(UrlManager.InsertStaff, Method.POST);
                request.AddHeader("X-Token", _kernel.Token);

                var name = _current.GetStaffName();
                var d = new Staff
                {
                    siteId = _current.AreaId,
                    mobile = _current.GetMobile(),
                    surname = name,
                    pageLines = 30.ToString(),
                    password = "123",
                    operatorName = "test",
                    roleId = _current.RoleId,
                    status = "正常",
                    district = new List<string>()
                };
                request.AddJsonBody(d);
                _kernel.Client.ExecuteAsync(request, response =>
                {
                    var data = response.Content;
                    _Logger.Trace(data);
                    try
                    {
                        var staffResult = JsonConvert.DeserializeObject<BaseAddResult>(data);
                        var staffid = staffResult.id;
                        _current.StaffId = staffid;
                    }
                    catch (Exception exception)
                    {
                        _Logger.Error(exception);
                    }

                    _Logger.Trace($"{d.ToString()}创建成功");
                });
            }
        }

       

        public void RunGetRoleName()
        {
            var request = new RestRequest(UrlManager.GetUserInfo, Method.GET);
            request.AddHeader("X-Token", _kernel.Token);
            _kernel.Client.ExecuteAsync(request, response =>
            {
                _Logger.Trace(response.ResponseUri.AbsoluteUri);
                _Logger.Debug(response.Content);
                try
                {
                    var result = JsonConvert.DeserializeObject<LoginDialog.UserInfoResult>(response.Content);
                    _kernel.RoleName = result.data.role.ToList();
                    _Logger.Trace(_kernel.RoleName);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }
            });
        }
    }
}