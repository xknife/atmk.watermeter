﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock
{
    partial class MockDistrictDataView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._propertyGrid = new System.Windows.Forms.PropertyGrid();
            this._ClearDataButton = new System.Windows.Forms.Button();
            this._listBox = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._singleButton = new System.Windows.Forms.Button();
            this._smallDataButton = new System.Windows.Forms.Button();
            this._largeDataButton = new System.Windows.Forms.Button();
            this._BuildMockHistoryDataButton = new System.Windows.Forms.Button();
            this._BuildOwnerButton = new System.Windows.Forms.Button();
            this._SimulationButton = new System.Windows.Forms.Button();
            this._BuildDistrictButton = new System.Windows.Forms.Button();
            this._BuildAreaButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _propertyGrid
            // 
            this._propertyGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this._propertyGrid.HelpVisible = false;
            this._propertyGrid.Location = new System.Drawing.Point(0, 0);
            this._propertyGrid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._propertyGrid.Name = "_propertyGrid";
            this._propertyGrid.Size = new System.Drawing.Size(194, 566);
            this._propertyGrid.TabIndex = 0;
            // 
            // _ClearDataButton
            // 
            this._ClearDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ClearDataButton.BackColor = System.Drawing.Color.Khaki;
            this._ClearDataButton.Location = new System.Drawing.Point(705, 12);
            this._ClearDataButton.Name = "_ClearDataButton";
            this._ClearDataButton.Size = new System.Drawing.Size(118, 28);
            this._ClearDataButton.TabIndex = 2;
            this._ClearDataButton.Text = "数据清理";
            this._ClearDataButton.UseVisualStyleBackColor = false;
            this._ClearDataButton.Click += new System.EventHandler(this._ClearDataButton_Click);
            // 
            // _listBox
            // 
            this._listBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._listBox.FormattingEnabled = true;
            this._listBox.ItemHeight = 17;
            this._listBox.Location = new System.Drawing.Point(200, 0);
            this._listBox.Name = "_listBox";
            this._listBox.Size = new System.Drawing.Size(493, 565);
            this._listBox.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._BuildAreaButton);
            this.groupBox1.Controls.Add(this._BuildDistrictButton);
            this.groupBox1.Controls.Add(this._SimulationButton);
            this.groupBox1.Controls.Add(this._BuildOwnerButton);
            this.groupBox1.Controls.Add(this._BuildMockHistoryDataButton);
            this.groupBox1.Location = new System.Drawing.Point(705, 180);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(118, 354);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "随机仿真数据";
            // 
            // _singleButton
            // 
            this._singleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._singleButton.Location = new System.Drawing.Point(705, 60);
            this._singleButton.Name = "_singleButton";
            this._singleButton.Size = new System.Drawing.Size(118, 28);
            this._singleButton.TabIndex = 9;
            this._singleButton.Text = "参数: 单个数据";
            this._singleButton.UseVisualStyleBackColor = true;
            this._singleButton.Click += new System.EventHandler(this._singleButton_Click);
            // 
            // _smallDataButton
            // 
            this._smallDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._smallDataButton.Location = new System.Drawing.Point(705, 91);
            this._smallDataButton.Name = "_smallDataButton";
            this._smallDataButton.Size = new System.Drawing.Size(118, 28);
            this._smallDataButton.TabIndex = 10;
            this._smallDataButton.Text = "参数: 小数据";
            this._smallDataButton.UseVisualStyleBackColor = true;
            this._smallDataButton.Click += new System.EventHandler(this._smallDataButton_Click);
            // 
            // _largeDataButton
            // 
            this._largeDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._largeDataButton.Location = new System.Drawing.Point(705, 123);
            this._largeDataButton.Name = "_largeDataButton";
            this._largeDataButton.Size = new System.Drawing.Size(118, 28);
            this._largeDataButton.TabIndex = 11;
            this._largeDataButton.Text = "参数: 大数据";
            this._largeDataButton.UseVisualStyleBackColor = true;
            this._largeDataButton.Click += new System.EventHandler(this._largeDataButton_Click);
            // 
            // _BuildMockHistoryDataButton
            // 
            this._BuildMockHistoryDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildMockHistoryDataButton.Location = new System.Drawing.Point(6, 122);
            this._BuildMockHistoryDataButton.Name = "_BuildMockHistoryDataButton";
            this._BuildMockHistoryDataButton.Size = new System.Drawing.Size(106, 28);
            this._BuildMockHistoryDataButton.TabIndex = 5;
            this._BuildMockHistoryDataButton.Text = "模拟历史报数";
            this._BuildMockHistoryDataButton.UseVisualStyleBackColor = true;
            this._BuildMockHistoryDataButton.Click += new System.EventHandler(this._BuildMockHistoryDataButton_Click);
            // 
            // _BuildOwnerButton
            // 
            this._BuildOwnerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildOwnerButton.Location = new System.Drawing.Point(6, 88);
            this._BuildOwnerButton.Name = "_BuildOwnerButton";
            this._BuildOwnerButton.Size = new System.Drawing.Size(106, 28);
            this._BuildOwnerButton.TabIndex = 4;
            this._BuildOwnerButton.Text = "创建业主与水表";
            this._BuildOwnerButton.UseVisualStyleBackColor = true;
            this._BuildOwnerButton.Click += new System.EventHandler(this._BuildOwnerButton_Click);
            // 
            // _SimulationButton
            // 
            this._SimulationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._SimulationButton.Location = new System.Drawing.Point(6, 156);
            this._SimulationButton.Name = "_SimulationButton";
            this._SimulationButton.Size = new System.Drawing.Size(106, 28);
            this._SimulationButton.TabIndex = 7;
            this._SimulationButton.Text = "仿真报数";
            this._SimulationButton.UseVisualStyleBackColor = true;
            this._SimulationButton.Click += new System.EventHandler(this._SimulationButton_Click);
            // 
            // _BuildDistrictButton
            // 
            this._BuildDistrictButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildDistrictButton.Location = new System.Drawing.Point(6, 54);
            this._BuildDistrictButton.Name = "_BuildDistrictButton";
            this._BuildDistrictButton.Size = new System.Drawing.Size(106, 28);
            this._BuildDistrictButton.TabIndex = 3;
            this._BuildDistrictButton.Text = "创建分区";
            this._BuildDistrictButton.UseVisualStyleBackColor = true;
            this._BuildDistrictButton.Click += new System.EventHandler(this._Build35DistrictButton_Click);
            // 
            // _BuildAreaButton
            // 
            this._BuildAreaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._BuildAreaButton.Location = new System.Drawing.Point(6, 20);
            this._BuildAreaButton.Name = "_BuildAreaButton";
            this._BuildAreaButton.Size = new System.Drawing.Size(106, 28);
            this._BuildAreaButton.TabIndex = 8;
            this._BuildAreaButton.Text = "创建项目";
            this._BuildAreaButton.UseVisualStyleBackColor = true;
            this._BuildAreaButton.Click += new System.EventHandler(this._BuildAreaButton_Click);
            // 
            // MockDistrictDataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 566);
            this.Controls.Add(this._largeDataButton);
            this.Controls.Add(this._smallDataButton);
            this.Controls.Add(this._singleButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._listBox);
            this.Controls.Add(this._ClearDataButton);
            this.Controls.Add(this._propertyGrid);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MockDistrictDataView";
            this.Text = "回龙观小区压力仿真";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PropertyGrid _propertyGrid;
        private System.Windows.Forms.Button _ClearDataButton;
        private System.Windows.Forms.ListBox _listBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _singleButton;
        private System.Windows.Forms.Button _smallDataButton;
        private System.Windows.Forms.Button _largeDataButton;
        private System.Windows.Forms.Button _BuildDistrictButton;
        private System.Windows.Forms.Button _SimulationButton;
        private System.Windows.Forms.Button _BuildOwnerButton;
        private System.Windows.Forms.Button _BuildMockHistoryDataButton;
        private System.Windows.Forms.Button _BuildAreaButton;
    }
}