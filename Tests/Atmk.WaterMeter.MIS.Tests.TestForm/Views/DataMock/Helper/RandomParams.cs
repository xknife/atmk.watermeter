﻿using NKnife.Utility;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper
{
    /// <summary>
    /// KeyData的随机生成参数
    /// </summary>
    class RandomParams
    {
        private readonly UtilityRandom _random = new UtilityRandom();

        public RandomParams()
        {
            MaxYongShuiLiang = _random.Next(5 , 12);
            MinYongShuiLiang = _random.Next(1 , 5);
        }

        public int MaxYongShuiLiang { get; set; }
        public int MinYongShuiLiang { get; set; }

    }
}