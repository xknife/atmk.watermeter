﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper
{
    public class UtilBuildArea
    {
        private static readonly ILog _Logger = LogManager.GetLogger<UtilBuildDistrict>();
        private readonly CurrentMock _current;

        private readonly Kernel _kernel;

        public UtilBuildArea()
        {
            _kernel = DI.Get<Kernel>();
            _current = DI.Get<CurrentMock>();
        }
        public void Run(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var request = new RestRequest(UrlManager.InsertArea, Method.POST);
                request.AddHeader("X-Token", _kernel.Token);

                var name = _current.GetAreaName();
                var d = new Area
                {
                    siteName = name,
                    siteAddress = "回龙观11街24楼101街道办",
                    longitude = 555.11,
                    latitude = 888.777,
                    elevation=0,
                    remark=""
                };
                request.AddJsonBody(d);
                _kernel.Client.ExecuteAsync(request, response =>
                {
                    var data = response.Content;
                    _Logger.Trace(data);
                    try
                    {
                        var areaReturn = JsonConvert.DeserializeObject<BaseAddResult>(data);
                        var areaId = areaReturn.id;
                        _current.AreaId = areaId;
                    }
                    catch (Exception exception)
                    {
                        _Logger.Error(exception);
                    }

                    _Logger.Trace($"{d.ToString()}创建成功");
                });
            }
        }
    }
}
