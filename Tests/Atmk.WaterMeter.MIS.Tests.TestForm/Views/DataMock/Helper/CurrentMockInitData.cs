﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper
{
    class CurrentMockInitData
    {
        public List<string> Names { get; }
        public Dictionary<int, string> NumToZh { get; }

        public List<string> StaffNames { get; }
        public List<string> FamilyNames { get; }

        public CurrentMockInitData()
        {
            Names = new List<string>(new[]
            {
                "学富", "经纶苑", "学贯", "博学府", "云趣", "卓尔园", "团锦", "繁花庄", "似锦", "百花", "姹紫", "红山峰",
                "万里", "碧空苑", "湛蓝", "森林里", "美玉", "清秀园", "秋爽", "丹桂庄", "高云", "红叶", "金风", "公园里"
            });
            NumToZh = new Dictionary<int, string>();
            NumToZh.Add(1, "一");
            NumToZh.Add(2, "二");
            NumToZh.Add(3, "三");
            NumToZh.Add(4, "四");
            NumToZh.Add(5, "五");
            NumToZh.Add(6, "六");
            NumToZh.Add(7, "七");
            NumToZh.Add(8, "八");
            NumToZh.Add(9, "九");

            StaffNames = new List<string>(new[]
            {
                "学富", "经纶", "学贯", "博学", "云趣", "卓尔", "团锦", "繁花", "似锦", "百花", "姹紫", "红山",
                "万里", "碧空", "湛蓝", "森林", "美玉", "清秀", "秋爽", "丹桂", "高云", "红叶", "金风", "园里"
            });
            FamilyNames = new List<string>(new[]
            {
                "赵", "钱", "孙", "李", "周", "武", "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨",
                "朱", "秦", "尤", "许", "何", "吕"
            });
        }
    }
}