﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Dialogs;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using NKnife.Utility;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper
{
    class UtilBuildOwner
    {
        private static readonly ILog _Logger = LogManager.GetLogger<UtilBuildOwner>();

        private readonly Kernel _kernel;
        private readonly CurrentMock _current;
        private readonly UtilityRandom _random = new UtilityRandom();

        public UtilBuildOwner()
        {
            _kernel = DI.Get<Kernel>();
            _current = DI.Get<CurrentMock>();
        }

        public void Run(MockParam param)
        {
            //每小区有20-40幢楼，每楼4-8单元，每单元6-30层，每层4-8户
            int lou = _random.Next(param.楼min, param.楼max);
            int danyuan = _random.Next(param.单元min, param.单元max);
            int ceng = _random.Next(param.层min, param.层max);
            int hu = _random.Next(param.户min, param.户max);

            var count = _current.Districts.Count * lou * danyuan * ceng * hu;
            var msg = $"随机生成{count}个业主及水表";
            _Logger.Info($"准备{msg}");

            if (count > 1)
            {
                if (MessageBox.Show($@"准备{msg}", @"Ready", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return;
                }
            }

            foreach (var district in _current.Districts)
            {
                for (int a = 0; a < lou; a++)
                {
                    for (int b = 0; b < danyuan; b++)
                    {
                        for (int c = 0; c < ceng; c++)
                        {
                            for (int d = 0; d < hu; d++)
                            {
                                var request = new RestRequest(UrlManager.AddOwnerMeter, Method.POST);
                                request.AddHeader("X-Token", _kernel.Token);

                                var did = district.Key;
                                var owner = new Owner();
                                owner.districtCode = did;
                                owner.houseNumber = $"{a + 1}楼{b + 1}单元{c + 1}层{d + 1}室";
                                owner.latitude = 22.22;
                                owner.elevation = 33.33;
                                owner.longitude = 44.44;
                                var meter = new vo.WaterMeter();
                                meter.meterNumber = $"{_random.Next(111111, 555555)}{_random.Next(666666, 999999)}";
                                meter.commType = "CT_NB";
                                meter.meterDatas = new MeterDatas();

                                var wm = new MeterAndOwner(did) {owner = owner, waterMeter = meter};

                                request.AddJsonBody(wm);
                                _kernel.Client.ExecuteAsync(request, response =>
                                {
                                    var data = response.Content;
                                    _Logger.Trace(data);
                                    try
                                    {
                                        var respReturn = JsonConvert.DeserializeObject<ownerResult>(data);
                                        //保存所有表，key是这块表的NBId
                                        _current.MeterMap.Add(respReturn.respNbId, wm);
                                        _Logger.Trace($"{wm},NbID:{respReturn.respNbId}创建成功");
                                    }
                                    catch (Exception exception)
                                    {
                                        _Logger.Error(exception);
                                        _Logger.Trace($"{wm}创建失败");
                                    }
                                });
                                Thread.Sleep(5);
                            }
                        }
                    }
                }
            }
            if (count > 1)
                MessageBox.Show($@"{msg}...工作已完成");
        }
    }

    public class ownerResult : Result
    {
        public string respOwnerId { get; set; }
        public string respMeterId { get; set; }
        public string respNbId { get; set; }
    }
}