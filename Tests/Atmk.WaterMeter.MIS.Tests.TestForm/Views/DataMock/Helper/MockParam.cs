﻿using System.ComponentModel;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper
{
    internal class MockParam
    {
        [Category("系统")] [DisplayName("项目数量")] public int AreaCount { get; set; } = 1;
        [Category("系统")] [DisplayName("操作员人数")] public int staffCount { get; set; } = 1;

        [Category("总体")] [DisplayName("分区数量")] public int DistrictCount { get; set; } = 2;

        [Category("小区设置")] public int 楼max { get; set; } = 5; // 40;
        [Category("小区设置")] public int 楼min { get; set; } = 3; //20;

        [Category("小区设置")] public int 单元max { get; set; } = 5; // 8;
        [Category("小区设置")] public int 单元min { get; set; } = 3; //4;

        [Category("小区设置")] public int 层max { get; set; } = 6; //30;
        [Category("小区设置")] public int 层min { get; set; } = 4; //6;

        [Category("小区设置")] public int 户max { get; set; } = 3; //8;
        [Category("小区设置")] public int 户min { get; set; } = 2; //4;
    }
}