﻿using System;
using System.Threading;
using Atmk.WaterMeter.MIS.TestForm.Common;
using Atmk.WaterMeter.MIS.TestForm.Dialogs;
using Atmk.WaterMeter.MIS.TestForm.Views.DataMock.Helper;
using Common.Logging;
using Newtonsoft.Json;
using NKnife.IoC;
using RestSharp;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock
{
    public partial class MockDistrictDataView : DockContent
    {
        private readonly MockParam _mockParam = new MockParam();
        private static readonly ILog _Logger = LogManager.GetLogger<MockDistrictDataView>();

        public MockDistrictDataView()
        {
            InitializeComponent();
            _propertyGrid.SelectedObject = _mockParam;
        }

     
        private void _singleButton_Click(object sender, EventArgs e)
        {
            _mockParam.DistrictCount = 1;
            _mockParam.楼min = 1;
            _mockParam.楼max = 1;
            _mockParam.单元min = 1;
            _mockParam.单元max = 1;
            _mockParam.层min = 1;
            _mockParam.层max = 1;
            _mockParam.户min = 1;
            _mockParam.户max = 1;
            _propertyGrid.Refresh();
        }

        private void _smallDataButton_Click(object sender, EventArgs e)
        {
            _mockParam.DistrictCount = 3;
            _mockParam.楼min = 3;
            _mockParam.楼max = 5;
            _mockParam.单元min = 3;
            _mockParam.单元max = 5;
            _mockParam.层min = 3;
            _mockParam.层max = 5;
            _mockParam.户min = 2;
            _mockParam.户max = 4;
            _propertyGrid.Refresh();
        }

        private void _largeDataButton_Click(object sender, EventArgs e)
        {
            _mockParam.DistrictCount = 35;
            _mockParam.楼min = 30;
            _mockParam.楼max = 40;
            _mockParam.单元min = 4;
            _mockParam.单元max = 8;
            _mockParam.层min = 6;
            _mockParam.层max = 20;
            _mockParam.户min = 4;
            _mockParam.户max = 8;
            _propertyGrid.Refresh();
        }

        private void _ClearDataButton_Click(object sender, EventArgs e)
        {
            var kernel = DI.Get<Kernel>();
            var request = new RestRequest(UrlManager.ClearData, Method.DELETE);

            kernel.Client.ExecuteAsync(request, response =>
            {
                _Logger.Trace(response.ResponseUri.AbsoluteUri);
                _Logger.Debug(response.Content);
                try
                {
                    var signReturn = JsonConvert.DeserializeObject<Result>(response.Content);
                    _Logger.Debug(signReturn.errcode == 0 ? "数据库数据清理完成。" : "数据库数据清理失败。");
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                    _Logger.Error("数据库数据清理失败。");
                }
            });
          
        }

        private void _Build35DistrictButton_Click(object sender, EventArgs e)
        {
            new UtilBuildDistrict().Run(_mockParam.DistrictCount);
            LogView($"回龙观地区{_mockParam.DistrictCount}个小区创建完毕。");
        }

        private void _BuildOwnerButton_Click(object sender, EventArgs e)
        {
            new Thread(() => { new UtilBuildOwner().Run(_mockParam); }).Start();
            LogView("创建业主与水表线程启动");
        }

        private void _BuildMockHistoryDataButton_Click(object sender, EventArgs e)
        {
            new Thread(() => { new UtilHistory().Run(); }).Start();
            LogView("所有水表2年历史数据创建完毕。");
        }

        private void _SimulationButton_Click(object sender, EventArgs e)
        {
            LogView("水表自动报数仿真开始（每小时上报）。");
        }

        private void LogView(string msg)
        {
            _listBox.Items.Insert(0, $"{DateTime.Now:HH:mm:ss.fff}\t{msg}");
        }

        private void _BuildAreaButton_Click(object sender, EventArgs e)
        {
            new UtilBuildArea().Run(_mockParam.AreaCount);
            LogView($"回龙观地区{_mockParam.AreaCount}个项目创建完毕。");
        }

      
    }
}
