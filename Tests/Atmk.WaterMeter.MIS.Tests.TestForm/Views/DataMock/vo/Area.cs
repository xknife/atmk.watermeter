﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo
{
    /// <summary>
    /// 项目
    /// </summary>
    public class Area
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string siteName { get; set; }
        /// <summary>
        /// 项目地址
        /// </summary>
        public string siteAddress { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// 高程
        /// </summary>
        public double elevation { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }
}
