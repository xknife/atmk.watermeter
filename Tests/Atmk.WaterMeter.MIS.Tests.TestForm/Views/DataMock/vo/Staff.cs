﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo
{
    public class Staff
    {
        /// <summary>
        /// 登陆名称
        /// </summary>
        public string operatorName { get; set; }
        /// <summary>
        /// 实际姓名
        /// </summary>
        public string surname { get; set; }
        /// <summary>
        /// 电话号
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// 每行显示
        /// </summary>
        public string pageLines { get; set; }
        /// <summary>
        /// 角色Id
        /// </summary>
        public string roleId { get; set; }
        /// <summary>
        /// 项目Id
        /// </summary>
        public string siteId { get; set; }
        /// <summary>
        /// 操作员状态(是否启用)
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 片区地址-以后不会采用
        /// </summary>
        public List<string> district { get; set; }
    }
}
