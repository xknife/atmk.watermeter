﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo
{
    public class MeterAndOwner
    {
        public MeterAndOwner(string districtId)
        {
            this.districtId = districtId;
        }

        /// <summary>
        /// 片区Id
        /// </summary>
        public string districtId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Owner owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public WaterMeter waterMeter { get; set; }

        /// <summary>返回表示当前对象的字符串。</summary>
        /// <returns>表示当前对象的字符串。</returns>
        public override string ToString()
        {
            return $"{owner.houseNumber}-{waterMeter.meterNumber}";
        }
    }
}