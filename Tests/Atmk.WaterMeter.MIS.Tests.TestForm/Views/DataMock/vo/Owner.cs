﻿namespace Atmk.WaterMeter.MIS.TestForm.Views.DataMock.vo
{
    public class Owner
    {
        /// <summary>
        /// 片区编码
        /// </summary>
        public string districtCode { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string ownerName { get; set; }
        /// <summary>
        /// 门牌号
        /// </summary>
        public string houseNumber { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 经度 double类型
        /// </summary>
        public double longitude { get; set; }
        /// <summary>
        /// 纬度 double类型
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// 高程(海拔)  double类型
        /// </summary>
        public double elevation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OwnerMessage ownerMessage { get; set; }
    }
}