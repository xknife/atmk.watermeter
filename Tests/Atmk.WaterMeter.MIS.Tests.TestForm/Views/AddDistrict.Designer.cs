﻿namespace Atmk.WaterMeter.MIS.TestForm.Views
{
    partial class AddDistrict
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._DistrictNameText = new System.Windows.Forms.TextBox();
            this._ParentId = new System.Windows.Forms.TextBox();
            this._projectId = new System.Windows.Forms.TextBox();
            this._Add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "片区名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "上级Id";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "项目Id";
            // 
            // _DistrictNameText
            // 
            this._DistrictNameText.Location = new System.Drawing.Point(139, 60);
            this._DistrictNameText.Name = "_DistrictNameText";
            this._DistrictNameText.Size = new System.Drawing.Size(311, 21);
            this._DistrictNameText.TabIndex = 3;
            // 
            // _ParentId
            // 
            this._ParentId.Enabled = false;
            this._ParentId.Location = new System.Drawing.Point(139, 109);
            this._ParentId.Name = "_ParentId";
            this._ParentId.Size = new System.Drawing.Size(311, 21);
            this._ParentId.TabIndex = 4;
            // 
            // _projectId
            // 
            this._projectId.Enabled = false;
            this._projectId.Location = new System.Drawing.Point(139, 164);
            this._projectId.Name = "_projectId";
            this._projectId.Size = new System.Drawing.Size(311, 21);
            this._projectId.TabIndex = 5;
            // 
            // _Add
            // 
            this._Add.Location = new System.Drawing.Point(352, 228);
            this._Add.Name = "_Add";
            this._Add.Size = new System.Drawing.Size(98, 58);
            this._Add.TabIndex = 6;
            this._Add.Text = "添加";
            this._Add.UseVisualStyleBackColor = true;
            this._Add.Click += new System.EventHandler(this._Add_Click);
            // 
            // AddDistrict
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 450);
            this.Controls.Add(this._Add);
            this.Controls.Add(this._projectId);
            this.Controls.Add(this._ParentId);
            this.Controls.Add(this._DistrictNameText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddDistrict";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "添加片区";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _DistrictNameText;
        private System.Windows.Forms.TextBox _ParentId;
        private System.Windows.Forms.TextBox _projectId;
        private System.Windows.Forms.Button _Add;
    }
}