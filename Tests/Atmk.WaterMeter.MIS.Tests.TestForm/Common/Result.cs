﻿namespace Atmk.WaterMeter.MIS.TestForm.Common
{
    public class Result
    {
        public int errcode { get; set; }
        public string msg { get; set; }
    }
}