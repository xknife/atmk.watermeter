﻿using System.Collections.Generic;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestForm.Common
{
    class Kernel
    {
        private string _url = "https://www.firstdomainbyxiang.info:15000";
        public string Token { get; set; }
        public List<string> RoleName { get; set;}

        public string Url
        {
            get => _url;
            set
            {
                _url = value;
                Client = new RestClient(Url);
            }
        }

        public RestClient Client { get; set; }

        public Kernel()
        {
            Client = new RestClient(Url);
        }
    }
}
