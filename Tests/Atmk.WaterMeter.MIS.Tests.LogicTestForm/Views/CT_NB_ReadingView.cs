﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    public partial class CT_NB_ReadingView : DockContent
    {
        private readonly IDataStoreLogic _dataStoreLogic;
        public CT_NB_ReadingView(
            IDataStoreLogic dataStoreLogic
            )
        {
            _dataStoreLogic = dataStoreLogic;
            InitializeComponent();
            BindClick();
        }

        private void BindClick()
        {
            _WriteReadingBtn.Click +=(s,e)=>
            {
                const ulong read = 36715;
                var dd = new DeviceData
                {
                    valve = 0,
                    voltage = 255,
                    voltageState = 0,
                    warning = 0,
                    current = Convert.ToBase64String(BitConverter.GetBytes(read
                    ).Reverse().Skip(2).ToArray()),
                    history = GetHistoryValue(35012, read, 12)
                };
                _dataStoreLogic.MeterRecordSave("123123123", dd, DateTime.Now);
            };
        }

        private string GetHistoryValue(ulong s, ulong e, int len)
        {
            var value = (e - s) / 12;
            var list = new List<string>();
            for (ulong i = 0; i < 12; i++)
            {

                list.Add(Convert.ToBase64String(BitConverter.GetBytes(e + value * i
                ).Skip(2).Reverse().ToArray()));
            }
            return string.Join("", list.ToArray());
        }
    }
}
