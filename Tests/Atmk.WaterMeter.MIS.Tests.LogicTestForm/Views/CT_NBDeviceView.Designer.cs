﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class CT_NBDeviceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._AddDevice = new System.Windows.Forms.Button();
            this._DelDevice = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._MeterNoText = new System.Windows.Forms.TextBox();
            this._DeviceId = new System.Windows.Forms.TextBox();
            this._listBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // _AddDevice
            // 
            this._AddDevice.Location = new System.Drawing.Point(35, 137);
            this._AddDevice.Name = "_AddDevice";
            this._AddDevice.Size = new System.Drawing.Size(119, 52);
            this._AddDevice.TabIndex = 0;
            this._AddDevice.Text = "注册设备";
            this._AddDevice.UseVisualStyleBackColor = true;
            // 
            // _DelDevice
            // 
            this._DelDevice.Location = new System.Drawing.Point(181, 137);
            this._DelDevice.Name = "_DelDevice";
            this._DelDevice.Size = new System.Drawing.Size(119, 52);
            this._DelDevice.TabIndex = 1;
            this._DelDevice.Text = "删除设备";
            this._DelDevice.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "水表编号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "平台设备Id";
            // 
            // _MeterNoText
            // 
            this._MeterNoText.Location = new System.Drawing.Point(114, 31);
            this._MeterNoText.Name = "_MeterNoText";
            this._MeterNoText.Size = new System.Drawing.Size(332, 21);
            this._MeterNoText.TabIndex = 4;
            // 
            // _DeviceId
            // 
            this._DeviceId.Enabled = false;
            this._DeviceId.Location = new System.Drawing.Point(114, 82);
            this._DeviceId.Name = "_DeviceId";
            this._DeviceId.Size = new System.Drawing.Size(332, 21);
            this._DeviceId.TabIndex = 5;
            // 
            // _listBox
            // 
            this._listBox.FormattingEnabled = true;
            this._listBox.ItemHeight = 12;
            this._listBox.Location = new System.Drawing.Point(503, 16);
            this._listBox.Name = "_listBox";
            this._listBox.Size = new System.Drawing.Size(285, 400);
            this._listBox.TabIndex = 6;
            // 
            // CT_NBDeviceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._listBox);
            this.Controls.Add(this._DeviceId);
            this.Controls.Add(this._MeterNoText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._DelDevice);
            this.Controls.Add(this._AddDevice);
            this.Name = "CT_NBDeviceView";
            this.Text = "CT_NBDeviceView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _AddDevice;
        private System.Windows.Forms.Button _DelDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _MeterNoText;
        private System.Windows.Forms.TextBox _DeviceId;
        private System.Windows.Forms.ListBox _listBox;
    }
}