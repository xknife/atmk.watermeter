﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class StatisticsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._PropertyGrid = new System.Windows.Forms.PropertyGrid();
            this._上月阶梯用水量Buttion = new System.Windows.Forms.Button();
            this._统计信息Buttion = new System.Windows.Forms.Button();
            this._费用报警Button = new System.Windows.Forms.Button();
            this._ListBox = new System.Windows.Forms.ListBox();
            this._上季度片区用水Button = new System.Windows.Forms.Button();
            this._上一年片区用水Button = new System.Windows.Forms.Button();
            this._ValueListBox = new System.Windows.Forms.ListBox();
            this._关阀业主Button = new System.Windows.Forms.Button();
            this._用水情况Button = new System.Windows.Forms.Button();
            this._昨日抄表数据统计Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._comboxProject = new System.Windows.Forms.ComboBox();
            this._ChartButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _PropertyGrid
            // 
            this._PropertyGrid.Location = new System.Drawing.Point(484, 81);
            this._PropertyGrid.Name = "_PropertyGrid";
            this._PropertyGrid.Size = new System.Drawing.Size(178, 436);
            this._PropertyGrid.TabIndex = 0;
            // 
            // _上月阶梯用水量Buttion
            // 
            this._上月阶梯用水量Buttion.Location = new System.Drawing.Point(12, 81);
            this._上月阶梯用水量Buttion.Name = "_上月阶梯用水量Buttion";
            this._上月阶梯用水量Buttion.Size = new System.Drawing.Size(94, 47);
            this._上月阶梯用水量Buttion.TabIndex = 1;
            this._上月阶梯用水量Buttion.Text = "上月阶梯用水总量";
            this._上月阶梯用水量Buttion.UseVisualStyleBackColor = true;
            // 
            // _统计信息Buttion
            // 
            this._统计信息Buttion.Location = new System.Drawing.Point(12, 243);
            this._统计信息Buttion.Name = "_统计信息Buttion";
            this._统计信息Buttion.Size = new System.Drawing.Size(94, 47);
            this._统计信息Buttion.TabIndex = 2;
            this._统计信息Buttion.Text = "统计信息";
            this._统计信息Buttion.UseVisualStyleBackColor = true;
            // 
            // _费用报警Button
            // 
            this._费用报警Button.Location = new System.Drawing.Point(12, 349);
            this._费用报警Button.Name = "_费用报警Button";
            this._费用报警Button.Size = new System.Drawing.Size(94, 47);
            this._费用报警Button.TabIndex = 3;
            this._费用报警Button.Text = "费用报警";
            this._费用报警Button.UseVisualStyleBackColor = true;
            // 
            // _ListBox
            // 
            this._ListBox.FormattingEnabled = true;
            this._ListBox.ItemHeight = 12;
            this._ListBox.Location = new System.Drawing.Point(145, 81);
            this._ListBox.Name = "_ListBox";
            this._ListBox.Size = new System.Drawing.Size(317, 436);
            this._ListBox.TabIndex = 4;
            // 
            // _上季度片区用水Button
            // 
            this._上季度片区用水Button.Location = new System.Drawing.Point(12, 134);
            this._上季度片区用水Button.Name = "_上季度片区用水Button";
            this._上季度片区用水Button.Size = new System.Drawing.Size(94, 47);
            this._上季度片区用水Button.TabIndex = 5;
            this._上季度片区用水Button.Text = "上季度片区用水";
            this._上季度片区用水Button.UseVisualStyleBackColor = true;
            // 
            // _上一年片区用水Button
            // 
            this._上一年片区用水Button.Location = new System.Drawing.Point(12, 187);
            this._上一年片区用水Button.Name = "_上一年片区用水Button";
            this._上一年片区用水Button.Size = new System.Drawing.Size(94, 47);
            this._上一年片区用水Button.TabIndex = 6;
            this._上一年片区用水Button.Text = "上一年片区用水";
            this._上一年片区用水Button.UseVisualStyleBackColor = true;
            // 
            // _ValueListBox
            // 
            this._ValueListBox.FormattingEnabled = true;
            this._ValueListBox.ItemHeight = 12;
            this._ValueListBox.Location = new System.Drawing.Point(693, 81);
            this._ValueListBox.Name = "_ValueListBox";
            this._ValueListBox.Size = new System.Drawing.Size(223, 436);
            this._ValueListBox.TabIndex = 7;
            // 
            // _关阀业主Button
            // 
            this._关阀业主Button.Location = new System.Drawing.Point(12, 402);
            this._关阀业主Button.Name = "_关阀业主Button";
            this._关阀业主Button.Size = new System.Drawing.Size(94, 47);
            this._关阀业主Button.TabIndex = 8;
            this._关阀业主Button.Text = "关阀业主";
            this._关阀业主Button.UseVisualStyleBackColor = true;
            // 
            // _用水情况Button
            // 
            this._用水情况Button.Location = new System.Drawing.Point(12, 296);
            this._用水情况Button.Name = "_用水情况Button";
            this._用水情况Button.Size = new System.Drawing.Size(94, 47);
            this._用水情况Button.TabIndex = 9;
            this._用水情况Button.Text = "用水情况";
            this._用水情况Button.UseVisualStyleBackColor = true;
            // 
            // _昨日抄表数据统计Button
            // 
            this._昨日抄表数据统计Button.Location = new System.Drawing.Point(12, 455);
            this._昨日抄表数据统计Button.Name = "_昨日抄表数据统计Button";
            this._昨日抄表数据统计Button.Size = new System.Drawing.Size(94, 47);
            this._昨日抄表数据统计Button.TabIndex = 10;
            this._昨日抄表数据统计Button.Text = "昨日抄表数据统计";
            this._昨日抄表数据统计Button.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(25, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "选择项目";
            // 
            // _comboxProject
            // 
            this._comboxProject.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._comboxProject.FormattingEnabled = true;
            this._comboxProject.Location = new System.Drawing.Point(129, 23);
            this._comboxProject.Name = "_comboxProject";
            this._comboxProject.Size = new System.Drawing.Size(199, 29);
            this._comboxProject.TabIndex = 12;
            // 
            // _ChartButton
            // 
            this._ChartButton.Location = new System.Drawing.Point(368, 17);
            this._ChartButton.Name = "_ChartButton";
            this._ChartButton.Size = new System.Drawing.Size(94, 47);
            this._ChartButton.TabIndex = 13;
            this._ChartButton.Text = "统计所有";
            this._ChartButton.UseVisualStyleBackColor = true;
            this._ChartButton.Click += new System.EventHandler(this._ChartButton_Click);
            // 
            // StatisticsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 646);
            this.Controls.Add(this._ChartButton);
            this.Controls.Add(this._comboxProject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._昨日抄表数据统计Button);
            this.Controls.Add(this._用水情况Button);
            this.Controls.Add(this._关阀业主Button);
            this.Controls.Add(this._ValueListBox);
            this.Controls.Add(this._上一年片区用水Button);
            this.Controls.Add(this._上季度片区用水Button);
            this.Controls.Add(this._ListBox);
            this.Controls.Add(this._费用报警Button);
            this.Controls.Add(this._统计信息Buttion);
            this.Controls.Add(this._上月阶梯用水量Buttion);
            this.Controls.Add(this._PropertyGrid);
            this.Name = "StatisticsView";
            this.Text = "StatisticsView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PropertyGrid _PropertyGrid;
        private System.Windows.Forms.Button _上月阶梯用水量Buttion;
        private System.Windows.Forms.Button _统计信息Buttion;
        private System.Windows.Forms.Button _费用报警Button;
        private System.Windows.Forms.ListBox _ListBox;
        private System.Windows.Forms.Button _上季度片区用水Button;
        private System.Windows.Forms.Button _上一年片区用水Button;
        private System.Windows.Forms.ListBox _ValueListBox;
        private System.Windows.Forms.Button _关阀业主Button;
        private System.Windows.Forms.Button _用水情况Button;
        private System.Windows.Forms.Button _昨日抄表数据统计Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _comboxProject;
        private System.Windows.Forms.Button _ChartButton;
    }
}