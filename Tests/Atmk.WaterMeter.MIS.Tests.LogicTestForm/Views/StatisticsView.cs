﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Logic.Statistics;
using Atmk.WaterMeter.MIS.LogicTestForm.Common;
using Autofac;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    public partial class StatisticsView : DockContent
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IWaterConsumptionLogic _waterConsumptionLogic;
        private readonly IAccountsLogic _accountsLogic;
        private readonly IOwnersLogic _ownersLogic;
        private readonly IRegionLogic _regionLogic;
        private readonly IWaterMetersLogic _waterMetersLogic;
        private readonly IUserCenterLogic _userCenterLogic;
        private readonly ILoginLogic _loginLogic;
        private readonly IChartLogic _chartLogic;
      

        public StatisticsView(
            IWaterConsumptionLogic waterConsumptionLogic,
            IAccountsLogic accountsLogic,
            IOwnersLogic ownersLogic,
            IRegionLogic regionLogic,
            IWaterMetersLogic waterMetersLogic,
            IDistrictLogic districtLogic,
            IUserCenterLogic userCenterLogic,
            ILoginLogic loginLogic,
            IChartLogic chartLogic
          
        )
        {
          
            _waterConsumptionLogic = waterConsumptionLogic;
            _accountsLogic = accountsLogic;
            _ownersLogic = ownersLogic;
            _regionLogic = regionLogic;
            _waterMetersLogic = waterMetersLogic;
            _userCenterLogic = userCenterLogic;
            _loginLogic = loginLogic;
            _chartLogic = chartLogic;
            InitializeComponent();
            BindingEventArgs();
            BindControlDatas();
        }
        private string projectId= "5e890445-3a5c-41f6-8b64-304cd34c1565";

        private void BindControlDatas()
        {
            try
            {
                _Logger.Info("超级管理员登陆");
                _loginLogic.LoginMatching("admin", "123456", out string staffId, out string roleId, out string areaId);
                var projects = _userCenterLogic.SelectAreas(staffId, "");
                var list = new Dictionary<string, string>();
                projects.ForEach(s => list[s.id] = s.siteName);
                IntPtr i = this.Handle;
                BindList(_comboxProject, list);
                _comboxProject.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _Logger.Error(e);
            }
        }
        private void BindList(ComboBox comboBox, Dictionary<string, string> value)
        {
            Invoke((EventHandler)delegate
            {
                var item = value.Select(a => new
                {
                    key = a.Key,
                    value = a.Value
                });
                comboBox.DataSource = item.ToList();
                comboBox.DisplayMember = "value";
                comboBox.ValueMember = "key";
            });
        }
        private void BindingEventArgs()
        {
            _comboxProject.SelectedIndexChanged += (s, e) =>
            {
                projectId = _comboxProject.SelectedValue.ToString();
            };
            _统计信息Buttion.Click += (s, e) =>
            {
                var result = _regionLogic.GetStatisticsInfoData(projectId);
                _PropertyGrid.SelectedObject = result;
            };
            _用水情况Button.Click += (s, e) =>
            {
                var result = _waterConsumptionLogic.GetWaterConsumptionInfoData(projectId);
                _PropertyGrid.SelectedObject = result;
            };

            _上月阶梯用水量Buttion.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id);
                var result = _waterConsumptionLogic.GetDistrictPriceStepWaterConsumption(Period.Month,
                    DateTime.Today.AddMonths(-1), distirctIds.ToArray());
                _ListBox.DataSource = result.list.OrderBy(m => m.districtName).ToList();
                _ListBox.DisplayMember = "districtName";
            };
            _上季度片区用水Button.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id).ToArray();
                var result = _waterConsumptionLogic.GetDistrictMonthWatherConsumption(Period.Quarter,
                    DateTimeHelper.GetQuarter(1, false), distirctIds);
                _ListBox.DataSource = result.list.OrderBy(m => m.districtName).ToList();
                _ListBox.DisplayMember = "districtName";
            };

            _上一年片区用水Button.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id).ToArray();
                var result = _waterConsumptionLogic.GetYearWaterConsumptions(Period.Year,
                    DateTime.Today.AddYears(-1), distirctIds);
                _ListBox.DataSource = result.list.OrderBy(m => m.districtName).ToList();
                _ListBox.DisplayMember = "districtName";
            };
            _费用报警Button.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id);
                var result  = _accountsLogic.GetCostReminderItemList(distirctIds.ToArray());
                _ListBox.DataSource = result.list.OrderBy(m=>m.ownerName).ToList();
                _ListBox.DisplayMember = "ownerName";
            };
            _关阀业主Button.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id);
                var result = _ownersLogic.GetCloseClosingValveOwnerItems(distirctIds.ToArray());
                _ListBox.DataSource=result.list.OrderBy(m => m.ownerName).ToList();
            };
            _昨日抄表数据统计Button.Click += (s, e) =>
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id);
                var result = _waterMetersLogic.GetDistrictReadRatioItems(DateTime.Now.AddDays(-1), distirctIds.ToArray());
                _ListBox.DataSource = result.list;
                _ListBox.DisplayMember = "districtName";
            };


            _PropertyGrid.SelectedGridItemChanged += (s, e) =>
            {
                if (!(e.NewSelection.Value is List<double> list)) return;
                var souce = list.Select((t, i) => $"{i + 1}月  {t}").ToList();
                _ValueListBox.DataSource = souce;
            };
            _ListBox.SelectedIndexChanged += (s, e) =>
            {
                var lb = (ListBox) s;
                _PropertyGrid.SelectedObject = lb.SelectedItem;
            };

            
        }

        private async void _ChartButton_Click(object sender, EventArgs e)
        {
            var result = await _chartLogic.GetChartAsync(projectId);
            _PropertyGrid.SelectedObject = result;
        }
    }
}