﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    public partial class AccountView : DockContent
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IAccountManageLogic _accountManageLogic;
        public AccountView(
            IAccountManageLogic accountManageLogic
            )
        {
            _accountManageLogic = accountManageLogic;
            InitializeComponent();
            BindClick();
        }

        private void BindClick()
        {
            _GetPaymentList.Click += (s, e) =>
            {
                Token token = GetToken();
                GetPaymentList(token);
            };
            listView1.ItemSelectionChanged += (s, e) =>
            {
                var lv = (ListView) s;
                if (lv.SelectedItems.Count > 0)
                    _ItempropertyGrid.SelectedObject = lv.SelectedItems[0].Tag;
            };
        }

        private Token GetToken()
        {
            var token = new Token();
            token.payload.areaid = "5e890445-3a5c-41f6-8b64-304cd34c1565";
            return token;
        }

        private void GetPaymentList(Token token)
        {
            var result = _accountManageLogic.SelectPayment(token.payload.id, "", 1, 20, new DateTime(2019, 1, 3), new DateTime(2019, 2, 14), 2,
                out var count);
            ViewResult(result);
            _Logger.Trace(result);
        }

        private void ViewResult(object result)
        {
            var list = (List<object>) result;
            foreach (var item in list)
            {
                var listItem = new ListViewItem
                {
                    Tag = item,
                    Text = ((dynamic) item).ownerName.ToString()
                };
                listView1.Items.Add(listItem);
            }
        }
    }
}
