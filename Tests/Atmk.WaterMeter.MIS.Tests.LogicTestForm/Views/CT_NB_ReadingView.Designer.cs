﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class CT_NB_ReadingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._WriteReadingBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _WriteReadingBtn
            // 
            this._WriteReadingBtn.Location = new System.Drawing.Point(12, 12);
            this._WriteReadingBtn.Name = "_WriteReadingBtn";
            this._WriteReadingBtn.Size = new System.Drawing.Size(117, 63);
            this._WriteReadingBtn.TabIndex = 0;
            this._WriteReadingBtn.Text = "写入读数";
            this._WriteReadingBtn.UseVisualStyleBackColor = true;
            // 
            // CT_NB_ReadingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._WriteReadingBtn);
            this.Name = "CT_NB_ReadingView";
            this.Text = "CT_NB_ReadingView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _WriteReadingBtn;
    }
}