﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask;
using Newtonsoft.Json;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    public partial class StatisticsTaskView : DockContent
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IStatisticsTask _statisticsTask;
        private readonly IChartLogic _chartLogic;
        private readonly ILoginLogic _loginLogic;
        private readonly IUserCenterLogic _userCenterLogic;

        public StatisticsTaskView(
            IStatisticsTask statisticsTask,
            IChartLogic chartLogic,
            IUserCenterLogic userCenterLogic,
            ILoginLogic loginLogic
        )
        {
            _chartLogic = chartLogic;
            _statisticsTask = statisticsTask;
            _loginLogic = loginLogic;
            _userCenterLogic = userCenterLogic;
            InitializeComponent();
            BindControlDatas();
            BindEventArgs();
        }

        private string projectId = "";

        private void BindControlDatas()
        {
            try
            {
                _Logger.Info("超级管理员登陆");
                _loginLogic.LoginMatching("admin", "123456", out string staffId, out string roleId, out string areaId);
                var projects = _userCenterLogic.SelectAreas(staffId, "");
                var list = new Dictionary<string, string>();
                projects.ForEach(s => list[s.id] = s.siteName);
                IntPtr i = this.Handle;
                BindList(_comboxProject, list);
                _comboxProject.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _Logger.Error(e);
            }
        }

        private void BindList(ComboBox comboBox, Dictionary<string, string> value)
        {
            Invoke((EventHandler) delegate
            {
                var item = value.Select(a => new
                {
                    key = a.Key,
                    value = a.Value
                });
                comboBox.DataSource = item.ToList();
                comboBox.DisplayMember = "value";
                comboBox.ValueMember = "key";
            });
        }

        private void BindEventArgs()
        {
            _comboxProject.SelectedIndexChanged += (s, e) => { projectId = _comboxProject.SelectedValue.ToString(); };
            _开启Button.Click += (s, e) =>
            {
                var btn = (Button) s;
                if (btn.Text == "开启")
                {
                    btn.Text = "关闭";
                    _statisticsTask.Run(projectId);
                }
                else
                {
                    btn.Text = "开启";
                    _statisticsTask.Stop();
                }
            };

            _读取Button.Click += (s, e) =>
            {
                var result = _chartLogic.GetChart(projectId);
                var json = JsonConvert.SerializeObject(result);
                Info(json);
            };
        }
        private void Info(string message, bool rn = true)
        {
            Invoke((EventHandler)delegate
            {
                if (rn)
                    _InfoText.Text =
                        _InfoText.Text.Insert(0, $"{DateTime.Now.ToShortTimeString()}:" + message + "\r\n");
                else
                {
                    _InfoText.AppendText(message);
                }
            });
        }
    }
}