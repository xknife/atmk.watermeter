﻿using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.helper
{
    class DistrictNameHelper
    {
        public List<string> Names { get; }
        // ReSharper disable once InconsistentNaming
        public Dictionary<int, string> NumToCN { get; }

        public DistrictNameHelper()
        {
            Names = new List<string>(new[]
            {
                "学富", "经纶苑", "学贯", "博学府", "云趣", "卓尔园", "团锦", "繁花庄", "似锦", "百花", "姹紫", "红山峰",
                "万里", "碧空苑", "湛蓝", "森林里", "美玉", "清秀园", "秋爽", "丹桂庄", "高云", "红叶", "金风", "公园里"
            });
            NumToCN = new Dictionary<int, string>
            {
                {1, "一"},
                {2, "二"},
                {3, "三"},
                {4, "四"},
                {5, "五"},
                {6, "六"},
                {7, "七"},
                {8, "八"},
                {9, "九"}
            };
        }
    }
}