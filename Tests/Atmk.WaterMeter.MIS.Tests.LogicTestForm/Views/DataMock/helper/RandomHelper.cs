﻿using System;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.helper
{
    public class RandomHelper
    {
        public static Random Random { get; }

        static RandomHelper()
        {
            for (int i = 0; i < 100; i++)
            {
                int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
                Random = new Random(seed);
            }
        }
    }
}
