﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo
{
    public class Role
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string roleName { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }
}
