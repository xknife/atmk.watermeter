﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo
{
    public class OwnerMessage
    {
        /// <summary>
        /// 详细地址
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 证件类型
        /// </summary>
        public string idType { get; set; }
        /// <summary>
        /// 身份证号码
        /// </summary>
        public string idNumber { get; set; }
        /// <summary>
        /// 建筑面积
        /// </summary>
        public string builtArea { get; set; }
        /// <summary>
        /// 人口数
        /// </summary>
        public string populationNumber { get; set; }
    }
}
