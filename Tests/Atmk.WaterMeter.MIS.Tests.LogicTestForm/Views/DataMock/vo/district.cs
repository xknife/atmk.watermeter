﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo
{
    // ReSharper disable once InconsistentNaming
    public class district
    {
        public district(
            string projectId,
            string districtId,
            string districtName,
            string remark = "",
            string parentDistrictId = "")
        {
            this.projectId = projectId;
            this.districtId = districtId;
            this.districtName = districtName;
            this.remark = remark;
            this.parentDistrictId = parentDistrictId;
        }

        // ReSharper disable once InconsistentNaming
        public string projectId { get; set; }

        // ReSharper disable once InconsistentNaming
        public string districtId { get; set; }

        // ReSharper disable once InconsistentNaming
        public string parentDistrictId { get; set; }

        // ReSharper disable once InconsistentNaming
        public string districtName { get; set; }

        /// <summary>
        /// 协议里有高程(海拔)参数，但现阶段不使用
        /// </summary>
        public string elevation { get; set; } = "0";

        // ReSharper disable once InconsistentNaming
        public string remark { get; set; }

        /// <summary>返回表示当前对象的字符串。</summary>
        /// <returns>表示当前对象的字符串。</returns>
        public override string ToString()
        {
            return $"{districtName}";
        }
    }
}