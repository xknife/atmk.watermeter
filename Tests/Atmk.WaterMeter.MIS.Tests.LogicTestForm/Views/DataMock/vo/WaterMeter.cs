﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo
{
    public class WaterMeter
    {
        /// <summary>
        ///     水表Id
        /// </summary>
        public string meterId { get; set; }

        /// <summary>
        ///     表类型
        /// </summary>
        public string meterType { get; set; }

        /// <summary>
        ///     表编号
        /// </summary>
        public string meterNumber { get; set; }

        /// <summary>
        ///     通讯类型
        /// </summary>
        public string commType { get; set; }

        /// <summary>
        ///     缴费方式
        /// </summary>
        public string refillType { get; set; }

        /// <summary>
        ///     表状态
        /// </summary>
        public string meterState { get; set; }

        /// <summary>
        ///     价格id
        /// </summary>
        public string priceId { get; set; }

        /// <summary>
        /// </summary>
        public MeterDatas meterDatas { get; set; }
    }
}