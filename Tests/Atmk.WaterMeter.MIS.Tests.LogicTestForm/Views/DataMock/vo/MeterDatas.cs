﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo
{
    public class MeterDatas
    {
        /// <summary>
        /// 集中器
        /// </summary>
        public string concentratorName { get; set; }
        /// <summary>
        /// 采集器
        /// </summary>
        public string collector { get; set; }
        /// <summary>
        /// 表底数
        /// </summary>
        public string meterBottomNumber { get; set; }
        /// <summary>
        /// 倍率
        /// </summary>
        public string multipleRate { get; set; }
        /// <summary>
        /// 抄表员
        /// </summary>
        public string meterReader { get; set; }
        /// <summary>
        /// 变比
        /// </summary>
        public string variableRatio { get; set; }
        /// <summary>
        /// 修正值
        /// </summary>
        public string correctionValue { get; set; }
        /// <summary>
        /// 表型号
        /// </summary>
        public string meterModel { get; set; }
        /// <summary>
        /// 安装日期
        /// </summary>
        public string installDate { get; set; }
        /// <summary>
        /// 日用量
        /// </summary>
        public string dailyDosage { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        public string accountNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 中继标志
        /// </summary>
        public string relaySign { get; set; }
        /// <summary>
        /// 工作模式
        /// </summary>
        public string workingMode { get; set; }
        /// <summary>
        /// 限容功率
        /// </summary>
        public string powerLimitingPpower { get; set; }
        /// <summary>
        /// 声光报警值
        /// </summary>
        public string acoustoOpticAlarmValue { get; set; }
        /// <summary>
        /// 拉闸报警值
        /// </summary>
        public string gateAlarmValue { get; set; }
        /// <summary>
        /// 透支用量
        /// </summary>
        public string overdraftDosage { get; set; }
        /// <summary>
        /// 最大充值额度
        /// </summary>
        public string maximumRechargeLlimit { get; set; }
        /// <summary>
        /// 最小剩余额度
        /// </summary>
        public string minimumResidualAmount { get; set; }
        /// <summary>
        /// 透支额度
        /// </summary>
        public string lineOfCredit { get; set; }
        /// <summary>
        /// 冻结模式
        /// </summary>
        public string freezingMode { get; set; }
        /// <summary>
        /// 冻结日期
        /// </summary>
        public string freezingData { get; set; }
        /// <summary>
        /// 小数点位数
        /// </summary>
        public string decimals { get; set; }
        /// <summary>
        /// 出厂日期
        /// </summary>
        public string dateOfProduction { get; set; }
        /// <summary>
        /// 出厂编号
        /// </summary>
        public string factoryNumber { get; set; }
        /// <summary>
        /// 安装地址
        /// </summary>
        public string installAddress { get; set; }
    }
}