﻿using System.Collections.Generic;
using NKnife.Util;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.current
{
    class KeyDataBuilder
    {
        private readonly UtilRandom _random = new UtilRandom();

        /// <summary>
        /// 所有表的上一次数据。key是表的NbId, value是该表相关数据的封装
        /// </summary>
        public Dictionary<string, KeyData> MeterMap { get; set; } = new Dictionary<string, KeyData>();

        public KeyData GetKeyData(string meterId)
        {
            //先拿到表的前一天读数
            if (!MeterMap.TryGetValue(meterId, out KeyData keyData))
            {
                //如果没有，新建，代表该表的第一天数据
                keyData = new KeyData();
                MeterMap.Add(meterId, keyData);
            }

            FillHistoryAndCurrent(keyData);
            keyData.Valve = 0;
            keyData.Voltage = (int) (3.25 * 100 - 100);
            keyData.Time = keyData.Time.AddDays(1);

            return keyData;
        }

        private const int BEI = 1000;

        public void FillHistoryAndCurrent(KeyData keyData)
        {
            var min = 1;//keyData.Params.MinYongShuiLiang * BEI;
            var max = 9;//keyData.Params.MaxYongShuiLiang * BEI;

            var day = UtilRandom.Next(min, max) / 30; //每天用水量
            var hourBase = day / 24; //每小时用水量

            var current = keyData.Current * BEI;

            for (int i = 0; i < 24; i++)
            {
                var h = keyData.Time.Hour;
                var m = keyData.Time.Month;
                int hour = 0;
                switch (GetLevel(h, m))
                {
                    case 0:
                        hour = hourBase - UtilRandom.Next(hourBase / 2, hourBase);
                        break;
                    case 1:
                        hour = hourBase - UtilRandom.Next(hourBase / 3, hourBase / 2);
                        break;
                    case 2:
                        hour = hourBase + UtilRandom.Next(hourBase / 3, hourBase / 2);
                        break;
                    case 3:
                        hour = hourBase + UtilRandom.Next(hourBase / 2, hourBase);
                        break;
                }

                current += hour;
                keyData.History[i] = current / BEI;
            }
            keyData.Current = current / BEI;
        }


        private static int GetLevel(int h, int m)
        {
            int level = 0; //用水量级别，0-3级，level大用水量越大
            if (h >= 1 && h < 6) //深夜
            {
                level = 0;
            }
            else if (h >= 6 && h < 8) //早晨
            {
                level = 2;
            }
            else if (h >= 8 && h < 11) //上午
            {
                level = 1;
            }
            else if (h >= 11 && h < 13) //中午
            {
                level = 2;
            }
            else if (h >= 13 && h < 17) //下午
            {
                level = 1;
            }
            else if (h >= 17 && h < 20) //傍晚
            {
                if (m >= 6 && m <= 9) //夏天
                    level = 3;
                else
                    level = 2;
            }
            else //晚上
            {
                if (m >= 6 && m <= 9) //夏天
                    level = 2;
                else
                    level = 1;
            }

            return level;
        }
    }
}