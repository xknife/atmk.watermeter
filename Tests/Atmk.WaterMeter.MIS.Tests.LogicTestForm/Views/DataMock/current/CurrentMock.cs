﻿using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.helper;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.current
{
    /// <summary>
    /// 本次Mock的数据
    /// </summary>
    public class CurrentMock
    {
        private Random _random = RandomHelper.Random;
        private DistrictNameHelper _init = new DistrictNameHelper();

        /// <summary>
        /// 本次Mock创建的Area(片区的根)
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 操作员Id
        /// </summary>
        public string StaffId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 本次Mock创建的片区ID集合
        /// </summary>
        public Dictionary<string, string> Districts { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// 本次Mock创建的水表集合，Key是该表的NBID
        /// </summary>
        public Dictionary<string, MeterAndOwner> MeterMap { get; set; } = new Dictionary<string, MeterAndOwner>();

        public void SaveDistrict(string id, string name)
        {
            KeyValuePair<string, string> pair = new KeyValuePair<string, string>(id, BuildDistrictName());
            if (!Districts.ContainsKey(id))
            {
                Districts.Add(pair.Key, pair.Value);
            }
        }

        public string GetAreaName()
        {
            return "回龙观项目";
        }

        public string GetDistrictName()
        {
            return BuildDistrictName();
        }

        public string GetMobile()
        {
            return $"{_random.Next(130, 199)}{_random.Next(0, 999999999).ToString().PadLeft(9, '0')}";
        }

        #region District Helper

        private int _currentDistrictNameNum = 0;
        private string _currentDistrictName;

        private string BuildDistrictName()
        {
            if (_currentDistrictNameNum == 0)
            {
                _currentDistrictNameNum = _random.Next(1, 3);
                var index = _random.Next(0, _init.Names.Count);
                _currentDistrictName = _init.Names[index];
                _init.Names.RemoveAt(index);
            }

            string name;
            if (_currentDistrictNameNum <= 1)
                name = $"{_currentDistrictName}小区";
            else
                name = $"{_currentDistrictName}{_init.NumToCN[_currentDistrictNameNum]}区";
            _currentDistrictNameNum--;
            return name;
        }

        #endregion
    }
}