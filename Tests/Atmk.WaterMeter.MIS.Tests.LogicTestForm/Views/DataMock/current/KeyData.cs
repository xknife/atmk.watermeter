﻿using System;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.helper;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.current
{
    /// <summary>
    /// 关键数据
    /// </summary>
    class KeyData
    {
        private readonly Random _random = RandomHelper.Random;

        public KeyData()
        {
            //随机给出一个安装时间
            //假设2016.5.1整个项目实施完成，安装时间在这之前两个月内
            //再将当天的时间进行随机处理
            var dateOffset = _random.Next(2, 60);
            var timeOffset = _random.Next(1, 3600 * 24);
            Time = DateTime.Now.AddDays(-5).AddSeconds(-timeOffset);
            //Time = (new DateTime(2016, 5, 1)).AddDays(-dateOffset).AddSeconds(-timeOffset);
        }

        public float Current { get; set; } = 0;
        public float[] History { get; set; }=new float[24];
        public DateTime Time { get; set; }
        public float Voltage { get; set; } = 3.3F;

        /// <summary>
        /// 阀门开关状态,0阀打开状态,1阀关闭状态
        /// </summary>
        public int Valve { get; set; } = 0;
    }
}