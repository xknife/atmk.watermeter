﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public class Class8 : BaseMockClass
    {
        public Class8(Class7 last)
        {
            Last = last;
        }

        protected override void MockMethodRun()
        {
            throw new NotImplementedException();
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; } = null;
    }
}
