﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public class Class3 : BaseMockClass
    {
        public Class3(Class2 last, Class4 next)
        {
            Last = last;
            Next = next;
        }


        protected override void MockMethodRun()
        {
            throw new NotImplementedException();
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; }
    }
}
