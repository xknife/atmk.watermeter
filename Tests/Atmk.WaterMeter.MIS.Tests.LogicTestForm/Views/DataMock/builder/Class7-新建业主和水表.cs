﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.current;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.helper;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.vo;
using NLog;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public class Class7 : BaseMockClass
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();

        private readonly Random _random = RandomHelper.Random;
        private readonly MockParam _param;
        private readonly CurrentMock _current;

        public Class7(Class6 last, Class8 next, MockParam param, CurrentMock current)
        {
            Last = last;
            Next = next;
            _param = param;
            _current = current;
        }

        protected override void MockMethodRun()
        {
            //每小区有20-40幢楼，每楼4-8单元，每单元6-30层，每层4-8户
            int lou = _random.Next(_param.楼min, _param.楼max);
            int danyuan = _random.Next(_param.单元min, _param.单元max);
            int ceng = _random.Next(_param.层min, _param.层max);
            int hu = _random.Next(_param.户min, _param.户max);

            var count = _current.Districts.Count * lou * danyuan * ceng * hu;
            var msg = $"随机生成{count}个业主及水表";
            _Logger.Info($"准备{msg}");

            if (count > 1)
            {
                if (MessageBox.Show($@"准备{msg}", @"Ready", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return;
                }
            }

            foreach (var district in _current.Districts)
            {
                for (int a = 0; a < lou; a++)
                {
                    for (int b = 0; b < danyuan; b++)
                    {
                        for (int c = 0; c < ceng; c++)
                        {
                            for (int d = 0; d < hu; d++)
                            {
                                var did = district.Key;
                                var owner = new Owner();
                                owner.districtCode = did;
                                owner.houseNumber = $"{a + 1}楼{b + 1}单元{c + 1}层{d + 1}室";
                                owner.latitude = 22.22;
                                owner.elevation = 33.33;
                                owner.longitude = 44.44;
                                var meter = new vo.WaterMeter();
                                meter.meterNumber = $"{_random.Next(111111, 555555)}{_random.Next(666666, 999999)}";
                                meter.commType = "CT_NB";
                                meter.meterDatas = new MeterDatas();

                                var wm = new MeterAndOwner(did) { owner = owner, waterMeter = meter };
                                //
                                //                                request.AddJsonBody(wm);
                                //                                _kernel.Client.ExecuteAsync(request, response =>
                                //                                {
                                //                                    var data = response.Content;
                                //                                    _Logger.Trace(data);
                                //                                    try
                                //                                    {
                                //                                        var respReturn = JsonConvert.DeserializeObject<ownerResult>(data);
                                //                                        //保存所有表，key是这块表的NBId
                                //                                        _current.MeterMap.Add(respReturn.respNbId, wm);
                                //                                        _Logger.Trace($"{wm},NbID:{respReturn.respNbId}创建成功");
                                //                                    }
                                //                                    catch (Exception exception)
                                //                                    {
                                //                                        _Logger.Error(exception);
                                //                                        _Logger.Trace($"{wm}创建失败");
                                //                                    }
                                //                                });
                                //                                Thread.Sleep(5);
                            }
                        }
                    }
                }
            }
            if (count > 1)
                MessageBox.Show($@"{msg}...工作已完成");
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; }

    }

    public class ownerResult
    {
        public string respOwnerId { get; set; }
        public string respMeterId { get; set; }
        public string respNbId { get; set; }
    }
}