﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public class Class5 : BaseMockClass
    {
        public Class5(Class4 last, Class6 next)
        {
            Last = last;
            Next = next;
        }

        protected override void MockMethodRun()
        {
            throw new NotImplementedException();
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; }
    }
}
