﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public interface IMockClass
    {
        void Run();
        IMockClass Last { get; }
        IMockClass Next { get; }
    }
}
