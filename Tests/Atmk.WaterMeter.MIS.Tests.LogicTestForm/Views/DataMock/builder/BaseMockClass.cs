﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public abstract class BaseMockClass : IMockClass
    {
        public void Run()
        {
            MockMethodRun();
            Next?.Run();
        }

        protected abstract void MockMethodRun();

        public abstract IMockClass Last { get; }
        public abstract IMockClass Next { get; }
    }
}
