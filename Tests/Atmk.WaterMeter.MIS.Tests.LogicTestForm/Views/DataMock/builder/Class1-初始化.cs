﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    /// <summary>
    /// 初始化
    /// </summary>
    public class Class1 : BaseMockClass
    {
        public Class1()
        {
        }

        protected override void MockMethodRun()
        {
            //无事可做
        }

        public override IMockClass Last { get; } = null;
        public override IMockClass Next { get; } 
    }
}
