﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    public class Class6 : BaseMockClass
    {
        public Class6(Class5 last, Class7 next)
        {
            Last = last;
            Next = next;
        }

        protected override void MockMethodRun()
        {
            throw new NotImplementedException();
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; }
    }
}
