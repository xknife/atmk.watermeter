﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Logic;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock.builder
{
    /// <summary>
    /// 新建项目
    /// </summary>
    public class Class2 : BaseMockClass
    {
        private readonly IUserCenterLogic _logic;

        public Class2(IUserCenterLogic logic, Class1 last, Class3 next)
        {
            _logic = logic;
            Last = last;
            Next = next;
        }

        protected override void MockMethodRun()
        {
            _logic.AddSite(null, out _, out _);
        }

        public override IMockClass Last { get; }
        public override IMockClass Next { get; }

    }
}
