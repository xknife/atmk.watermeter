﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class AreaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._查询项目和操作员Button = new System.Windows.Forms.Button();
            this._ResultTreeView = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // _查询项目和操作员Button
            // 
            this._查询项目和操作员Button.Location = new System.Drawing.Point(33, 68);
            this._查询项目和操作员Button.Name = "_查询项目和操作员Button";
            this._查询项目和操作员Button.Size = new System.Drawing.Size(113, 53);
            this._查询项目和操作员Button.TabIndex = 0;
            this._查询项目和操作员Button.Text = "查询项目和操作员";
            this._查询项目和操作员Button.UseVisualStyleBackColor = true;
            // 
            // _ResultTreeView
            // 
            this._ResultTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ResultTreeView.Location = new System.Drawing.Point(314, 12);
            this._ResultTreeView.Name = "_ResultTreeView";
            this._ResultTreeView.Size = new System.Drawing.Size(454, 426);
            this._ResultTreeView.TabIndex = 3;
            // 
            // AreaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._ResultTreeView);
            this.Controls.Add(this._查询项目和操作员Button);
            this.Name = "AreaView";
            this.Text = "AreaView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _查询项目和操作员Button;
        private System.Windows.Forms.TreeView _ResultTreeView;
    }
}