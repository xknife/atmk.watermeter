﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.ReadDataMock
{
    partial class WriteDataMock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._comboxProject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._WriteReadingBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this._FirstReading = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._InfoText = new System.Windows.Forms.RichTextBox();
            this._DistcomboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this._FirstDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this._clearBtn = new System.Windows.Forms.Button();
            this._RemoveRecords = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._RunDaysText = new System.Windows.Forms.TextBox();
            this._EndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this._RunPerLabel = new System.Windows.Forms.Label();
            this._ProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // _comboxProject
            // 
            this._comboxProject.BackColor = System.Drawing.Color.White;
            this._comboxProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboxProject.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._comboxProject.FormattingEnabled = true;
            this._comboxProject.Location = new System.Drawing.Point(134, 47);
            this._comboxProject.Name = "_comboxProject";
            this._comboxProject.Size = new System.Drawing.Size(199, 29);
            this._comboxProject.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(30, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 21);
            this.label1.TabIndex = 13;
            this.label1.Text = "选择项目";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(30, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 21);
            this.label2.TabIndex = 16;
            this.label2.Text = "片区信息";
            // 
            // _WriteReadingBtn
            // 
            this._WriteReadingBtn.Location = new System.Drawing.Point(34, 381);
            this._WriteReadingBtn.Name = "_WriteReadingBtn";
            this._WriteReadingBtn.Size = new System.Drawing.Size(117, 63);
            this._WriteReadingBtn.TabIndex = 17;
            this._WriteReadingBtn.Text = "写入读数";
            this._WriteReadingBtn.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(384, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 21);
            this.label4.TabIndex = 19;
            this.label4.Text = "运行信息";
            // 
            // _FirstReading
            // 
            this._FirstReading.Location = new System.Drawing.Point(135, 177);
            this._FirstReading.Name = "_FirstReading";
            this._FirstReading.Size = new System.Drawing.Size(100, 21);
            this._FirstReading.TabIndex = 20;
            this._FirstReading.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(31, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 21);
            this.label3.TabIndex = 21;
            this.label3.Text = "初始读数";
            // 
            // _InfoText
            // 
            this._InfoText.BackColor = System.Drawing.Color.White;
            this._InfoText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._InfoText.Location = new System.Drawing.Point(388, 111);
            this._InfoText.Name = "_InfoText";
            this._InfoText.ReadOnly = true;
            this._InfoText.Size = new System.Drawing.Size(546, 351);
            this._InfoText.TabIndex = 22;
            this._InfoText.Text = "";
            // 
            // _DistcomboBox
            // 
            this._DistcomboBox.BackColor = System.Drawing.Color.White;
            this._DistcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DistcomboBox.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._DistcomboBox.FormattingEnabled = true;
            this._DistcomboBox.Location = new System.Drawing.Point(134, 111);
            this._DistcomboBox.Name = "_DistcomboBox";
            this._DistcomboBox.Size = new System.Drawing.Size(199, 29);
            this._DistcomboBox.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(31, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 21);
            this.label5.TabIndex = 24;
            this.label5.Text = "初始日期";
            // 
            // _FirstDateTimePicker
            // 
            this._FirstDateTimePicker.Location = new System.Drawing.Point(134, 225);
            this._FirstDateTimePicker.Name = "_FirstDateTimePicker";
            this._FirstDateTimePicker.Size = new System.Drawing.Size(101, 21);
            this._FirstDateTimePicker.TabIndex = 25;
            this._FirstDateTimePicker.Value = new System.DateTime(2018, 1, 18, 0, 0, 0, 0);
            // 
            // _clearBtn
            // 
            this._clearBtn.Location = new System.Drawing.Point(817, 39);
            this._clearBtn.Name = "_clearBtn";
            this._clearBtn.Size = new System.Drawing.Size(117, 36);
            this._clearBtn.TabIndex = 26;
            this._clearBtn.Text = "清空";
            this._clearBtn.UseVisualStyleBackColor = true;
            // 
            // _RemoveRecords
            // 
            this._RemoveRecords.Location = new System.Drawing.Point(166, 381);
            this._RemoveRecords.Name = "_RemoveRecords";
            this._RemoveRecords.Size = new System.Drawing.Size(117, 63);
            this._RemoveRecords.TabIndex = 27;
            this._RemoveRecords.Text = "清除记录";
            this._RemoveRecords.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(30, 309);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 21);
            this.label6.TabIndex = 28;
            this.label6.Text = "执行天数";
            // 
            // _RunDaysText
            // 
            this._RunDaysText.Location = new System.Drawing.Point(134, 309);
            this._RunDaysText.Name = "_RunDaysText";
            this._RunDaysText.ReadOnly = true;
            this._RunDaysText.Size = new System.Drawing.Size(100, 21);
            this._RunDaysText.TabIndex = 29;
            // 
            // _EndDateTimePicker
            // 
            this._EndDateTimePicker.Location = new System.Drawing.Point(135, 265);
            this._EndDateTimePicker.Name = "_EndDateTimePicker";
            this._EndDateTimePicker.Size = new System.Drawing.Size(101, 21);
            this._EndDateTimePicker.TabIndex = 31;
            this._EndDateTimePicker.Value = new System.DateTime(2019, 4, 28, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(32, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 21);
            this.label7.TabIndex = 30;
            this.label7.Text = "结束日期";
            // 
            // _RunPerLabel
            // 
            this._RunPerLabel.AutoSize = true;
            this._RunPerLabel.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._RunPerLabel.Location = new System.Drawing.Point(503, 47);
            this._RunPerLabel.Name = "_RunPerLabel";
            this._RunPerLabel.Size = new System.Drawing.Size(0, 21);
            this._RunPerLabel.TabIndex = 32;
            // 
            // _ProgressBar
            // 
            this._ProgressBar.Location = new System.Drawing.Point(388, 82);
            this._ProgressBar.Name = "_ProgressBar";
            this._ProgressBar.Size = new System.Drawing.Size(546, 23);
            this._ProgressBar.TabIndex = 33;
            // 
            // WriteDataMock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 568);
            this.Controls.Add(this._ProgressBar);
            this.Controls.Add(this._RunPerLabel);
            this.Controls.Add(this._EndDateTimePicker);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._RunDaysText);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._RemoveRecords);
            this.Controls.Add(this._clearBtn);
            this.Controls.Add(this._FirstDateTimePicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._DistcomboBox);
            this.Controls.Add(this._InfoText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._FirstReading);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._WriteReadingBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._comboxProject);
            this.Controls.Add(this.label1);
            this.Name = "WriteDataMock";
            this.Text = "WriteDataMock";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _comboxProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button _WriteReadingBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _FirstReading;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox _InfoText;
        private System.Windows.Forms.ComboBox _DistcomboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker _FirstDateTimePicker;
        private System.Windows.Forms.Button _clearBtn;
        private System.Windows.Forms.Button _RemoveRecords;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _RunDaysText;
        private System.Windows.Forms.DateTimePicker _EndDateTimePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label _RunPerLabel;
        private System.Windows.Forms.ProgressBar _ProgressBar;
    }
}