﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class AccountView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._GetPaymentList = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this._ItempropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // _GetPaymentList
            // 
            this._GetPaymentList.Location = new System.Drawing.Point(12, 37);
            this._GetPaymentList.Name = "_GetPaymentList";
            this._GetPaymentList.Size = new System.Drawing.Size(143, 55);
            this._GetPaymentList.TabIndex = 0;
            this._GetPaymentList.Text = "查询非水费扣费的费用";
            this._GetPaymentList.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(12, 117);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(143, 250);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // _ItempropertyGrid
            // 
            this._ItempropertyGrid.Location = new System.Drawing.Point(445, 37);
            this._ItempropertyGrid.Name = "_ItempropertyGrid";
            this._ItempropertyGrid.Size = new System.Drawing.Size(307, 396);
            this._ItempropertyGrid.TabIndex = 3;
            // 
            // AccountView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 473);
            this.Controls.Add(this._ItempropertyGrid);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this._GetPaymentList);
            this.Name = "AccountView";
            this.Text = "AccountView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _GetPaymentList;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.PropertyGrid _ItempropertyGrid;
    }
}