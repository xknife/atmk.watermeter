﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.WeiXin
{
    public partial class WeiXinCode2SessionView : DockContent
    {
        public const string AppIdTest = "wx5b7cb1ed8dd04f8e";
        public const string AppSecretTest = "de4e25faa443f116242bf615b7c4c04c";
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IWxLogin _wxLogin;
        public WeiXinCode2SessionView(IWxLogin wxLogin)
        {
            _wxLogin = wxLogin;
            InitializeComponent();
        }

        private async void _Code2Session_Click(object sender, EventArgs e)
        {
            try
            {
                var result = await _wxLogin.LoginCode2Session(AppIdTest, AppSecretTest, _AppCodeTxt.Text);
                _propertyGrid.SelectedObject = result;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
         
        }
    }
}
