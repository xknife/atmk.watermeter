﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views.WeiXin
{
    partial class WeiXinCode2SessionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Code2Session = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._AppCodeTxt = new System.Windows.Forms.TextBox();
            this._propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // _Code2Session
            // 
            this._Code2Session.Location = new System.Drawing.Point(508, 31);
            this._Code2Session.Name = "_Code2Session";
            this._Code2Session.Size = new System.Drawing.Size(119, 52);
            this._Code2Session.TabIndex = 1;
            this._Code2Session.Text = "API_Code2Session";
            this._Code2Session.UseVisualStyleBackColor = true;
            this._Code2Session.Click += new System.EventHandler(this._Code2Session_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Code";
            // 
            // _AppCodeTxt
            // 
            this._AppCodeTxt.Location = new System.Drawing.Point(68, 48);
            this._AppCodeTxt.Name = "_AppCodeTxt";
            this._AppCodeTxt.Size = new System.Drawing.Size(386, 21);
            this._AppCodeTxt.TabIndex = 3;
            // 
            // _propertyGrid
            // 
            this._propertyGrid.Location = new System.Drawing.Point(26, 111);
            this._propertyGrid.Name = "_propertyGrid";
            this._propertyGrid.Size = new System.Drawing.Size(613, 302);
            this._propertyGrid.TabIndex = 4;
            // 
            // WeiXinCode2SessionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 450);
            this.Controls.Add(this._propertyGrid);
            this.Controls.Add(this._AppCodeTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._Code2Session);
            this.Name = "WeiXinCode2SessionView";
            this.Text = "WeiXinApiView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _Code2Session;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _AppCodeTxt;
        private System.Windows.Forms.PropertyGrid _propertyGrid;
    }
}