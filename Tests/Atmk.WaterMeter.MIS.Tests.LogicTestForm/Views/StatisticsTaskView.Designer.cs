﻿namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    partial class StatisticsTaskView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._开启Button = new System.Windows.Forms.Button();
            this._读取Button = new System.Windows.Forms.Button();
            this._comboxProject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._InfoText = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // _开启Button
            // 
            this._开启Button.Location = new System.Drawing.Point(12, 72);
            this._开启Button.Name = "_开启Button";
            this._开启Button.Size = new System.Drawing.Size(104, 42);
            this._开启Button.TabIndex = 11;
            this._开启Button.Text = "开启";
            this._开启Button.UseVisualStyleBackColor = true;
            // 
            // _读取Button
            // 
            this._读取Button.Location = new System.Drawing.Point(12, 151);
            this._读取Button.Name = "_读取Button";
            this._读取Button.Size = new System.Drawing.Size(104, 42);
            this._读取Button.TabIndex = 12;
            this._读取Button.Text = "读取";
            this._读取Button.UseVisualStyleBackColor = true;
            // 
            // _comboxProject
            // 
            this._comboxProject.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._comboxProject.FormattingEnabled = true;
            this._comboxProject.Location = new System.Drawing.Point(116, 21);
            this._comboxProject.Name = "_comboxProject";
            this._comboxProject.Size = new System.Drawing.Size(199, 29);
            this._comboxProject.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 21);
            this.label1.TabIndex = 13;
            this.label1.Text = "选择项目";
            // 
            // _InfoText
            // 
            this._InfoText.BackColor = System.Drawing.Color.White;
            this._InfoText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._InfoText.Location = new System.Drawing.Point(228, 72);
            this._InfoText.Name = "_InfoText";
            this._InfoText.ReadOnly = true;
            this._InfoText.Size = new System.Drawing.Size(546, 351);
            this._InfoText.TabIndex = 23;
            this._InfoText.Text = "";
            // 
            // StatisticsTaskView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._InfoText);
            this.Controls.Add(this._comboxProject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._读取Button);
            this.Controls.Add(this._开启Button);
            this.Name = "StatisticsTaskView";
            this.Text = "StatisticsTaskView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _开启Button;
        private System.Windows.Forms.Button _读取Button;
        private System.Windows.Forms.ComboBox _comboxProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox _InfoText;
    }
}