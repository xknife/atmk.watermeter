﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.TestApp.Common.Utils;
using Newtonsoft.Json;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Views
{
    public partial class AreaView : DockContent
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IDistrictLogic _districtLogic;
        private readonly IUserCenterLogic _userCenterLogic;

        public AreaView(
            IDistrictLogic districtLogic,
            IUserCenterLogic userCenterLogic
        )
        {
            _districtLogic = districtLogic;
            _userCenterLogic = userCenterLogic;
            InitializeComponent();
            BindingEventArgs();
        }

        private void BindingEventArgs()
        {
            _查询项目和操作员Button.Click += (s, e) =>
            {
               var result = _userCenterLogic.SelectUserSite("b168c4f5-cc63-4fcf-8f6e-2155d4431264",
                    "e0fc9d93-bed0-4790-8a59-f2d4f389c198");
                var json = JsonConvert.SerializeObject(result);
                MessageBox.Show(json);
                //BindTree(json);
            };
        }
        private void BindTree(string data)
        {
            Invoke((EventHandler)delegate {
                JsonTreeViewHelper.BindTreeView(_ResultTreeView, data);
            });
        }
    }
}
