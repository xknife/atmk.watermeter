﻿namespace Atmk.WaterMeter.MIS.LogicTestForm
{
    partial class LogicWorkBench
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this._StripContainer = new System.Windows.Forms.ToolStripContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.逻辑LToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._StatisticsStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._项目片区操作员ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._账户费用逻辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._电信平台设备管理指令ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._上传读数逻辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.集成IToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成集成数据BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.功能ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._定时统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.微信ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._登陆code2SessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._写入抄表Mock数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._StripContainer.TopToolStripPanel.SuspendLayout();
            this._StripContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _StripContainer
            // 
            // 
            // _StripContainer.ContentPanel
            // 
            this._StripContainer.ContentPanel.Size = new System.Drawing.Size(1008, 704);
            this._StripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._StripContainer.Location = new System.Drawing.Point(0, 0);
            this._StripContainer.Name = "_StripContainer";
            this._StripContainer.Size = new System.Drawing.Size(1008, 729);
            this._StripContainer.TabIndex = 0;
            this._StripContainer.Text = "toolStripContainer1";
            // 
            // _StripContainer.TopToolStripPanel
            // 
            this._StripContainer.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件FToolStripMenuItem,
            this.逻辑LToolStripMenuItem,
            this.集成IToolStripMenuItem,
            this.功能ToolStripMenuItem,
            this.微信ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件FToolStripMenuItem
            // 
            this.文件FToolStripMenuItem.Name = "文件FToolStripMenuItem";
            this.文件FToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.文件FToolStripMenuItem.Text = "文件(&F)";
            // 
            // 逻辑LToolStripMenuItem
            // 
            this.逻辑LToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._StatisticsStripMenuItem,
            this._项目片区操作员ToolStripMenuItem,
            this._账户费用逻辑ToolStripMenuItem,
            this._电信平台设备管理指令ToolStripMenuItem,
            this._上传读数逻辑ToolStripMenuItem});
            this.逻辑LToolStripMenuItem.Name = "逻辑LToolStripMenuItem";
            this.逻辑LToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.逻辑LToolStripMenuItem.Text = "逻辑(&L)";
            // 
            // _StatisticsStripMenuItem
            // 
            this._StatisticsStripMenuItem.Name = "_StatisticsStripMenuItem";
            this._StatisticsStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this._StatisticsStripMenuItem.Text = "统计逻辑";
            this._StatisticsStripMenuItem.Click += new System.EventHandler(this._StatisticsStripMenuItem_Click);
            // 
            // _项目片区操作员ToolStripMenuItem
            // 
            this._项目片区操作员ToolStripMenuItem.Name = "_项目片区操作员ToolStripMenuItem";
            this._项目片区操作员ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this._项目片区操作员ToolStripMenuItem.Text = "项目片区操作员";
            // 
            // _账户费用逻辑ToolStripMenuItem
            // 
            this._账户费用逻辑ToolStripMenuItem.Name = "_账户费用逻辑ToolStripMenuItem";
            this._账户费用逻辑ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this._账户费用逻辑ToolStripMenuItem.Text = "账户费用逻辑";
            // 
            // _电信平台设备管理指令ToolStripMenuItem
            // 
            this._电信平台设备管理指令ToolStripMenuItem.Name = "_电信平台设备管理指令ToolStripMenuItem";
            this._电信平台设备管理指令ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this._电信平台设备管理指令ToolStripMenuItem.Text = "电信平台设备管理指令";
            // 
            // _上传读数逻辑ToolStripMenuItem
            // 
            this._上传读数逻辑ToolStripMenuItem.Name = "_上传读数逻辑ToolStripMenuItem";
            this._上传读数逻辑ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this._上传读数逻辑ToolStripMenuItem.Text = "上传读数逻辑";
            // 
            // 集成IToolStripMenuItem
            // 
            this.集成IToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.生成集成数据BToolStripMenuItem});
            this.集成IToolStripMenuItem.Name = "集成IToolStripMenuItem";
            this.集成IToolStripMenuItem.Size = new System.Drawing.Size(56, 21);
            this.集成IToolStripMenuItem.Text = "集成(&I)";
            // 
            // 生成集成数据BToolStripMenuItem
            // 
            this.生成集成数据BToolStripMenuItem.Name = "生成集成数据BToolStripMenuItem";
            this.生成集成数据BToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.生成集成数据BToolStripMenuItem.Text = "生成集成数据(&B)";
            this.生成集成数据BToolStripMenuItem.Click += new System.EventHandler(this.生成集成数据BToolStripMenuItem_Click);
            // 
            // 功能ToolStripMenuItem
            // 
            this.功能ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._定时统计ToolStripMenuItem,
            this._写入抄表Mock数据ToolStripMenuItem});
            this.功能ToolStripMenuItem.Name = "功能ToolStripMenuItem";
            this.功能ToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.功能ToolStripMenuItem.Text = "功能(&F)";
            // 
            // _定时统计ToolStripMenuItem
            // 
            this._定时统计ToolStripMenuItem.Name = "_定时统计ToolStripMenuItem";
            this._定时统计ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._定时统计ToolStripMenuItem.Text = "定时统计";
            // 
            // 微信ToolStripMenuItem
            // 
            this.微信ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._登陆code2SessionToolStripMenuItem});
            this.微信ToolStripMenuItem.Name = "微信ToolStripMenuItem";
            this.微信ToolStripMenuItem.Size = new System.Drawing.Size(64, 21);
            this.微信ToolStripMenuItem.Text = "微信(&W)";
            // 
            // _登陆code2SessionToolStripMenuItem
            // 
            this._登陆code2SessionToolStripMenuItem.Name = "_登陆code2SessionToolStripMenuItem";
            this._登陆code2SessionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this._登陆code2SessionToolStripMenuItem.Text = "登陆code2Session";
            // 
            // _写入抄表Mock数据ToolStripMenuItem
            // 
            this._写入抄表Mock数据ToolStripMenuItem.Name = "_写入抄表Mock数据ToolStripMenuItem";
            this._写入抄表Mock数据ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this._写入抄表Mock数据ToolStripMenuItem.Text = "写入抄表Mock数据";
            // 
            // LogicWorkBench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this._StripContainer);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LogicWorkBench";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this._StripContainer.TopToolStripPanel.ResumeLayout(false);
            this._StripContainer.TopToolStripPanel.PerformLayout();
            this._StripContainer.ResumeLayout(false);
            this._StripContainer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer _StripContainer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 逻辑LToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _StatisticsStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _项目片区操作员ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _账户费用逻辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _电信平台设备管理指令ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _上传读数逻辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 集成IToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成集成数据BToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 功能ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _定时统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 微信ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _登陆code2SessionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _写入抄表Mock数据ToolStripMenuItem;
    }
}

