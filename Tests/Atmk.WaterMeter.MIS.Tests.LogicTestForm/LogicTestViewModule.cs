﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.LogicTestForm.Dialogs;
using Atmk.WaterMeter.MIS.LogicTestForm.Views;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.ReadDataMock;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.WeiXin;
using Autofac;

namespace Atmk.WaterMeter.MIS.LogicTestForm
{
    public class LogicTestViewModule
    {
        public static void Bind(ContainerBuilder builder)
        {
            builder.RegisterType<LoginDialog>().As<LoginDialog>();
            builder.RegisterType<LogicWorkBench>().As<LogicWorkBench>();
            builder.RegisterType<StatisticsView>().As<StatisticsView>();
            builder.RegisterType<StatisticsTaskView>().As<StatisticsTaskView>();
            builder.RegisterType<AreaView>().As<AreaView>();
            builder.RegisterType<AccountView>().As<AccountView>();
            builder.RegisterType<CT_NBDeviceView>().As<CT_NBDeviceView>();
            builder.RegisterType<CT_NB_ReadingView>().As<CT_NB_ReadingView>();

            builder.RegisterType<BuildBaseDataView>().As<BuildBaseDataView>();
            builder.RegisterType<WeiXinCode2SessionView>().As<WeiXinCode2SessionView>();
            builder.RegisterType<WriteDataMock>().As<WriteDataMock>();
        }
       
    }
}
