﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.LogicTestForm.Views;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.DataMock;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.ReadDataMock;
using Atmk.WaterMeter.MIS.LogicTestForm.Views.WeiXin;
using NLog;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.LogicTestForm
{
    public partial class LogicWorkBench : Form
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly AccountView _accountView;
        private readonly AreaView _areaView;
        private readonly BuildBaseDataView _buildBaseDataView;
        private readonly CT_NBDeviceView _ctNbDeviceView;
        private readonly CT_NB_ReadingView _ctNbReadingView;
        private readonly StatisticsView _statistics;
        private readonly StatisticsTaskView _statisticsTaskView;
        private readonly WeiXinCode2SessionView _weiXinCode2SessionView;
        private readonly WriteDataMock _writeDataMock;

        public LogicWorkBench(
            StatisticsView statistics,
            AreaView areaView,
            AccountView accountView,
            CT_NBDeviceView ctNbDeviceView,
            CT_NB_ReadingView ctNbReadingView,
            BuildBaseDataView buildBaseDataView,
            StatisticsTaskView statisticsTaskView,
            WeiXinCode2SessionView weiXinCode2SessionView,
            WriteDataMock writeDataMock
        )
        {
            _Logger.Info("Workbench init...");
            InitializeComponent();
            InitializeDockPanel();
            _statistics = statistics;
            _areaView = areaView;
            _accountView = accountView;
            _ctNbDeviceView = ctNbDeviceView;
            _ctNbReadingView = ctNbReadingView;
            _buildBaseDataView = buildBaseDataView;
            _statisticsTaskView = statisticsTaskView;
            _weiXinCode2SessionView = weiXinCode2SessionView;
            _writeDataMock = writeDataMock;
            BindingEventArgs();
        }

        private void BindingEventArgs()
        {
            _项目片区操作员ToolStripMenuItem.Click += (s, e) => { _areaView.Show(_dockPanel, DockState.Document); };
            _账户费用逻辑ToolStripMenuItem.Click += (s, e) => { _accountView.Show(_dockPanel, DockState.Document); };
            _电信平台设备管理指令ToolStripMenuItem.Click += (s, e) => { _ctNbDeviceView.Show(_dockPanel, DockState.Document); };
            _上传读数逻辑ToolStripMenuItem.Click += (s, e) => { _ctNbReadingView.Show(_dockPanel, DockState.Document); };
            _定时统计ToolStripMenuItem.Click += (s, e) => { _statisticsTaskView.Show(_dockPanel, DockState.Document); };
            _登陆code2SessionToolStripMenuItem.Click += (s, e) => { _weiXinCode2SessionView.Show(_dockPanel, DockState.Document); };
            _写入抄表Mock数据ToolStripMenuItem.Click += (s, e) => { _writeDataMock.Show(_dockPanel, DockState.Document); };
        }

        private void _StatisticsStripMenuItem_Click(object sender, EventArgs e)
        {
            _statistics.Show(_dockPanel, DockState.Document);
        }

        private void 生成集成数据BToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _buildBaseDataView.Show(_dockPanel, DockState.Document);
        }

        #region DockPanel

        private const string DOCK_PANEL_CONFIG = "dockpanel3.config";
        private DockPanel _dockPanel;
        private LoggerView _loggerView;

        private static string GetLayoutConfigFile()
        {
            var dir = Path.GetDirectoryName(Application.ExecutablePath);
            return dir != null ? Path.Combine(dir, DOCK_PANEL_CONFIG) : DOCK_PANEL_CONFIG;
        }

        private void InitializeDockPanel()
        {
            SuspendLayout();


            _dockPanel = new DockPanel();
            _StripContainer.ContentPanel.Controls.Add(_dockPanel);

            _dockPanel.DocumentStyle = DocumentStyle.DockingWindow;
            _dockPanel.Theme = new VS2015BlueTheme();
            _dockPanel.Dock = DockStyle.Fill;
            _dockPanel.BringToFront();

            DockPanelLoadFromXml();

            if (_loggerView == null)
            {
                _loggerView = new LoggerView(); //DI.Get<LoggerView>();
                _loggerView.Show(_dockPanel, DockState.DockBottom);
            }

            PerformLayout();
            ResumeLayout(false);
        }

        /// <summary>
        ///     引发 <see cref="E:System.Windows.Forms.Form.Closing" /> 事件。
        /// </summary>
        /// <param name="e">
        ///     包含事件数据的 <see cref="T:System.ComponentModel.CancelEventArgs" />。
        /// </param>
        protected override void OnClosing(CancelEventArgs e)
        {
            DockPanelSaveAsXml();
            base.OnClosing(e);
        }

        /// <summary>
        ///     控件提供了一个保存布局状态的方法，它默认是没有保存的。
        /// </summary>
        private void DockPanelSaveAsXml()
        {
            _dockPanel.SaveAsXml(GetLayoutConfigFile());
        }

        private void DockPanelLoadFromXml()
        {
            //加载布局
            var deserializeDockContent = new DeserializeDockContent(GetViewFromPersistString);
            var configFile = GetLayoutConfigFile();
            if (File.Exists(configFile))
                _dockPanel.LoadFromXml(configFile, deserializeDockContent);
        }

        private IDockContent GetViewFromPersistString(string persistString)
        {
            //            if (persistString == typeof(InterfaceTreeView).ToString())
            //            {
            //                if (_InterfaceTreeViewMenuItem.Checked)
            //                    return _InterfaceTreeView;
            //            }
            //            if (persistString == typeof(DataMangerView).ToString())
            //            {
            //                if (_DataManagerViewMenuItem.Checked)
            //                    return _DataManagerView;
            //            }
            //            if (persistString == typeof(CommandConsoleView).ToString())
            //            {
            //                if (_CommandConsoleViewMenuItem.Checked)
            //                    return _CommandConsoleView;
            //            }
            return null;
        }

        #endregion
    }
}