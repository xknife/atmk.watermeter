﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.Kernel;
using Atmk.WaterMeter.MIS.LogicTestForm.Dialogs;
using Autofac;

namespace Atmk.WaterMeter.MIS.LogicTestForm
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var builder = new ContainerBuilder();
            IoCManager.Instance.Register(builder);
            //AutofacManager.Bind(builder);
            LogicTestViewModule.Bind(builder);
            using (var scope = builder.Build().BeginLifetimeScope())
            {
                Application.Run(scope.Resolve<LogicWorkBench>());
            }
          
        }
    }
}
