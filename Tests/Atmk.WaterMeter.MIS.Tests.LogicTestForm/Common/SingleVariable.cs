﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons;
using Autofac;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Common
{
    public class SingleVariable
    {
        private static SingleVariable _SingleVariable;
        private SingleVariable()
        {

        }
        public static SingleVariable Instance()
        {
            return _SingleVariable ?? (_SingleVariable = new SingleVariable());
        }
        public Token Token { get; set; }

    }
}
