﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.LogicTestForm.Common
{
    public class MathsHelper
    {
        /// <summary>
        /// 获取百分比
        /// </summary>
        /// <param name="n"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double Percent(int n, int d)
        {
            var percent = Convert.ToDouble(n) / Convert.ToDouble(d);
            return percent;
        }
        /// <summary>
        /// 获取百分比字符串
        /// </summary>
        /// <param name="n"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string PercentString(int n, int d)
        {
            var percent = Percent(n, d);
            var pString = percent.ToString("p2");//可以得到5.88%
            return pString;
        }
    }
}
