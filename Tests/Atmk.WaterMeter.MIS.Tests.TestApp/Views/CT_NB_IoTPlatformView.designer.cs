﻿namespace Atmk.WaterMeter.MIS.TestApp.Views
{
    partial class CT_NB_IoTPlatformView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._listBox = new System.Windows.Forms.ListBox();
            this._DeviceId = new System.Windows.Forms.TextBox();
            this._MeterNoText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._DelDevice = new System.Windows.Forms.Button();
            this._AddDevice = new System.Windows.Forms.Button();
            this._IMEI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._DevicesBtn = new System.Windows.Forms.Button();
            this._ReadMetersBtn = new System.Windows.Forms.Button();
            this._ValveOpenBtn = new System.Windows.Forms.Button();
            this._ValveCloseBtn = new System.Windows.Forms.Button();
            this._PropertyGrid = new System.Windows.Forms.PropertyGrid();
            this._HourlyFreezingBtn = new System.Windows.Forms.Button();
            this._TreeView = new System.Windows.Forms.TreeView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._FreezeTypeComboBox = new System.Windows.Forms.ComboBox();
            this._FreezingDataGetBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _listBox
            // 
            this._listBox.FormattingEnabled = true;
            this._listBox.ItemHeight = 12;
            this._listBox.Location = new System.Drawing.Point(12, 36);
            this._listBox.Name = "_listBox";
            this._listBox.Size = new System.Drawing.Size(187, 532);
            this._listBox.TabIndex = 13;
            // 
            // _DeviceId
            // 
            this._DeviceId.Enabled = false;
            this._DeviceId.Location = new System.Drawing.Point(303, 100);
            this._DeviceId.Name = "_DeviceId";
            this._DeviceId.Size = new System.Drawing.Size(332, 21);
            this._DeviceId.TabIndex = 12;
            // 
            // _MeterNoText
            // 
            this._MeterNoText.Location = new System.Drawing.Point(303, 16);
            this._MeterNoText.Name = "_MeterNoText";
            this._MeterNoText.Size = new System.Drawing.Size(332, 21);
            this._MeterNoText.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "平台设备Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "水表编号";
            // 
            // _DelDevice
            // 
            this._DelDevice.Location = new System.Drawing.Point(364, 150);
            this._DelDevice.Name = "_DelDevice";
            this._DelDevice.Size = new System.Drawing.Size(119, 52);
            this._DelDevice.TabIndex = 8;
            this._DelDevice.Text = "删除设备";
            this._DelDevice.UseVisualStyleBackColor = true;
            // 
            // _AddDevice
            // 
            this._AddDevice.Location = new System.Drawing.Point(218, 150);
            this._AddDevice.Name = "_AddDevice";
            this._AddDevice.Size = new System.Drawing.Size(119, 52);
            this._AddDevice.TabIndex = 7;
            this._AddDevice.Text = "注册设备";
            this._AddDevice.UseVisualStyleBackColor = true;
            // 
            // _IMEI
            // 
            this._IMEI.Location = new System.Drawing.Point(303, 59);
            this._IMEI.Name = "_IMEI";
            this._IMEI.Size = new System.Drawing.Size(332, 21);
            this._IMEI.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "IMEI";
            // 
            // _DevicesBtn
            // 
            this._DevicesBtn.Location = new System.Drawing.Point(516, 150);
            this._DevicesBtn.Name = "_DevicesBtn";
            this._DevicesBtn.Size = new System.Drawing.Size(119, 52);
            this._DevicesBtn.TabIndex = 16;
            this._DevicesBtn.Text = "获取所有设备信息";
            this._DevicesBtn.UseVisualStyleBackColor = true;
            // 
            // _ReadMetersBtn
            // 
            this._ReadMetersBtn.Location = new System.Drawing.Point(218, 222);
            this._ReadMetersBtn.Name = "_ReadMetersBtn";
            this._ReadMetersBtn.Size = new System.Drawing.Size(119, 52);
            this._ReadMetersBtn.TabIndex = 17;
            this._ReadMetersBtn.Text = "读取设备数据";
            this._ReadMetersBtn.UseVisualStyleBackColor = true;
            // 
            // _ValveOpenBtn
            // 
            this._ValveOpenBtn.Location = new System.Drawing.Point(364, 222);
            this._ValveOpenBtn.Name = "_ValveOpenBtn";
            this._ValveOpenBtn.Size = new System.Drawing.Size(119, 52);
            this._ValveOpenBtn.TabIndex = 18;
            this._ValveOpenBtn.Text = "开阀";
            this._ValveOpenBtn.UseVisualStyleBackColor = true;
            // 
            // _ValveCloseBtn
            // 
            this._ValveCloseBtn.Location = new System.Drawing.Point(516, 222);
            this._ValveCloseBtn.Name = "_ValveCloseBtn";
            this._ValveCloseBtn.Size = new System.Drawing.Size(119, 52);
            this._ValveCloseBtn.TabIndex = 19;
            this._ValveCloseBtn.Text = "关阀";
            this._ValveCloseBtn.UseVisualStyleBackColor = true;
            // 
            // _PropertyGrid
            // 
            this._PropertyGrid.Location = new System.Drawing.Point(218, 359);
            this._PropertyGrid.Name = "_PropertyGrid";
            this._PropertyGrid.Size = new System.Drawing.Size(417, 204);
            this._PropertyGrid.TabIndex = 20;
            // 
            // _HourlyFreezingBtn
            // 
            this._HourlyFreezingBtn.Location = new System.Drawing.Point(364, 307);
            this._HourlyFreezingBtn.Name = "_HourlyFreezingBtn";
            this._HourlyFreezingBtn.Size = new System.Drawing.Size(119, 52);
            this._HourlyFreezingBtn.TabIndex = 23;
            this._HourlyFreezingBtn.Text = "冻结数据指令";
            this._HourlyFreezingBtn.UseVisualStyleBackColor = true;
            // 
            // _TreeView
            // 
            this._TreeView.Location = new System.Drawing.Point(665, 36);
            this._TreeView.Name = "_TreeView";
            this._TreeView.Size = new System.Drawing.Size(376, 532);
            this._TreeView.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "设备列表";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(663, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 26;
            this.label5.Text = "平台响应数据";
            // 
            // _FreezeTypeComboBox
            // 
            this._FreezeTypeComboBox.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this._FreezeTypeComboBox.FormattingEnabled = true;
            this._FreezeTypeComboBox.Items.AddRange(new object[] {
            "24小时冻结",
            "日冻结",
            "月冻结"});
            this._FreezeTypeComboBox.Location = new System.Drawing.Point(218, 318);
            this._FreezeTypeComboBox.Name = "_FreezeTypeComboBox";
            this._FreezeTypeComboBox.Size = new System.Drawing.Size(121, 27);
            this._FreezeTypeComboBox.TabIndex = 27;
            // 
            // _FreezingDataGetBtn
            // 
            this._FreezingDataGetBtn.Location = new System.Drawing.Point(516, 307);
            this._FreezingDataGetBtn.Name = "_FreezingDataGetBtn";
            this._FreezingDataGetBtn.Size = new System.Drawing.Size(119, 52);
            this._FreezingDataGetBtn.TabIndex = 28;
            this._FreezingDataGetBtn.Text = "冻结数据获取";
            this._FreezingDataGetBtn.UseVisualStyleBackColor = true;
            // 
            // CT_NB_IoTPlatformView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1273, 599);
            this.Controls.Add(this._FreezingDataGetBtn);
            this.Controls.Add(this._FreezeTypeComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._TreeView);
            this.Controls.Add(this._HourlyFreezingBtn);
            this.Controls.Add(this._PropertyGrid);
            this.Controls.Add(this._ValveCloseBtn);
            this.Controls.Add(this._ValveOpenBtn);
            this.Controls.Add(this._ReadMetersBtn);
            this.Controls.Add(this._DevicesBtn);
            this.Controls.Add(this._IMEI);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._listBox);
            this.Controls.Add(this._DeviceId);
            this.Controls.Add(this._MeterNoText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._DelDevice);
            this.Controls.Add(this._AddDevice);
            this.Name = "CT_NB_IoTPlatformView";
            this.Text = "CT_NB_IoTPlatformView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _listBox;
        private System.Windows.Forms.TextBox _DeviceId;
        private System.Windows.Forms.TextBox _MeterNoText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _DelDevice;
        private System.Windows.Forms.Button _AddDevice;
        private System.Windows.Forms.TextBox _IMEI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _DevicesBtn;
        private System.Windows.Forms.Button _ReadMetersBtn;
        private System.Windows.Forms.Button _ValveOpenBtn;
        private System.Windows.Forms.Button _ValveCloseBtn;
        private System.Windows.Forms.PropertyGrid _PropertyGrid;
        private System.Windows.Forms.Button _HourlyFreezingBtn;
        private System.Windows.Forms.TreeView _TreeView;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox _FreezeTypeComboBox;
        private System.Windows.Forms.Button _FreezingDataGetBtn;
    }
}