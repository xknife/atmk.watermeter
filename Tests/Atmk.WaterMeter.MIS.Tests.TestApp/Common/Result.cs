﻿namespace Atmk.WaterMeter.MIS.TestApp.Common
{
    public class Result
    {
        public int errcode { get; set; }
        public string msg { get; set; }
    }
}