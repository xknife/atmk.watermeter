﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Atmk.WaterMeter.MIS.TestApp.Common.Utils
{
    public class JTypeDescriptor : ICustomTypeDescriptor
    {
        public JTypeDescriptor(JObject jobject)
        {
            JObject = jobject ?? throw new ArgumentNullException(nameof(jobject));
        }

        // NOTE: the property grid needs at least one r/w property otherwise it will not show properly in collection editors...
        public JObject JObject { get; set; }

        public override string ToString()
        {
            // we display this object's serialized json as the display name, for example
            return JObject.ToString(Formatting.None);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            // browse the JObject and build a list of pseudo-properties
            var props = new List<PropertyDescriptor>();
            foreach (var kv in JObject)
            {
                props.Add(new Prop(kv.Key, kv.Value, null));
            }
            return new PropertyDescriptorCollection(props.ToArray());
        }

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return null;
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return null;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return null;
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return null;
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            throw new NotImplementedException();
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return null;
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return null;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            throw new NotImplementedException();
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            throw new NotImplementedException();
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return ((ICustomTypeDescriptor)this).GetProperties(null);
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        // represents one dynamic pseudo-property
        private class Prop : PropertyDescriptor
        {
            private object _value;

            public Prop(string name, object value, Attribute[] attrs)
                : base(name, attrs)
            {
                _value = ComputeValue(value);
                PropertyType = _value == null ? typeof(object) : _value.GetType();
            }

            private static object ComputeValue(object value)
            {
                if (value == null)
                    return null;

                if (value is JArray array)
                {
                    // we use the arraylist because that's all the property grid needs...
                    var list = new ArrayList();
                    for (var i = 0; i < array.Count; i++)
                    {
                        if (array[i] is JObject subo)
                        {
                            var td = new JTypeDescriptor(subo);
                            list.Add(td);
                        }
                        else
                        {
                            if (array[i] is JValue jv)
                            {
                                list.Add(jv.Value);
                            }
                            else
                            {
                                // etc.
                            }
                        }
                    }
                    // we don't support adding things
                    return ArrayList.ReadOnly(list);
                }
                else
                {
                    // etc.
                }
                return value;
            }

            public override bool CanResetValue(object component)
            {
                return false;
            }

            public override Type ComponentType => typeof(object);

            public override object GetValue(object component)
            {
                return _value;
            }

            public override bool IsReadOnly => false;

            public override Type PropertyType { get; }

            public override void ResetValue(object component)
            {
            }

            public override void SetValue(object component, object value)
            {
                _value = value;
            }

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }
        }
    }
}
