﻿namespace Atmk.WaterMeter.MIS.TestApp.Models.Core
{
    public class OwnerMessage
    {
        /// <summary>
        /// 
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 身份证
        /// </summary>
        public string idType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string idNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string builtArea { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string populationNumber { get; set; }
    }

    public class Owner
    {
        /// <summary>
        /// 
        /// </summary>
        public string ownerId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ownerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string houseNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OwnerMessage ownerMessage { get; set; }
    }

    public class MeterDatas
    {
        /// <summary>
        /// 
        /// </summary>
        public string concentratorName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string collector { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string meterBottomNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string multipleRate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string meterReader { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string variableRatio { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string correctionValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string meterModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string installDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string dailyDosage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string accountNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string relaySign { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string workingMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string powerLimitingPpower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string acoustoOpticAlarmValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string gateAlarmValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string overdraftDosage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string maximumRechargeLlimit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string minimumResidualAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lineOfCredit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string freezingMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string freezingData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string decimals { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string dateOfProduction { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string factoryNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string installAddress { get; set; }
    }

    public class WaterMeter
    {
        /// <summary>
        /// 冷水水表
        /// </summary>
        public string meterType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string meterNumber { get; set; }
        /// <summary>
        /// 后付费
        /// </summary>
        public string refillType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string meterState { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string priceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MeterDatas meterDatas { get; set; }
    }

    public class OwnerMeterModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string districtId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Owner owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public WaterMeter waterMeter { get; set; }
    }
}
