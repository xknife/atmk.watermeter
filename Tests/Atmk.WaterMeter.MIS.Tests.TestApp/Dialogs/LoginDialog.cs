﻿using System;
using System.Diagnostics;
using Atmk.WaterMeter.MIS.TestApp.Common;
using Newtonsoft.Json;
using NKnife.Win.Forms;
using NLog;
using RestSharp;

namespace Atmk.WaterMeter.MIS.TestApp.Dialogs
{
    public partial class LoginDialog : SimpleForm
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        //private string Url = "https://www.firstdomainbyxiang.info:15000/";
        //private string Url = "http://127.0.0.1:5000/";
        private string Url = "https://water-meter-demo.aixiyou.com:3385/";
        public LoginDialog()
        {
            InitializeComponent();

#if !DEBUG
            Url = "https://water-meter-demo.aixiyou.com:3385/";
#endif
            _urlTextBox.Text = Url;
        }

        private void _acceptButton_Click(object sender, EventArgs e)
        {
            var kernel = new Kernel();
            kernel.Url = _urlTextBox.Text;
            var request = new RestRequest(UrlManager.SignIn, Method.POST);
            request.AddJsonBody(new sign_vo(_userNameTextBox.Text, _passwordTextBox.Text));

            kernel.Client.ExecuteAsync(request, response =>
            {
                //_Logger.Trace(response.ResponseUri.AbsoluteUri);
                _Logger.Debug(response.Content);
                try
                {
                    var signReturn = JsonConvert.DeserializeObject<SignResult>(response.Content);
                    kernel.Token = signReturn.data.token;
                    _Logger.Trace(kernel.Token);
                }
                catch (Exception exception)
                {
                    _Logger.Error(exception);
                }

            });
            Close();
        }
       
        internal class sign_vo
        {
            public sign_vo(string username, string password)
            {
                this.username = username;
                this.password = password;
            }
            public string username { get; set; }
            public string password { get; set; }
        }

        public class TokenData
        {
            public string token { get; set; }
        }

        public class SignResult : Result
        {
            public TokenData data { get; set; }
        }
       

        public class UserInfoResult:Result
        {
            /// <summary>
            /// 
            /// </summary>
            public UserInfoData data { get; set; }
        }
        public class UserInfoData : IData
        {
            public string[] role { get; set; }
            public object[] authority { get; set; }
            public string name { get; set; }
            /// <summary>
            /// 项目id
            /// </summary>
            public string projectId { get; set; }
        }
        /// <summary>
        /// 接口数据基类，用于统一返回数据用
        /// </summary>
        public interface IData
        {
        }

    }
}
