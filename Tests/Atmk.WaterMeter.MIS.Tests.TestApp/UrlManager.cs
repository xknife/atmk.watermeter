﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.TestApp
{
    class UrlManager
    {
        private const string BASE = "/api/v1/";

        public static string SignIn = $"{BASE}Sign/SignIn";

        public static string GetCharDatas = $"{BASE}Chart/GetCharDatas/123";

        public static string GetUserInfo = $"{BASE}/UserCenter/UserInfo";

        public static string GetAreaUser = $"{BASE}/System/UserSite";

        public static string DeleteDistrict = $"{BASE}/Districts/DeletDistricts";
        
        public static string GetDistricts = $"{BASE}/Districts/SelectDistricts";

        public static string InsertDistricts = $"{BASE}Districts/InsertDistricts";

        public static string InsertArea = $"{BASE}System/Sites";

        public static string InsertStaff = $"{BASE}System/Users";

        public static string InsertRoles = $"{BASE}System/Roles";

        public static string DeviceDataChanged = $"/api/CT_Callback/DeviceDataChanged";

        public static string AddOwnerMeter = $"{BASE}OwnerMeter/AddOwnerMeter";

        public static string GetQueryOwMe = $"{BASE}OwnerMeter/SelectQueryOwMe";

        public static string SelectSearch = $"{BASE}OwnerMeter/SelectSearch";

        public static string GetTreeNodes = $"{BASE}Districts/Nodes";

        #region 阶梯水价

        public static string GetPriceStep = $"{BASE}System/PriceStep";
        public static string GetPriceStepByMeterType = $"{GetPriceStep}/MeterType";
        public static string AddPriceStep = $"{GetPriceStep}/Price";
        public static string AddFee = $"{GetPriceStep}/Fee";

        #endregion

        public static string ClearData = $"/Data";

        #region 电信平台
        private const string CTBASE = "/api/v1/tp/ct/";
        public static string CT_Device = $"{CTBASE}mg/device";
        public static string CT_DevInfo = $"{CTBASE}rd/record";
        public static string CT_Valve = $"{CTBASE}cm/valve";
        public static string CT_SetFreezeing = $"{CTBASE}cm/freezing";
        public static string CT_GetFreezeing = $"{CTBASE}rd/freezing";

        #endregion

    }
}
