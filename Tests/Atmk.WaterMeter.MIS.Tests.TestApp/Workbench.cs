﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atmk.WaterMeter.MIS.TestApp.Dialogs;
using Atmk.WaterMeter.MIS.TestApp.Views;
using Common.Logging;
using WeifenLuo.WinFormsUI.Docking;

namespace Atmk.WaterMeter.MIS.TestApp
{
    public partial class Workbench : Form
    {
        private static readonly ILog _Logger = LogManager.GetLogger<Workbench>();

        private bool _isLogin;

        public Workbench()
        {
            _Logger.Info("Workbench init...");
            InitializeComponent();
            InitializeDockPanel();
            Activated += (s, e) =>
            {
                new Thread(() =>
                {
                    if (!_isLogin)
                    {
                        _isLogin = true;
                        var dialog = new LoginDialog();
                        this.ThreadSafeInvoke(() =>
                        {
                            dialog.ShowDialog(this);
                            Activate();
                            Focus();
                        });
                    }
                }).Start();
            };
            _设备管理ToolStripMenuItem.Click += (s, e) =>
            {
                var view = new CT_NB_IoTPlatformView();
                view.Show(_dockPanel, DockState.Document);
            };
        }


        #region DockPanel

        private const string DOCK_PANEL_CONFIG = "dockpanel3.config";
        private DockPanel _dockPanel;
        private LoggerView _loggerView;

        private static string GetLayoutConfigFile()
        {
            var dir = Path.GetDirectoryName(Application.ExecutablePath);
            return dir != null ? Path.Combine(dir, DOCK_PANEL_CONFIG) : DOCK_PANEL_CONFIG;
        }

        private void InitializeDockPanel()
        {
            SuspendLayout();

            _dockPanel = new DockPanel();
            _StripContainer.ContentPanel.Controls.Add(_dockPanel);

            _dockPanel.DocumentStyle = DocumentStyle.DockingWindow;
            _dockPanel.Theme = new VS2015BlueTheme();
            _dockPanel.Dock = DockStyle.Fill;
            _dockPanel.BringToFront();

            DockPanelLoadFromXml();

            if (_loggerView == null)
            {
                _loggerView = new LoggerView(); //DI.Get<LoggerView>();
                _loggerView.Show(_dockPanel, DockState.DockBottom);
            }

            PerformLayout();
            ResumeLayout(false);
        }

        /// <summary>
        ///   引发 <see cref="E:System.Windows.Forms.Form.Closing" /> 事件。
        /// </summary>
        /// <param name="e">
        ///   包含事件数据的 <see cref="T:System.ComponentModel.CancelEventArgs" />。
        /// </param>
        protected override void OnClosing(CancelEventArgs e)
        {
            DockPanelSaveAsXml();
            base.OnClosing(e);
        }

        /// <summary>
        ///     控件提供了一个保存布局状态的方法，它默认是没有保存的。
        /// </summary>
        private void DockPanelSaveAsXml()
        {
            _dockPanel.SaveAsXml(GetLayoutConfigFile());
        }

        private void DockPanelLoadFromXml()
        {
            //加载布局
            var deserializeDockContent = new DeserializeDockContent(GetViewFromPersistString);
            var configFile = GetLayoutConfigFile();
            if (File.Exists(configFile))
                _dockPanel.LoadFromXml(configFile, deserializeDockContent);
        }

        private IDockContent GetViewFromPersistString(string persistString)
        {
            if (persistString == typeof(LoggerView).ToString())
            {
                return _loggerView;
            }

            //            if (persistString == typeof(InterfaceTreeView).ToString())
            //            {
            //                if (_InterfaceTreeViewMenuItem.Checked)
            //                    return _InterfaceTreeView;
            //            }
            //            if (persistString == typeof(DataMangerView).ToString())
            //            {
            //                if (_DataManagerViewMenuItem.Checked)
            //                    return _DataManagerView;
            //            }
            //            if (persistString == typeof(CommandConsoleView).ToString())
            //            {
            //                if (_CommandConsoleViewMenuItem.Checked)
            //                    return _CommandConsoleView;
            //            }
            return null;
        }

        #endregion
    }

}
