﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public static class Tools
    {
        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();   //获取类型
            MemberInfo[] memberInfos = type.GetMember(en.ToString());   //获取成员
            if (memberInfos != null && memberInfos.Length > 0)
            {
                DescriptionAttribute[] attrs = memberInfos[0].GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];   //获取描述特性

                if (attrs != null && attrs.Length > 0)
                {
                    return attrs[0].Description;    //返回当前描述
                }
            }
            return en.ToString();
        }

        //阀门状态
        public static string ValveStatic(int? value)
        {
            switch (value)
            {
                case 0: return "关阀";
                case 1: return "开阀";
                case 2: return "异常";
                case 3: return "异常";
                default: return "";
            }
        }

        //数据状态
        public static string Value24Type(int? value)
        {
            switch (value)
            {
                case 0: return "";
                case 1: return "当前数据";
                case 2: return "冻结数据";
                case 3: return "换表";
                case 4: return "销表";
                default: return "";
            }
        }


        public static List<District> getNodeNext(List<District> districts, string id, List<District> districtsOld, List<District> result_last_District)
        {

            if (districts.Any(m => m.Id.ToString() == id && m.NodeType == 2))
            {
                result_last_District.AddRange(districts.Where(m => m.Id.ToString() == id));
            }
            else
            {
                foreach (var item in districtsOld.Where(m => m.Parent == id))
                {
                    getNodeNext(districtsOld.Where(m => m.Parent == id).ToList(), item.Id.ToString(), districtsOld, result_last_District);
                }
            }
            return result_last_District;
        }
    }
}
