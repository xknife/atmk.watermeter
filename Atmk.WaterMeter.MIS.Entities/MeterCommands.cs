﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     水表指令集合记录
    /// </summary>
    public class MeterCommands : BaseRecord
    {
        public MeterCommands()
        {
            CommandStatus = Enums.CommandStatus.未发送;
            CommandCount = 0;
        }

        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MeterNumber { get; set; }

        /// <summary>
        ///     水表指令内容
        /// </summary>
        [Display(Name = "水表指令内容")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(200, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Command { get; set; }

        /// <summary>
        ///     指令状态
        /// </summary>
        [Display(Name = "指令状态")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public CommandStatus CommandStatus { get; set; }

        /// <summary>
        ///     指令发送次数
        /// </summary>
        [Display(Name = "指令发送次数")]
        public int CommandCount { get; set; }

//        /// <summary>
//        ///     指令创建时间
//        /// </summary>
//        [Display(Name = "指令创建时间")]
//        [Required(ErrorMessage = "{0}不能为空")]
//        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
//        public DateTime CreatTime { get; set; }
//
        /// <summary>
        ///     指令最近运行时间
        /// </summary>
        [Display(Name = "水表指令最近运行时间")]
        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
        public DateTime ExecuteTime { get; set; }
    }
}