﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities
{
    public class ProjectConfigurationModel
    {
        /// <summary>
        /// 单位名称
        /// </summary>
        public string company { get; set; } = string.Empty;
        /// <summary>
        /// 备份路径
        /// </summary>
        public string backupPath { get; set; } = string.Empty;
        /// <summary>
        /// 退出时自动备份
        /// </summary>
        public string autoBackup { get; set; } = string.Empty;
        /// <summary>
        /// 自动备份保存天数
        /// </summary>
        public string keepDays { get; set; } = string.Empty;
        /// <summary>
        /// 出厂用量
        /// </summary>
        public string usage { get; set; } = string.Empty;
        /// <summary>
        /// 小数位
        /// </summary>
        public string decimalLength { get; set; } = string.Empty;
        /// <summary>
        /// 票据编号生成方式：自动生成，手工输入.0或1
        /// </summary>
        public string invoiceNumberCreate { get; set; } = string.Empty;
        /// <summary>
        /// 票据编号前缀
        /// </summary>
        public string invoiceNumberPrefix { get; set; } = string.Empty;
        /// <summary>
        /// 票据编号长度
        /// </summary>
        public string invoiceNumberLength { get; set; } = string.Empty;
        /// <summary>
        /// 当前票据编号
        /// </summary>
        public string invoiceCurrentNumber { get; set; } = string.Empty;
        /// <summary>
        /// 票据编号设置说明,可以是富文本文字
        /// </summary>
        public string invoiceSettingTips { get; set; } = string.Empty;
        /// <summary>
        /// 用户编号生成方式：自动生成，手工输入
        /// </summary>
        public string userNumberCreate { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string userNumberPrefix { get; set; } = string.Empty;
        /// <summary>
        /// 使用片区编码
        /// </summary>
        public string userDistrictCode { get; set; } = string.Empty;
        /// <summary>
        /// 用户编号长度
        /// </summary>
        public string userNumberLength { get; set; } = string.Empty;
        /// <summary>
        /// 当前用户编号
        /// </summary>
        public string userCurrentNumber { get; set; } = string.Empty;
        /// <summary>
        /// 用户编号设置说明,可以是富文本文字
        /// </summary>
        public string userSettingTips { get; set; } = string.Empty;
        /// <summary>
        /// 数据采集服务器IP
        /// </summary>
        public string serviceIP { get; set; } = string.Empty;
        /// <summary>
        /// 端口
        /// </summary>
        public string port { get; set; } = string.Empty;
        /// <summary>
        /// 连续控阀次数
        /// </summary>
        public string controlCount { get; set; } = string.Empty;
        /// <summary>
        /// 欠费自动关阀,0或1
        /// </summary>
        public string autoClose { get; set; } = string.Empty;
        /// <summary>
        /// 欠费关阀限定值
        /// </summary>
        public string limitValue { get; set; } = string.Empty;


        /// <summary>
        /// 集中器通讯方式：固定IP与非固定IP，二者只传一个.0或1
        /// </summary>
        public string communicationType { get; set; } = string.Empty;
        /// <summary>
        /// 缴费方式（后付费，预付费，账户预存），多选
        /// </summary>
        public string payType { get; set; } = string.Empty;
        /// <summary>
        /// 表类型（冷水水表、生活热水水表、直饮水水表、中水水表、FS型大水表），多选
        /// </summary>
        public string meterType { get; set; } = string.Empty;

    }
}
