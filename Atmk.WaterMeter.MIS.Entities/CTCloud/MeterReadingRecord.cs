﻿using System;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.CTCloud
{
    /// <summary>
    ///     实体类。一个描述电信云Post进本平台的数据包，该数据包包含水表读数和水表当前状态；该数据包描述水表日常上报数据的基本格式。
    /// </summary>
    public class MeterReadingRecord : BaseRecord
    {
        /*
        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// 片区ID
        /// </summary>
        public string DistrictId { get; set; }
        /// <summary>
        /// 业主ID
        /// </summary>
        public string OwerId { get; set; }
        /// <summary>
        /// 水表ID
        /// </summary>
        public string MeterId { get; set; }
        /// <summary>
        /// 水表编号
        /// </summary>
        public string Number { get; set; }
        */
        /// <summary>
        ///     电信设备标识码
        /// </summary>
        /// </summary>
        public string CTDeviceId { get; set; }

        /// <summary>
        ///     上报时间
        /// </summary>
        public DateTime ReadTime { get; set; }

        /// <summary>
        ///     上报读数
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        ///     阀门状态
        /// </summary>
        public int ValveState { get; set; }

        /// <summary>
        ///     电池电压
        /// </summary>
        public double Voltage { get; set; }

        /// <summary>
        ///     报警
        /// </summary>
        public int? Warning { get; set; }

        /// <summary>
        ///     消息类型
        /// </summary>
        public int? MsgType { get; set; }
        /// <summary>
        /// 24小时历史数据
        /// </summary>
        public string history { get; set; }

        /*
        /// <summary>
        /// 冻结数据类型
        /// </summary>
        public int? Value24Type { get; set; }

        /// <summary>
        /// 最后读数（暂时没用）
        /// </summary>
        public double? LastValue { get; set; }
        */
    }
}