﻿using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.CTCloud
{
    public partial class MeterCodeIdMap : BaseRecord
    {
        /// <summary>
        /// 水表编号
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 电信设备标识码
        /// </summary>
        public string CtNbId { get; set; }
        /// <summary>
        /// 水表IMEI编号
        /// </summary>
        public string Imei { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// 片区ID
        /// </summary>
        public string DistrictId { get; set; }
        /// <summary>
        /// 业主ID
        /// </summary>
        public string OwerId { get; set; }
        /// <summary>
        /// 水表ID
        /// </summary>
        public string MeterId { get; set; }
    }
}
