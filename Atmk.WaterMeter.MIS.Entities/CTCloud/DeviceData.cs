﻿namespace Atmk.WaterMeter.MIS.Entities.CTCloud
{
    /// <summary>
    /// 水表上送数据
    /// </summary>
    public class DeviceData
    {
        public DeviceData()
        {
        }

        /// <summary>
        /// 获取实际电压值
        /// </summary>
        public double voltageTrue
        {
            get
            {
                var value = (voltage + 150)/100d;
                return value > 0 ? value : 0;
            }
        }

        /// <summary>
        /// 电压的转换值。例如：当实际电压值为3.25v时，协议中数据为:(3.25x100)-150=175
        /// </summary>
        public int voltage { get; set; }

        /// <summary>
        /// 警报 
        /// 0  表示设备正常运行
        /// 1  表示水表数据读取异常
        /// 2  表示水表数据读取校验失败
        /// 4  电压偏低,默认小于3.2v时
        /// 8  电压过低,默认小于3.0v时
        /// 16 电池掉电,默认小于2.8v时
        /// </summary>
        public int warning { get; set; }

        /// <summary>
        /// 阀门状态 
        /// 0 阀门关闭 
        /// 1 阀门打开 
        /// 2 阀门运行 ：表示设备阀门当前处于运行状态
        /// 3 阀门开关测试超时：表示在进行开关阀测试时，阀门在一定的时间内未完成一次开关，这里超时时间可通过AT指令进行配置
        /// </summary>
        public int valve { get; set; }

        /// <summary>
        /// 电压状态 0 电压正常 1 电压偏低 2 电压过低 3 电压掉电
        /// </summary>
        public int voltageState { get; set; }

        /// <summary>
        /// 水表当前读数,固定6字节,(base64编码)。
        /// </summary>
        public string current { get; set; }

        /// <summary>
        /// 水表24小时读数,固定每小时读数6字节,(base64编码)
        /// </summary>
        public string history { get; set; }

        /// <summary>
        /// 磁攻击报警
        /// </summary>
        public int msgtype { get; set; }
    }
}