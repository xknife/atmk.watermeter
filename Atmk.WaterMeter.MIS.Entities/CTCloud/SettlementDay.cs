﻿using System;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.CTCloud
{
    /// <summary>
    /// 结算表
    /// </summary>
    public partial class SettlementDay : BaseRecord
    {
        /// <summary>
        /// 水表编号
        /// </summary>
        public string MeterNumber { get; set; }
        /// <summary>
        /// 当前读数
        /// </summary>
        public double Value { get; set; }
        /// <summary>
        /// 上数时间
        /// </summary>
        public DateTime ReadTime { get; set; }
        /// <summary>
        /// 用量
        /// </summary>
        public double Dosage { get; set; }
        /// <summary>
        /// 结算状态
        /// </summary>
        public int SettlementState { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime? OperatorTime { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorUserId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// 片区ID
        /// </summary>
        public string DistrictId { get; set; }
        /// <summary>
        /// 业主ID
        /// </summary>
        public string OwerId { get; set; }
        /// <summary>
        /// 水表ID
        /// </summary>
        public string MeterId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
    }
}
