﻿namespace Atmk.WaterMeter.MIS.Entities.CTCloud
{
    /// <summary>
    /// 下发命令 阀门控制
    /// </summary>
    public class ValveControlData
    {   
        /// <summary>
        /// 0关 1开 
        /// </summary>
        public int operation { get; set; }
        /// <summary>
        /// 消息长度
        /// </summary>
        public int msglen { get; set; } = 1;
        /// <summary>
        /// 起始位置
        /// </summary>
        public int start { get; set; } = 2;
        /// <summary>
        /// 结束位置
        /// </summary>
        public int stop { get; set; } = 1;
        /// <summary>
        /// 数据包除帧头帧尾部分的总长度
        /// </summary>
        public int crc { get; set; } = 1;

        public string endpointid { get; set; }
        /// <summary>
        /// 时间字符串:格式转为base64
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// 功能类型 C0 192
        /// </summary>
        public int msgtype { get; set; } = 192;
    }
}
