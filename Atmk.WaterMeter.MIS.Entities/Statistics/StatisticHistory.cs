﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Statistics
{
    /// <summary>
    /// 统计信息：预先存储需要的统计信息
    /// </summary>
    public class StatisticHistory:BaseRecord
    {
        /// <summary>
        ///项目Id
        /// </summary>
        [Display(Name = "项目Id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 统计类型
        /// </summary>
        [Display(Name = "统计类型")]
        public StatisticType StatisticType { get; set; }
    }
}
