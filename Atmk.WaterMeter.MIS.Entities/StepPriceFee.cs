﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     价格费用分项信息
    /// </summary>
    public class PrcieStepFee : BaseEntity
    {
        public PrcieStepFee(string name) : base(name)
        {
        }

        [Display(Name = "价格1")]
        public double Price1 { get; set; }

        [Display(Name = "价格2")]
        public double Price2 { get; set; }

        [Display(Name = "价格3")]
        public double Price3 { get; set; }

        [Display(Name = "价格Id")]
        [StringLength(60, ErrorMessage = "{0}的长度不可超过{1}")]
        public string PiceStepId { get; set; }
    }
}