﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     角色实体
    /// </summary>
    public class Role : BaseEntity
    {
        public Role(string name) : base(name)
        {
        }

        //TODO:设置数据库定制特性
        public Guid ProjectId { get; set; }

        /// <summary>
        ///     权限信息
        /// </summary>
        [MaxLength]
        public string Parmissions { get; set; }
    }
}