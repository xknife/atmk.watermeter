﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     抄表记录对象
    /// </summary>
    public class MeterReadingRecord : BaseRecord
    {
        public MeterReadingRecord()
        {
            CloseState = CloseState.UnCLose;
        }

        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }

        /// <summary>
        ///     水表数据读取时间
        /// </summary>
        [Display(Name = "水表读取时间")]
        [Required(ErrorMessage = "{0}不能为空")]
        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
        public DateTime ReadTime { get; set; }

        /// <summary>
        ///     本次水表值
        /// </summary>
        [Display(Name = "水表值")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(float.MinValue, float.MaxValue, ErrorMessage = "水表读数错误")]
        public double Value { get; set; }

        ///// <summary>
        /////     上次水表值
        ///// </summary>
        //[Display(Name = "上次水表值")]
        //[Required(ErrorMessage = "{0}不能为空")]
        //[Range(float.MinValue, float.MaxValue, ErrorMessage = "水表读数错误")]
        //public double LastValue { get; set; }

        ///// <summary>
        /////     本次水表实际读数
        ///// </summary>
        //[Display(Name = "水表读数")]
        //[Required(ErrorMessage = "{0}不能为空")]
        //[Range(float.MinValue, float.MaxValue, ErrorMessage = "水表读数错误")]
        //public double ReadValue { get; set; }

        ///// <summary>
        /////     上次水表实际读数
        ///// </summary>
        //[Display(Name = "上次水表读数")]
        //[Required(ErrorMessage = "{0}不能为空")]
        //[Range(float.MinValue, float.MaxValue, ErrorMessage = "水表读数错误")]
        //public double LastReadValue { get; set; }

        /// <summary>
        ///     阀门状态
        /// </summary>
        [Display(Name = "阀门状态")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public ValveState ValveState { get; set; }

        /// <summary>
        ///     当前电压
        /// </summary>
        [Display(Name = "当前电压")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Voltage { get; set; }

        /// <summary>
        ///     报警状态
        /// </summary>
        [Display(Name = "报警状态")]
        public WarningState? Warning { get; set; }

        /// <summary>
        ///     记录的使用状态
        /// </summary>
        [Required]
        public CloseState CloseState { get; set; }
        /// <summary>
        ///   功能类型  04 磁攻击
        /// </summary>
        public int? MsgType { get; set; }
        /// <summary>
        ///   数据类型 
        ///   1当前数据  2历史数据   
        /// </summary>
        public Value24Type Value24Type { get; set; }

        //public string MeterID { get; set; }


        //public Meter Meter { get; set; }

    }
}