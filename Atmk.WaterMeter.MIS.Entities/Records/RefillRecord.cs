﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     缴费记录
    /// </summary>
    public class RefillRecord : BaseRecord
    {
        /// <summary>
        ///     业主Id
        /// </summary>
        [Display(Name = "业主编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }

        /// <summary>
        ///     账户Id
        /// </summary>
        [Display(Name = "账户编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string AccountId { get; set; }

        /// <summary>
        ///     用户Id
        /// </summary>
        [Display(Name = "操作员编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string StaffId { get; set; }

        /// <summary>
        ///     缴费类型
        /// </summary>
        [Display(Name = "缴费类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        public AmountChangedUpMode RefillType { get; set; }

        /// <summary>
        ///     缴费金额
        /// </summary>
        [Display(Name = "缴费金额")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "缴费金额值错误")]
        public decimal RefillSum { get; set; }

        /// <summary>
        ///     应缴金额
        /// </summary>
        [Display(Name = "应缴金额")]
        public decimal ShouldPayment { get; set; }

        /// <summary>
        ///     上次余额
        /// </summary>
        [Display(Name = "上次余额")]
        public decimal LastBalance { get; set; }

        /// <summary>
        ///     撤销充值标识 0初始，1 撤销
        /// </summary>
        [Display(Name = "撤销充值")]
        public int Rescind { get; set; }

        /// <summary>
        ///     缴费信息
        /// </summary>
        [MaxLength]
        public string RefillMessage { get; set; }
    }
}