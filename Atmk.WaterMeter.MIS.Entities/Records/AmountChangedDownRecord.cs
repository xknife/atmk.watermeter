﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     扣费记录
    /// </summary>
    public class AmountChangedDownRecord : BaseRecord
    {
        /// <summary>
        ///     业主Id
        /// </summary>
        [Display(Name = "业主Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }

        /// <summary>
        ///     水表Id
        /// </summary>
        [Display(Name = "水表Id")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MeterNumber { get; set; }

        /// <summary>
        ///     账户Id
        /// </summary>
        [Display(Name = "账户Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string AccountId { get; set; }

        /// <summary>
        ///     用户编号
        /// </summary>
        [Display(Name = "操作员Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string StaffId { get; set; }

        /// <summary>
        ///     结算Id
        /// </summary>
        [Display(Name = "结算ID")]
        public string SettlementId { get; set; }

        /// <summary>
        ///     扣费类型
        /// </summary>
        [Display(Name = "扣费类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        public AmountChangedDownMode AmountChangedDownMode { get; set; }

        /// <summary>
        ///     扣费金额
        /// </summary>
        [Display(Name = "扣费金额")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "缴费金额值错误")]
        public decimal PaymentSum { get; set; }

        /// <summary>
        ///     扣费信息
        /// </summary>
        [MaxLength]
        public string PaymentMessage { get; set; }
    }
}