﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    /// 微信小程序订单记录-仅存储用
    /// </summary>
    public class WxSrOrderRecord:BaseRecord
    {
        public WxSrOrderRecord()
        {
            OrderStatus = OrderStatus.Create;
        }

        /// <summary>
        /// 小程序用户Id
        /// </summary>
        [Display(Name = "小程序用户Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OpenId { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [Display(Name = "订单编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(32, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OutTradeNo { get; set; }

        /// <summary>
        ///     业主Id
        /// </summary>
        [Display(Name = "业主Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }

        /// <summary>
        /// 抄表系统账户Id
        /// </summary>
        [Display(Name = "账户编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string AccountId { get; set; }

        /// <summary>
        /// 门牌号，应做本级片区内的去重逻辑
        /// </summary>
        [Display(Name = "门牌号")]
        [StringLength(40, ErrorMessage = "{0}的长度不可超过{1}")]
        public string HouseNumber { get; set; }

        /// <summary>
        /// 水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }


        /// <summary>
        /// 缴费金额
        /// </summary>
        [Display(Name = "缴费金额")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "缴费金额值错误")]
        public decimal RefillSum { get; set; }

        /// <summary>
        /// 应缴金额
        /// </summary>
        [Display(Name = "应缴金额")]
        public decimal ShouldPayment { get; set; }

        /// <summary>
        /// 上次余额
        /// </summary>
        [Display(Name = "上次余额")]
        public decimal LastBalance { get; set; }


        /// <summary>
        /// 订单状态
        /// </summary>
        [Display(Name = "订单状态")]
        [Required(ErrorMessage = "{0}不能为空")]
        public OrderStatus OrderStatus { get; set; }
    }
}
