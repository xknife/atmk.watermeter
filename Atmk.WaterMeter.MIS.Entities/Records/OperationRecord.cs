﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     水表操作记录
    /// </summary>
    public class OperationRecord : BaseRecord
    {
        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }

        /// <summary>
        ///     水表操作类型
        /// </summary>
        [Display(Name = "水表操作类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public OperationType OperationType { get; set; }

        /// <summary>
        ///     水表操作记录时间
        /// </summary>
        [Display(Name = "操作时间")]
        [Required(ErrorMessage = "{0}不能为空")]
        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
        public DateTime OperationTime { get; set; }
    }
}