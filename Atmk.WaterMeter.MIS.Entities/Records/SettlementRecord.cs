﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     结算记录实体类
    /// </summary>
    public class SettlementRecord : BaseRecord
    {
        /// <summary>
        ///     结算时间
        /// </summary>
        [Display(Name = "结算时间")]
        [Required(ErrorMessage = "{0}不能为空")]
        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
        public DateTime SettlementTime { get; set; }

        /// <summary>
        ///     表编号
        /// </summary>
        public string MeterNumber { get; set; }

        /// <summary>
        ///     开始读数时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        ///     结束读数时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        ///     开始读数
        /// </summary>
        public int StartRead { get; set; }

        /// <summary>
        ///     最终读数
        /// </summary>
        public int EndRead { get; set; }

        /// <summary>
        ///     结算水表用水总量
        /// </summary>
        [Required]
        public int MeterValue { get; set; }

        /// <summary>
        ///     结算价格
        /// </summary>
        public decimal Price1 { get; set; }

        /// <summary>
        ///     结算金额
        /// </summary>
        [Required]
        public decimal SettlementMoney { get; set; }

        /// <summary>
        ///     抄表记录ID集合
        /// </summary>
        [MaxLength]
        public string MeterReadingIds { get; set; }


        /// <summary>
        ///     结算数据信息
        /// </summary>
        [MaxLength]
        public string SettlementMessage { get; set; }

        /// <summary>
        ///     付费状态 0 未付费 1 已付费
        /// </summary>
        [Required]
        public int PaymentStatus { get; set; }
    }
}