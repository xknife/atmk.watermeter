﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Records
{
    /// <summary>
    ///     水表冻结数据
    /// </summary>
    public class FrozenDataRecord : BaseRecord
    {
        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }

        /// <summary>
        ///     冻结数据日期
        /// </summary>
        [Display(Name = "冻结数据日期")]
        [Required(ErrorMessage = "{0}不能为空")]
        [DataType(DataType.DateTime, ErrorMessage = "时间格式错误")]
        public DateTime FrozenDate { get; set; }

        /// <summary>
        ///     冻结类型
        /// </summary>
        [Display(Name = "冻结类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        public FrozenType FrozenType { get; set; }

        /// <summary>
        ///     冻结单位值，具体是月、日、时根据冻结类型FrozenType判断
        /// </summary>
        [Display(Name = "冻结单位")]
        [Required(ErrorMessage = "{0}不能为空")]
        public int Unit { get; set; }

        /// <summary>
        ///     冻结读数
        /// </summary>
        [Display(Name = "冻结读数")]
        [Required(ErrorMessage = "{0}不能为空")]
        public int FrozenRead { get; set; }

        /// <summary>
        ///     水表冻结实际数据
        /// </summary>
        [Display(Name = "水表冻结实际数据")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string FrozenRecord { get; set; }

        /// <summary>
        ///     记录的使用状态
        /// </summary>
        [Required]
        public CloseState CloseState { get; set; }
    }
}