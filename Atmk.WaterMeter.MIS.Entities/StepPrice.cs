﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     阶梯水价
    /// </summary>
    public class StepPrice : BaseEntity
    {
        public StepPrice(string name) : base(name)
        {
        }

        //TODO:设置数据库定制特性
        public Guid ProjectId { get; set; }

        /// <summary>
        ///     水表类型
        /// </summary>
        [Display(Name = "水表类型")]
        public MeterType MeterType { get; set; }

        /// <summary>
        ///     价格单位
        /// </summary>
        [Display(Name = "价格单位")]
        public PriceUnit PriceUnit { get; set; }

        /// <summary>
        ///     分界点1
        /// </summary>
        [Display(Name = "分界点1")]
        public int CutPoint1 { get; set; }


        /// <summary>
        ///     分界点2
        /// </summary>
        [Display(Name = "分界点2")]
        public int CutPoint2 { get; set; }
    }
}