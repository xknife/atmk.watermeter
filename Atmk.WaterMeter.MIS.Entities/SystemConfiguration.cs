﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     系统（平台）参数
    /// </summary>
    public class SystemConfiguration : BaseConfigurationEntity
    {
        public SystemConfiguration(string name) : base(name)
        {
        }
        //
        //        /// <summary>
        //        ///     分类名称
        //        /// </summary>
        //        [Display(Name = "分类名称")]
        //        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        //        public string Category { get; set; }
    }
}