﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    ///     水表读数记录处理状态
    /// </summary>
    public enum CloseState
    {
        /// <summary>
        ///     未结算
        /// </summary>
        UnCLose = 0,

        /// <summary>
        ///     已结算
        /// </summary>
        Close = 1
    }
}