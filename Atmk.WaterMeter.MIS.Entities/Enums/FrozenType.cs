﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 冻结类型
    /// </summary>
    public enum FrozenType
    {
        /// <summary>
        /// 小时冻结
        /// </summary>
        Hour = 0,
        /// <summary>
        /// 日冻结
        /// </summary>
        Day  = 1,
        /// <summary>
        /// 月冻结
        /// </summary>
        Month = 2
    }
}