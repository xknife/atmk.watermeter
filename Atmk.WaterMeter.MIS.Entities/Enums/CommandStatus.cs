﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    ///     指令发送状态枚举
    /// </summary>
    public enum CommandStatus
    {
        未发送 = 1,
        已发送 = 2,
        已送达 = 3,
        已执行 = 4,
        等待 = 5,
        未下发 = 6,
        命令过期 = 7,
        执行成功 = 8,
        执行失败 = 9,
        执行超时 = 10,
        撤销 = 11,
        其他 = 12
    }
}