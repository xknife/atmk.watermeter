﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 账户状态
    /// </summary>
    public enum AccountStatus
    {
        开户 = 1,
        销户 = 0
    }
}
