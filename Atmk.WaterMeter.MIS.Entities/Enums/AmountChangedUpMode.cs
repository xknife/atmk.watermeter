﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    ///     账户发生金额增加的几种方式
    /// </summary>
    public enum AmountChangedUpMode
    {
        充值 = 0,
        减免 = 1,
        误收费 = 2
    }
}