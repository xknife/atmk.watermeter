﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    ///     账户发生金额减少的几种方式
    /// </summary>
    public enum AmountChangedDownMode
    {
        水费扣费 = 0,
        撤销充值 = 1,
        账户退费 = 2
    }
}