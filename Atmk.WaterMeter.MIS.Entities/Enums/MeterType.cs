﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    public enum MeterType
    {
        /// <summary>
        /// 冷水水表
        /// </summary>
        [Description("冷水水表")]
        ColdWaterMeter,
        /// <summary>
        /// 生活热水水表
        /// </summary>
        [Description("生活热水水表")]
        HotWaterMeter,
        /// <summary>
        /// 直饮水水表
        /// </summary>
        [Description("直饮水水表")]
        DrinkingWaterMeter,
        /// <summary>
        /// 中水水表
        /// </summary>
        [Description("中水水表")]
        ReclaimedWaterMeter,
        /// <summary>
        /// FS型大水表
        /// </summary>
        [Description("FS型大水表")]
        FSModelWaterMeter
    }
}
