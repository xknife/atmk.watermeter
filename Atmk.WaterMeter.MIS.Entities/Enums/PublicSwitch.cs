﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    public static class PublicSwitch
    {
        public static string ValveType(int? value)
        {
            switch (value)
            {
                case 0:
                    return "关";
                case 1:
                    return "开";
                case 2:
                    return "异常";
                case 3:
                    return "未检测";
                default:
                    return "";
            }
        }
    }
}
