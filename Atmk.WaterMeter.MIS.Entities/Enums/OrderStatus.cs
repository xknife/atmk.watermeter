﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 订单的基本状态
    /// </summary>
    public enum OrderStatus
    {
        [Description("创建")]
        Create = 0,
        [Description("执行")]
        Execute = 1,
        [Description("完成")]
        Finish = 2,
        [Description("撤销")]
        Cancel = 3,
        [Description("异常")]
        Error = 4
    }
}
