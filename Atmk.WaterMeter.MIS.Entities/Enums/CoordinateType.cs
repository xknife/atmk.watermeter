﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    ///     坐标点类型枚举
    /// </summary>
    public enum CoordinateType
    {
        /// <summary>
        /// 项目坐标
        /// </summary>
        Project = 0,
        /// <summary>
        /// 片区坐标
        /// </summary>
        District = 1,
        /// <summary>
        /// 房子坐标
        /// </summary>
        Owner = 2
    }
}