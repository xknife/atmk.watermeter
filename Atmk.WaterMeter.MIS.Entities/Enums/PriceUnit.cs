﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 阶梯价格单位枚举，用于表示每单位多少元，现有单位包括立方米和升
    /// </summary>
    public enum PriceUnit
    {
        /// <summary>
        /// 单位:元/立方米
        /// </summary>
        [Description("元/方")]
        stere,
        /// <summary>
        /// 单位:元/升
        /// </summary>
        [Description("元/升")]
        litre
    }
}
