﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 参数类型（一般在查询时传递使用）
    /// </summary>
    public enum ProjectParameterType
    {
        /// <summary>
        ///     缴费类型
        /// </summary>
        Refill = 1,

        /// <summary>
        ///     扣费类型
        /// </summary>
        Payment = 2,

        /// <summary>
        ///     水表类型
        /// </summary>
        Meter = 3,

        /// <summary>
        ///     其他
        /// </summary>
        Other = 0
    }
}