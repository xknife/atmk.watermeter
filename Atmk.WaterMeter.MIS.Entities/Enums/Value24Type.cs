﻿using System.ComponentModel;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 冻结数据类型
    /// </summary>
    public enum Value24Type
    {
        [Description("未知")]
        zero,
        [Description("当前数据")]
        one,
        [Description("冻结数据")]
        two,
        [Description("换表")]
        three,
        [Description("销表")]
        four
    }
}