﻿using System.ComponentModel;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 报警的使用状态
    /// </summary>
    public enum WarningState
    {
        [Description("正常")]
        Ok=0,
        [Description("异常")]
        Error1=1,
        [Description("异常")]
        Error2=2
    }
}