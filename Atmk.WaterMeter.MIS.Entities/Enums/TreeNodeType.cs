﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 片区类型
    /// </summary>
    public enum TreeNodeType
    {
        /// <summary>
        /// 根节点，代表项目的节点
        /// </summary>
        root,
        /// <summary>
        /// 片区中间节点
        /// </summary>
        branch,
        /// <summary>
        /// 片区最后的节点，该节点后不能添加片区，可以添加叶子节点
        /// </summary>
        last,
        /// <summary>
        /// 叶子节点，代表业主信息端的节点
        /// </summary>
        leaf
    }
}
