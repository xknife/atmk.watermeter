﻿using System.ComponentModel;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 阀门的使用状态
    /// </summary>
    public enum ValveState
    {
        [Description("关阀")]
        Closed = 0,
        [Description("开阀")]
        Opening = 1,
        [Description("异常")]
        Running=2,
        [Description("异常")]
        Error = 3
    }
}