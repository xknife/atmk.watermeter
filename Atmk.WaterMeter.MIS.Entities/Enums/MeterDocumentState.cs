﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 水表档案状态
    /// </summary>
    public enum MeterDocumentState
    {
        建档 = 0,
        使用 = 1,
        销户 = 2
    }
}