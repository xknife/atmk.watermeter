﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    public enum CommandType
    {
        其他指令 = 0,

        抄表指令 = 1,
        开阀指令 = 2,
        关阀指令 = 3,
    }
}