﻿namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 记录状态枚举
    /// </summary>
    public enum RecordStateEnum
    {
        /// <summary>
        /// 普通状态
        /// </summary>
        Normal = 0,

        /// <summary>
        /// 删除状态
        /// </summary>
        Deleted = 1,

        /// <summary>
        /// 撤销状态
        /// </summary>
        Rescind = 1
    }
}
