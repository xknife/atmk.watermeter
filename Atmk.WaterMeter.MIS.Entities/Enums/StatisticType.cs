﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Enums
{
    /// <summary>
    /// 统计类型，里面的类型枚举根据统计需要的ViewModel来分类。
    /// </summary>
    public enum StatisticType
    {
        //TODO:统计类型及定时任务需要优化
        //先按照图表显示需要的ViewModel来分统计类型。
        //二期需要细致划分统计类型，实现功能：统计接口的ViewModel可通过拼接统计信息里的数据，组合成所需的各种统计数据。

        /// <summary>
        /// 费用报警
        /// </summary>
        CostReminderData,

        /// <summary>
        /// 关阀列表
        /// </summary>
        ClosingvalveData,

        /// <summary>
        /// 昨日用水量
        /// </summary>
        YesterdayConsumptionData,

        /// <summary>
        /// 上季度片区用水量
        /// </summary>
        LastSeasonWaterConsumptionsCountData,

        /// <summary>
        /// 上月片区阶梯用水量统计
        /// </summary>
        LastMonthDistrictConsumptionsCountData,

        /// <summary>
        /// 去年片区按月用水总量统计
        /// </summary>
        LastYearWaterConsumptionsCountData,

        /// <summary>
        /// 昨日各片区抄表数据统计
        /// </summary>
        DistrictReadRatioData,

        /// <summary>
        /// 统计信息
        /// </summary>
        StatisticsInfoData,

        /// <summary>
        /// 用水情况
        /// </summary>
        WaterConsumptionInfoData
    }
}