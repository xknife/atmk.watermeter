﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities
{
    /// <summary>
    ///     水表类型类
    /// </summary>
    public class MeterModelInformation : BaseEntity
    {
        public MeterModelInformation(string name) : base(name)
        {
        }

        /// <summary>
        ///     最大量程
        /// </summary>
        [Display(Name = "最大量程")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MaxRange { get; set; }

        /// <summary>
        ///     流量上限
        /// </summary>
        [Display(Name = "流量上限")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MaxFlow { get; set; }

        /// <summary>
        ///     使用年限
        /// </summary>
        [Display(Name = "使用年限")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string LifeTime { get; set; }

        /// <summary>
        ///     口径
        /// </summary>
        [Display(Name = "口径")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Caliber { get; set; }

        /// <summary>
        ///     协议
        /// </summary>
        [Display(Name = "协议")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Protocol { get; set; }

        /// <summary>
        ///     厂商代码
        /// </summary>
        [Display(Name = "厂商代码")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string FactoryCode { get; set; }

        /// <summary>
        ///     厂商名称
        /// </summary>
        [Display(Name = "厂商名称")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string FactoryName { get; set; }
    }
}