﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    ///     分区信息实体
    /// </summary>
    public class District : BaseEntity
    {
        public District(string name)
            : base(name)
        {
        }

        /// <summary>
        ///     片区父级Id
        /// </summary>
        [Display(Name = "编码")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Parent { get; set; }

        /// <summary>
        /// 片区节点类型
        /// </summary>
        [Display(Name = "节点类型")]
        public TreeNodeType NodeType { get; set; }

        /// <summary>
        ///  项目Id
        /// </summary>
        [Display(Name = "项目Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string ProjectId { get; set; }


    }
}