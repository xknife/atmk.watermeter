﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    ///     水表的拥有者，水表计量的对象，即房子实体。房子实体有可能在未销售时就已安装水表，也可能在二手销售给其他业主。
    /// </summary>
    public class Owner : BaseEntity
    {
        private string _houseNumber;

        public Owner(string name) 
            : base(name)
        {
        }

        /// <summary>
        ///     片区Id
        /// </summary>
        [Display(Name = "片区Id")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string DistrictId { get; set; }

        /// <summary>
        ///     门牌号，应做本级片区内的去重逻辑
        /// </summary>
        [Display(Name = "门牌号")]
        [StringLength(40, ErrorMessage = "{0}的长度不可超过{1}")]
        public string HouseNumber
        {
            get => _houseNumber;
            set
            {
                _houseNumber = value;
                //Name = value;//房子的门牌号是房子的核心索引属性，Name与门牌号相同
            }
        }

        /// <summary>
        ///     电话号
        /// </summary>
        [Display(Name = "电话号")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Mobile { get; set; }



    }
}