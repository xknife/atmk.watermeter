﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    /// 项目区域，简称项目
    /// 项目以我公司进行销售的合同为边界（他并不以地域为边界，例如成都可能有多个自来水公司或房地产单位与我公司签订水表购买合同）；
    /// </summary>
    public class Project : BaseEntity
    {
        public Project(string name) 
            : base(name)
        {
        }

        /// <summary>
        /// 项目的真实物理地址
        /// </summary>
        [MaxLength]
        public string Address { get; set; }
    }
}