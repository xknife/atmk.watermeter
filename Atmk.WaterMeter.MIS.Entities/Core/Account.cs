﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    ///     Owner对象的财务账户
    /// </summary>
    public class Account : BaseEntity
    {
        public Account(string name) 
            : base(name)
        {
        }

        /// <summary>
        ///     用户编号
        /// </summary>
        [Display(Name = "用户Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }

        /// <summary>
        ///     账户金额
        /// </summary>
        [Display(Name = "账户余额")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(0, double.MaxValue, ErrorMessage = "账户金额错误")]
        public decimal Balance { get; set; }

        /// <summary>
        ///     欠款金额
        /// </summary>
        [Display(Name = "欠款金额")]
        [Required(ErrorMessage = "{0}不能为空")]
        [Range(0, double.MaxValue, ErrorMessage = "欠款金额错误")]
        public decimal Arrears { get; set; }

        /// <summary>
        ///     账户状态
        /// </summary>
        [Display(Name = "账户状态")]
        [Required(ErrorMessage = "{0}不能为空")]
        public AccountStatus AccountStatus { get; set; } = AccountStatus.开户;
    }
}