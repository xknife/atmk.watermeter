﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Records;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    ///     水表实体类
    /// </summary>
    public class Meter : BaseEntity
    {
        public Meter(string name)
            : base(name)
        {
        }

        /// <summary>
        ///     Number
        /// </summary>
        [Display(Name = "旧水表编号")]

        public string OldMeterNumber { get; set; }

        /// <summary>
        ///     Number
        /// </summary>
        [Display(Name = "水表编号")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MeterNumber { get; set; }

        /// <summary>
        ///     水表类型
        /// </summary>
        [Display(Name = "水表类型")]
        [StringLength(30, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MeterType { get; set; }

        /// <summary>
        ///     通讯类型（LoRa, NB等）
        /// </summary>
        [Display(Name = "通讯类型")]
        [StringLength(30, ErrorMessage = "{0}的长度不可超过{1}")]
        public string CommType { get; set; }

        /// <summary>
        ///     水价Id
        /// </summary>
        [Display(Name = "水价Id")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string StepPriceId { get; set; }

        /// <summary>
        ///     业主Id
        /// </summary>
        [Display(Name = "业主Id")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }

        /// <summary>
        ///     收费类型
        /// </summary>
        [Display(Name = "收费类型")]
        public string RefillType { get; set; }

        /// <summary>
        ///     水表记录状态
        /// </summary>
        [Display(Name = "水表记录状态")]
        [StringLength(10, ErrorMessage = "{0}的长度不可超过{1}")]
        public string MeterState { get; set; }

        public ICollection<MeterReadingRecord> MeterReadingRecords { get; set; }

    }
}