﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Core
{
    /// <summary>
    ///     本系统的使用者（操作者），如自来水公司的员工，或物业收费员等等
    /// </summary>
    public class Staff : BaseEntity
    {
        public Staff(string name) : base(name)
        {
        }

        /// <summary>
        ///     用户密码
        /// </summary>
        [Display(Name = "密码")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Password { get; set; }

        /// <summary>
        ///     角色id
        /// </summary>
        [Display(Name = "角色")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string RoleId { get; set; }

        /// <summary>
        ///     每页显示行数
        /// </summary>
        [Display(Name = "显示行数")]
        public int PageLines { get; set; }

        /// <summary>
        ///     区域Id
        /// </summary>
        [Display(Name = "区域Id")]
        [MaxLength]
        public string DistrictId { get; set; }

        /// <summary>
        ///     站点ID
        /// </summary>
        [Display(Name = "站点Id")]
        [StringLength(200, ErrorMessage = "{0}的长度不可超过{1}")]
        public string SiteId { get; set; }

        /// <summary>
        ///     操作员姓名
        /// </summary>
        [Display(Name = "姓名")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Surname { get; set; }

        /// <summary>
        ///     操作员电话号
        /// </summary>
        [Display(Name = "电话")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Mobile { get; set; }
    }
}