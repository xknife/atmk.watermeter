﻿using System;
using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities
{
    
    /// <summary>
    ///     面向项目的配置信息集合
    /// </summary>
    public class ProjectConfiguration : BaseConfigurationEntity
    {
        public ProjectConfiguration(string name) 
            : base(name)
        {
        }

        //TODO:设置数据库定制特性
        public Guid ProjectId { get; set; }  
//
//
//        /// <summary>
//        ///     参数类型
//        /// </summary>
//        [Required]
//        public ProjectParameterType ProjectParameterType { get; set; }
//
//        /// <summary>
//        ///     参数数据
//        /// </summary>
//        [Display(Name = "参数数据")]
//        [Required(ErrorMessage = "{0}不能为空")]
//        [StringLength(300, ErrorMessage = "{0}的长度不可超过{1}")]
//        public string ParameterValue { get; set; }

    }
}