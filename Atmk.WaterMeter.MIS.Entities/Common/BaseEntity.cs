﻿namespace Atmk.WaterMeter.MIS.Entities.Common
{
    /// <summary>
    /// DB表基础属性
    /// </summary>
    public abstract class BaseEntity : BaseRecord, IEntity
    {
        protected BaseEntity(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}