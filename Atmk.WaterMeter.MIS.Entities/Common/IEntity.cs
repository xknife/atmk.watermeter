﻿using System.ComponentModel.DataAnnotations;

namespace Atmk.WaterMeter.MIS.Entities.Common
{
    public interface IEntity : IRecord
    {
        /// <summary>
        /// 对象名称
        /// </summary>
        [Display(Name = "名称"), Required(ErrorMessage = "{0}不能为空"), StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        string Name { get; set; }
    }
}
