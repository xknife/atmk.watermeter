﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Common
{
    public abstract class BaseConfigurationEntity : BaseEntity
    {
        /// <inheritdoc />
        protected BaseConfigurationEntity(string name)
            : base(name)
        {
        }

        /// <summary>
        ///     以Json字符串进行保存的配置信息
        /// </summary>
        [Display(Name = "Json配置信息")]
        [MaxLength()]
        public string Config { get; set; }
    }
}
