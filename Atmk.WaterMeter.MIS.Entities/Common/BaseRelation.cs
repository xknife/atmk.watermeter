﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Atmk.WaterMeter.MIS.Entities.Common
{
    /// <summary>
    /// 一些关系表（一对多，多对多等）的基础类，一般保存ID与ID之间的关系
    /// </summary>
    public abstract class BaseRelation
    {
        [Key] //主键 
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        //设置自增
        public int Id { get; set; }
    }
}
