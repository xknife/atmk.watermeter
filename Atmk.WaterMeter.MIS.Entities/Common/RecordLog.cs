﻿using System;

namespace Atmk.WaterMeter.MIS.Entities.Common
{
    /// <summary>
    /// IRecord类中Log属性中保存时的Json临时对象
    /// </summary>
    public class RecordLog
    {
        /// <summary>
        /// 记录类
        /// </summary>
        /// <param name="system">系统操作员</param>
        /// <param name="datetime">记录时间</param>
        /// <param name="note">便签,注释</param>
        public RecordLog(string system, DateTime datetime, string note = "")
        {
            user = system;
            SetTimeString(datetime);
            this.note = note;
        }

        public void SetTimeString(DateTime datetime)
        {
            time = datetime.ToString("yyMMdd.HHmmss.fff");
        }

        public string user { get; set; }
        public string time { get; set; }
        public string note { get; set; }
    }
}