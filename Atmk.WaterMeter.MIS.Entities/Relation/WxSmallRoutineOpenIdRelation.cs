﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Common;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Entities.Relation
{
    /// <summary>
    /// 微信小程序用户OpenId与抄表系统业主和账户的关系
    /// </summary>
    public class WxSmallRoutineOpenIdRelation:BaseRelation
    {
        public WxSmallRoutineOpenIdRelation()
        {
            RecordState = RecordStateEnum.Normal;
           
        }
        /// <summary>
        /// 小程序用户Id
        /// </summary>
        [Display(Name = "小程序用户Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OpenId { get; set; }
        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(20, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }
        /// <summary>
        ///     业主Id
        /// </summary>
        [Display(Name = "业主Id")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string OwnerId { get; set; }
        /// <summary>
        ///     账户Id
        /// </summary>
        [Display(Name = "账户编号")]
        [Required(ErrorMessage = "{0}不能为空")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string AccountId { get; set; }

        /// <summary>
        /// 记录状态
        /// </summary>
        [Required]
        public  RecordStateEnum RecordState { get; set; }
    }
}
