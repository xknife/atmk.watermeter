﻿using System.ComponentModel.DataAnnotations;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Relation
{
    /// <summary>
    ///     水表编号与Nb
    /// </summary>
    public class IdAndNbIdRelation : BaseRelation
    {
        /// <summary>
        ///     水表编号
        /// </summary>
        [Display(Name = "水表编号")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string Number { get; set; }

        /// <summary>
        /// IMEI号
        /// </summary>
        [Display(Name = "IMEI")]
        [StringLength(50, ErrorMessage = "{0}的长度不可超过{1}")]
        public string IMEI { get; set; }

        /// <summary>
        ///     水表Nb平台Id
        /// </summary>
        [Display(Name = "水表电信NB-IoT平台Id")]
        [StringLength(200, ErrorMessage = "{0}的长度不可超过{1}")]
        public string CT_NbId { get; set; }

        // ReSharper disable once InconsistentNaming
        public string AreaId { get; set; }
        public string OwerId { get; set; }
        public string DistrictId { get; set; }
        public string MeterId { get; set; }

    }
}