﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Atmk.WaterMeter.MIS.Entities.Common;

namespace Atmk.WaterMeter.MIS.Entities.Relation
{
    /// <summary>
    /// 存储操作员各自的片区显示顺序
    /// </summary>
    public class StaffDistrictSequence : BaseRelation
    {
        /// <summary>
        /// 操作员Id
        /// </summary>
        [Display(Name = "操作员Id")]
        public string StaffId { get; set; }
        /// <summary>
        /// 片区Id
        /// </summary>
        [Display(Name = "片区Id")]
        public string DistrictId { get; set; }
        /// <summary>
        /// 显示顺序
        /// </summary>
        [Display(Name = "显示顺序")]
        public int DisplaySequence { get; set; }
    }
}
