﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.EF.ModelPuls
{
    public class Out_SettleDayGroup
    {
        public string CtdeviceId { get; set; }
        public string MeterNumber { get; set; }
        public decimal WaterVolume { get; set; }
        public decimal Dosage_Year { get; set; }
        public string PiceStepId { get; set; }
        public string OwnerId { get; set; }
        public string MeterId { get; set; }
        public int CutPoint1 { get; set; }
        public int CutPoint2 { get; set; }
        public decimal Price1 { get; set; }
        public decimal Price2 { get; set; }
        public decimal Price3 { get; set; }
        public Meter meter { get; set; }
    }
}
