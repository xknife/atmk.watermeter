﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class SettlementDay
    {
        public int Id { get; set; }
        public string CtdeviceId { get; set; }
        public string MeterNo { get; set; }
        public double Value { get; set; }
        public DateTime ReadTime { get; set; }
        public double Dosage { get; set; }
        public int SettlementState { get; set; }
        public DateTime? OperatorTime { get; set; }
        public string OperatorUserId { get; set; }
        public string Remarks { get; set; }
    }
}
