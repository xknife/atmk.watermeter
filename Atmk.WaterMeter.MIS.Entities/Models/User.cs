﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class User
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
        public int PageLines { get; set; }
        public string DistrictId { get; set; }
        public string SiteId { get; set; }
        public string Surname { get; set; }
        public string Mobile { get; set; }
    }
}
