﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class TopSelect
    {
        public int Id { get; set; }
        public string DistrictId { get; set; }
        public DateTime? CreateDate { get; set; }
        public decimal? Dosage { get; set; }
    }
}
