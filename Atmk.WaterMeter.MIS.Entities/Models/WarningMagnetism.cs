﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class WarningMagnetism
    {
        public string Id { get; set; }
        public string CtdeviceId { get; set; }
        public string MeterNo { get; set; }
        public double Value { get; set; }
        public string Voltage { get; set; }
        public int ValveState { get; set; }
        public int? Warning { get; set; }
        public int? MsgType { get; set; }
        public string CSQ24 { get; set; }
        public string Value24 { get; set; }
        public DateTime ReadTime { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
