﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class DistrictSequences
    {
        public int Id { get; set; }
        public string StaffId { get; set; }
        public string DistrictId { get; set; }
        public int DisplaySequence { get; set; }
    }
}
