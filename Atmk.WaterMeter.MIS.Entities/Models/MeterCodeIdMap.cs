﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class MeterCodeIdMap
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string CtNbId { get; set; }
        public string Imei { get; set; }
        public string ProjectId { get; set; }
        public string DistrictId { get; set; }
        public string OwerId { get; set; }
        public string MeterId { get; set; }
    }
}
