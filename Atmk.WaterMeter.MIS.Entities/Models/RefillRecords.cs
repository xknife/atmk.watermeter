﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class RefillRecords
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string OwnerId { get; set; }
        public string AccountId { get; set; }
        public string StaffId { get; set; }
        public int RefillType { get; set; }
        public decimal RefillSum { get; set; }
        public decimal ShouldPayment { get; set; }
        public decimal LastBalance { get; set; }
        public int Rescind { get; set; }
        public string RefillMessage { get; set; }
    }
}
