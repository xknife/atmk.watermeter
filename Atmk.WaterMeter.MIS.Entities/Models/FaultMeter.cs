﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class FaultMeter
    {
        public int Id { get; set; }
        public string MeterNo { get; set; }
        public string CtdeviceId { get; set; }
        public double Value { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
