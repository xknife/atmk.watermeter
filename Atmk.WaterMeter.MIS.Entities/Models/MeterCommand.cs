﻿using Atmk.WaterMeter.MIS.Entities.Common;
using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class MeterCommand : BaseRecord
    {
        //public string Id { get; set; }
        //public string Memo { get; set; }
        //public string Log { get; set; }
        //public DateTime CreateTime { get; set; }
        //public int RecordState { get; set; }
        public string MeterNumber { get; set; }
        public string Command { get; set; }
        public int CommandStatus { get; set; }
        public int CommandCount { get; set; }
        public DateTime ExecuteTime { get; set; }
    }
}
