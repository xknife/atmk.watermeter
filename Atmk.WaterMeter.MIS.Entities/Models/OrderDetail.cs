﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class OrderDetail
    {
        public int Id { get; set; }
        public int CostType { get; set; }
        public decimal? WaterVolume { get; set; }
        public decimal? WaterPrice { get; set; }
        public string WxMessages { get; set; }
        public string RechargeChannels { get; set; }
        public decimal Money { get; set; }
        public decimal? Balance { get; set; }
        public string OwnerId { get; set; }
        public string MeterNumber { get; set; }
        public string MeterId { get; set; }
        public int? Modification { get; set; }
        public DateTime? ModificationTime { get; set; }
        public DateTime CreateTime { get; set; }
        public string Remarks { get; set; }
    }
}
