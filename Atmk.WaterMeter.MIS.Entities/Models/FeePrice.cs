﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class FeePrice
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string Name { get; set; }
        public decimal Price1 { get; set; }
        public decimal Price2 { get; set; }
        public decimal Price3 { get; set; }
        public string PiceStepId { get; set; }
    }
}
