﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class WxSrRelations
    {
        public int Id { get; set; }
        public string OpenId { get; set; }
        public string Number { get; set; }
        public string OwnerId { get; set; }
        public string AccountId { get; set; }
        public int RecordState { get; set; }
    }
}
