﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class PaymentRecords
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string OwnerId { get; set; }
        public string MeterNumber { get; set; }
        public string AccountId { get; set; }
        public string StaffId { get; set; }
        public string SettlementId { get; set; }
        public int AmountChangedDownMode { get; set; }
        public decimal PaymentSum { get; set; }
        public string PaymentMessage { get; set; }
    }
}
