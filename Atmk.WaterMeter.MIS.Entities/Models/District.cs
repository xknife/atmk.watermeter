﻿using Atmk.WaterMeter.MIS.Entities.Common;
using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class District : BaseRecord
    {
        //public string Id { get; set; }
        //public string Memo { get; set; }
        //public string Log { get; set; }
        //public DateTime CreateTime { get; set; }
        //public int RecordState { get; set; }
        public string Name { get; set; }
        public string Parent { get; set; }
        public int NodeType { get; set; }
        public string ProjectId { get; set; }
    }
}
