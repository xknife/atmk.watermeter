﻿using Atmk.WaterMeter.MIS.Entities.Common;
using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class WxSrOrderRecords: BaseRecord
    {
        //public string Id { get; set; }
        //public string Memo { get; set; }
        //public string Log { get; set; }
        //public DateTime CreateTime { get; set; }
        //public int RecordState { get; set; }
        public string OpenId { get; set; }
        public string OutTradeNo { get; set; }
        public int OrderStatus { get; set; }
        public string AccountId { get; set; }
        public string HouseNumber { get; set; }
        public decimal LastBalance { get; set; }
        public string Number { get; set; }
        public string OwnerId { get; set; }
        public decimal RefillSum { get; set; }
        public decimal ShouldPayment { get; set; }
    }
}
