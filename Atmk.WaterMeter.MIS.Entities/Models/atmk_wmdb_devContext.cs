﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class atmk_wmdb_devContext : DbContext
    {
        public atmk_wmdb_devContext()
        {
        }

        public atmk_wmdb_devContext(DbContextOptions<atmk_wmdb_devContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DataBase> DataBase { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<DistrictSequences> DistrictSequences { get; set; }
        public virtual DbSet<FeePrice> FeePrice { get; set; }
        public virtual DbSet<MapParameter> MapParameter { get; set; }
        public virtual DbSet<Meter> Meter { get; set; }
        public virtual DbSet<MeterCommand> MeterCommand { get; set; }
        public virtual DbSet<MeterOperationRecords> MeterOperationRecords { get; set; }
        public virtual DbSet<MeterReadingRecord> MeterReadingRecord { get; set; }
        public virtual DbSet<ModelNum> ModelNum { get; set; }
        public virtual DbSet<OrderDetail> OrderDetail { get; set; }
        public virtual DbSet<Owner> Owner { get; set; }
        public virtual DbSet<ParameterList> ParameterList { get; set; }
        public virtual DbSet<PaymentRecords> PaymentRecords { get; set; }
        public virtual DbSet<PrcieStep> PrcieStep { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<RefillRecords> RefillRecords { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<SettlementDay> SettlementDay { get; set; }
        public virtual DbSet<StatisticHistories> StatisticHistories { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<WarningMagnetism> WarningMagnetism { get; set; }
        public virtual DbSet<WxRechargeRecords> WxRechargeRecords { get; set; }
        public virtual DbSet<WxSrOrderRecords> WxSrOrderRecords { get; set; }
        public virtual DbSet<WxSrRelations> WxSrRelations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=127.0.0.1;userid=root;pwd=atmk_2019;port=3306;database=atmk_wmdb_custum_luxi;CharSet=utf8;sslmode=none;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<DataBase>(entity =>
            {
                entity.ToTable("DataBase", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Config).HasColumnType("longtext");

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.ToTable("District", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.NodeType).HasColumnType("int(11)");

                entity.Property(e => e.Parent)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<DistrictSequences>(entity =>
            {
                entity.ToTable("DistrictSequences", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DisplaySequence).HasColumnType("int(11)");

                entity.Property(e => e.DistrictId)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.Property(e => e.StaffId)
                    .IsRequired()
                    .HasColumnType("char(36)");
            });

            modelBuilder.Entity<FeePrice>(entity =>
            {
                entity.ToTable("FeePrice", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.PiceStepId)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Price1).HasColumnType("decimal(10,0)");

                entity.Property(e => e.Price2).HasColumnType("decimal(10,0)");

                entity.Property(e => e.Price3).HasColumnType("decimal(10,0)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MapParameter>(entity =>
            {
                entity.ToTable("MapParameter", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.NodeId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NodeType).HasColumnType("int(11)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Meter>(entity =>
            {
                entity.ToTable("Meter", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CommType)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.CtdeviceId)
                    .HasColumnName("CTdeviceId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictId)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.Imei)
                    .IsRequired()
                    .HasColumnName("IMEI")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.MeterNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MeterState)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MeterType)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OldMeterNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.PrcieStepId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectId)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.RefillType).HasColumnType("longtext");
            });

            modelBuilder.Entity<MeterCommand>(entity =>
            {
                entity.ToTable("MeterCommand", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Command)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CommandCount).HasColumnType("int(11)");

                entity.Property(e => e.CommandStatus).HasColumnType("int(11)");

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.ExecuteTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.MeterNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MeterOperationRecords>(entity =>
            {
                entity.ToTable("MeterOperationRecords", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OperationTime).HasColumnType("datetime(6)");

                entity.Property(e => e.OperationType).HasColumnType("int(11)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MeterReadingRecord>(entity =>
            {
                entity.ToTable("MeterReadingRecord", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CtdeviceId)
                    .IsRequired()
                    .HasColumnName("CTdeviceId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsgType).HasColumnType("int(2)");

                entity.Property(e => e.Value24)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ValveState).HasColumnType("int(11)");

                entity.Property(e => e.Voltage)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Warning).HasColumnType("int(2)");
            });

            modelBuilder.Entity<ModelNum>(entity =>
            {
                entity.ToTable("ModelNum", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Caliber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.FactoryCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FactoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LifeTime)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.MaxFlow)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MaxRange)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.Protocol)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.ToTable("OrderDetail", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Balance).HasColumnType("decimal(30,0)");

                entity.Property(e => e.CostType).HasColumnType("int(1)");

                entity.Property(e => e.MeterId)
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.MeterNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Modification).HasColumnType("int(1)");

                entity.Property(e => e.Money).HasColumnType("decimal(30,2)");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.RechargeChannels)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).HasColumnType("longtext");

                entity.Property(e => e.WaterPrice).HasColumnType("decimal(30,2)");

                entity.Property(e => e.WaterVolume).HasColumnType("decimal(30,2)");

                entity.Property(e => e.WxMessages).HasColumnType("longtext");
            });

            modelBuilder.Entity<Owner>(entity =>
            {
                entity.ToTable("Owner", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.DistrictId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HouseNumber)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<ParameterList>(entity =>
            {
                entity.ToTable("ParameterList", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Config).HasColumnType("longtext");

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.ProjectId)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<PaymentRecords>(entity =>
            {
                entity.ToTable("PaymentRecords", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AmountChangedDownMode).HasColumnType("int(11)");

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.MeterNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMessage).HasColumnType("longtext");

                entity.Property(e => e.PaymentSum).HasColumnType("decimal(65,2)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.SettlementId).HasColumnType("longtext");

                entity.Property(e => e.StaffId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PrcieStep>(entity =>
            {
                entity.ToTable("PrcieStep", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.CutPoint1).HasColumnType("int(11)");

                entity.Property(e => e.CutPoint2).HasColumnType("int(11)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.MeterType).HasColumnType("int(11)");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.PriceUnit).HasColumnType("int(11)");

                entity.Property(e => e.ProjectId)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.ToTable("Project", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnType("longtext");

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<RefillRecords>(entity =>
            {
                entity.ToTable("RefillRecords", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.LastBalance).HasColumnType("decimal(65,2)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.RefillMessage).HasColumnType("longtext");

                entity.Property(e => e.RefillSum).HasColumnType("decimal(65,2)");

                entity.Property(e => e.RefillType).HasColumnType("int(11)");

                entity.Property(e => e.Rescind).HasColumnType("int(11)");

                entity.Property(e => e.ShouldPayment).HasColumnType("decimal(65,2)");

                entity.Property(e => e.StaffId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.ToTable("Roles", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.Parmissions).HasColumnType("longtext");

                entity.Property(e => e.ProjectId)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });

            modelBuilder.Entity<SettlementDay>(entity =>
            {
                entity.ToTable("SettlementDay", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CtdeviceId)
                    .IsRequired()
                    .HasColumnName("CTdeviceId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperatorUserId)
                    .HasColumnName("OperatorUserID")
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.ReadTime).HasColumnType("date");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SettlementState).HasColumnType("int(2)");
            });

            modelBuilder.Entity<StatisticHistories>(entity =>
            {
                entity.ToTable("StatisticHistories", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.ProjectId).HasColumnType("longtext");

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.StatisticType).HasColumnType("int(11)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.DistrictId).HasColumnType("longtext");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasColumnType("longtext");

                entity.Property(e => e.PageLines).HasColumnType("int(11)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.RoleId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WarningMagnetism>(entity =>
            {
                entity.ToTable("WarningMagnetism", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CtdeviceId)
                    .IsRequired()
                    .HasColumnName("CTdeviceId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsgType).HasColumnType("int(2)");

                entity.Property(e => e.Value24)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ValveState).HasColumnType("int(11)");

                entity.Property(e => e.Voltage)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Warning).HasColumnType("int(2)");
            });

            modelBuilder.Entity<WxRechargeRecords>(entity =>
            {
                entity.ToTable("WxRechargeRecords", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.HouseNumber)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastBalance).HasColumnType("decimal(65,30)");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OpenId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.RefillSum).HasColumnType("decimal(65,30)");

                entity.Property(e => e.Rescind).HasColumnType("int(11)");

                entity.Property(e => e.ShouldPayment).HasColumnType("decimal(65,30)");
            });

            modelBuilder.Entity<WxSrOrderRecords>(entity =>
            {
                entity.ToTable("WxSrOrderRecords", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id)
                    .HasColumnType("char(36)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime(6)");

                entity.Property(e => e.HouseNumber)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastBalance)
                    .HasColumnType("decimal(65,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.Log).HasColumnType("longtext");

                entity.Property(e => e.Memo).HasColumnType("longtext");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OpenId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderStatus).HasColumnType("int(11)");

                entity.Property(e => e.OutTradeNo)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");

                entity.Property(e => e.RefillSum)
                    .HasColumnType("decimal(65,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ShouldPayment)
                    .HasColumnType("decimal(65,2)")
                    .HasDefaultValueSql("0.00");
            });

            modelBuilder.Entity<WxSrRelations>(entity =>
            {
                entity.ToTable("WxSrRelations", "atmk_wmdb_custum_luxi");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OpenId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordState).HasColumnType("int(11)");
            });
        }
    }
}
