﻿using Atmk.WaterMeter.MIS.Entities.Common;
using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class Meter: BaseRecord
    {
        //public string Id { get; set; }
        //public string Memo { get; set; }
        //public string Log { get; set; }
        //public DateTime CreateTime { get; set; }
        //public int RecordState { get; set; }
        public string MeterNumber { get; set; }
        public string Imei { get; set; }
        public string OldMeterNumber { get; set; }
        public string MeterType { get; set; }
        public string CommType { get; set; }
        public string CtdeviceId { get; set; }
        public string RefillType { get; set; }
        public string MeterState { get; set; }
        public string PrcieStepId { get; set; }
        public string OwnerId { get; set; }
        public string DistrictId { get; set; }
        public string ProjectId { get; set; }
    }
}
