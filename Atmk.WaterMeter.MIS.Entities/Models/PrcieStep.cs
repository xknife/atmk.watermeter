﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class PrcieStep
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string Name { get; set; }
        public string ProjectId { get; set; }
        public int MeterType { get; set; }
        public int PriceUnit { get; set; }
        public int CutPoint1 { get; set; }
        public int CutPoint2 { get; set; }
    }
}
