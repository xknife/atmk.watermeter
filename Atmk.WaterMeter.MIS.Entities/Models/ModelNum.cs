﻿using System;
using System.Collections.Generic;

namespace Atmk.WaterMeter.MIS.Entities.Models
{
    public partial class ModelNum
    {
        public string Id { get; set; }
        public string Memo { get; set; }
        public string Log { get; set; }
        public DateTime CreateTime { get; set; }
        public int RecordState { get; set; }
        public string Name { get; set; }
        public string MaxRange { get; set; }
        public string MaxFlow { get; set; }
        public string LifeTime { get; set; }
        public string Caliber { get; set; }
        public string Protocol { get; set; }
        public string FactoryCode { get; set; }
        public string FactoryName { get; set; }
    }
}
