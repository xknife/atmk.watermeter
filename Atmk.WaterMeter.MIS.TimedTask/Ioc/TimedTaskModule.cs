﻿using Autofac;

namespace Atmk.WaterMeter.MIS.TimedTask.Ioc
{
    public class TimedTaskModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            //builder.RegisterType<OpenValveService>().AsSelf().SingleInstance();
            builder.RegisterType<SettlementService>().AsSelf().SingleInstance();
        }
    }
}