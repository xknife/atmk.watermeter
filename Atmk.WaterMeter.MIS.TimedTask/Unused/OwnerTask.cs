﻿using System;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask;
using NLog;

namespace Atmk.WaterMeter.MIS.TimedTask.Unused
{
    /// <summary>
    /// 与业主(户)有关的统计
    /// </summary>
    public class OwnerTask : IOwnerTask
    {
        private readonly IAccountsLogic _accountsLogic;
        private readonly IOwnersLogic _ownersLogic;
        private readonly IRegionLogic _regionLogic;
        private readonly IStatisticHistoryLogic _statisticHistoryLogic;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public OwnerTask(
            IAccountsLogic accountsLogic,
            IOwnersLogic ownersLogic,
            IRegionLogic regionLogic,
            IStatisticHistoryLogic statisticHistoryLogic
        )
        {
            _accountsLogic = accountsLogic;
            _ownersLogic = ownersLogic;
            _regionLogic = regionLogic;
            _statisticHistoryLogic = statisticHistoryLogic;
        }

        public OwnerTask()
        {
        }

        /// <summary>
        /// 执行与业主(户)有关的统计
        /// </summary>
        public void Start(string projectId)
        {
            try
            {
                #region 之前备份
                //var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                //var distirctIds = districts.Select(d => d.Id.ToId()).ToArray();
                ////费用提醒
                //var costRemidertask = new Task(() =>
                //{
                //    var data = _accountsLogic.GetCostReminderItemList(distirctIds);
                //    _statisticHistoryLogic.Save(projectId, StatisticType.CostReminderData, data);
                //    var jsData = JsonConvert.SerializeObject(data);
                //    _logger.Info(jsData);
                //});
                ////关阀
                //var closeValveTask = new Task(() =>
                //{
                //    var data = _ownersLogic.GetCloseClosingValveOwnerItems(distirctIds);
                //    _statisticHistoryLogic.Save(projectId, StatisticType.ClosingvalveData, data);
                //    var jsData = JsonConvert.SerializeObject(data);
                //    _logger.Info(jsData);
                //});
                //costRemidertask.Start();
                //closeValveTask.Start(); 
                #endregion


            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
    }
}