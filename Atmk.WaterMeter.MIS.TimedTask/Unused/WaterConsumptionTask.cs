﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Newtonsoft.Json;
using NLog;

namespace Atmk.WaterMeter.MIS.TimedTask.Unused
{
    /// <summary>
    /// 水量统计任务
    /// </summary>
    public class WaterConsumptionTask : IWaterConsumptionTask
    {
        private readonly IRegionLogic _regionLogic;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IWaterConsumptionLogic _waterConsumptionLogic;
        private readonly IWaterMetersLogic _waterMetersLogic;
        private readonly IStatisticHistoryLogic _statisticHistoryLogic;

        public WaterConsumptionTask(
            IRegionLogic regionLogic,
            IWaterConsumptionLogic waterConsumptionLogic,
            IWaterMetersLogic waterMetersLogic,
            IStatisticHistoryLogic statisticHistoryLogic
        )
        {
            _regionLogic = regionLogic;
            _waterConsumptionLogic = waterConsumptionLogic;
            _waterMetersLogic = waterMetersLogic;
            _statisticHistoryLogic = statisticHistoryLogic;
        }

        /// <summary>
        /// 执行水量统计任务
        /// </summary>
        public void Start(string projectId)
        {
            try
            {
                var districts = _regionLogic.GetLeafDistrictEntities(projectId);
                var distirctIds = districts.Select(d => d.Id).ToArray();

                var task = new Task(() =>
                {
                    TotalWater(projectId);
                    LastMonthPriceStepWater(projectId, distirctIds);
                    LastQuqrterWater(projectId, distirctIds);
                    LastYearWater(projectId, distirctIds);
                    YesterdayConsumptionData(projectId, distirctIds);
                });
                task.Start();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        private void LastYearWater(string projectId, string[] distirctIds)
        {
            var lastYearWaterdata = _waterConsumptionLogic.GetYearWaterConsumptions(Period.Year,
                DateTime.Today.AddYears(-1), distirctIds);
            _statisticHistoryLogic.Save(projectId, StatisticType.LastYearWaterConsumptionsCountData,
                lastYearWaterdata);
            var jsData = JsonConvert.SerializeObject(lastYearWaterdata, Formatting.Indented);
            _logger.Debug("上一年用水量: " + jsData);
        }

        private void LastQuqrterWater(string projectId, string[] distirctIds)
        {
            var lastQuarterWaterdata = _waterConsumptionLogic.GetDistrictMonthWatherConsumption(Period.Quarter,
                DateTimeHelper.GetQuarter(1, false), distirctIds);
            _statisticHistoryLogic.Save(projectId, StatisticType.LastSeasonWaterConsumptionsCountData,
                lastQuarterWaterdata);
            var jsData = JsonConvert.SerializeObject(lastQuarterWaterdata, Formatting.Indented);
            _logger.Debug("上季度用水量: " + jsData);
        }

        private void LastMonthPriceStepWater(string projectId, string[] distirctIds)
        {
            var lastMonthPriceStepWaterdata = _waterConsumptionLogic.GetDistrictPriceStepWaterConsumption(
                Period.Month,
                DateTime.Today.AddMonths(-1), distirctIds);
            _statisticHistoryLogic.Save(projectId, StatisticType.LastMonthDistrictConsumptionsCountData,
                lastMonthPriceStepWaterdata);
            var jsData = JsonConvert.SerializeObject(lastMonthPriceStepWaterdata, Formatting.Indented);
            _logger.Debug("上月阶梯用水: " + jsData);
        }

        private void YesterdayConsumptionData(string projectId, string[] distirctIds)
        {
            var yesterdayConsumptionData = _waterConsumptionLogic.GetWaterConsumption(
                Period.Day,
                DateTime.Today.AddDays(-1), distirctIds);
            _statisticHistoryLogic.Save(projectId, StatisticType.YesterdayConsumptionData,
                yesterdayConsumptionData);
            var jsData = JsonConvert.SerializeObject(yesterdayConsumptionData, Formatting.Indented);
            _logger.Debug("昨日各业主用水总量: " + jsData);
        }

        private void TotalWater(string projectId)
        {
            var totalWaterData = _waterConsumptionLogic.GetWaterConsumptionInfoData(projectId);
            _statisticHistoryLogic.Save(projectId, StatisticType.WaterConsumptionInfoData, totalWaterData);
            var jsData = JsonConvert.SerializeObject(totalWaterData, Formatting.Indented);
            _logger.Debug("用水统计: " + jsData);
        }
        //var totalTask = new Task(() =>
        //{
        //    var data = _waterConsumptionLogic.GetWaterConsumptionInfoData(projectId);
        //    var jsData = JsonConvert.SerializeObject(data);
        //    _logger.Info(jsData);
        //});
        //var lastMonthPriceStepWaterTask = new Task(() =>
        //{
        //    var data = _waterConsumptionLogic.GetDistrictPriceStepWaterConsumption(Period.Month,
        //        DateTime.Today.AddMonths(-1), distirctIds);
        //    var jsData = JsonConvert.SerializeObject(data);
        //    _logger.Info(jsData);
        //});
        //var lastQuarterWaterTask = new Task(() =>
        //{
        //    var data = _waterConsumptionLogic.GetDistrictMonthWatherConsumption(Period.Quarter,
        //        DateTimeHelper.GetQuarter(1, false), distirctIds);
        //    var jsData = JsonConvert.SerializeObject(data);
        //    _logger.Info(jsData);
        //});
        //var lastYearWaterTask = new Task(() =>
        //{
        //    var data = _waterConsumptionLogic.GetYearWaterConsumptions(Period.Year,
        //        DateTime.Today.AddYears(-1), distirctIds);
        //    var jsData = JsonConvert.SerializeObject(data);
        //    _logger.Info(jsData);
        //});

        //totalTask.Start();
        //lastMonthPriceStepWaterTask.Start();
        //lastQuarterWaterTask.Start();
        //lastYearWaterTask.Start();
    }
}