﻿using System;
using System.Threading;
using Atmk.WaterMeter.MIS.Commons.Interfaces.TimeTask;
using NLog;

namespace Atmk.WaterMeter.MIS.TimedTask.Unused
{
    public class StatisticsTask:IStatisticsTask
    {
        private static Timer timer;
        private const int MIN = 60000;
        private const int HOUR = 60 * MIN;
        private const int DAY = 24 * HOUR;
        private const int HALFDAY = 12 * HOUR;

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IOwnerTask _ownerTask;
        private readonly IRegionTask _regionTask;
        private readonly IWaterConsumptionTask _waterConsumptionTask;


        public StatisticsTask(
            IOwnerTask ownerTask,
            IRegionTask regionTask,
            IWaterConsumptionTask waterConsumption
        )
        {
            _ownerTask = ownerTask;
            _regionTask = regionTask;
            _waterConsumptionTask = waterConsumption;
        }

        public StatisticsTask()
        {
        }

        /// <summary>
        /// 运行统计任务 
        /// </summary>
        public void Run(string projectId ="")
        {
            //设定时间
            const int period = HOUR;
            StatisticRouting(projectId);
            //开启线程
            //timer = new Timer(StatisticRouting, projectId, 1000, period);
            
            //TODO:后期开发，需要做成线程管理器，根据不同项目对应不同线程
        }
        /// <summary>
        /// 停止任务运行
        /// </summary>
        public void Stop()
        {
            timer.Dispose();
        }

        /// <summary>
        /// 统计路由：拆分为多个统计任务，并执行
        /// </summary>
        private void StatisticRouting(string state)
        {
            try
            {
                //统计所有所在项目的数据统计及存储任务
                var projectId = "5e890445-3a5c-41f6-8b64-304cd34c1565";
                if (state!=null)
                {
                    if (state.ToString() != "")
                        projectId = state.ToString();
                }
             
                _ownerTask.Start(projectId);
                //_regionTask.Start(projectId);
                //_waterConsumptionTask.Start(projectId);
            }
            catch (Exception e)
            {
               _logger.Error(e);
            }
        }

    }
}
