﻿using System;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Entities;

namespace Atmk.WaterMeter.MIS.Logic
{
    public class MeterModelNumLogic:IMeterModelLogic
    {
        private readonly IRepository _repository;


        public MeterModelNumLogic(
            IRepository repository
        )
        {
            _repository = repository;
        }
        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <returns></returns>
        public  object SelectModNum(string id = "")
        {
            var entities =
                Enumerable.ToList<MeterModelInformation>(_repository.FindAll<MeterModelInformation>());
            if (id.Length > 0)
                entities = entities.Where(e => e.Id.ToString() == id).ToList();
            var result = new object[entities.Count];
            for (var i = 0; i < entities.Count; i++)
            {
                result[i] = new
                {
                    id = entities[i].Id.ToString(), //id
                    title = entities[i].Name, //名称
                    factoryCode = entities[i].FactoryCode, //厂商代码
                    factoryName = entities[i].FactoryName, //厂商名称
                    maxRange = entities[i].MaxRange, //最大量程
                    maxFlow = entities[i].MaxFlow,
                    lifeTime = entities[i].LifeTime, //使用年限
                    caliber = entities[i].Caliber, //口径
                    protocol = entities[i].Protocol, //协议
                    remark = entities[i].Memo //备注信息
                };
            }
            return result;
        }

        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public  bool AddModNum(dynamic obj, out Exception exception)
        {
            exception = null;
            try
            {
                var name = obj.title.ToString();
                var entities =
                    Enumerable.ToList<MeterModelInformation>(_repository.FindAll<MeterModelInformation>());
                if (entities.Any(r => r.Name == name))
                {
                    exception = new Exception("水表类型名称重复");
                    return false;
                }
                var entity = new MeterModelInformation(name)
                {
                    FactoryCode = obj.factoryCode.ToString(),
                    FactoryName = obj.factoryName.ToString(),
                    MaxRange = obj.maxRange.ToString(),
                    MaxFlow = obj.maxFlow.ToString(),
                    LifeTime = obj.lifeTime.ToString(),
                    Caliber = obj.caliber.ToString(),
                    Protocol = obj.protocol.ToString(),
                    Memo = obj.remark.ToString()
                };
                if (_repository.Add(entity) > 0)
                    return true;
                exception = new Exception("添加失败");
                return false;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }

        /// <summary>
        /// 查询水表型号信息
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public  bool UpdateModNum(dynamic obj, out Exception exception)
        {
            try
            {
                exception = null;
                var entities = Enumerable
                    .Where<MeterModelInformation>(_repository.FindAll<MeterModelInformation>(),
                        p => p.Id.ToString() == obj.id.ToString()).ToList();
                if (!entities.Any())
                {
                    exception = new Exception("水表类型信息不存在，无法修改");
                    return false;
                }
                var entity = entities.First();
                entity.Name = obj.title.ToString();
                entity.FactoryCode = obj.factoryCode.ToString();
                entity.FactoryName = obj.factoryName.ToString();
                entity.MaxRange = obj.maxRange.ToString();
                entity.MaxFlow = obj.maxFlow.ToString();
                entity.LifeTime = obj.lifeTime.ToString();
                entity.Caliber = obj.caliber.ToString();
                entity.Protocol = obj.protocol.ToString();
                entity.Memo = obj.remark.ToString();
                if (_repository.Update(entity) > 0)
                    return true;
                exception = new Exception("修改失败");
                return false;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }

        /// <summary>
        /// 删除水表型号信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public  bool DeleteModNum(string id, out Exception exception)
        {
            try
            {
                exception = null;
                if (Enumerable.Count<MeterModelInformation>(_repository.FindAll<MeterModelInformation>(),
                        r => r.Id.ToString() == id) == 0)
                {
                    exception = new Exception("没有水表型号信息");
                    return false;
                }
                var entity = Enumerable.First<MeterModelInformation>(_repository
                    .FindAll<MeterModelInformation>(), r => r.Id.ToString() == id);

                if (_repository.Remove(entity) > 0)
                    return true;
                exception = new Exception("删除失败");
                return false;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }
    }
}