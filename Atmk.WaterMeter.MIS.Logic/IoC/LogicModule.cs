﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.CT_NBIoT;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin;
using Atmk.WaterMeter.MIS.Logic.CT_NBIoT;
using Atmk.WaterMeter.MIS.Logic.Mock;
using Atmk.WaterMeter.MIS.Logic.Statistics;
using Atmk.WaterMeter.MIS.Logic.WeiXin;
using Autofac;

namespace Atmk.WaterMeter.MIS.Logic.IoC
{
    public class LogicModule : Module
    {
        public static bool IsDevPhase { get; set; } = false;

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            //builder.RegisterType<ChartLogicMock>().As<IChartLogic>();

            builder.RegisterType<BaseInfoLogic>().As<IBaseInfoLogic>();
            builder.RegisterType<DistrictLogic>().As<IDistrictLogic>();
           
            builder.RegisterType<MeterCommandsLogic>().As<IMeterCommandsLogic>();
            builder.RegisterType<CT_NBIoT_CommandSender>().As<ICommandSender>();
            builder.RegisterType<MeterModelNumLogic>().As<IMeterModelLogic>();
            builder.RegisterType<OwnerMeterLogic>().As<IOwnerMeterLogic>();
            builder.RegisterType<MeterLogic>().As<IMeterLogic>();
            builder.RegisterType<PriceStepLogic>().As<IPriceStepLogic>();
            builder.RegisterType<QueryDatasLogic>().As<IQueryDatasLogic>();
            builder.RegisterType<RolesLogic>().As<IRolesLogic>();
            builder.RegisterType<SettlementManageLogic>().As<ISettlementManageLogic>();
            builder.RegisterType<SystemParamentsLogic>().As<ISystemParamentsLogic>();
            builder.RegisterType<UserCenterLogic>().As<IUserCenterLogic>();
            builder.RegisterType<LoginLogic>().As<ILoginLogic>();
            builder.RegisterType<ReponstoryManageLogic>().As<IReponstoryManageLogic>();
            builder.RegisterType<AccountsLogic>().As<IAccountsLogic>();
            builder.RegisterType<OwnersLogic>().As<IOwnersLogic>();
            builder.RegisterType<RegionLogic>().As<IRegionLogic>();
            //builder.RegisterType<ConsumptionStatisticsLogicMock>().As<IWaterConsumptionLogic>();//采用mock
            builder.RegisterType<WaterConsumptionLogic>().As<IWaterConsumptionLogic>(); 
            builder.RegisterType<WaterMetersLogic>().As<IWaterMetersLogic>();
            builder.RegisterType<DataStoreLogic>().As<IDataStoreLogic>();

            builder.RegisterType<EditCoordinatesLogic>().As<IEditCoordinatesLogic>();
            builder.RegisterType<EditCoordinatesLogic>().As<ISelectCoordinatesLogic>();

            builder.RegisterType<ProjectsLogic>().As<IProjectsLogic>();
            builder.RegisterType<StaffLogic>().As<IStaffLogic>();

            builder.RegisterType<ChartLogicMock>().As<IChartLogic>();
            //builder.RegisterType<ChartLogic>().As<IChartLogic>();

            builder.RegisterType<StatisticHistoryLogic>().As<IStatisticHistoryLogic>();

            // builder.RegisterType<MergeStatisticsLogic>().As<IMergeStatisticsLogic>();

            builder.RegisterType<WxAccountLogic>().As<IWxAccountLogic>();
            //builder.RegisterType<WxAccountLogicMock>().As<IWxAccountLogic>();
            builder.RegisterType<WxOtherLogicMock>().As<IWxOtherLogic>();

            builder.RegisterType<CT_PlatformDevideManageLogic>().As<ICT_PlatformDevideManageLogic>();
            builder.RegisterType<CT_PlatformDeviceInfoLogic>().As<ICT_PlatformDeviceInfoLogic>();
            builder.RegisterType<WxRechargeRecordLogic>().As<IWxRechargeRecordLogic>();
            builder.RegisterType<AccountManageLogic>().As<IAccountManageLogic>();
        }
    }
}
