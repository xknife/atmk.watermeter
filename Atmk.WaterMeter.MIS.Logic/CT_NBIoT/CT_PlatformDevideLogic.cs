﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.CT_NBIoT;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;

namespace Atmk.WaterMeter.MIS.Logic.CT_NBIoT
{
   
    /// <summary>
    /// 电信设备平台管理逻辑
    /// </summary>
    public class CT_PlatformDevideManageLogic : ICT_PlatformDevideManageLogic
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly ICT_NBIoT_DevicesManage _ctNbioTDevicesManage;

        public CT_PlatformDevideManageLogic(ICT_NBIoT_DevicesManage ctNbioTDevicesManage)
        {
            _ctNbioTDevicesManage = ctNbioTDevicesManage;
        }
        /// <summary>
        /// 向平台添加设备
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="model"></param>
        /// <param name="name"></param>
        /// <returns>返回值里 Message为创建成功后的设备Id</returns>
        public async Task<BooleanResult> AddDevice(string imei, string model, string name)
        {
            var result = new BooleanResult{Success = false};
            var deviceId = _ctNbioTDevicesManage.RegisterDevice(imei);
            if (deviceId == Guid.Empty)
                return await Task.Run(() =>
                {
                    result.ErrorMessage = "注册设备失败";
                    return result;
                });
            result.Message = deviceId.ToId();
            var success = _ctNbioTDevicesManage.UpdateDeviceInfo(deviceId.ToId(), name, model);
            result.Success = success;
            return await Task.Run(() =>
            {
                result.ErrorMessage = success ? "": "绑定注册设备名称时失败";
                return result;
            });
        }
        /// <summary>
        /// 删除平台上指定设备
        /// </summary>
        /// <param name="deviceid"></param>
        /// <returns>返回值 bool 和 ErrorMessage</returns>
        public async Task<BooleanResult> DeleteDevice(string deviceid)
        {
            var success = _ctNbioTDevicesManage.DeleteDevice(deviceid);
            var result = new BooleanResult {Success = success};
            return await Task.Run(() =>
            {
                result.ErrorMessage = success ? "" : "删除设备失败";
                return result;
            });
        }
        /// <summary>
        /// 获取平台所有设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResDevices> GetDevices()
        {
            return await Task.Run(() => _ctNbioTDevicesManage.GetDevices());
        }

    }
}