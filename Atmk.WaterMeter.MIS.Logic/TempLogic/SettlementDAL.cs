﻿using Atmk.WaterMeter.EF.ModelPuls;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atmk.WaterMeter.MIS.Logic.TempLogic
{

    public class SettlementDAL
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public int Settlement(Token token, string aid, DateTime month)
        {
            using (var context = ContextBuilder.Build())
            {
                using (var tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        List<District> total_districts;
                        //递归查出所有为叶子节点的片区
                        var Projects2district = context.District.Where(m => m.ProjectId == token.payload.areaid).ToList();
                        total_districts = Tools.getNodeNext(Projects2district, aid, Projects2district, new List<District>());
                        var districts_ids = total_districts.Select(m1 => m1.Id.ToString());

                        //年 已结算表的水表
                        //List<SettlementDay> settleDays_Year = context.SettlementDay.Where(m => districts_ids.Contains(m.DistrictId) && m.ReadTime.Year == month.Year).ToList();
                        List<SettlementDay> settleDays_Year = context.SettlementDay.Where(m => m.ReadTime.Year == month.Year).ToList();
                        //月 未结算的水表
                        List<SettlementDay> settleDays_Month = settleDays_Year.Where(m => m.ReadTime.Month == month.Month && m.SettlementState == 0).ToList();

                        //按表号分组
                        var meterNumbers = settleDays_Month.GroupBy(m => m.CtdeviceId).Select(m => m.Key).ToArray();

                        var prcieStep = context.PrcieStep.Where(m => m.ProjectId == token.payload.areaid).ToList();
                        var prcieStepIds = prcieStep.Select(m => m.Id.ToString());
                        var feePrices = context.FeePrice.Where(m => prcieStepIds.Contains(m.PiceStepId)).ToList();
                        var total_Meter = context.Meter.Where(m => meterNumbers.Contains(m.MeterNumber)).ToList();

                        //表用量（年度）
                        var settleDaysGroup_Year = settleDays_Year.Where(m => m.SettlementState == 1);
                        //表用量（月）
                        var settleDaysGroup = settleDays_Month.GroupBy(m => m.CtdeviceId).Select(k =>
                        new Out_SettleDayGroup
                        {
                            MeterNumber = k.Key,
                            WaterVolume = Convert.ToDecimal(k.Sum(l => l.Dosage)),
                            Dosage_Year = Convert.ToDecimal(settleDaysGroup_Year.Where(m => m.CtdeviceId == k.Key).Sum(m => m.Dosage))
                        });

                        var result_SettleDayGroup = new List<Out_SettleDayGroup>();
                        foreach (var item in settleDaysGroup)
                        {
                            var nr = new Out_SettleDayGroup();
                            Meter meter = total_Meter.First(m => m.MeterNumber == item.MeterNumber);
                            FeePrice feePrice = feePrices.First(m => m.PiceStepId == meter.PrcieStepId);
                            nr.MeterNumber = item.MeterNumber;
                            nr.WaterVolume = item.WaterVolume;
                            nr.Dosage_Year = item.Dosage_Year;
                            nr.PiceStepId = meter.PrcieStepId;
                            nr.OwnerId = meter.OwnerId;
                            nr.MeterId = meter.Id;
                            nr.CutPoint1 = prcieStep.First(m => m.Id.ToString() == meter.PrcieStepId).CutPoint1;
                            nr.CutPoint2 = prcieStep.First(m => m.Id.ToString() == meter.PrcieStepId).CutPoint2;
                            nr.Price1 = feePrice.Price1;
                            nr.Price2 = feePrice.Price2;
                            nr.Price3 = feePrice.Price3;
                            result_SettleDayGroup.Add(nr);
                        }

                        foreach (var item in result_SettleDayGroup)
                        {
                            var orderDetail = new OrderDetail();
                            orderDetail.CostType = 1;//水表扣费
                            orderDetail.RechargeChannels = "水表扣费";
                            orderDetail.WaterVolume = item.WaterVolume;
                            orderDetail.OwnerId = item.OwnerId;
                            orderDetail.MeterNumber = item.MeterNumber;
                            orderDetail.MeterId = item.MeterId;
                            orderDetail.CreateTime = DateTime.Now;
                            //阶梯280以上
                            if (item.Dosage_Year >= item.CutPoint2)
                            {
                                orderDetail.WaterPrice = item.Price3;
                                orderDetail.Money = -item.WaterVolume * item.Price3;
                            }
                            //阶梯160-280
                            if (item.Dosage_Year >= item.CutPoint1 && item.Dosage_Year < item.CutPoint2)
                            {
                                //判断年用水量 + 本次用水量 是否超过第二阶梯
                                if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint2)
                                {
                                    //阶梯2用水量
                                    decimal JT2 = (item.CutPoint2 - item.Dosage_Year);
                                    //阶梯3用水量
                                    decimal JT3 = item.WaterVolume - JT2;
                                    decimal part2 = JT2 * item.Price2;
                                    decimal part3 = JT3 * item.Price3;
                                    orderDetail.WaterPrice = 777;//3个7 的价格  看备注
                                    orderDetail.Money = -(part2 + part3);
                                    orderDetail.Remarks = "部分1用量：" + JT2 + "，部分1价格：" + item.Price2 + "。部分2用量：" + JT3 + "，部分2价格：" + item.Price3;
                                }
                                else
                                {
                                    //年用水量 + 本次用水量 未超过第二阶梯
                                    orderDetail.WaterPrice = item.Price2;
                                    orderDetail.Money = -item.WaterVolume * item.Price2;
                                }
                            }
                            //阶梯0-160
                            if (item.Dosage_Year < item.CutPoint1)
                            {
                                decimal JT1 = item.CutPoint1 - item.Dosage_Year;
                                decimal JT2 = 0;
                                decimal JT3 = 0;
                                if ((item.Dosage_Year + item.WaterVolume) < item.CutPoint1)
                                {
                                    JT1= item.WaterVolume - item.Dosage_Year;
                                }
                                //判断年用水量 + 本次用水量 是否超过第一阶梯
                                if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint1 && (item.Dosage_Year + item.WaterVolume) <= item.CutPoint2)
                                {
                                    JT2 = item.WaterVolume - JT1;
                                    orderDetail.WaterPrice = 777;
                                    orderDetail.Remarks = "部分1用量：" + JT1 + "方 价格：" + item.Price1 + "；部分2用量：" + JT2 + "方 价格：" + item.Price2;
                                }
                                //判断年用水量 + 本次用水量 是否超过第二阶梯
                                if ((item.Dosage_Year + item.WaterVolume) > item.CutPoint2)
                                {
                                    JT2 = item.CutPoint2 - item.CutPoint1;
                                    JT3 = item.WaterVolume - (JT1 + JT2);
                                    orderDetail.WaterPrice = 777;
                                    orderDetail.Remarks = "部分1用量：" + JT1 + "方 价格：" + item.Price1 + "；部分2用量：" + JT2 + "方 价格：" + item.Price2 + "；部分3用量：" + JT3 + "，价格：" + item.Price3;
                                }
                                decimal part1 = JT1 * item.Price1;
                                decimal part2 = JT2 * item.Price2;
                                decimal part3 = JT3 * item.Price3;
                                orderDetail.WaterPrice = orderDetail.WaterPrice == 777 ? 777 : item.Price1;//3个7 的价格  看备注
                                orderDetail.Money = -(part1 + part2 + part3);
                            }
                            if (orderDetail.Money!=0)
                            {
                                context.Add(orderDetail);
                            }
                        }
                        //int Count = context.SaveChanges();
                        //改变该月水表的 结算状态
                        foreach (var item in settleDays_Month)
                        {
                            item.SettlementState = 1;
                            context.Update(item);
                        }
                        int Count = context.SaveChanges();
                        tran.Commit();
                        return Count;

                    }
                    catch (Exception ex)
                    {
                        _logger.Fatal($"结算失败:{ex.Message}");
                        tran.Rollback();
                        return 0;
                    }
                }
            }
        }
    }
}
