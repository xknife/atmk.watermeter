﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.ViewModels;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Logic
{
    public class MergeStatisticsLogic : IMergeStatisticsLogic
    {
        private readonly IBaseInfoLogic _baseInfoLogic;
        private readonly IMeterReadingRepository _meterReadingRepository;
        private readonly IUserCenterLogic _userCenterLogic;


        public MergeStatisticsLogic(
            IBaseInfoLogic baseInfoLogic,
            IMeterReadingRepository meterReadingRepository,
            IUserCenterLogic userCenterLogic
        )
        {
            _baseInfoLogic = baseInfoLogic;
            _meterReadingRepository = meterReadingRepository;
            _userCenterLogic = userCenterLogic;
        }

        #region 水表抄表统计

        /// <summary>
        /// 获取抄回/未抄回数据
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public object MeterCopyCount(Token userPayload, DateTime date)
        {
            var watermeters = MeterForDistrictList(userPayload);
            var meterCodes = watermeters.Select(w => w.MeterNumber).ToList();
            //获取抄回记录
            var copyback = _meterReadingRepository.NowDataMeterReadingRecords(meterCodes, date.Date);
            var backData = new
            {
                copyBack = copyback.Count,
                notCopyBack = watermeters.Count - copyback.Count
            };
            return backData;
        }

        /// <summary>
        /// 根据token获取用户负责的所有水表
        /// </summary>
        /// <param name="userPayload"></param>
        /// <returns></returns>
        public List<OwnerMeterViewModel> MeterForDistrictList(Token userPayload)
        {
            //获取区域集合
            var districts = _userCenterLogic.DistrictCodeList(userPayload);
            //获取所有区域内水表
            return _baseInfoLogic.OwnerMeterData("", districts.ToArray());
        }

        /// <summary>
        /// 获取水表状态统计
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public object MeterStateCount(Token userPayload, DateTime date)
        {
            //获取所有水表
            var watermeters = MeterForDistrictList(userPayload);
            var meterCodes = watermeters.Select(w => w.MeterNumber).ToList();
            //获取抄回水表记录
            var copyback = _meterReadingRepository.NowDataMeterReadingRecords(meterCodes, date.Date);
            //获取水表最新数据
            var nearDatas = _meterReadingRepository.LastDateMeterReadingRecords(meterCodes, date);
            //获取当前日期通讯异常数据
            var linkFail = nearDatas
                .Where(m => (date.Date - m.ReadTime.Date).Days >= 3);
            //获取欠压水表
            var tapBad = nearDatas.Where(n => n.TapState == TapEnum.异常.ToString());
            //获取阀门异常水表
            var underVoltage = nearDatas.Where(n => n.Voltage == VoltageEnum.欠压.ToString());
            //水表状态信息
            var watermeterData = new
            {
                linkFail = linkFail.Count(), //通讯异常
                tapBad = tapBad.Count(), //阀门异常
                underVoltage = underVoltage.Count(), //欠压
                notCopyBack = watermeters.Count - copyback.Count //未抄回
            };
            return watermeterData;
        }

        /// <summary>
        /// 获取一年的月用水量
        /// </summary>
        /// <param name="userPayload"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public object WaterConsumptionMonth(Token userPayload, DateTime date)
        {
            //获取所有水表
            var watermeters = MeterForDistrictList(userPayload);
            var meterCodes = watermeters.Select(w => w.MeterNumber).ToList();
            //获取所有水表记录
            return null;
        }

        #endregion
    }
}