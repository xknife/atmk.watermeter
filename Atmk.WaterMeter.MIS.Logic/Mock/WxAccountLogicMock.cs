﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class WxAccountLogicMock:IWxAccountLogic
    {
        /// <summary>
        /// 通过用户id获取账户信息
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <param name="houseNumber"></param>
        /// <param name="balance"></param>
        /// <param name="meterNo">水表编号，如果有就填上</param>
        /// <returns></returns>
        public Task<bool> SelectAccount(string openId, out string meterNumber, out string houseNumber, out decimal balance,
            string meterNo = "")
        {
            meterNumber = "";
            houseNumber = "";
            balance = 0.00m;
            if (meterNo == "")
                return Task.Run(() => false);
            meterNumber = meterNo;
            houseNumber = "二楼3单元301";
            balance = 21.16m;
            return Task.Run(() => true);
        }

        /// <summary>
        /// 通过用户id获取水表编号集合--复数时用
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public Task<List<string>> SelectAllMeterNumber(string openId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 添加用户Id和水表的关系记录，并返回该水表所在位置的门牌号和
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <param name="houseNumber"></param>
        /// <param name="balance"></param>
        /// <returns></returns>
        public Task<bool> AddAccount(string openId, string meterNumber, out string houseNumber, out decimal balance)
        {
            houseNumber = "二楼3单元301";
            balance = 21.16m;
            return Task.Run(() => true);
        }

        /// <summary>
        /// 注销用户Id和水表关系记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        public Task<bool> LogoutAccount(string openId, string meterNumber)
        {
            return Task.Run(() => true);
        }

        /// <summary>
        /// 获取该名用户的缴费充值记录
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="meterNumber"></param>
        /// <returns></returns>
        public Task<List<WxRechargeRecords>> ChargingRecords(string openId, string meterNumber)
        {
            return Task.Run(() => WxRecordMock.GetRefillRecords(openId, meterNumber));
        }
    }
}
