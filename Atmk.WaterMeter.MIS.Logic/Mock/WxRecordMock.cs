﻿using System;
using System.Collections.Generic;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class WxRecordMock
    {
        public static List<WxRechargeRecords> GetRefillRecords(string openId,string meterNo)
        {
            var len = 10;
            var result = new List<WxRechargeRecords>();
            for (int i = 0; i < len; i++)
            {
                var wx = new WxRechargeRecords
                {
                    OpenId = openId,
                    AccountId = openId,
                    Number=meterNo,
                    RefillSum = new Random(Simple.GetRandomSeed()).Next(1, 500)*1.00m,
                    HouseNumber = "二楼3单元301",
                CreateTime = DateTime.Now.AddDays(-1*i),
                };
                result.Add(wx);
            }
            return result;
        }

    }
}
