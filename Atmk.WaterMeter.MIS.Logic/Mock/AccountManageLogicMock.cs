﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class AccountManageLogicMock : IAccountManageLogic
    {


        /// <summary>
        ///     查询扣费信息
        /// </summary>
        /// <param name="payloadId"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="ownerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public object SelectPayment(string payloadId, string aid, int page, int pageSize, out int count, string ownerId,
            string accountId)
        {
            throw new NotImplementedException();
        }

        

        /// <summary>
        ///     费用减免--暂停
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <param name="money"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public bool FeeDerate(Token token, string userId, string accountId, decimal money, out Exception exception)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     查询充值记录
        /// </summary>
        /// <param name="payloadId"></param>
        /// <param name="aid"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="count"></param>
        /// <param name="searchType"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public object SelectRefills(string payloadId, string aid, int page, int pageSize, DateTime startTime,
            DateTime endTime, out int count, string searchType, string searchValue)
        {
            throw new NotImplementedException();
        }

      
        public object SelectAccount(string areaId, string aid, int page, int pageSize, out int count, string searchType = "", string searchValue = "")
        {
            count = 99;
            return new object();
        }

        public bool RechargeAccount(string staffId, string staffName, string ownerId, string accountId, decimal payment, decimal lastBalance, decimal refill, out Exception exception)
        {
            throw new NotImplementedException();
        }

        public bool ReturnsAccount(string staffId, string ownerId, string accountId, dynamic returns, out Exception exception)
        {
            throw new NotImplementedException();
        }

        public object SelectPayment(string areaId, string aid, int page, int pageSize, DateTime starTime, DateTime endTime, int paymentType, out int count, string searchType = "", string searchValue = "", string ownerId = "", string accountId = "")
        {
            throw new NotImplementedException();
        }

        public bool RevertRefill(string staffId, string ownerId, string accountId, string refillId, out Exception exception)
        {
            throw new NotImplementedException();
        }

        public bool ClosingAccount(string staffId, string staffName, string ownerId, string accountId, out string message)
        {
            message = "";
            return true;
        }
    }
}