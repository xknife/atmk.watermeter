﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Utils;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class ChartDateCatchMock
    {
        public static readonly string[] DistrictList =
            {"坝上村", "泸西仓库", "丁合村", "山林哨村", "杨柳坝村", "艳色坡村", "羊格黑村", "水秧田村"};

        private static readonly string[] StaffNames =
        {
            "学富", "经纶", "学贯", "博学", "云趣", "卓尔", "团锦", "繁花", "似锦", "百花", "姹紫", "红山",
            "万里", "碧空", "湛蓝", "森林", "美玉", "清秀", "秋爽", "丹桂", "高云", "红叶", "金风", "园里"
        };

        private static readonly string[] FamilyNames =
        {
            "赵", "钱", "孙", "李", "周", "武", "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨",
            "朱", "秦", "尤", "许", "何", "吕"
        };

        /// <summary>
        /// 获取随机姓名
        /// </summary>
        public static string RondemName
        {
            get
            {
                var len = RandValue(0, 9) > 3 ? 2 : 1;
                var name =
                    $"{FamilyNames[RandValue(0, FamilyNames.Length)]}{StaffNames[RandValue(0, StaffNames.Length)].Substring(0, len)}";
                return name;
            }
        }

        /// <summary>
        /// 获取户数
        /// </summary>
        public static int Owner => 20 * 72 + 33 * 48 + 23 * 60 + 18 * 72+ 16 * 72+ 8 * 62+ 17 * 72+ 10 * 72;

        /// <summary>
        /// 获取月阶梯用水量
        /// </summary>
        /// <param name="disName"></param>
        /// <returns></returns>
        public static List<double> GetMonthBydisc(string disName)
        {
            var value = new List<double>();
            var first = 0d;
            var sec = 0d;
            var tr = 0d;
            switch (disName)
            {
                case "龙禧苑二区":
                    first = computevalue(20, 72);
                    if (first > 20 * 72 * 13)
                        sec = first - 20 * 72 * 13;
                    break;
                case "流星花园":
                    first = computevalue(33, 48);
                    if (first > 33 * 48 * 13)
                        sec = first - 33 * 48 * 13;
                    break;
                case "田园风光":
                    first = computevalue(23, 60);
                    if (first > 23 * 60 * 13)
                        sec = first - 23 * 60 * 13;
                    break;
                case "龙锦苑四区":
                    first = computevalue(18, 72);
                    if (first > 18 * 72 * 13)
                        sec = first - 18 * 72 * 13;
                    break;
                case "龙锦苑六区":
                    first = computevalue(18, 72);
                    if (first > 16 * 72 * 13)
                        sec = first - 16 * 72 * 13;
                    break;
                case "龙回苑":
                    first = computevalue(18, 72);
                    if (first > 8 * 62 * 13)
                        sec = first - 8 * 62 * 13;
                    break;
                case "天龙苑":
                    first = computevalue(18, 72);
                    if (first > 17 * 72 * 13)
                        sec = first - 17 * 72 * 13;
                    break;
                case "回龙观小区":
                    first = computevalue(18, 72);
                    if (first > 10 * 72 * 13)
                        sec = first - 10 * 72 * 13;
                    break;
                default:
                    break;
            }
            value.Add(first);
            value.Add(sec);
            value.Add(tr);
            return value;
        }

        private static double computevalue(int lou, int hu)
        {
            return RandValue(lou * hu * 3, lou * hu * 15);
        }

        private static double computevalue2(int lou, int hu)
        {
            return RandValue(lou * hu * 0, lou * hu * 6);
        }

        private static double computevalue3(int lou, int hu)
        {
            return RandValue(lou * hu * 0, lou * hu * 6);
        }

        private static int RandValue(int start, int end)
        {
            return new Random(Simple.GetRandomSeed()).Next(start, end);
        }
    }
}