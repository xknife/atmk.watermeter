﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{

    public class EditCoordinatesLogicMock : IEditCoordinatesLogic,ISelectCoordinatesLogic
    {
       
        public bool DeleteDistrictCoordinates(string nodeId)
        {
            MemoryMock._CoordinatsMock = MemoryMock._CoordinatsMock.Where(c => c.nodeId != nodeId).ToList();
            return true;
        }

        public bool DeletePoint(string nodeId)
        {
            MemoryMock._CoordinatsMock = MemoryMock._CoordinatsMock.Where(c => c.nodeId != nodeId).ToList();
            return true;
        }

        public bool InsertDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId)
        {
            var list = coordinates.Select(c => new PostPointMapCoordinat
            {
                coordinates = c,
                nodeId = nodeId,
                nodeType = CoordinateType.District
            });
            MemoryMock._CoordinatsMock.AddRange(list);
            return true;
        }

        public bool InsertPoint(MapCoordinat mapCoordinat, string nodeId, CoordinateType coordinateType)
        {
            
            MemoryMock._CoordinatsMock.Add(new PostPointMapCoordinat{nodeId=nodeId,nodeType=coordinateType,coordinates=mapCoordinat});
            return true;
        }

        public List<GetDistrictCoordinates> SelectDistrictMapCoordinat(string areaId, string districtId = "")
        {
            var list = MemoryMock._CoordinatsMock.Where(c=>c.nodeType==CoordinateType.District&&WhereDistrict(c.nodeId,districtId))
                .GroupBy(c=>c.nodeId,c=>c.coordinates)
                .Select(v=>new GetDistrictCoordinates
                    {
                        districtId = v.Key,
                        districtName = $"District{v.Key}",
                        coordinates =  v.ToList()
                    }
                );

            return list.ToList();
        }

        private bool WhereDistrict(string selDisId, string districtId)
        {
            if (districtId == "")
                return true;
            return selDisId == districtId;
        }

        public PostPointMapCoordinat SelectPointMapCoordinat(string nodeid)
        {
            var res = MemoryMock._CoordinatsMock.Where(c => c.nodeId == nodeid).ToList();
            return res.Any() ? res.First() : new PostPointMapCoordinat();
        }

        public List<PostPointMapCoordinat> SelectPointMapCoordinat(string id, CoordinateType coordinateType)
        {

            var list = MemoryMock._CoordinatsMock.Where(c => c.nodeType == CoordinateType.Owner).ToList();
            return list;
        }

        public bool UpdateDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId)
        {
            DeleteDistrictCoordinates(nodeId);
            InsertDistrictCoordinates(coordinates,nodeId);
            return true;
        }

        public bool UpdatePoint(MapCoordinat mapCoordinat, string nodeId)
        {
            DeletePoint(nodeId);
            InsertPoint(mapCoordinat, nodeId, CoordinateType.Project);
            return true;
        }

        /// <inheritdoc />
        public List<MapCoordinat> GetCoordinate(string districtId)
        {
            var list = MemoryMock._CoordinatsMock.Where(c => c.nodeType == CoordinateType.District &&c.nodeId==districtId.ToString())
               .Select(c => new MapCoordinat
                    {
                       lng = c.coordinates.lng,
                       lat = c.coordinates.lat
                    }
                );

            return list.ToList();
        }
    }
}
