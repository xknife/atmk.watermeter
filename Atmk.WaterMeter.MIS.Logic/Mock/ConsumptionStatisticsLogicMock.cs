﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
  public class ConsumptionStatisticsLogicMock : IWaterConsumptionLogic
    {
       
        public YesterdayConsumptionData GetWaterConsumption(Period period, DateTime dateTime, params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        public List<double> GetPriceStepWaterConsumption(Period period, DateTime dateTime, string ownerId)
        {
            throw new NotImplementedException();
        }

        public WaterConsumptionsCountData GetDistrictMonthWatherConsumption(Period period, DateTime dateTime,
            params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        public YearWaterConsumptionsCountData GetYearWaterConsumptions(Period period, DateTime dateTime,
            params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        public WaterConsumptionsCountData GetDistrictPriceStepWaterConsumption(Period period, DateTime dateTime,
            params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        public WaterConsumptionInfoData GetWaterConsumptionInfoData(params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        public WaterConsumptionInfoData GetWaterConsumptionInfoData(string areaId)
        {
            throw new NotImplementedException();
        }
    }
}