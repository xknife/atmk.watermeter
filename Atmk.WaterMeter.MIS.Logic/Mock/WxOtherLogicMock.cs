﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.WeiXin;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class WxOtherLogicMock : IWxOtherLogic
    {
        /// <summary>
        /// 获取微信问答答案
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> SelectWxQuestions(string openId)
        {
            return await Task.Run(() =>
            {
                var dic = GetMockWxQuestions(openId);
                return dic;
            });
        }

        private Dictionary<string, string> GetMockWxQuestions(string openId)
        {
            var dic = new Dictionary<string, string>
            {
                [$"{1}.如何绑定水表号?"] = $"点击水费充值，首次进入会要求输入水表编号或者扫描水表条形码获取水表编号。",
                [$"{2}.如何充值?"] = $"如果该微信账户的小程序绑定过水表，再次点击进去，就可以进行充值操作。",
                [$"{3}.查看该微信账户的充值记录?"] = $"点击充值记录，可以看到该微信账户的充值记录。",
                [$"{4}.如何解除绑定?"] = $"点击解绑账户，再按操作点击解除绑定，确认，就能解除水表号与该微信账户的绑定了。",
                [$"{5}.解绑后再次绑定相同水表会怎样?"] = $"通过小程序再次绑定曾经绑定水表，则会继续看到该微信账户下该水表的相关信息，和充值历史记录。"
            };
            return dic;
        }

    }
}