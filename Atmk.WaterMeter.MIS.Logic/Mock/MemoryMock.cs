﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;

namespace Atmk.WaterMeter.MIS.Logic.Mock
{
    public class MemoryMock
    {
        /// <summary>
        /// 缓存
        /// </summary>
        public static List<PostPointMapCoordinat> _CoordinatsMock = new List<PostPointMapCoordinat>();
    }
}
