﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Logic
{
    public class LoginLogic:ILoginLogic
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IStaffRepository _staffRepository;

        public LoginLogic(
            IStaffRepository staffRepository
        )
        {
            _staffRepository = staffRepository;
        }

        public int Add(int i, int j)
        {
            return i * 10 + j;
        }

        /// <inheritdoc />
        public bool LoginMatching(string name, string password, out string staffId, out string roleId,out string areaId)
        {
            try
            {
                var staffs = _staffRepository.FindByName(name);
                staffId = "";
                roleId = "";
                areaId = "";
                foreach (var entity in staffs)
                {
                    if (!(entity is User user)) continue;
                    if (user.Password != password) continue;//密码未进行加密解密操作，后续添加 2018.8.28 xza
                    staffId = user.Id.ToString();
                    roleId = user.RoleId;
                    areaId = user.SiteId;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
          
        }

        public User LoginMatching(string name, string password)
        {
            try
            {
                return _staffRepository.FindByName(name, password);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

        }
    }
}
