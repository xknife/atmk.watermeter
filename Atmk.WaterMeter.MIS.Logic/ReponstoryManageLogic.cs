﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;
using NLog;

namespace Atmk.WaterMeter.MIS.Logic
{
    public class ReponstoryManageLogic : BaseLogic, IReponstoryManageLogic
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IRepository _repository;
        //private readonly IMeterCodeIdMapRepository _meterCodeIdMapRepository;
        private readonly IDistrictRepository _districtRepository;

        public ReponstoryManageLogic(
            IRepository repository,
            //IMeterCodeIdMapRepository meterCodeIdMapRepository,
            IDistrictRepository districtRepository
        )
        {
            _repository = repository;
            //_meterCodeIdMapRepository = meterCodeIdMapRepository;
            _districtRepository = districtRepository;
        }

        public bool AllClear()
        {
            throw new NotImplementedException();
        }

        public bool ClearRecords()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 初始化抄表记录
        /// </summary>
        /// <returns></returns>
        public bool MeterRecordClear(params string[] meterNumbers)
        {
            try
            {
                var meterReocrd = _repository.FindAll<MeterReadingRecord>()
                    //.Where(m => meterNumbers.Contains(m.Number))
                    .ToArray();
                //var resint = _repository.Remove(meterReocrd);
                //_logger.Debug($"初始化完成{meterReocrd}数据{resint}条");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool StaffClear()
        {
            throw new NotImplementedException();
        }


        ///// <summary>
        ///// 初始化所有数据
        ///// </summary>
        ///// <returns></returns>
        //public bool AllClear()
        //{
        //    try
        //    {
        //        var area = _repository.FindAll<Project>().ToArray();
        //        var district = _repository.FindAll<District>().ToArray();
        //        var owner = _repository.FindAll<Owner>().ToArray();
        //        var meter = _repository.FindAll<Meter>().ToArray();
        //        var meterCodeIdMap = _meterCodeIdMapRepository.Seletc().ToArray();
        //        var meterReocrd = _repository.FindAll<MeterReadingRecord>().ToArray();
        //        var meterFroze = _repository.FindAll<FrozenDataRecord>().ToArray();

        //        var resint = _repository.Remove(meter);
        //        _logger.Debug($"初始化完成{meter}数据{resint}条");
        //        resint = _repository.Remove(owner);
        //        _logger.Debug($"初始化完成{owner}数据{resint}条");
        //        resint = _repository.Remove(district);
        //        _logger.Debug($"初始化完成{district}数据{resint}条");
        //        resint = _meterCodeIdMapRepository.Remove(meterCodeIdMap);
        //        _logger.Debug($"初始化完成{meterCodeIdMap}数据{resint}条");
        //        resint = _repository.Remove(meterReocrd);
        //        _logger.Debug($"初始化完成{meterCodeIdMap}数据{resint}条");
        //        resint = _repository.Remove(meterFroze);
        //        _logger.Debug($"初始化完成{meterCodeIdMap}数据{resint}条");
        //        resint = _repository.Remove(area);
        //        _logger.Debug($"初始化完成{area}数据{resint}条");
        //        resint = _districtRepository.Clear();
        //        _logger.Debug($"初始化完成DistrictSequences数据{resint}条");
        //        _logger.Debug($"初始化数据完成");
        //        StaffClear();
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error(e);
        //        return false;
        //    }
        //}

        //public bool ClearRecords()
        //{
        //    try
        //    {
        //        var frozens = _repository.FindAll<FrozenDataRecord>().ToArray();
        //        var operations = _repository.FindAll<OperationRecord>().ToArray();
        //        var reading = _repository.FindAll<MeterReadingRecord>().ToArray();
        //        var payment = _repository.FindAll<AmountChangedDownRecord>().ToArray();
        //        var refills = _repository.FindAll<RefillRecord>().ToArray();
        //        var settlement = _repository.FindAll<SettlementRecord>().ToArray();
        //        var resint = _repository.Remove(frozens);
        //        _logger.Debug($"初始化完成{frozens}数据{resint}条");
        //        resint = _repository.Remove(operations);
        //        _logger.Debug($"初始化完成{operations}数据{resint}条");
        //        resint = _repository.Remove(reading);
        //        _logger.Debug($"初始化完成{reading}数据{resint}条");
        //        resint = _repository.Remove(payment);
        //        _logger.Debug($"初始化完成{payment}数据{resint}条");
        //        resint = _repository.Remove(refills);
        //        _logger.Debug($"初始化完成{refills}数据{resint}条");
        //        resint = _repository.Remove(settlement);
        //        _logger.Debug($"初始化完成{settlement}数据{resint}条");
        //        _logger.Debug($"初始化记录完成");
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error(e);
        //        return false;
        //    }
        //}

        /// <summary>
        /// 初始化管理员
        /// </summary>
        ///// <returns></returns>
        //public bool StaffClear()
        //{
        //    _logger.Debug("初始化管理员开始");
        //    var role = new Role("超级管理员")
        //    {
        //        Parmissions = ""
        //    };
        //    var role2 = new Role("管理员")
        //    {
        //        Parmissions = ""
        //    };
        //    role.ModifiedLog("system", "初始化超级管理员");
        //    var staff = new Staff("admin")
        //    {
        //        Surname = "管理员",
        //        Password = "123456",
        //        Mobile = "15201320283",
        //        PageLines = 20,
        //        CreateTime = DateTime.Now,
        //        RoleId = role.Id.ToString(),
        //    };
        //    staff.ModifiedLog("system", "初始化用户信息");

        //    var staffEntities = _repository.FindAll<Staff>();
        //    var rolesEntities = _repository.FindAll<Role>();
        //    _repository.Remove(staffEntities.ToArray());
        //    _repository.Remove(rolesEntities.ToArray());
        //    _repository.Add(role);
        //    _repository.Add(role2);
        //    _repository.Add(staff);
        //    _logger.Debug("初始化管理员完成");
        //    return true;
        //}
    }
}