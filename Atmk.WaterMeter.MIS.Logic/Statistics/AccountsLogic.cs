﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using NLog;

namespace Atmk.WaterMeter.MIS.Logic.Statistics
{
    public class AccountsLogic : IAccountsLogic
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IOwnerRepository _ownerRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IProjectConfigurationRepository _projectConfigurationRepository;

        public AccountsLogic(
            IOwnerRepository ownerRepository,
            IAccountRepository accountRepository,
            IProjectConfigurationRepository projectConfigurationRepository
        )
        {
            _ownerRepository = ownerRepository;
            _accountRepository = accountRepository;
            _projectConfigurationRepository = projectConfigurationRepository;
        }

        public CostReminderData GetCostReminderItemList(params string[] leafDistrictIds)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取费用提醒列表
        /// </summary>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        //public CostReminderData GetCostReminderItemList(params string[] leafDistrictIds)
        //{
        //    //TODO:添加费用提醒额度参数--暂时默认30
        //    decimal costLine = 30;
        //    //获取所有片区下业主信息
        //    var owners = _ownerRepository.GetOwners(leafDistrictIds);
        //    //获取业主账户信息
        //    var accounts = _accountRepository.GetAccounts(owners.Select(o => o.Id.ToString()).ToArray());

        //    var list = accounts
        //        .Where(a => a.Balance <= costLine)
        //        .Select(a => new
        //        {
        //            a,
        //            o = owners.First(n => n.Id.ToString() == a.OwnerId)
        //        }).Select(ao => new CostReminderItem
        //        {
        //            accountId = ao.a.Id.ToString(),
        //            balance = Convert.ToDouble(Math.Round(ao.a.Balance, 4)),
        //            ownerId = ao.o.Id.ToString(),
        //            ownerName = ao.o.Name
        //        }).ToList();

        //    return new CostReminderData {total = list.Count.ToString(), list = list};
        //}
    }
}