﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using NLog;

namespace Atmk.WaterMeter.MIS.Logic.Statistics
{
    public class WaterMetersLogic : IWaterMetersLogic
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IOwnerRepository _ownerRepository;
        private readonly IMeterRepository _meterRepository;
        private readonly IMeterReadingRepository _meterReadingRepository;
        private readonly IDistrictRepository _districtRepository;

        public WaterMetersLogic(
            IOwnerRepository ownerRepository,
            IMeterRepository meterRepository,
            IMeterReadingRepository meterReadingRepository,
            IDistrictRepository districtRepository
        )
        {
            _ownerRepository = ownerRepository;
            _meterRepository = meterRepository;
            _meterReadingRepository = meterReadingRepository;
            _districtRepository = districtRepository;
        }
        /// <summary>
        /// 按片区抄回率数据
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="leafDistrictIds"></param>
        /// <returns></returns>
        public DistrictReadRatioData GetDistrictReadRatioItems(DateTime dateTime, params string[] leafDistrictIds)
        {
            //获取片区
            var districts = _districtRepository.GetDistrictsById(leafDistrictIds);
            //获取业主
            var owners = _ownerRepository.GetOwners(leafDistrictIds);
            //获取所有水表
            var meters = _meterRepository.GetMetersByOwnerIds(owners.Select(o => o.Id.ToString()).ToArray());
            //获取该日期收到的最新抄表数据
            var meterReading =
                _meterReadingRepository.NowDataMeterReadingRecords(meters.Select(m => m.MeterNumber).ToList(),
                    dateTime);
            var result = new DistrictReadRatioData();
            foreach (var dis in districts)
            {
                var ownerIds = owners.Where(o => o.DistrictId == dis.Id.ToString()).Select(o => o.Id.ToString());
                var meterNumbers = meters.Where(m => ownerIds.Contains(m.OwnerId)).Select(m=>m.MeterNumber).ToList();
                var meterCount = meterNumbers.Count();
                var readingCount = meterReading.Count(r => meterNumbers.Contains(r.CtdeviceId));
                result.list.Add(new DistrictReadRatioItem
                {
                    districtId = dis.Id.ToString(),
                    districtName = dis.Name,
                    readRatio = readingCount!=0?readingCount * 100.00 / meterCount:readingCount * 100.00
                });
            }
            result.total = result.list.Count.ToString();
            return result;
        }

        /// <summary>
        /// 获取传入片区下，所有水表的数量
        /// </summary>
        /// <param name="districtIds"></param>
        /// <returns></returns>
        public int GetMeterCount(params string[] districtIds)
        {
            throw new NotImplementedException();
        }

      

    }
}
