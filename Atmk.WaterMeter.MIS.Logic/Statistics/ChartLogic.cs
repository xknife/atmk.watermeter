﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Enums;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Chart;
using Atmk.WaterMeter.MIS.Entities.Enums;
using NLog;

namespace Atmk.WaterMeter.MIS.Logic.Statistics
{
    public class ChartLogic 
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IWaterConsumptionLogic _waterConsumptionLogic;
        private readonly IAccountsLogic _accountsLogic;
        private readonly IOwnersLogic _ownersLogic;
        private readonly IRegionLogic _regionLogic;
        private readonly IWaterMetersLogic _waterMetersLogic;
        private readonly IStatisticHistoryLogic _statisticHistoryLogic;

        public ChartLogic(
            IWaterConsumptionLogic waterConsumptionLogic,
            IAccountsLogic accountsLogic,
            IOwnersLogic ownersLogic,
            IRegionLogic regionLogic,
            IWaterMetersLogic waterMetersLogic,
            IStatisticHistoryLogic statisticHistoryLogic
        )
        {
            _waterConsumptionLogic = waterConsumptionLogic;
            _accountsLogic = accountsLogic;
            _ownersLogic = ownersLogic;
            _regionLogic = regionLogic;
            _waterMetersLogic = waterMetersLogic;
            _statisticHistoryLogic = statisticHistoryLogic;
        }
        /// <summary>
        /// 同步获取
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public ChartDatas GetChart(string projectId)
        {
            return CreatHistoryChart(projectId);
            //return CreatChart(projectId);
        }

        /// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<ChartDatas> GetChartAsync(string projectId)
        {
            var task = new Task<ChartDatas>(() => GetChart(projectId));
            task.Start();
            return await task;
            //task.Start();
            //return task.Result;
        }

        private ChartDatas CreatHistoryChart(string areaId)
        {
            var datas = new ChartDatas
            {
                closingvalveData =
                    _statisticHistoryLogic.GetNewData<ClosingvalveData>(areaId, StatisticType.ClosingvalveData),
                costReminderData =
                    _statisticHistoryLogic.GetNewData<CostReminderData>(areaId, StatisticType.CostReminderData),
                statisticsInfoData =
                    _statisticHistoryLogic.GetNewData<StatisticsInfoData>(areaId, StatisticType.StatisticsInfoData),
                waterConsumptionInfoData =
                    _statisticHistoryLogic.GetNewData<WaterConsumptionInfoData>(areaId,
                        StatisticType.WaterConsumptionInfoData),
                districtReadRatioData =
                    _statisticHistoryLogic.GetNewData<DistrictReadRatioData>(areaId,
                        StatisticType.DistrictReadRatioData),
                yesterdayConsumptionData =
                    _statisticHistoryLogic.GetNewData<YesterdayConsumptionData>(areaId,
                        StatisticType.YesterdayConsumptionData),
                lastMonthDistrictConsumptionsCountData =
                    _statisticHistoryLogic.GetNewData<WaterConsumptionsCountData>(areaId,
                        StatisticType.LastMonthDistrictConsumptionsCountData),
                lastSeasonWaterConsumptionsCountData =
                    _statisticHistoryLogic.GetNewData<WaterConsumptionsCountData>(areaId,
                        StatisticType.LastSeasonWaterConsumptionsCountData),
                lastYearWaterConsumptionsCountData =
                    _statisticHistoryLogic.GetNewData<YearWaterConsumptionsCountData>(areaId,
                        StatisticType.LastYearWaterConsumptionsCountData)
            };
            return datas;
        }

        /// <summary>
        /// 拼接返回的数据统计
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        private ChartDatas CreatChart(string areaId)
        {
            var lealDistricts = _regionLogic.GetLeafDistrictEntities(areaId).Select(f => f.Id.ToString()).ToArray();
            var datas = new ChartDatas
            {
                closingvalveData = _ownersLogic.GetCloseClosingValveOwnerItems(lealDistricts),
                costReminderData = _accountsLogic.GetCostReminderItemList(lealDistricts),
                districtReadRatioData =
                    _waterMetersLogic.GetDistrictReadRatioItems(DateTime.Now.AddDays(-1), lealDistricts),
                lastMonthDistrictConsumptionsCountData =
                    _waterConsumptionLogic.GetDistrictPriceStepWaterConsumption(Period.Month,
                        DateTime.Today.AddMonths(-1), lealDistricts),
                lastSeasonWaterConsumptionsCountData =
                    _waterConsumptionLogic.GetDistrictMonthWatherConsumption(Period.Quarter,
                        DateTimeHelper.GetQuarter(1, false), lealDistricts),
                lastYearWaterConsumptionsCountData =
                    _waterConsumptionLogic.GetYearWaterConsumptions(Period.Year,
                        DateTime.Today.AddYears(-1), lealDistricts),
                statisticsInfoData = _regionLogic.GetStatisticsInfoData(areaId),
                waterConsumptionInfoData = _waterConsumptionLogic.GetWaterConsumptionInfoData(areaId),
                //yesterdayConsumptionData = _waterConsumptionLogic.
            };
            return datas;
        }
    }
}
