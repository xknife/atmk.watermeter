﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;
using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Logic
{
    public class EditCoordinatesLogic : IEditCoordinatesLogic, ISelectCoordinatesLogic
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IMapRepository _mapRepository;
        private readonly IDistrictRepository _districtRepository;

        public EditCoordinatesLogic(
            IMapRepository mapRepository,
            IDistrictRepository districtRepository
        )
        {
            _districtRepository = districtRepository;
            _mapRepository = mapRepository;
        }

        #region 编辑

        /// <summary>
        /// 添加片区坐标集合
        /// </summary>
        /// <param name="coordinates"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool InsertDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId)
        {
            var list = coordinates.Select(c => new MapParameter
            {
                NodeId = nodeId,
                NodeType = (int)CoordinateType.District,
                Elevation = 0,
                Latitude = c.lat,
                Longitude = c.lng
            }).ToList();
            for (var i = 0; i < list.Count(); i++)
            {
                list[i].Sequence = i;
            }
            return _mapRepository.AddRang(list.ToArray()) > 0;
        }

        /// <summary>
        /// 修改片区坐标集合
        /// </summary>
        /// <param name="mapCoordinat"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool UpdateDistrictCoordinates(List<MapCoordinat> coordinates, string nodeId)
        {
            _mapRepository.DeleteTrue(nodeId);
            return InsertDistrictCoordinates(coordinates, nodeId);
        }

        /// <summary>
        /// 删除坐标点
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool DeleteDistrictCoordinates(string nodeId)
        {
            return _mapRepository.DeleteTrue(nodeId) > 0;
        }

        /// <summary>
        /// 添加坐标点
        /// </summary>
        /// <param name="postPoint"></param>
        /// <returns></returns>
        public bool InsertPoint(MapCoordinat mapCoordinat, string nodeId, CoordinateType coordinateType)
        {
            var mapParameter = new MapParameter
            {
                NodeId = nodeId,
                Latitude = mapCoordinat.lat,
                Longitude = mapCoordinat.lng,
                Elevation = 0,
                Radius = 1,
                NodeType = (int)coordinateType
            };
            return _mapRepository.Add(mapParameter) > 0;
        }

        /// <summary>
        /// 修改坐标点
        /// </summary>
        /// <param name="mapCoordinat"></param>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool UpdatePoint(MapCoordinat mapCoordinat, string nodeId)
        {
            var map = _mapRepository.FirstCoordinatePoint(nodeId);
            map.Latitude = mapCoordinat.lat;
            map.Longitude = mapCoordinat.lng;
            return _mapRepository.Update(map) > 0;
        }

        /// <summary>
        /// 删除坐标点
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool DeletePoint(string nodeId)
        {
            return _mapRepository.DeleteTrue(nodeId) > 0;
        }

        #endregion

        #region 查询

        /// <summary>
        /// 根据坐标点关联的节点id(Project、District、业主的Id)获取坐标点
        /// </summary>
        /// <returns></returns>
        public PostPointMapCoordinat SelectPointMapCoordinat(string nodeid)
        {
            var res = _mapRepository.FirstCoordinatePoint(nodeid);
            return new PostPointMapCoordinat
            {
                nodeId = nodeid,
                coordinates = new MapCoordinat {lat = res.Latitude, lng = res.Longitude},
                nodeType = (CoordinateType)res.NodeType
            };
        }

        /// <summary>
        /// 获取片区或项目id下的业主坐标点集合
        /// </summary>
        /// <param name="id"></param>
        /// <param name="coordinateType"></param>
        /// <returns></returns>
        public List<PostPointMapCoordinat> SelectPointMapCoordinat(string id, CoordinateType coordinateType)
        {
            //var list = _mapRepository.CoordinatesPoints(id, coordinateType);
            //return list.Select(m => new PostPointMapCoordinat
            //{
            //    nodeId = m.NodeId,
            //    coordinates = new MapCoordinat {lat = m.Latitude, lng = m.Longitude},
            //    nodeType = m.NodeType
            //}).ToList();
           return new List<PostPointMapCoordinat>();
        }

        /// <summary>
        /// 获取项目Id下的片区坐标集合
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="districtId"></param>
        /// <returns></returns>
        public List<GetDistrictCoordinates> SelectDistrictMapCoordinat(string areaId, string districtId = "")
        {
            var district = _districtRepository.GetDistrictEntities(areaId).ToList();
            if (districtId.Length > 0)
                district = district.Where(d => d.Id == districtId).ToList();
            //var MapDistrictId = _mapRepository.
            var result = new List<GetDistrictCoordinates>();
            district.ForEach(d =>
                {
                    var list = GetCoordinate(d.Id);
                    if(list.Count>0)
                        result.Add(
                            new GetDistrictCoordinates
                            {
                                coordinates=list,
                                districtId=d.Id,
                                districtName=d.Name
                            });
                }
            );
            return result;
        }

        /// <summary>
        /// 获取片区坐标集合
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        public List<MapCoordinat> GetCoordinate(string districtId)
        {
            var list = _mapRepository.CoordinatesPoints(districtId, CoordinateType.District).OrderBy(m=>m.Sequence);
            return list.Select(m => new MapCoordinat
            {
                lat = m.Latitude,
                lng = m.Longitude
            }).ToList();
        }

        #endregion
    }
}