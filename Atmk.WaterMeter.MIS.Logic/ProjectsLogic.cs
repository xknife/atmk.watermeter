﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Entities.Models;

namespace Atmk.WaterMeter.MIS.Logic
{
    /// <summary>
    /// 各个水表项目名称的信息管理逻辑
    /// </summary>
    public class ProjectsLogic : IProjectsLogic
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IAreaRepository _areaRepository;

        public ProjectsLogic(
            IAreaRepository areaRepository
        )
        {
            _areaRepository = areaRepository;
        }

        /// <inheritdoc />
        public Project GetProject(Guid areaId)
        {
            return _areaRepository.GetAreaEntities(areaId.ToId());
        }
    }
}