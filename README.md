# Atmk.WaterMeter.MIS

#### 项目介绍
奥特美克户用水表管理信息系统，该系统是奥特美克公司户用水表项目的软件子项目。本系统通过LoRa、GPRS等通讯方式与公司的智能水表进行通讯，远程控制水表进行阀门控制。并可以自动查抄水表上的记录信息，并通过内部算法进行费用结算，通过系统里各个水表业主的账号情况，进行计费扣费等水费收缴操作，或通过用户对业主进行账户更改。还有各种数据记录的查询、分析功能，便于用户统计水表使用情况，以及业主消费情况等。并以图表形式直观体现出来，便于用户整理。
#### 软件项目结构说明
抄表系统整体采用前后端分离的BS结构，所采用的技术也均是时下流行的免费开发工具和开源的框架技术，即降低成本又有利于后续开发、升级和维护。
后端应用服务端，采用的是使用基于.NET Core开源框架的C#进行研发的可跨平台部署的应用服务系统。.NET Core的优点是可跨平台、具有微服务特性和可扩展性。后期还可跟据需求通过开源包实现分布式服务。
前端WebSite开发采用的是vue.js，是具有MVVM模式的js框架。其核心优点是数据驱动和组件化。且方便与服务端的api接口对接。
整套系统部署在Lixnux系统Ubuntu下，并通过Nginx服务器进行反向代理https网址和守护前端网站进程，通过Supervisor守护进程保证后端应用服务运行。数据库则采用可跨平台的，免费的开源数据库MySQL进行数据处理。
#### 软件流程图
![软件示意图](https://images.gitee.com/uploads/images/2019/0516/095250_b58b6ef7_496336.png "图片1.png")
#### 各层项目说明
1. **Documents** 文档存放
2. **Tests** 单元测试
3. **Atmk.WaterMeter.MIS** 应用服务层,软件项目启动项,该项目归属于应用层View，是抄表应用服务系统对外接口部分。具有restful结构的WebApi，依赖项Kernel项目
4. **Atmk.WaterMeter.MIS.Commons**  公共工具类库，所有系统内用到的开发的公共资源放入这里。一期里该项目是Commons与Helper合并的，依赖项是Entities项目和GateWay.CT_NBIoT.ViewModel项目。
5. **Atmk.WaterMeter.MIS.Kernel**  内核管理项目,因为改层项目负责对所有项目需要被使用的对象进行依赖注入配置，因此依赖项是除了View层以外的其他项目。依赖注入功能采用的是开源开发包Autofac。
6. **Atmk.WaterMeter.MIS.Commons.Datas**  公共工具类库.数据库连接管理类库
7. **Atmk.WaterMeter.MIS.Services.DataAccess**  数据库操作服务类库,数据中心里的DAL,由应用服务层调用
8. **Atmk.WaterMeter.MIS.Logic** 该项目是逻辑处理层，用于处理业务逻辑。依赖项是Commons
9. **Atmk.WaterMeter.MIS.Services.Auth** 该项目属于业务逻辑层中的鉴权验证，进行访问鉴权验证的层。依赖项是Commons
10. **Atmk.WaterMeter.MIS.GateWay.CT_NBIoT** 电信NB平台通讯接口，处理与电信平台接口对接，属于应用层的第三方接口。依赖项是Commons
11. **Atmk.WaterMeter.MIS.Pay.WeiXin** 微信支付业务层，用于处理微信支付业务的功能，属于应用层的第三方接口。依赖项是Commons
12. **Atmk.WaterMeter.MIS.TimedTask** 定时运行任务项目，属于任务执行层Task里，用于应用服务在运行过程中，定时自动处理的业务逻辑层。依赖项是Commons
13. **Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel"** 电信平台接口对接需要的ViewModel层。 依赖项Entities项目
14. **Atmk.WaterMeter.MIS.Entities"** 抄表系统使用的实体Model层，因系统的ORM采取core first方式设计，其实体对象与数据库表一一对应，亦可理解为数据库表的对象代码化。因对领域模型和数据库表实体定义混淆了，所以一期将实体Model层代替了Domain Model层被使用，计划二期修正。无依赖项
