﻿using Atmk.WaterMeter.MIS.Commons.IoC;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.IoC;
using Atmk.WaterMeter.MIS.Logic.IoC;
using Atmk.WaterMeter.MIS.Pay.WeiXin.IoC;
using Atmk.WaterMeter.MIS.Services.Auth.IoC;
using Atmk.WaterMeter.MIS.Services.DataAccess.IoC;
using Atmk.WaterMeter.MIS.TimedTask.Ioc;
using Autofac;

namespace Atmk.WaterMeter.MIS.Kernel
{
    public class IoCManager
    {
        #region Instance
        private static readonly IoCManager _Manager = new IoCManager();
        private IoCManager()
        {
        }
        public static IoCManager Instance => _Manager;
        #endregion

        public void Register(ContainerBuilder builder)
        {
            builder.RegisterModule(new CommonModule());
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterModule(new LogicModule());
            builder.RegisterModule(new CT_NBIoTModule());
            builder.RegisterModule(new AuthModule());
            builder.RegisterModule(new TimedTaskModule());
            builder.RegisterModule(new WeiXinModule());
        }
    }
}
