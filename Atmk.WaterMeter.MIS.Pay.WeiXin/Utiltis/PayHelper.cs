﻿using System;
using System.Security.Cryptography;
using Atmk.WaterMeter.MIS.Commons.Utils;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Utiltis
{
    public class PayHelper
    {
        /// <summary>
        /// MD5方式的微信支付签名生成
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="nonceStr"></param>
        /// <param name="prepay_id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string PaySignMD5(string appId, string nonceStr, string prepay_id,string key)
        {
            var timeStamp = DateTime.Now.TimeStamp();

            var temp =
                $"appId={appId}&nonceStr={nonceStr}&package=prepay_id={prepay_id}&signType=MD5&timeStamp={timeStamp}&key={key}";
            var md5 = MD5Encrypt.Encrypt(temp);
            return md5;
        }
    }
}
