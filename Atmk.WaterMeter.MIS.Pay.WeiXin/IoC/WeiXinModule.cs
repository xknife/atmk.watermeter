﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin;
using Atmk.WaterMeter.MIS.Pay.WeiXin.Logic;
using Autofac;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.IoC
{
    public class WeiXinModule: Module
    {
        public static bool IsDevPhase { get; set; } = true;

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<WxLogin>().As<IWxLogin>();
            builder.RegisterType<WxPaymentLogic>().As<IWxPayment>();
        }
    }
}
