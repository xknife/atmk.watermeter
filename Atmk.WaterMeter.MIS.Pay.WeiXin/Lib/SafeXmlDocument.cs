﻿using System.Xml;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Lib
{
    public sealed class SafeXmlDocument:XmlDocument
    {
        public SafeXmlDocument()
        {
            this.XmlResolver = null;
        }
    }
}
