﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Lib
{
    /// <summary>
    /// 微信支付调用的配置文件
    /// </summary>
    public sealed class WxPayConfig
    {
        //配置参数
        public string AppId { get; }
        public string OpenId { get; }
        public string MchId { get; }
        public string CallBackUrl { get; }
        public string MisKey { get; }
        public string Ipv4 { get; set; }
        public WxPayConfig(string adppid, string openId, string mchId, string callBackUrl, string key, string ipv4)
        {
            AppId = adppid;
            OpenId = openId;
            MchId = mchId;
            CallBackUrl = callBackUrl;
            MisKey = key;
            Ipv4 = ipv4;
        }
    }
}
