﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Lib
{
    public static class WxPayDataEx
    {
        public static ResponUnifiedorder ToResponUnifiedorder(this WxPayData wxPayData)
        {
            var result = new ResponUnifiedorder();
            if(wxPayData.GetValue("return_code").ToString()== "FAIL")
                throw new WxPayException(wxPayData.GetValue("return_msg").ToString());
            //返回值处理
            if (!wxPayData.IsSet("appid") || !wxPayData.IsSet("prepay_id") || wxPayData.GetValue("prepay_id").ToString() == "")
            {
                throw new WxPayException("缺少appid或prepay_id");
            }
            foreach (var property in result.GetType().GetProperties())
            {
                property.SetValue(result, GetValueOrEmpty(wxPayData, property.Name));
            }
            return result;
        }

        private static string GetValueOrEmpty(WxPayData wxPayData, string key)
        {
            return wxPayData.IsSet(key) ? wxPayData.GetValue(key).ToString() : string.Empty;
        }
    }
}