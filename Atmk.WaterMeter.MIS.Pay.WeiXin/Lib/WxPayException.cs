﻿using System;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Lib
{
    public class WxPayException : Exception 
    {
        public WxPayException(string msg) : base(msg) 
        {

        }
     }
}