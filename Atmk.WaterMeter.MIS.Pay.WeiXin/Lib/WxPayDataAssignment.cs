﻿using System;
using NLog;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin.Lib
{
    /// <summary>
    /// 对WxPayData进行赋值和值处理的类
    /// </summary>
    public class WxPayDataAssignment
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 输出统一下单的表单WxPayData      
        /// 统一下单的赋值处理 赋值及校验配置   
        /// </summary>
        /// <param name="inputObj"> 提交给统一下单API的参数</param>
        /// <param name="wxConfig">微信服务配置</param>
        /// <returns>成功时返回赋值处理后的WxPayData</returns>
        public static WxPayData OutUnifiedOrder(WxPayData inputObj,WxPayConfig wxConfig)
        {
            //检测必填参数
            if (!inputObj.IsSet("out_trade_no"))
            {
                throw new WxPayException("缺少统一支付接口必填参数out_trade_no！");
            }
            if (!inputObj.IsSet("body"))
            {
                throw new WxPayException("缺少统一支付接口必填参数body！");
            }
            if (!inputObj.IsSet("total_fee"))
            {
                throw new WxPayException("缺少统一支付接口必填参数total_fee！");
            }
            if (!inputObj.IsSet("trade_type"))
            {
                throw new WxPayException("缺少统一支付接口必填参数trade_type！");
            }

            //关联参数
            if (inputObj.GetValue("trade_type").ToString() == "JSAPI" && !inputObj.IsSet("openid"))
            {
                throw new WxPayException("统一支付接口中，缺少必填参数openid！trade_type为JSAPI时，openid为必填参数！");
            }
            if (inputObj.GetValue("trade_type").ToString() == "NATIVE" && !inputObj.IsSet("product_id"))
            {
                throw new WxPayException("统一支付接口中，缺少必填参数product_id！trade_type为JSAPI时，product_id为必填参数！");
            }

            //异步通知url未设置，则使用配置文件中的url
            if (!inputObj.IsSet("notify_url"))
            {
                inputObj.SetValue("notify_url", wxConfig.CallBackUrl);//异步通知url
            }

            inputObj.SetValue("appid", wxConfig.AppId);//公众账号ID
            inputObj.SetValue("mch_id", wxConfig.MchId);//商户号
            inputObj.SetValue("spbill_create_ip", wxConfig.Ipv4);//终端ip	  	    
            inputObj.SetValue("nonce_str", GenerateNonceStr());//随机字符串
            inputObj.SetValue("sign_type", WxPayData.SIGN_TYPE_MD5);//签名类型

            //签名
            inputObj.SetValue("sign", inputObj.MakeSign(wxConfig.MisKey));
           
            return inputObj;
        }


        /**
        * 根据当前系统时间加随机序列来生成订单号
         * @return 订单号
        */
        public static string GenerateOutTradeNo(string mch_id)
        {
            var ran = new Random();
            return $"{mch_id}{DateTime.Now:yyyyMMddHHmmss}{ran.Next(999)}";
        }

        /**
        * 生成时间戳，标准北京时间，时区为东八区，自1970年1月1日 0点0分0秒以来的秒数
         * @return 时间戳
        */
        public static string GenerateTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /**
        * 生成随机串，随机串包含字母或数字
        * @return 随机串
        */
        public static string GenerateNonceStr()
        {
            return Guid.NewGuid().ToId().Replace("-", "").ToUpper();
            //RandomGenerator randomGenerator = new RandomGenerator();
            //return randomGenerator.GetRandomUInt().ToString();
        }
    }
}