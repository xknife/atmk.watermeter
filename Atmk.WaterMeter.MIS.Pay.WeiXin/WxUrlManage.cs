﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.Pay.WeiXin
{
    public class WxUrlManage
    {
        /// <summary>
        /// 微信服务url
        /// </summary>
        public const string Urlcode2Session = "https://api.weixin.qq.com/";
        /// <summary>
        /// 微信支付url
        /// </summary>
        public const string Urlunifiedorder = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        /// <summary>
        /// 微信支付url
        /// </summary>
        public const string UrlunifiedorderDemo = "https://www.firstdomainbyxiang.info:15000/api/v1/pay/wx/pay.action";
    }
}
