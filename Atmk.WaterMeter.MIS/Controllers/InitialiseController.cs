﻿using System;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Base;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Controllers
{
    /// <inheritdoc />
    //[Route("api/System/[controller]")]
    [Route("api/[controller]")]
    public class InitialiseController : BaseApiController
    {
        private static ILogger<InitialiseController> _logger;
        private readonly IReponstoryManageLogic _reponstoryManageLogic;

        /// <inheritdoc />
        public InitialiseController(
            ILogger<InitialiseController> logger,
            IReponstoryManageLogic reponstoryManageLogic)
        {
            _logger = logger;
            _reponstoryManageLogic = reponstoryManageLogic;
        }

        /// <summary>
        ///     初始化管理员用户
        /// </summary>
        /// <returns></returns>
        [HttpGet("/AdminData")]
        public async Task<IActionResult> InitialiseAdmin()
        {
            try
            {
             
                _reponstoryManageLogic.StaffClear();
                return Ok(new {errcode = 0, msg = "添加成功"});
            }
            catch (Exception e)
            {
                return Ok(new {errcode = 1, msg = $"初始化失败:{e.Message}"});
            }
        }
        /// <summary>
        /// 清空记录
        /// </summary>
        /// <returns></returns>
        [HttpDelete("/Record")]
        public async Task<IActionResult> InitialiseRecoreds()
        {
            try
            {

                _reponstoryManageLogic.ClearRecords();
                return Ok(new { errcode = 0, msg = "初始化记录成功" });
            }
            catch (Exception e)
            {
                return Ok(new { errcode = 1, msg = $"初始化失败:{e.Message}" });
            }
        }
        /// <summary>
        ///     初始化数据
        /// </summary>
        /// <returns></returns>
        [HttpDelete("/Data")]
        public IActionResult InitialiseFastData()
        {
            try
            {
                _logger.LogDebug("开始初始化数据");
              _reponstoryManageLogic.AllClear();
                return Ok(new {errcode = 0, msg = "初始化成功" });
            }
            catch (Exception e)
            {
                return Ok(new {errcode = 1, msg = $"初始化失败:{e.Message}"});
            }
        }
    }
}