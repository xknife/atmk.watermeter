﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Base;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Controllers
{
    /// <summary>
    /// 阶梯价格接口
    /// </summary>
    [Authorize]
    public class PriceStepController : BaseApiController
    {
        private static ILogger<ParamentsController> _logger;
        private readonly TokenHelper _tokenHelper;
        private readonly IPriceStepLogic _priceStepLogic;


        /// <inheritdoc />
        public PriceStepController(
            ILogger<ParamentsController> logger,
            IPriceStepLogic priceStepLogic,
            TokenHelper tokenHelper)
        {
            _logger = logger;
            _tokenHelper = tokenHelper;
            _priceStepLogic = priceStepLogic;
        }
        /// <summary>
        /// 查询价格
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/PriceStep/{projectId}")]
        public IActionResult PriceStepGet(string projectId)
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();

                try
                {
                    return Ok(
                        new
                        {
                            errcode = 0,
                            msg = "ok",
                            data = _priceStepLogic.SelectPriceFee(projectId)
                        });
                }
                catch (ArgumentException e)
                {
                    _logger.LogError(e.Message, e);
                    return Ok(new
                    {
                        errcode = 1,
                        msg = $"传入的项目id错误"
                    });
                }
            
               
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取参数数据错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 查询价格
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/MeterType")]
        public IActionResult PriceStepGet([FromBody]dynamic obj)
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                var projectId = obj.projectId.ToString();
                string meterType = obj.meterType;
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _priceStepLogic.SelectPriceFee(projectId, meterType)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取参数数据错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 添加价格
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/Price")]
        public IActionResult PriceStepPost([FromBody]dynamic obj)
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                var projectId = obj.projectId.ToString();
                var priceName = obj.title.ToString();
                if (_priceStepLogic.SameName(priceName,string.Empty,projectId))
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加价格失败,价格名称重复"
                        });
                }
                var result = _priceStepLogic.InsertPrice(obj,projectId);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加价格失败"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"添加价格错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 修改价格
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/PricePut")]
        public IActionResult PriceStepPut([FromBody] dynamic obj)
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                var result = _priceStepLogic.UpdatePrice(obj);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改价格失败"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改价格错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 删除价格
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/PriceDelete")]
        public IActionResult DeleteRoles([FromBody] dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                var result = _priceStepLogic.DeletePrice(id);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"删除价格失败"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"删除价格数据错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 添加费用
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/Fee")]
        public IActionResult PricefeePost([FromBody]dynamic obj)
        {
            try
            {
                var result = _priceStepLogic.Insertfee(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加费用错误, PriceStepLogic.Insertfee:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"添加费用错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 修改费用
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/FeePut")]
        public IActionResult PricefeePut([FromBody] dynamic obj)
        {
            try
            {
                var result = _priceStepLogic.Updatefee(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改费用错误,PriceStepLogic.Updatefee:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改费用错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 删除费用
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/PriceStep/FeeDelete")]
        public IActionResult Deletefee([FromBody] dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                var result = _priceStepLogic.Deletefee(id, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"删除费用数据错误,PriceStepLogic.DeletePrice:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"删除费用数据错误,错误:{e.Message}"
                });
            }

        }
    }
}
