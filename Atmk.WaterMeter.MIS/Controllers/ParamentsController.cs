﻿using System;
using Atmk.WaterMeter.MIS.Base;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Controllers
{
    /// <summary>
    /// 项目参数配置
    /// </summary>
    [Authorize]
    public class ParamentsController:BaseApiController
    {
        private static ILogger<ParamentsController> _logger;
        private readonly TokenHelper _tokenHelper;
        private readonly IMeterModelLogic _meterModelLogic;
        private readonly ISystemParamentsLogic _systemParamentsLogic;
        private readonly IUserCenterLogic _userCenterLogic;
        private readonly IPriceStepLogic _priceStepLogic;

        /// <inheritdoc />
        public ParamentsController(
            ILogger<ParamentsController> logger,
            IMeterModelLogic meterModelLogic,
            ISystemParamentsLogic systemParamentsLogic,
            IUserCenterLogic userCenterLogic,
            IPriceStepLogic priceStepLogic,
            TokenHelper tokenHelper)
        {
            _logger = logger;
            _meterModelLogic = meterModelLogic;
            _systemParamentsLogic = systemParamentsLogic;
            _tokenHelper = tokenHelper;
            _userCenterLogic = userCenterLogic;
            _priceStepLogic = priceStepLogic;
        }

        /// <summary>
        /// 查询项目初始参数
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/IniParam/{projectId}")]
        public IActionResult InitialParamentGet(string projectId)
        {
            try
            {
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _systemParamentsLogic.InitialParamentSelect(projectId)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取初始参数失败"
                });
            }

        }
        /// <summary>
        /// 修改项目初始参数
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/IniParam")]
        public IActionResult InitialParamentPut([FromBody] dynamic obj)
        {
            try
            {
                var pidGuid = obj.projectId.ToString();
                var result = _systemParamentsLogic.InitialParamentUpdate(pidGuid, obj, out string warn); 
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改信息错误:{warn}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改角色信息错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 查询项目参数
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/SysParam/{projectId}")]
        public IActionResult SysParamentGet(string projectId)
        {
            try
            {
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _systemParamentsLogic.SystemParamentSelect(projectId)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取参数数据错误,错误:{e.Message}"
                });
            }

        }
        
        /// <summary>
        /// 修改项目参数
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/SysParam")]
        public IActionResult SysParamentPut([FromBody] dynamic obj)
        {
            try
            {
                var pidGuid = obj.projectId.ToString();
                var result = _systemParamentsLogic.SystemParamentUpdate(pidGuid,obj,out string warn);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改系统参数错误{warn}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改系统参数错误,错误:{e.Message}"
                });
            }
        }

        /*
        /// <summary>
        /// 查询基础参数
        /// </summary>

        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/SysBaseData")]
        public IActionResult SysBaseDataGet()
        {
            try
            {
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _systemParamentsLogic.SysBaseDataSelect()
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取基础参数错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 查询基础参数
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/SysBaseData/GateName")]
        public IActionResult SysBaseDataGet([FromBody]dynamic obj)
        {
            try
            {
                var gateName = obj.name.ToString();
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _systemParamentsLogic.SysBaseDataSelect(gateName)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取基础参数错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 添加基础项目
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/SysBaseData")]
        public IActionResult SysBaseDataPost([FromBody]dynamic obj)
        {
            try
            {
                var result = _systemParamentsLogic.AddSysBaseData(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加错误, SystemParamentsLogic.AddSysBaseData:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"添加价格错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 修改基础项目
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/SysBaseData/Update")]
        public IActionResult SysBaseDataPut([FromBody] dynamic obj)
        {
            try
            {
                var result = _systemParamentsLogic.UpdateSysBaseData(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改基础项目错误,SystemParamentsLogic.UpdateSysBaseData:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改基础项目错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 删除基础项目参数
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/SysBaseData/Delete")]
        public IActionResult SysBaseDataDelete([FromBody] dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                var result = _systemParamentsLogic.DeleteBaseData(id, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"删除基础项目参数错误,SystemParamentsLogic.DeleteBaseData:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"删除基础项目参数错误,错误:{e.Message}"
                });
            }

        }
        */

        /// <summary>
        /// 查询个人配置参数
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/IndParam")]
        public IActionResult IndParamentGet()
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _userCenterLogic.SelectLines(token.payload.id)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取行数错误,错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 修改个人参数
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/IndParam")]
        public IActionResult IndParamentPut([FromBody] dynamic obj)
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                var result = _userCenterLogic.UpdateLines(obj, token.payload.id, out string exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"{exception}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改个人参数错误,错误:{e.Message}"
                });
            }

        }
       
        /// <summary>
        /// 查询表型号
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/MeterModNum")]
        public IActionResult MeterModNumGet()
        {
            try
            {
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _meterModelLogic.SelectModNum()
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"查询表型号错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 查询表型号
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/MeterModNum/Id")]
        public IActionResult MeterModNumGet([FromBody]dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = _meterModelLogic.SelectModNum(id)
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"查询表型号错误:{e.Message}"
                });
            }
        }
        /// <summary>
        /// 添加表型号
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/MeterModNum")]
        public IActionResult MeterModNumPost([FromBody]dynamic obj)
        {
            try
            {
                var result = _meterModelLogic.AddModNum(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加表型号错误, PriceStepLogic.Insertfee:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"添加表型号错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 修改表型号
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/MeterModNum/Put")]
        public IActionResult MeterModNumPut([FromBody] dynamic obj)
        {
            try
            {
                var result = _meterModelLogic.UpdateModNum(obj, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"修改表型号错误,PriceStepLogic.Updatefee:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改表型号错误,错误:{e.Message}"
                });
            }

        }
        /// <summary>
        /// 删除表型号
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/MeterModNum/Delete")]
        public IActionResult DMeterModNumelete([FromBody] dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                var result = _meterModelLogic.DeleteModNum(id, out Exception exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"删除表型号数据错误,PriceStepLogic.DeletePrice:{exception.Message}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"删除表型号数据错误,错误:{e.Message}"
                });
            }

        }
    }
}
