﻿using System;
using System.Diagnostics;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace Atmk.WaterMeter.MIS.Controllers.Web
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly LogFileParseHelper _helper;

        public HomeController(LogFileParseHelper helper)
        {
            _helper = helper;
        }

        public IActionResult Index()
        {
            ViewData["today"] = DateTime.Now.ToLongDateString();
            return View();
        }

        public IActionResult MsLog()
        {
            ViewData["log"] = _helper.BuildSimpleAllLogTable();
            return View();
        }

        public IActionResult OwnLog()
        {
            ViewData["log"] = _helper.BuildSimpleOwnLogTable();
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
