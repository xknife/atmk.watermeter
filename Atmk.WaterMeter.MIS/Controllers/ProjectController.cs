﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Base;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Area;
using Atmk.WaterMeter.MIS.Commons.ViewModels.Result;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Controllers
{
    /// <summary>
    /// 项目管理接口
    /// </summary>
    [Authorize]
    public class ProjectController:BaseApiController
    {
        private static ILogger<ProjectController> _logger;
        private readonly TokenHelper _tokenHelper;
        private readonly IUserCenterLogic _userCenterLogic;
        private readonly ISignService _signService;

        /// <inheritdoc />
        public ProjectController(
            ISignService signService,
            ILogger<ProjectController> logger,
            IBaseInfoLogic baseInfoLogic,
            IUserCenterLogic userCenterLogic,
            TokenHelper tokenHelper)
        {
            _logger = logger;
            _userCenterLogic = userCenterLogic;
            _tokenHelper = tokenHelper;
            _signService = signService;
        }
        /// <summary>
        /// 获取所有项目
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/{X-Version}/System/Sites")]
        public IActionResult Sites()
        {
            try
            {
                var token = _tokenHelper.GetTokenByHttpContext();
                var result = _userCenterLogic.SelectAreas(token.payload.id, "");
                return Ok(new DataResult
                {
                    errcode = 0,
                    msg = "ok",
                    data = new AreaData { siteList = result }
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"{e.Message}"
                });
            }
        }

        /// <summary>
        /// 添加站点信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/Sites/")]
        public IActionResult AddSite([FromBody] dynamic obj)
        {
            try
            {
                var result = _userCenterLogic.AddSite(obj, out string exception, out string siteId);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"添加站点数据错误,_userCenterLogic.AddSite:{exception}",
                            siteId = ""
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok",
                        id = siteId
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"添加站点数据错误,错误:{e.Message}"
                });
            }
        }

        /// <summary>
        /// 修改站点信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/Sites/Update")]
        public IActionResult EditRoles([FromBody] dynamic obj)
        {
            try
            {
                var result = _userCenterLogic.UpdateSite(obj, out string exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"{exception}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"修改站点数据错误,错误:{e.Message}"
                });
            }
        }

        /// <summary>
        /// 删除站点信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("/api/{X-Version}/System/Sites/Delete")]
        public IActionResult DeleteSites([FromBody] dynamic obj)
        {
            try
            {
                var id = obj.id.ToString();
                var result = _userCenterLogic.DeleteSite(id, out string exception);
                if (!result)
                {
                    return Ok(
                        new
                        {
                            errcode = 1,
                            msg = $"{exception}"
                        });
                }
                return Ok(
                    new
                    {
                        errcode = 0,
                        msg = "ok"
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"删除站点信息错误,错误:{e.Message}"
                });
            }
        }
    }
}
