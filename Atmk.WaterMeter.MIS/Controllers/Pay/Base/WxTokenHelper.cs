﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Services.Auth;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Atmk.WaterMeter.MIS.Controllers.Pay.Base
{
    /// <summary>
    /// 微信token获取方法
    /// </summary>
    public class WxTokenHelper
    {
        /// <summary>
        /// 解析HttpContext携带的Token编码，并返回Token实体 
        /// </summary>
        private WxSrToken GetTokenByHttpContext(ActionExecutingContext context)
        {
            //header或者query带有x-token参数
            var token = context.HttpContext.Request.Headers["X-Token"];
            token = string.IsNullOrEmpty(token) ? context.HttpContext.Request.Query["x-token"] : token;
            return string.IsNullOrEmpty(token) ? null : JwtHelper.Decode<WxSrToken>(token);
        }
    }
}
