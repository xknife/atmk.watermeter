﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Commons.ViewModels.WeiXin;

namespace Atmk.WaterMeter.MIS.Controllers.Pay.Base
{
    public static class WxConvertVm
    {
        public static WxPayDataVm ToPayDataVm(this ResponUnifiedorder unifiedorder)
        {
            var result = new WxPayDataVm
            {
                appId = unifiedorder.appid,
                nonceStr = unifiedorder.nonce_str,
                package = $"prepay_id={unifiedorder.prepay_id}",
                signType = "MD5",
                timeStamp = DateTime.Now.TimeStamp().ToString()
            };
            var stringTemp =
                $"appId={result.appId}&nonceStr={result.nonceStr}&package={result.package}" +
                $"&signType=MD5&timeStamp={result.timeStamp}&key={WxConfigManage.GetMchIdKey("publish")}";
            result.paySign = MD5Encrypt.Encrypt(stringTemp);
            return result;
        }
    }
}