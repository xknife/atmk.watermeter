﻿namespace Atmk.WaterMeter.MIS.Controllers.Pay.Base
{
    public class WxConfigManage
    {
        private const string AppIdTest = "wx5b7cb1ed8dd04f8e";
        private const string AppSecretTest = "de4e25faa443f116242bf615b7c4c04c";

        /// <summary>
        /// 前端提供的测试平台的AppId
        /// </summary>
        private const string AppIdDemo = "wxd7ea55184e5c8d68";
        /// <summary>
        /// 前端提供的测试平台的AppSecret
        /// </summary>
        private const string AppSecretDemo = "db078b03932c8366d3a4b44df4cb2127";
        /// <summary>
        ///  前端提供的测试平台的商户号
        /// </summary>
        private const string MchIdDemo = "1266444601";
        /// <summary>
        /// 微信支付使用的服务商key值
        /// </summary>
        private const string keyDemo = "wxs35zpd3biyxb4cliime3rlypfuvtrf";

        /// <summary>
        /// 回调消息路径
        /// </summary>
        //public const string CallbackUrlDemo = "https://water-meter-demo.aixiyou.com:3385/api/v1/pay/wx/pay.action";
        public const string CallbackUrlDemo = "https://luxi.aotemeike.online/api/v1/pay/wx/pay.action";
        /// <summary>
        /// 回调消息路径
        /// </summary>
        public const string CallbackUrlTest = "https://www.firstdomainbyxiang.info:15000/api/v1/pay/wx/pay.action";
        /// <summary>
        /// 微信小程序AppId
        /// </summary>
        private const string AppId = "wx723642ec2df8e710";
        /// <summary>
        /// 微信小程序AppSecret密钥
        /// </summary>
        private const string AppSecret = "bbcb39d001c4b309c7a2e61dca4fccf4";
        /// <summary>
        /// 微信小程序支付商户号
        /// </summary>
        private const string MchId = "1502237031";
        /// <summary>
        /// 微信小程序支付商户密钥
        /// </summary>
        private const string MchIdKey = "7a5772684df575ba9193c246b7af6dcc";

        public static string GetMchId(string type)
        {
            switch (type.ToLower())
            {
                case "demo":
                    return MchIdDemo;
                case "publish":
                    return MchId;
                default:
                    return MchId;
            }
        }

        public static string GetMchIdKey(string type)
        {
            switch (type.ToLower())
            {
                case "demo":
                    return keyDemo;
                case "publish":
                    return MchIdKey;
                default:
                    return MchIdKey;
            }
        }

        /// <summary>
        /// 获取Appid
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetAppid(string type)
        {
            switch (type.ToLower())
            {
                case "demo":
                    return AppIdDemo;
                case "test":
                    return AppIdTest;
                case "publish":
                    return AppId;
                default:
                    return AppId;
            }

        }
        /// <summary>
        /// 获取AppSecret
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetAppSecret(string type)
        {
            switch (type.ToLower())
            {
                case "demo":
                    return AppSecretDemo;
                case "test":
                    return AppSecretTest;
                case "publish":
                    return AppSecret;
                default:
                    return AppSecret;
            }

        }

    }
}
