﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Services.Auth;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Atmk.WaterMeter.MIS.Controllers.Pay.Base
{
    /// <summary>
    /// 登录验证
    /// </summary>
    public class Wx_AuthorizeAttribute : Attribute, IActionFilter
    {
        /// <summary>
        /// 当Action执行后
        /// </summary>
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        /// <summary>
        /// 当Action执行时
        /// </summary>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            //var token = context.HttpContext.Request.Headers["X-Token"];
            //Console.WriteLine(token);
        }
       
    }
}
