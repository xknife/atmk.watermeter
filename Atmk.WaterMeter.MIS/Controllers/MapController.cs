﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Base;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic.Statistics;
using Atmk.WaterMeter.MIS.Commons.ViewModels;
using Atmk.WaterMeter.MIS.Commons.ViewModels.MapCoordinates;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Atmk.WaterMeter.MIS.Controllers
{
    [Authorize]
    public class MapController : BaseApiController
    {
        private static ILogger<MapController> _logger;
        private readonly TokenHelper _tokenHelper;
        private readonly IEditCoordinatesLogic _editCoordinatesLogic;
        private readonly ISelectCoordinatesLogic _selectCoordinatesLogic;

        /// <summary>
        /// 地图坐标接口
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="editCoordinatesLogic"></param>
        /// <param name="selectCoordinatesLogic"></param>
        /// <param name="tokenHelper"></param>
        public MapController(
            ILogger<MapController> logger,
            IEditCoordinatesLogic editCoordinatesLogic,
            ISelectCoordinatesLogic selectCoordinatesLogic,
            TokenHelper tokenHelper
        )
        {
            _logger = logger;
            _tokenHelper = tokenHelper;
            _editCoordinatesLogic = editCoordinatesLogic;
            _selectCoordinatesLogic = selectCoordinatesLogic;
        }

        #region District

        /// <summary>
        ///     获取获取项目下片区名称及片区下地图坐标集合
        /// </summary>
        /// <returns></returns>
        [HttpGet, ActionName("District")]
        public IActionResult GetDistrict()
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var areaId = token.payload.areaid;
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "查询失败"
            };
            try
            {
                var result = _selectCoordinatesLogic.SelectDistrictMapCoordinat(areaId);
                return Ok(new MapsDistrictResult
                    {
                        errcode = 0,
                        msg = "ok",
                        datas = new MapsDistrictDate {list = result}
                    }
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     获取片区下地图坐标的集合
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        [HttpGet("{districtId}"), ActionName("District")]
        public IActionResult GetDistrict(string districtId)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var areaId = token.payload.areaid;
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "查询失败"
            };
            try
            {
                var result = _selectCoordinatesLogic.SelectDistrictMapCoordinat(areaId, districtId);
                return Ok(new MapsDistrictResult
                    {
                        errcode = 0,
                        msg = "ok",
                        datas = new MapsDistrictDate {list = result}
                    }
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     添加片区下地图坐标的集合
        /// </summary>
        /// <param name="postDistrictCoordinates"></param>
        /// <returns></returns>
        [HttpPost, ActionName("District")]
        public IActionResult PostDistrict([FromBody] PostDistrictCoordinates postDistrictCoordinates)
        {
            var token = _tokenHelper.GetTokenByHttpContext();

            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "添加失败"
            };
            try
            {
                //前端未引用修改方法，临时采用修改方法代替添加，防止bug
                var result = _editCoordinatesLogic.UpdateDistrictCoordinates(postDistrictCoordinates.coordinates,
                    postDistrictCoordinates.districtId);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"添加失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     修改片区下地图坐标的集合
        /// </summary>
        /// <param name="districtId"></param>
        /// <param name="mapCoordinates"></param>
        /// <returns></returns>
        [HttpPut("{districtId}"), ActionName("District")]
        public IActionResult PutDistrict(string districtId, [FromBody] MapCoordinates mapCoordinates)
        {
            var token = _tokenHelper.GetTokenByHttpContext();

            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "修改失败"
            };
            try
            {
                var result = _editCoordinatesLogic.UpdateDistrictCoordinates(mapCoordinates.coordinates, districtId);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"修改失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     删除片区下的地图坐标集合
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        [HttpDelete("{districtId}"), ActionName("District")]
        public IActionResult DeleteDistrict(string districtId)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "删除失败"
            };
            try
            {
                var result = _editCoordinatesLogic.DeleteDistrictCoordinates(districtId);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"删除失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        #endregion

        #region 坐标点

        /// <summary>
        ///     获取获取片区下业主的坐标点集合
        /// </summary>
        /// <returns></returns>
        [HttpGet("{districtId}"), ActionName("Point/District")]
        public IActionResult GetDistrictCoordinates(string districtId)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var areaId = token.payload.areaid;
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "查询失败"
            };
            try
            {
                var result = _selectCoordinatesLogic.SelectPointMapCoordinat(districtId, CoordinateType.District);
                return Ok(new MapsPointResult
                    {
                        errcode = 0,
                        msg = "ok",
                        datas = new MapsPointData {list = result}
                    }
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     获取获取项目下业主的坐标点集合
        /// </summary>
        /// <returns></returns>
        [HttpGet, ActionName("Point/Area")]
        public IActionResult GetAreaCoordinates()
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var areaId = token.payload.areaid;
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "查询失败"
            };
            try
            {
                var result = _selectCoordinatesLogic.SelectPointMapCoordinat(areaId, CoordinateType.Project);
                return Ok(new MapsPointResult
                    {
                        errcode = 0,
                        msg = "ok",
                        datas = new MapsPointData {list = result}
                    }
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     获取指定节点坐标点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}"), ActionName("Point")]
        public IActionResult GetCoordinat(string id)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var areaId = token.payload.areaid;
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "查询失败"
            };
            try
            {
                var result = _selectCoordinatesLogic.SelectPointMapCoordinat(id);
                return Ok(new MapsPointResult
                    {
                        errcode = 0,
                        msg = "ok",
                        datas = new MapsPointData {list = new List<PostPointMapCoordinat>() {result}}
                    }
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     添加片区下地图坐标的集合
        /// </summary>
        /// <param name="postDistrictCoordinates"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Point")]
        public IActionResult PostCoordinat([FromBody] PostPointMapCoordinat postDistrictCoordinates)
        {
            var token = _tokenHelper.GetTokenByHttpContext();

            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "添加失败"
            };
            try
            {
                var result = _editCoordinatesLogic.InsertPoint(postDistrictCoordinates.coordinates,
                    postDistrictCoordinates.nodeId, postDistrictCoordinates.nodeType);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"添加失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     修改片区下地图坐标的集合
        /// </summary>
        /// <param name="id"></param>
        /// <param name="coordinates"></param>
        /// <returns></returns>
        [HttpPut("{id}"), ActionName("Point")]
        public IActionResult PutCoordinat(string id, [FromBody] MapCoordinat coordinates)
        {
            var token = _tokenHelper.GetTokenByHttpContext();

            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "添加失败"
            };
            try
            {
                var result = _editCoordinatesLogic.UpdatePoint(coordinates, id);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"添加失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        /// <summary>
        ///     删除片区下的地图坐标集合
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}"), ActionName("Point")]
        public IActionResult DeleteCoordinat(string id)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            var failResult = new BaseResult
            {
                errcode = 1,
                msg = "删除失败"
            };
            try
            {
                var result = _editCoordinatesLogic.DeletePoint(id);
                return Ok(result
                    ? new BaseResult {errcode = 0, msg = "ok"}
                    : new BaseResult {errcode = 1, msg = $"删除失败"});
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Ok(failResult);
            }
        }

        #endregion
    }
}