﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Models;
using Atmk.WaterMeter.MIS.Swaggers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;

namespace Atmk.WaterMeter.MIS.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        //private readonly ILogger<ValuesController> _logger;
        private readonly ILoginLogic _loginLogic;

        /// <inheritdoc />
        public ValuesController(ILoginLogic loginLogic)
        {
            //_logger = logger;
            _loginLogic = loginLogic;
        }
        private readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();
        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            _logger.Debug("测试调试");
            _logger.Fatal("测试订单");
            _logger.Info("测试info");
            _logger.Error("测试错误");
            //using (var context = Datas.ContextBuilder.Build())
            //{
            //    context.SettlementDay.ToList();
            //}
            using (var _context = ContextBuilder.Build())
            {
                return Ok(_context.SettlementDay.Take(50).ToList());
            }

            //using (var context = Datas.ContextBuilder.Build())
            //{
            //    //var test = context.MeterReadingRecords.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m.MeterID));
            //    //var result = context.MeterReadingRecords.Include(m => m.Meter).Take(100).ToList();
            //    //return Ok(new { rrr = result });
            //    _logger.LogInformation("Index Begin...");
            //    _logger.LogTrace("Index Begin...");
            //    _logger.LogDebug("Index Begin...");
            //    _logger.LogError("Index Begin...");
            //    return Ok(new { rrr = "ok" });
            //}

            //var test = context.MeterReadingRecords.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m.MeterID));
            //var result = context.MeterReadingRecords.Include(m => m.Meter).Take(100).ToList();
            //return Ok(new { rrr = result });

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            try
            {
                return _loginLogic.Add(id, id + 1).ToString();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw;
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}
