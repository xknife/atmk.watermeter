﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Datas;
using Atmk.WaterMeter.MIS.Entities.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace Atmk.WaterMeter.MIS.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    [Authorize]
    public class OrderDetailController : ControllerBase
    {
        protected Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly TokenHelper _tokenHelper;
        public OrderDetailController(
       TokenHelper tokenHelper)
        {
            _tokenHelper = tokenHelper;
        }

        [HttpPost("/api/{X-Version}/[controller]/GetOrderDetailByIdType0")]
        public IActionResult GetOrderDetailByIdType0([FromBody]dynamic obj)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            //验证用户信息
            string ownerId = obj.ownerId;
            int pageIndex = obj.pageIndex;

            //根据用户获取片区编码集合进行匹配
            try
            {
                using (var _context = ContextBuilder.Build())
                {
                    var pageSize = _context.User.FirstOrDefault(m => m.Id.ToString() == token.payload.id).PageLines;
                    var orderDetails = _context.OrderDetail.Where(m => m.OwnerId == ownerId&& m.CostType==0).OrderByDescending(m=>m.CreateTime);
                    //获取分页数据
                    var orderDetails_list = PaginatedList<object>.Create(orderDetails, pageIndex, pageSize);

                    return Ok(new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = new
                        {
                            total = orderDetails.Count(),
                            pageSize = 20,
                            list = orderDetails_list
                        }
                    });
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取数据错误,错误:{e.Message}"
                });
            }
        }


        [HttpPost("/api/{X-Version}/[controller]/GetOrderDetailByIdType1")]
        public IActionResult GetOrderDetailByIdType1([FromBody]dynamic obj)
        {
            var token = _tokenHelper.GetTokenByHttpContext();
            //验证用户信息
            string ownerId = obj.ownerId;
            int pageIndex = obj.pageIndex;

            //根据用户获取片区编码集合进行匹配
            try
            {
                using (var _context = ContextBuilder.Build())
                {
                    int pageSize = 8;
                    var orderDetails = _context.OrderDetail.Where(m => m.OwnerId == ownerId).OrderByDescending(m => m.CreateTime);
                    //获取分页数据
                    var orderDetails_list = PaginatedList<object>.Create(orderDetails, pageIndex, pageSize);

                    return Ok(new
                    {
                        errcode = 0,
                        msg = "ok",
                        data = new
                        {
                            total = orderDetails.Count(),
                            pageSize,
                            list = orderDetails_list
                        }
                    });
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Ok(new
                {
                    errcode = 1,
                    msg = $"获取数据错误,错误:{e.Message}"
                });
            }
        }

    }
}