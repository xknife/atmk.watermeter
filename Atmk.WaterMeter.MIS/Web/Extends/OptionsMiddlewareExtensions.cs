﻿using Atmk.WaterMeter.MIS.Web.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Atmk.WaterMeter.MIS.Web.Extends
{
    public static class OptionsMiddlewareExtensions
    {
        public static IApplicationBuilder UseOptions(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<OptionsMiddleware>();
        }
    }
}
