﻿using System;
using System.Net;
using Atmk.WaterMeter.MIS.Entities.Models;
using Atmk.WaterMeter.MIS.Logic.CT_NBIoT;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Web;


namespace Atmk.WaterMeter.MIS
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
            try
            {
                BuildWebHost(args).Run();
            }
            catch (Exception exception)
            {
                logger.Error(exception, "Stopped program because of exception");
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Any, 9006);
                    //options.Listen(IPAddress.Any, port, o => o.UseHttps("Cert//cert.pfx", "5gFUN511"));
                })
                .UseStartup<Startup>()
                .ConfigureServices(services => services.AddAutofac())
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders(); 
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseNLog() // NLog: Setup NLog for Dependency injection
                .Build();
        }
    }
}