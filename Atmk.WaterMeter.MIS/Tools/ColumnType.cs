﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Tools
{
    public static class ColumnType
    {
        public static string msgtype(int? value)
        {
            switch (value)
            {
                case 1:
                    return "";
                case 4:
                    return "磁攻击报警";
                default:
                    return "";
            }
        }
    }
}
