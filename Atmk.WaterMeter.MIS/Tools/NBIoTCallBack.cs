﻿using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Tools
{
    public class NBIoTCallBack
    {
        //private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        //public async void InsertSettlementDay(Entities.DeviceData serviceData, MeterCodeIdMap meterCodeIdMap, DateTime eventTime)
        //{
        //    await Task.Run(() =>
        //    {
        //        try
        //        {
        //            using (var context = new WaterMeterDBContext())
        //            {
        //                var settlementDay_Today = context.SettlementDay.FirstOrDefault(m => m.MeterNumber == meterCodeIdMap.Number && m.ReadTime.ToString("yyyy-MM-dd") == eventTime.ToString("yyyy-MM-dd"));

        //                var settlementDay_Yesterday = context.SettlementDay.Where(m => m.MeterNumber == meterCodeIdMap.Number && Convert.ToDateTime(m.ReadTime.ToString("yyyy-MM-dd")) < Convert.ToDateTime(eventTime.ToString("yyyy-MM-dd"))).OrderByDescending(m => m.ReadTime).FirstOrDefault();
        //                //var settlementDay_Yesterday = context.SettlementDay.Where(m => m.MeterNumber == meterCodeIdMap.Number).OrderByDescending(m => m.ReadTime).FirstOrDefault();
        //                //if (settlementDay_Yesterday != null && settlementDay_Yesterday.ReadTime.ToString("yyyy-MM-dd") == eventTime.ToString("yyyy-MM-dd"))
        //                //{
        //                //    settlementDay_Yesterday = context.SettlementDay.Where(m => m.MeterNumber == meterCodeIdMap.Number && Convert.ToDateTime(m.ReadTime.ToString("yyyy-MM-dd")) < Convert.ToDateTime(eventTime.ToString("yyyy-MM-dd"))).OrderByDescending(m => m.ReadTime).FirstOrDefault();
        //                //}
        //                var readString = Base64Convert.ConvertBytes(serviceData.current);
        //                var readInt = BytesConvert.BitConvertInts(readString, 6).First() / 100.00;

        //                if (settlementDay_Today == null)
        //                {
        //                    var SettlementDayEntity = new SettlementDay()
        //                    {
        //                        MeterNumber = meterCodeIdMap.Number,
        //                        ReadTime = eventTime,
        //                        Value = readInt,
        //                        SettlementState = 0,
        //                        Dosage = settlementDay_Yesterday == null ? 0 : readInt - settlementDay_Yesterday.Value,
        //                        AreaId = meterCodeIdMap.AreaId,
        //                        DistrictId = meterCodeIdMap.DistrictId,
        //                        OwerId = meterCodeIdMap.OwerId,
        //                        MeterId = meterCodeIdMap.MeterId
        //                    };
        //                    context.SettlementDay.Add(SettlementDayEntity);
        //                    context.SaveChanges();
        //                }
        //                else
        //                {
        //                    settlementDay_Today.Value = readInt;
        //                    settlementDay_Today.Dosage = settlementDay_Yesterday == null ? 0 : readInt - settlementDay_Yesterday.Value;
        //                    settlementDay_Today.ReadTime = eventTime;
        //                    context.Update(settlementDay_Today);
        //                    context.SaveChanges();
        //                }

        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error($"InsertSettlementDay方法 记录数据异常：{ex.Message}");
        //        }
        //    });
        //}
    }
}
