﻿using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Tools
{
    public static class PublicSelect
    {
        public static List<MeterReadingRecord> Get_MeterReadingRecords_ProjectId(string ProjectId, int? msgType)
        {
            using (var context = Datas.ContextBuilder.Build())
            {
                var total_districts = context.District.Where(m => m.ProjectId == ProjectId).ToList();
                var districts_ids = total_districts.Select(m1 => m1.Id.ToString());
                var total_owners = context.Owner.Where(o => districts_ids.Contains(o.DistrictId)).ToList();
                var owners_ids = total_owners.Select(m1 => m1.Id.ToString());
                var total_meters = context.Meter.Where(m => m.MeterState == "建档" && owners_ids.Contains(m.OwnerId)).ToList();
                var meterNumbers = total_meters.Select(m1 => m1.MeterNumber);

                var meterReads = context.MeterReadingRecord.Where(m => meterNumbers.Contains(m.CtdeviceId) && msgType == null ? true : m.MsgType == msgType).ToList();
                return meterReads;
            }
        }
    }
}
