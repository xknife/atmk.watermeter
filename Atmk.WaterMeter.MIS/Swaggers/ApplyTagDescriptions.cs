﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Atmk.WaterMeter.MIS.Swaggers
{
    /// <summary>
    /// 添加控制器模块说明
    /// </summary>
    public class ApplyTagDescriptions : IDocumentFilter
    {
        /// <summary>
        /// apply
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new List<Tag>
            {
                new Tag { Name = "Wx_Authentication", Description = "微信小程序登陆接口" },
                new Tag { Name = "Wx_SmallRoutine_Business", Description = "微信小程序业务接口" },
                new Tag {Name = "Wx_SmallRoutine_Notice",Description = "微信支付回调接口"},

                new Tag { Name = "Sign", Description = "登录相关接口" },
                new Tag { Name = "Project", Description = "项目信息管理相关接口" },
                new Tag { Name = "UserCenter", Description = "用户中心接口" },
                new Tag { Name = "AccountManage", Description = "账户管理接口" },
                new Tag { Name = "Analysis", Description = "分析接口" },
                new Tag { Name = "CT_Callback", Description = "CT-Iot电信回调接口" },
                new Tag { Name = "Districts", Description = "片区管理接口" },
                new Tag { Name = "Map", Description = "地图坐标管理接口" },
            };
        }
    }
}
