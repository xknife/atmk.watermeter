﻿using System;

namespace Atmk.WaterMeter.MIS.Swaggers
{
    /// <summary>
    /// 文件上传
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SwaggerFileUploadAttribute : Attribute
    {
        public bool Required { get; private set; }

        public SwaggerFileUploadAttribute(bool required = true)
        {
            this.Required = required;
        }
    }
}