﻿using System.Collections.Generic;
using System.Linq;
using Atmk.WaterMeter.MIS.Common;
using Atmk.WaterMeter.MIS.Controllers.Pay.Base;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Atmk.WaterMeter.MIS.Swaggers
{
    /// <summary>
    /// 操作过过滤器 添加通用参数等
    /// </summary>
    public class AssignOperationVendorExtensions : IOperationFilter
    {
        /// <summary>
        /// apply
        /// </summary>
        /// <param name="op"></param>
        /// <param name="context"></param>
        public void Apply(Operation op, OperationFilterContext context)
        {
            if (op == null || context == null)
                return;
            op.Parameters = op.Parameters ?? new List<IParameter>();

            var attrs = context.ApiDescription.ControllerAttributes();

            //token
            var isAuthor = attrs.Any(e => e.GetType() == typeof(AuthorizeAttribute))|| attrs.Any(e => e.GetType() == typeof(Wx_AuthorizeAttribute));
            if (isAuthor)
            {
                //in query header 
                op.Parameters.Insert(0, new NonBodyParameter()
                {
                    Name = "X-Token", In = "header", Description = "身份验证票据", Required = true, Type = "string"
                });
            }
            //接口版本
            var versionInfo = op.Parameters.FirstOrDefault(e => e.Name == "X-Version") as NonBodyParameter;
            if (versionInfo != null)
            {
                op.Parameters.Remove(versionInfo);
                versionInfo.Default = context.ApiDescription.GroupName ?? typeof(ApiVersions).GetEnumNames().LastOrDefault();
                versionInfo.Description = "接口版本";
                op.Parameters.Insert(0, versionInfo);
            }

            var fileAttr = attrs.OfType<SwaggerFileUploadAttribute>().FirstOrDefault();
            if (fileAttr != null)
            {
                //op.Parameters.Add("multipart/form-data");
                op.Parameters.Add(new NonBodyParameter
                {
                    Name = "file",
                    In = "formData",
                    Description = "上传图片",
                    Required = (fileAttr as SwaggerFileUploadAttribute).Required,
                    Type = "file"
                });

            }
        }
    }
}