﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Models.ModelPuls
{
    public class OwnerMeterEntity
    {
        public owner owner { get; set; } = new owner();

        public WaterMeter waterMeter { get; set; } = new WaterMeter();

    }
    public class owner
    { 
        public string ownerId { get; set; }
        public string ownerName { get; set; }
        public string houseNumber { get; set; }
        public string mobile { get; set; }
        public string ownerMessage { get; set; }
    }
    public class WaterMeter
    {
        public string meterType { get; set; }
        public string meterNumber { get; set; }
        public string refillType { get; set; }
        public string priceId { get; set; }
        public string commType { get; set; }
        public string imei { get; set; }
        public string meterBottomNumber { get; set; }
        public meterDatas meterDatas { get; set; } = new meterDatas();
    }
    public class meterDatas
    {
        public string collector { get; set; }
        public string meterBottomNumber { get; set; }
    }
}
