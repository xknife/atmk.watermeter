﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Models.ModelPuls
{
    public class PageEntity
    {
        public int page { get; set; }
        public string nodeType { get; set; }
        public string id { get; set; }
    }
}
