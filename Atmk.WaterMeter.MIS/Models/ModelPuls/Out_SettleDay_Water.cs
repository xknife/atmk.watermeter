﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Models.ModelPuls
{
    public class Out_SettleDay_Water
    {
        public string MeterNumber { get; set; }
        public double WaterVolume { get; set; }

    }
}
