﻿using Atmk.WaterMeter.MIS.Entities;
using Atmk.WaterMeter.MIS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Models.ModelPuls
{
    public class SettlementDayPuls: SettlementDay
    {
        public double Dosage_Year { get; set; }
    }
}
