﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atmk.WaterMeter.MIS.Models.Warning
{
    public class WarningModel
    {
        public string districtId { get; set; }
        //片区
        public string District { get; set; }
        //业主名
        public string OwnerName { get; set; }
        //手机号
        //public string Mobile { get; set; }
        //门牌号
        public string HouseNumber { get; set; }
        //水表号
        public string MeterNumber { get; set; }
        //当前值
        public string CurrentValue { get; set; }
        //电压
        public double Voltage { get; set; }
        //阀门状态
        public string ValueState { get; set; }
        //功能类型
        public string MsgType { get; set; }
        //数据上报时间
        public string ReadTime { get; set; }
    }
}
