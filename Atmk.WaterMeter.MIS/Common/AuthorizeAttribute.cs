﻿using System;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Services.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Atmk.WaterMeter.MIS.Common
{
    /// <summary>
    /// 会员登录验证
    /// </summary>
    public class AuthorizeAttribute : Attribute, IActionFilter
    {
        /// <summary>
        /// 当Action执行后
        /// </summary>
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        /// <summary>
        /// 当Action执行时
        /// </summary>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            //            var loginUser = SignService.GetTokenByHttpContext();
            //            if (loginUser == null)
            //                context.Result = new JsonResult(new { status = 0, msg = "登录失效，请重新登录" });
        }
    }
}
