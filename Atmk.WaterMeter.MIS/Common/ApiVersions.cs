﻿namespace Atmk.WaterMeter.MIS.Common
{
    /// <summary>
    ///     Api接口版本 每次新版本增加一个
    /// </summary>
    public enum ApiVersions
    {
        v1 = 1
        //v2 = 2,
        //v3 = 3,
        //v4 = 4,
    }
}