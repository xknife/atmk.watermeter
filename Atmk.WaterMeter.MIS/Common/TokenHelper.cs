﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Services.Auth;
using Microsoft.AspNetCore.Http;

namespace Atmk.WaterMeter.MIS.Common
{
    public class TokenHelper
    {
        private IHttpContextAccessor _httpContextAccessor;

        public TokenHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 解析HttpContext携带的Token编码，并返回Token实体 
        /// </summary>
        public Token GetTokenByHttpContext()
        {
            //if (_Token != null)
            //    return _Token;

            //header或者query带有x-token参数
            var token = _httpContextAccessor.HttpContext.Request.Headers["X-Token"];
            token = string.IsNullOrEmpty(token) ? _httpContextAccessor.HttpContext.Request.Query["x-token"] : token;
            if (string.IsNullOrEmpty(token))
                return null;
            return JwtHelper.Decode<Token>(token);
        }
    }
}
