﻿using Atmk.WaterMeter.MIS.Swaggers;
using Microsoft.AspNetCore.Mvc;

namespace Atmk.WaterMeter.MIS.Base
{
    /// <summary>
    ///     基础类
    /// </summary>
    [CustomRoute]
    public class BaseApiController : Controller
    {
    }
}