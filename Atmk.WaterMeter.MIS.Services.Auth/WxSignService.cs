﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin;

namespace Atmk.WaterMeter.MIS.Services.Auth
{
    public class WxSignService:IWxSignService
    {
        /// <summary>
        /// 获取微信小程序登陆后服务器返回的token
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="session_key"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool TryGetToken(string openid, string session_key, out string token)
        {
            token = "";
            token = JwtHelper.Encode(new WxSrToken { OpenId = openid,SessionKey=session_key});
            return true;

        }
    }
}
