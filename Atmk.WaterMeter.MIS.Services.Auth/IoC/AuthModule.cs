﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.WeiXin;
using Autofac;

namespace Atmk.WaterMeter.MIS.Services.Auth.IoC
{
    public class AuthModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<SignService>().As<ISignService>();
            builder.RegisterType<WxSignService>().As<IWxSignService>();
        }
    }
}
