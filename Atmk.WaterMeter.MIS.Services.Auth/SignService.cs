﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Atmk.WaterMeter.MIS.Commons;
using Atmk.WaterMeter.MIS.Commons.Interfaces;

namespace Atmk.WaterMeter.MIS.Services.Auth
{
    /// <summary>
    /// 用户签入签出服务
    /// </summary>
    public class SignService : ISignService
    {
        private Token _Token;

        /// <summary>
        /// 验证码token
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetVerifyCodeToken(string code)
        {
            try
            {
                return JwtHelper.Encode(new VerifyCodeToken {VerifyCode = code});
            }
            catch (Exception e)
            {
               throw new Exception(e.Message,e);
            }
           
        }
        /// <summary>
        /// 判断验证码与token是否一致
        /// </summary>
        /// <param name="code"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool CheckVerifyCodeToken(string code, string token)
        {
            var tcode = JwtHelper.Decode<VerifyCodeToken>(token);
            return tcode.VerifyCode.ToLower() == code.ToLower();
        }

        /// <summary>
        /// 生成Token的 Base64 编码
        /// </summary>
        public bool TrySign(string name, string staffId,string roleId,string areaId, out string token)
        {
            token = "";
            token = JwtHelper.Encode(new Token { payload = { id = staffId, name = name, role = roleId,areaid = areaId} });
            return true;
        }
     

        public void ClearToken()
        {
            _Token = null;
        }
    }
}