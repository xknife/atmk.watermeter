﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Interfaces
{
    /// <summary>
    /// 处理回调参数的逻辑问题
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ICT_NBIoTCallBackLogic
    {
        /// <summary>
        /// 平台数据上传回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        void MeterDatasLogic(DeviceDataPost value);

        /// <summary>
        /// 指令回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        void CommandStatusLogic(CommandRespond value);
    }
}
