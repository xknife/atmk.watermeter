﻿using System;
using System.Linq;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Repository;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.CallBack;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Logic
{
    /// <summary>
    /// 处理回调参数的逻辑问题
    /// </summary>
    public class NbCallBackLogic:ICT_NBIoTCallBackLogic
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        //private readonly IMeterCodeIdMapRepository _codeIdMapRepository;
        private readonly ICommandRepository _commandRepository;
        private readonly IDataStoreLogic _dataStore;

        /// <inheritdoc />
        public NbCallBackLogic(
            //IMeterCodeIdMapRepository codeIdMapRepository,
            ICommandRepository commandRepository,
        IDataStoreLogic dataStore
        )
        {
            //_codeIdMapRepository = codeIdMapRepository;
            _commandRepository = commandRepository;
            _dataStore = dataStore;
        }

        /// <summary>
        /// 平台数据上传回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        //public  void MeterDatasLogic(DeviceDataPost value, string meterCode, DateTime eventTime)
        //{
        //    //_logger.Info("电信平台回调数据：" + Newtonsoft.Json.JsonConvert.SerializeObject(value));
        //    //获取设备id,通过设备Id获取水表编号
        //    //var meterCode = _codeIdMapRepository.GetMeterNumberByNBId(value.deviceId);
        //    ////时间转换
        //    //var eventTime = Simple.CtStringConvertDateTime2(value.service.eventTime);
        //    ////异步向结算表写数据
        //    //_dataStore.InsertSettlementDay(meterCode, value.service.data, eventTime);
        //    //记录水表上传读数
        //    _dataStore.MeterRecordSave(meterCode, value.service.data, eventTime);
        //    //记录小时冻结数据
        //    //_logger.Info(Base64Convert.ConvertHexString(value.service.data.history));
        //    //旧版代码
        //    //var hourFrozens = Base64Convert.ConvertBytes(value.service.data.history);
        //    //_dataStore.HourFrozenSave(meterCode, hourFrozens, eventTime);

        //}

        /// <summary>
        /// 指令回调函数逻辑处理
        /// </summary>
        /// <param name="value"></param>
        public  void CommandStatusLogic(CommandRespond value)
        {
            try
            {
                //获取指令反馈参数
                var commandStatus = CommandCodeMapping.Builder().CodeMapping(value.result.resultCode);
                var commandId = value.commandId;
                //修改指令
                var cRepository = _commandRepository;
                var commands = cRepository.GetCommandsByCommandId(commandId);
                if (commands.Count == 0)
                    throw new Exception("获取指令数据为空");
                var command = commands.First();
                command.CommandStatus = (int)commandStatus;
                cRepository.Update(command);
            }
            catch (Exception e)
            {
                throw new Exception("CommandStatusLogic", e);
            }
        }
    }
}