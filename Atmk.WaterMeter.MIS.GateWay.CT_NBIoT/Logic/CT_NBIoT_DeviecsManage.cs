﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay;
using Atmk.WaterMeter.MIS.Commons.Utils;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Utils;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.RequestParam;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Visiters;
using Newtonsoft.Json;
using NLog;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Logic
{
    public class CT_NBIoT_DeviecsManage:ICT_NBIoT_DevicesManage
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly CT_NBIoT_DevicesVister _ctNbioTDevicesVister;
        private readonly TokenResult _Token;

        public CT_NBIoT_DeviecsManage()
        {
            _ctNbioTDevicesVister=new CT_NBIoT_DevicesVister(
                CT_NBIoT_ConfigParameter.AppId,
                CT_NBIoT_ConfigParameter.AppSecret,
                CT_NBIoT_ConfigParameter.PlatformID,
                CT_NBIoT_ConfigParameter.Port
                );
            _Token = _ctNbioTDevicesVister.GetToken();
        }

        /// <summary>
        /// 注册设备
        /// </summary>
        /// <param name="nodeId">IMEI</param>
        /// <returns>返回GUID，如果失败返回Guid.Empty</returns>
        public Guid RegisterDevice(string nodeId)
        {
            //获取参数,转成需要发送的viewmodel
            var mode = new DeviceManagePostParameter
            {
                nodeId = nodeId,
                verifyCode = nodeId
            };
            var result = _ctNbioTDevicesVister.PostDevice(_Token.AccessToken, mode);
            if (result.StatusCode == 200||result.StatusCode==204)
            {
               var dmpr = JsonConvert.DeserializeObject<DeviceManagePostResult>(result.Result);
                return new Guid(dmpr.deviceId);
            }
            _logger.Error($"注册设备失败,返回码{result.StatusCode} 信息{result.Result} 详细{result.Errcode}");
            return Guid.Empty;
        }

        /// <summary>
        /// 修改设备信息
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <param name="name">设备名称，默认填入水表号</param>
        /// <param name="model">设备型号，填入在电信平台设置好profile的设备型号</param>
        /// <returns></returns>
        public bool UpdateDeviceInfo(string deviceId, string name, string model)
        {
            var mode = new DeviceManagePutParameter
            {
                deviceId = deviceId,
                name = name,
                deviceType = model,
                model = "CNM50",
                manufacturerId = "automicwatermeter",
                manufacturerName= "automicwatermeter"
            };
            var result = _ctNbioTDevicesVister.PutDevice(_Token.AccessToken, mode);
            if (result.StatusCode == 200 || result.StatusCode == 204)
            {
                return true;
            }
            _logger.Error($"注册修改设备失败,返回码{result.StatusCode} 信息{result.Result} 详细{result.Errcode}");
            return false;
        }

        /// <summary>
        /// 删除设备
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns></returns>
        public bool DeleteDevice(string deviceId)
        {
            var result = _ctNbioTDevicesVister.DeleteDevice(_Token.AccessToken, deviceId);
            if (result.StatusCode == 204 || result.StatusCode == 200)
            {
                return true;
            }
            _logger.Error($"删除设备失败,返回码{result.StatusCode} 信息{result.Result} 详细{result.Errcode}");
            return false;
        }

        /// <summary>
        /// 判断设备状态
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns>
        /// 200 设备处于激活状态
        /// 201 设备处于未激活状态
        /// 404 设备不存在
        /// </returns>
        public int DeviceStatus(string deviceId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取所有设备信息
        /// </summary>
        /// <returns></returns>
        public ResDevices GetDevices()
        {
            var devParam = new DeviceGetParam();
            var result = _ctNbioTDevicesVister.GetDevice(_Token.AccessToken, devParam);
            if (result.StatusCode == 200 || result.StatusCode == 204)
            {
                var resDevices = JsonConvert.DeserializeObject<ResDevices>(result.Result);
                return resDevices;
            }
            _logger.Error($"获取所有设备信息失败,返回码{result.StatusCode} 信息{result.Result} 详细{result.Errcode}");
            return null;
        }
    }
}
