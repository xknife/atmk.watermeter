﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.DevicesManage;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.ViewModel.NaRespon;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Logic
{
    public class CT_NBIoT_DeviecsManageMock:ICT_NBIoT_DevicesManage
    {
        /// <summary>
        /// 注册设备
        /// </summary>
        /// <param name="nodeId">IMEI</param>
        /// <returns></returns>
        public Guid RegisterDevice(string nodeId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 修改设备信息
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <param name="name">设备名称，默认填入水表号</param>
        /// <param name="model">设备型号，填入在电信平台设置好profile的设备型号</param>
        /// <returns></returns>
        public bool UpdateDeviceInfo(string deviceId, string name, string model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除设备
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns></returns>
        public bool DeleteDevice(string deviceId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 判断设备状态
        /// </summary>
        /// <param name="deviceId">电信平台设备Id</param>
        /// <returns>
        /// 200 设备处于激活状态
        /// 201 设备处于未激活状态
        /// 404 设备不存在
        /// </returns>
        public int DeviceStatus(string deviceId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取所有设备信息
        /// </summary>
        /// <returns></returns>
        public ResDevices GetDevices()
        {
            throw new NotImplementedException();
        }
    }
}
