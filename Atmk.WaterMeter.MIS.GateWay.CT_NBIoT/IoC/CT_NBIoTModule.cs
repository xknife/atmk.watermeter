﻿using System;
using System.Collections.Generic;
using System.Text;
using Atmk.WaterMeter.MIS.Commons.Interfaces;
using Atmk.WaterMeter.MIS.Commons.Interfaces.GateWay;
using Atmk.WaterMeter.MIS.Commons.Interfaces.Logic;
using Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Logic;
using Autofac;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.IoC
{
    // ReSharper disable once InconsistentNaming
    public class CT_NBIoTModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
           // builder.RegisterType<IDataStoreLogic>().As<IDataStoreLogic>();
            builder.RegisterType<NbCallBackLogic>().As<ICT_NBIoTCallBackLogic>();
            builder.RegisterType<CT_NBIoT_CommandSenderHelper>().As<ICT_NBIoT_CommandSenderHelper>();
            builder.RegisterType<CT_NBIoT_DeviecsManage>().As<ICT_NBIoT_DevicesManage>();
        }
    }
}
