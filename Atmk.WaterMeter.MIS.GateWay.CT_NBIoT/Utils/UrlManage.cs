﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atmk.WaterMeter.MIS.GateWay.CT_NBIoT.Utils
{
    public class UrlManage
    {
        public static string PostDevice = "/iocm/app/reg/v1.2.0/devices";
        public static string PutDevice = "/iocm/app/dm/v1.4.0/devices";
        public static string DeleteDevice = "/iocm/app/dm/v1.4.0/devices";
        public static string GetDevices = "/iocm/app/dm/v1.4.0/devices";

    }
}
